//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    this.selectTrainingSalesman = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select t.id id_training_salesman, t.id training_schedule_id, s.training_name,s.training_schedule_code, s.training_schedule_start_date, s.training_schedule_end_date, status_approve, t.created_at from training_salesman t, training_schedules s where t.training_schedule_id = s.id and salesman_id = "' + salesman_id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.detailTrainingSalesman = function(req, res, next) {

        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT ts.id, training_schedule_code, training_schedule_start_date, training_schedule_end_date, training_code, t.title training_name, t.description, t.category, t.type, t.level, ts.created_at, ts.status_approve from training_schedules s, master_training t, training_salesman ts where s.training_id = t.id COLLATE utf8_unicode_ci and ts.training_schedule_id = s.id and ts.id = "' + id + '"', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'failed', result: [] });
                } else {
                    if (data.length > 0) {
                        return res.json(data[0]);
                    }
                }
            });
        });
    };
}
module.exports = new Todo();