//Auth : Lely
var connection = require('../../config/db');

function Todo() {

    this.selectEvent = function(req, res, next) {
        var id = req.query.id;
        if (id != null) {
            detailEvent(req, res, next);
            return;
        }
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM master_event', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json({ status: 200, message: 'success', result: data });
            });
        });
    };

    this.insertEvent = function(req, res) {
        var id = uuidv1();
        var event_type = req.body.event_type;
        var description = req.body.description;
        var created_at = new Date();
        var modified_at = new Date();
        var created_by = req.body.created_by;
        var modi_by = req.body.modi_by;

        connection.acquire(function(err, con) {
            con.query('INSERT INTO master_event (id, event_type, description, created_at, modified_at, created_by, modi_by) VALUES (?, ?, ?, ?, ?, ?, ?)', [id, event_type, description, created_at, modified_at, created_by, modi_by], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Master event creation failed' });
                } else {
                    res.send({ status: 200, message: 'Master event created successfully' });
                }
            });
        });
    };

    this.updateEvent = function(req, res) {
        var id = req.params.id;
        var event_type = req.body.event_type;
        var description = req.body.description;
        var modified_at = new Date();
        var modi_by = req.body.modi_by;
        connection.acquire(function(err, con) {
            con.query('UPDATE master_event SET event_type = ?, description = ?, modified_at = ?, modi_by = ? WHERE id = "' + id + '"', [event_type, description, modified_at, modi_by], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Master event update failed' });
                } else {
                    res.send({ status: 200, message: 'Master event updated successfully' });
                }
            });
        });
    };

    this.deleteEvent = function(req, res) {
        var id = req.params.id;
        connection.acquire(function(err, con) {
            con.query('DELETE FROM master_event WHERE id = ?', [id], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Failed to delete master event' });
                } else {
                    res.send({ status: 200, message: 'Deleted successfully master event' });
                }
            });
        });
    };

    function detailEvent(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM master_event WHERE id = "' + id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json({ status: 200, message: 'success', result: data });
            });
        });
    };

}
module.exports = new Todo();