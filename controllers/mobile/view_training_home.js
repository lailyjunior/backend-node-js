//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    this.selectCategoryTraining = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select * from master_training_category', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectTrainingAllByCategory = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select md.* from training_salesman ts, training_schedules tc, master_training mt, master_training_detail md where ts.training_schedule_id = tc.id and tc.training_id = mt.id COLLATE utf8_unicode_ci and mt.id = md.id_training and mt.category_id = "' + id + '" and ts.status_approve = 1 order by title_attachment', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectThisMonthTraining = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select md.* from training_salesman ts, training_schedules tc, master_training mt, master_training_detail md where ts.training_schedule_id = tc.id and tc.training_id = mt.id COLLATE utf8_unicode_ci and mt.id = md.id_training and category_detail= 0 and ts.status_approve = 1 order by title_attachment limit 5', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectThisMonthTrainingAll = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select md.* from training_salesman ts, training_schedules tc, master_training mt, master_training_detail md where ts.training_schedule_id = tc.id and tc.training_id = mt.id COLLATE utf8_unicode_ci and mt.id = md.id_training and category_detail= 0 and ts.status_approve = 1 order by title_attachment', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectVideoTraining = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select md.* from training_salesman ts, training_schedules tc, master_training mt, master_training_detail md where ts.training_schedule_id = tc.id and tc.training_id = mt.id COLLATE utf8_unicode_ci and mt.id = md.id_training and category_detail= 1 and ts.status_approve = 1 order by title_attachment limit 5', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectVideoTrainingAll = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select md.* from training_salesman ts, training_schedules tc, master_training mt, master_training_detail md where ts.training_schedule_id = tc.id and tc.training_id = mt.id COLLATE utf8_unicode_ci and mt.id = md.id_training and category_detail= 1 and ts.status_approve = 1 order by title_attachment', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectViewersTraining = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select * from master_training_detail where id = "' + id + '" ', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data[0]);
            });
        });
    };
}
module.exports = new Todo();