//Auth : Anggi
var connection = require('../../config/db');

var request = require('request');

function Todo() {
    const fs = require('fs');
    this.getCustomer = function(req, res, next) {
        var id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM customers WHERE salesman_id="' + id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };


    this.getCustomerDetail = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM customers WHERE id="' + id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data[0]);
            });
        });
    };

    // edit status generate customer
    this.edit = function(req, res) {
        var id = req.body.id;
        var status_generate_customer = req.body.status_generate_customer;

        connection.acquire(function(err, con) {
            con.query('UPDATE customer_prospect SET status_generate_customer=? WHERE id = "' + id + '"', [status_generate_customer], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Update failed' });
                } else {
                    res.send({ status: 200, message: 'Update successfully' });
                }
            });
        });
    };

    // detail customer
    this.updateCustomerTest = function(req, res) {

        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        var id = req.params.id;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var poscode_id = req.body.poscode_id;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_npwp = req.body.cust_npwp;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;

        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;

        var cust_sumber = req.body.cust_sumber;
        var salesman_id = req.body.salesman_id;
        var assigned_to = req.body.assigned_to;
        var cust_kendaraan = req.body.cust_kendaraan;
        var npwp_stnk = req.body.npwp_stnk;

        var modified = new Date();

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var cust_pic = req.body.cust_pic;
        var cust_type = req.body.cust_type;
        var cust_hp_wa = req.body.cust_hp_wa;

        var fileUploads = req.files;
        var docs = [];

        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT doc_1,doc_2,doc_3,doc_4,doc_5 FROM customers where id = "' + id + '"', function(err, data) {
                con.release();
                if (err) {
                    console.log("error");
                } else {

                    fileUploads.forEach(function(element, index) {
                        var image = data[0][element.fieldname];
                        if (image) {
                            fs.unlink(appRoot + image.replace("/files/", "/uploads/"), (err) => {
                                if (err) throw err;
                            });
                        }
                        docs[element.fieldname] = "/files/" + element.filename;
                    });
                    connection.acquire(function(err, conn) {
                        var queryStr = 'UPDATE customers SET id = ?, branch_id = ?,branch_code = ?,cust_jenis = ?,occupation_id = ?,kerja_kode = ?,cust_agama = ?,poscode_id = ?,kota_id = ?,propinsi_id = ?,cust_kelamin = ?,pos_kode = ?,pos_kota = ?,pos_prop = ?,pos_camat = ?,pos_lurah = ?,kota_id2 =?,propinsi_id2 = ?,pos_kode2 = ?,pos_kota2 =?,pos_prop2 = ?,pos_camat2 = ?,pos_lurah2 = ?,cust_nama = ?,cust_noktp = ?,cust_npwp = ?,cust_tgl_lahir = ?,cust_alamat = ?,cust_rt = ?,cust_rw =?,alamat_lengkap = ?,rt_stnk = ?,rw_stnk =?,nama_stnk = ?,alamat_stnk = ?,cust_fax = ?,cust_hp = ?,cust_telp_rumah = ?,cust_email = ?,cust_alamat2 = ?,noktp_stnk = ?,alamat_invoice = ?,alamat_kirim = ?,cust_telp_kantor = ?,cust_sumber = ?,salesman_id = ?,assigned_to = ?,cust_kendaraan = ?,npwp_stnk =?,modified = ?,id_motor = ?,type_motor = ?,kode_motor = ?,deskripsi_motor = ?,id_employee = ?,employee_kode = ?,employee_nama = ?,employee_jabatan = ?,id_event = ?,kode_event =?,nama_event =?,lokasi_event =?,cust_pic =?, cust_type=?,cust_hp_wa=?';

                        var queryParams = [id, branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, poscode_id, kota_id, propinsi_id, cust_kelamin, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_nama, cust_noktp, cust_npwp, cust_tgl_lahir, cust_alamat, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_hp, cust_telp_rumah, cust_email, cust_alamat2, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, cust_sumber, salesman_id, assigned_to, cust_kendaraan, npwp_stnk, modified, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, cust_pic, cust_type, cust_hp_wa];

                        for (var key in docs) {
                            var value = docs[key];
                            queryStr = queryStr + ", " + key + " = ?";
                            queryParams.push(value);
                        }

                        queryStr = queryStr + ' where id = "' + id + '"'

                        conn.query(queryStr, queryParams, function(err, result) {
                            conn.release();
                            if (err) {
                                res.send({ status: 400, message: 'Edit Customer Failed' });
                            } else {
                                res.send({ status: 200, message: 'Edit Customer Success' });
                            }
                        });
                    });
                }
            });
        });

    };

    //kirim ke log (generate & add)
    this.generateCustomerToLogs = function(req, res) {
        var id = uuidv1();
        var cust_id = req.body.cust_id
        var cust_code = req.body.cust_code
        var title = req.body.title
        var cust_nama = req.body.cust_nama
        var cust_pic = req.body.cust_pic
        var cust_org = req.body.cust_org
        var cust_noktp = req.body.cust_noktp
        var cust_alamat = req.body.cust_alamat
        var noktp_stnk = req.body.noktp_stnk
        var poscode_id = req.body.poscode_id
        var pos_kode = req.body.pos_kode
        var pos_camat = req.body.pos_camat
        var pos_lurah = req.body.pos_lurah
        var pos_kota = req.body.pos_kota
        var pos_prop = req.body.pos_prop
        var alamat_invoice = req.body.alamat_invoice
        var alamat_kirim = req.body.alamat_kirim
        var cust_telp_rumah = req.body.cust_telp_rumah
        var cust_telp_kantor = req.body.cust_telp_kantor
        var cust_fax = req.body.cust_fax
        var cust_hp = req.body.cust_hp
        var cust_email = req.body.cust_email
        var cust_tgl_lahir = req.body.cust_tgl_lahir
        var cust_agama = req.body.cust_agama
        var occupation_id = req.body.occupation_id
        var kerja_kode = req.body.kerja_kode
        var cust_kelamin = req.body.cust_kelamin
        var cust_jenis = req.body.cust_jenis
        var cust_npwp = req.body.cust_npwp
        var salesman_id = req.body.salesman_id
        var assigned_to = req.body.assigned_to
        var cust_sumber = req.body.cust_sumber
        var cust_npk = req.body.cust_npk
        var cust_kendaraan = req.body.cust_kendaraan
        var branch_id = req.body.branch_id
        var branch_code = req.body.branch_code
        var company_id = req.body.company_id
        var company_code = req.body.company_code
        var cust_type = req.body.cust_type
        var create_by = req.body.create_by
        var modi_by = req.body.modi_by
        var created = new Date();
        var modified = new Date();
        var kunci = req.body.kunci
        var kota_id = req.body.kota_id
        var propinsi_id = req.body.propinsi_id
        var nama_stnk = req.body.nama_stnk
        var alamat_stnk = req.body.alamat_stnk
        var alamat_lengkap = req.body.alamat_lengkap
        var npwp_stnk = req.body.npwp_stnk
        var cust_alamat2 = req.body.cust_alamat2
        var cek_alamat_kirim = req.body.cek_alamat_kirim
        var pos_kota2 = req.body.pos_kota2
        var pos_prop2 = req.body.pos_prop2
        var propinsi_id2 = req.body.propinsi_id2
        var pos_lurah2 = req.body.pos_lurah2
        var pos_kode2 = req.body.pos_kode2
        var kota_id2 = req.body.kota_id2
        var pos_camat2 = req.body.pos_camat2
        var lead_code = req.body.lead_code
        var lead_id = req.body.lead_id
        var cust_rt = req.body.cust_rt
        var cust_rw = req.body.cust_rw
        var dealer_id = req.body.dealer_id
        var dealer_code = req.body.dealer_code
        var rt_stnk = req.body.rt_stnk
        var rw_stnk = req.body.rw_stnk
        var doc_1 = req.body.doc_1
        var doc_2 = req.body.doc_2
        var doc_3 = req.body.doc_3
        var doc_4 = req.body.doc_4
        var doc_5 = req.body.doc_5
        var id_event = req.body.id_event
        var id_employee = req.body.id_employee
        var id_motor = req.body.id_motor
        var status_generate_spk = req.body.status_generate_spk
        var kode_event = req.body.kode_event
        var nama_event = req.body.nama_event
        var lokasi_event = req.body.lokasi_event
        var employee_kode = req.body.employee_kode
        var employee_nama = req.body.employee_nama
        var employee_jabatan = req.body.employee_jabatan
        var type_motor = req.body.type_motor
        var kode_motor = req.body.kode_motor
        var deskripsi_motor = req.body.deskripsi_motor
        var cust_hp_wa = req.body.cust_hp_wa

        connection.acquire(function(err, con) {

            connection.acquire(function(err, con) {
                con.query('INSERT INTO customer_logs (id,cust_id,cust_code,title,cust_nama,cust_pic,cust_org,cust_noktp,cust_alamat,noktp_stnk,poscode_id,pos_kode,pos_camat,pos_lurah,pos_kota,pos_prop,alamat_invoice,alamat_kirim,cust_telp_rumah,cust_telp_kantor,cust_fax,cust_hp,cust_email,cust_tgl_lahir,cust_agama,occupation_id,kerja_kode,cust_kelamin,cust_jenis,cust_npwp,salesman_id,assigned_to,cust_sumber,cust_npk,cust_kendaraan,branch_id,branch_code,company_id,company_code,cust_type,create_by,modi_by,created,modified,kunci,kota_id,propinsi_id,nama_stnk,alamat_stnk,alamat_lengkap,npwp_stnk,cust_alamat2,cek_alamat_kirim,pos_kota2,pos_prop2,propinsi_id2,pos_lurah2,pos_kode2,kota_id2,pos_camat2,lead_code,lead_id,cust_rt,cust_rw,dealer_id,dealer_code,rt_stnk,rw_stnk,doc_1,doc_2,doc_3,doc_4,doc_5,id_event,id_employee,id_motor,status_generate_spk,kode_event,nama_event,lokasi_event,employee_kode,employee_nama,employee_jabatan,type_motor,kode_motor,deskripsi_motor,cust_hp_wa) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, cust_id, cust_code, title, cust_nama, cust_pic, cust_org, cust_noktp, cust_alamat, noktp_stnk, poscode_id, pos_kode, pos_camat, pos_lurah, pos_kota, pos_prop, alamat_invoice, alamat_kirim, cust_telp_rumah, cust_telp_kantor, cust_fax, cust_hp, cust_email, cust_tgl_lahir, cust_agama, occupation_id, kerja_kode, cust_kelamin, cust_jenis, cust_npwp, salesman_id, assigned_to, cust_sumber, cust_npk, cust_kendaraan, branch_id, branch_code, company_id, company_code, cust_type, create_by, modi_by, created, modified, kunci, kota_id, propinsi_id, nama_stnk, alamat_stnk, alamat_lengkap, npwp_stnk, cust_alamat2, cek_alamat_kirim, pos_kota2, pos_prop2, propinsi_id2, pos_lurah2, pos_kode2, kota_id2, pos_camat2, lead_code, lead_id, cust_rt, cust_rw, dealer_id, dealer_code, rt_stnk, rw_stnk, doc_1, doc_2, doc_3, doc_4, doc_5, id_event, id_employee, id_motor, status_generate_spk, kode_event, nama_event, lokasi_event, employee_kode, employee_nama, employee_jabatan, type_motor, kode_motor, deskripsi_motor, cust_hp_wa],
                    function(err, result) {
                        con.release();
                        if (err) {
                            res.send({ status: 400, message: 'creation failed' });
                        } else {
                            res.send({ status: 200, message: 'created successfully' });
                        }
                    });
            });
        });
    };

    //dari generate ktp lama
    this.updateCustomerOld = function(req, res) {
        var id = req.body.id;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var poscode_id = req.body.poscode_id;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_npwp = req.body.cust_npwp;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;

        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;

        var cust_sumber = req.body.cust_sumber;
        var salesman_id = req.body.salesman_id;
        var assigned_to = req.body.assigned_to;
        var cust_kendaraan = req.body.cust_kendaraan;
        var npwp_stnk = req.body.npwp_stnk;

        var modified = new Date();

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var cust_pic = req.body.cust_pic;
        var cust_type = req.body.cust_type;
        var cust_hp_wa = req.body.cust_hp_wa;

        var doc_1 = req.body.doc_1;
        var doc_2 = req.body.doc_2;
        var doc_3 = req.body.doc_3;
        var doc_4 = req.body.doc_4;
        var doc_5 = req.body.doc_5;

        var status_generate_spk = "0";

        connection.acquire(function(err, con) {
            con.query('UPDATE customers SET branch_id = ?,branch_code = ?,cust_jenis = ?,occupation_id = ?,kerja_kode = ?,cust_agama = ?,poscode_id = ?,kota_id = ?,propinsi_id = ?,cust_kelamin = ?,pos_kode = ?,pos_kota = ?,pos_prop = ?,pos_camat = ?,pos_lurah = ?,kota_id2 =?,propinsi_id2 = ?,pos_kode2 = ?,pos_kota2 =?,pos_prop2 = ?,pos_camat2 = ?,pos_lurah2 = ?,cust_nama = ?,cust_noktp = ?,cust_npwp = ?,cust_tgl_lahir = ?,cust_alamat = ?,cust_rt = ?,cust_rw =?,alamat_lengkap = ?,rt_stnk = ?,rw_stnk =?,nama_stnk = ?,alamat_stnk = ?,cust_fax = ?,cust_hp = ?,cust_telp_rumah = ?,cust_email = ?,cust_alamat2 = ?,noktp_stnk = ?,alamat_invoice = ?,alamat_kirim = ?,cust_telp_kantor = ?,cust_sumber = ?,salesman_id = ?,assigned_to = ?,cust_kendaraan = ?,npwp_stnk =?,modified = ?,id_motor = ?,type_motor = ?,kode_motor = ?,deskripsi_motor = ?,id_employee = ?,employee_kode = ?,employee_nama = ?,employee_jabatan = ?,id_event = ?,kode_event =?,nama_event =?,lokasi_event =?,cust_pic =?, cust_type=?,cust_hp_wa=?, doc_1=?,doc_2=?,doc_3=?,doc_4=?,doc_5=?, status_generate_spk=? WHERE id = "' + id + '"', [branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, poscode_id, kota_id, propinsi_id, cust_kelamin, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_nama, cust_noktp, cust_npwp, cust_tgl_lahir, cust_alamat, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_hp, cust_telp_rumah, cust_email, cust_alamat2, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, cust_sumber, salesman_id, assigned_to, cust_kendaraan, npwp_stnk, modified, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, cust_pic, cust_type, cust_hp_wa, doc_1, doc_2, doc_3, doc_4, doc_5, status_generate_spk], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Update failed' });
                } else {
                    res.send({ status: 200, message: 'Update successfully' });
                }
            });
        });
    };

    // gnereate jika ktp baru
    this.generateCustomer = function(req, res) {
        var id = uuidv1();
        var cust_code = null;
        var poscode_id = req.body.poscode_id;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_alamat = req.body.cust_alamat;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;
        var salesman_id = req.body.salesman_id;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_npwp = req.body.cust_npwp;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;
        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var cust_kendaraan = req.body.cust_kendaraan;
        var cust_type = req.body.cust_type;
        var created = new Date();
        var modified = new Date();
        var npwp_stnk = req.body.npwp_stnk;
        var lead_id = req.body.lead_id;
        var lead_code = req.body.lead_code;
        var dealer_id = req.body.dealer_id;
        var dealer_code = req.body.dealer_code;
        var cust_sumber = req.body.cust_sumber;
        var doc_1 = req.body.doc_1;
        var doc_2 = req.body.doc_2;
        var doc_3 = req.body.doc_3;
        var doc_4 = req.body.doc_4;
        var doc_5 = req.body.doc_5;

        var status_generate_spk = "0";

        var cust_pic = req.body.cust_pic;
        var cust_org = req.body.cust_org;
        var assigned_to = req.body.assigned_to;
        var cust_npk = req.body.cust_npk;

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var company_id = req.body.company_id;
        var company_code = req.body.company_code;

        var cust_hp_wa = req.body.cust_hp_wa;

        connection.acquire(function(err, con) {
            con.query('select cust_code from customers order by created desc limit 1', function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Customer Failed' });
                } else {
                    // for CODE CP
                    if (result.length != 0) {
                        var test = result[0].cust_code;
                    } else {
                        var test = "C000000000000"
                    }

                    var pieces = test.split('C');
                    var prefix = pieces[0];
                    var lastNum = pieces[1];
                    lastNum = parseInt(lastNum, 10);
                    lastNum++;
                    cust_code = "C" + ("000000000000" + lastNum).substr(-6);

                    connection.acquire(function(err, con) {
                        con.query('INSERT INTO customers (id,cust_code,poscode_id,pos_kode,pos_kota,pos_prop,pos_camat,pos_lurah,cust_nama,cust_noktp,cust_alamat,cust_hp,cust_telp_rumah,noktp_stnk,alamat_invoice,alamat_kirim,cust_telp_kantor,salesman_id,branch_id,branch_code,cust_jenis,occupation_id,kerja_kode,cust_agama,kota_id,propinsi_id,cust_kelamin, kota_id2,propinsi_id2,pos_kode2,pos_kota2,pos_prop2,pos_camat2,pos_lurah2,cust_npwp,cust_rt,cust_rw,alamat_lengkap,rt_stnk,rw_stnk,nama_stnk,alamat_stnk,cust_fax,cust_email,cust_alamat2,cust_kendaraan,cust_type,created,modified,npwp_stnk,lead_id,lead_code,dealer_id,dealer_code,cust_sumber,doc_1,doc_2,doc_3,doc_4,doc_5,status_generate_spk,cust_pic,cust_org,assigned_to,cust_npk,cust_tgl_lahir,id_motor,type_motor,kode_motor,deskripsi_motor,id_employee,employee_kode,employee_nama,employee_jabatan,id_event,kode_event,nama_event,lokasi_event,company_id,company_code, cust_hp_wa) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, cust_code, poscode_id, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, cust_nama, cust_noktp, cust_alamat, cust_hp, cust_telp_rumah, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, salesman_id, branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, kota_id, propinsi_id, cust_kelamin, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_npwp, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_email, cust_alamat2, cust_kendaraan, cust_type, created, modified, npwp_stnk, lead_id, lead_code, dealer_id, dealer_code, cust_sumber, doc_1, doc_2, doc_3, doc_4, doc_5, status_generate_spk, cust_pic, cust_org, assigned_to, cust_npk, cust_tgl_lahir, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, company_id, company_code, cust_hp_wa],
                            function(err, result) {
                                con.release();
                                if (err) {
                                    res.send({ status: 400, message: 'creation failed' });
                                } else {
                                    res.send({ status: 200, message: 'created successfully' });
                                }
                            });
                    });
                }
            });
        });
    };

    // dari add ktp lama
    this.updateCustomerOldAdd = function(req, res) {

        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        var id = req.params.id;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var poscode_id = req.body.poscode_id;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_npwp = req.body.cust_npwp;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;

        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;

        var cust_sumber = req.body.cust_sumber;
        var salesman_id = req.body.salesman_id;
        var assigned_to = req.body.assigned_to;
        var cust_kendaraan = req.body.cust_kendaraan;
        var npwp_stnk = req.body.npwp_stnk;

        var modified = new Date();

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var cust_pic = req.body.cust_pic;
        var cust_type = req.body.cust_type;
        var cust_hp_wa = req.body.cust_hp_wa;

        var fileUploads = req.files;
        var docs = [];

        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT doc_1,doc_2,doc_3,doc_4,doc_5 FROM customers where id = "' + id + '"', function(err, data) {
                con.release();
                if (err) {
                    console.log("error");
                } else {

                    fileUploads.forEach(function(element, index) {
                        var image = data[0][element.fieldname];
                        if (image) {
                            fs.unlink(appRoot + image.replace("/files/", "/uploads/"), (err) => {
                                if (err) throw err;
                            });
                        }
                        docs[element.fieldname] = "/files/" + element.filename;
                    });
                    connection.acquire(function(err, conn) {
                        var queryStr = 'UPDATE customers SET id = ?, branch_id = ?,branch_code = ?,cust_jenis = ?,occupation_id = ?,kerja_kode = ?,cust_agama = ?,poscode_id = ?,kota_id = ?,propinsi_id = ?,cust_kelamin = ?,pos_kode = ?,pos_kota = ?,pos_prop = ?,pos_camat = ?,pos_lurah = ?,kota_id2 =?,propinsi_id2 = ?,pos_kode2 = ?,pos_kota2 =?,pos_prop2 = ?,pos_camat2 = ?,pos_lurah2 = ?,cust_nama = ?,cust_noktp = ?,cust_npwp = ?,cust_tgl_lahir = ?,cust_alamat = ?,cust_rt = ?,cust_rw =?,alamat_lengkap = ?,rt_stnk = ?,rw_stnk =?,nama_stnk = ?,alamat_stnk = ?,cust_fax = ?,cust_hp = ?,cust_telp_rumah = ?,cust_email = ?,cust_alamat2 = ?,noktp_stnk = ?,alamat_invoice = ?,alamat_kirim = ?,cust_telp_kantor = ?,cust_sumber = ?,salesman_id = ?,assigned_to = ?,cust_kendaraan = ?,npwp_stnk =?,modified = ?,id_motor = ?,type_motor = ?,kode_motor = ?,deskripsi_motor = ?,id_employee = ?,employee_kode = ?,employee_nama = ?,employee_jabatan = ?,id_event = ?,kode_event =?,nama_event =?,lokasi_event =?,cust_pic =?, cust_type=?,cust_hp_wa=?';

                        var queryParams = [id, branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, poscode_id, kota_id, propinsi_id, cust_kelamin, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_nama, cust_noktp, cust_npwp, cust_tgl_lahir, cust_alamat, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_hp, cust_telp_rumah, cust_email, cust_alamat2, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, cust_sumber, salesman_id, assigned_to, cust_kendaraan, npwp_stnk, modified, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, cust_pic, cust_type, cust_hp_wa];

                        for (var key in docs) {
                            var value = docs[key];
                            queryStr = queryStr + ", " + key + " = ?";
                            queryParams.push(value);
                        }

                        queryStr = queryStr + ' where id = "' + id + '"'

                        conn.query(queryStr, queryParams, function(err, result) {
                            conn.release();
                            if (err) {
                                res.send({ status: 400, message: 'Edit Customer Failed' });
                            } else {
                                res.send({ status: 200, message: 'Edit Customer Success' });
                            }
                        });
                    });
                }
            });
        });

    };

    // button add ktp baru
    this.addCustomerTest = function(req, res) {

        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        var fileUploads = req.files;
        var doc_1 = null;
        var doc_2 = null;
        var doc_3 = null;
        var doc_4 = null;
        var doc_5 = null;

        fileUploads.forEach(function(element, index) {
            if (index == 0) {
                doc_1 = "/files/" + element.filename
            } else if (index == 1) {
                doc_2 = "/files/" + element.filename
            } else if (index == 2) {
                doc_3 = "/files/" + element.filename
            } else if (index == 3) {
                doc_4 = "/files/" + element.filename
            } else if (index == 4) {
                doc_5 = "/files/" + element.filename
            }
        });

        var id = uuidv1();
        var cust_code = null;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var poscode_id = req.body.poscode_id;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_npwp = req.body.cust_npwp;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;

        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;

        var cust_sumber = req.body.cust_sumber;
        var salesman_id = req.body.salesman_id;
        var assigned_to = req.body.assigned_to;
        var cust_kendaraan = req.body.cust_kendaraan;
        var npwp_stnk = req.body.npwp_stnk;
        var created = new Date();
        var modified = new Date();

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var cust_pic = req.body.cust_pic;
        var cust_type = req.body.cust_type;
        var company_id = req.body.company_id;
        var company_code = req.body.company_code;
        var status_generate_spk = "0";
        var cust_hp_wa = req.body.cust_hp_wa;
        // var doc_1 = req.body.doc_1;
        // var doc_2 = req.body.doc_2;
        // var doc_3 = req.body.doc_3;
        // var doc_4 = req.body.doc_4;
        // var doc_5 = req.body.doc_5;

        connection.acquire(function(err, con) {
            con.query('select cust_code from customers order by created desc limit 1', function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Customer Failed' });
                } else {
                    // for CODE CP
                    if (result.length != 0) {
                        var test = result[0].cust_code;
                    } else {
                        var test = "C000000000000"
                    }

                    var pieces = test.split('C');
                    var prefix = pieces[0];
                    var lastNum = pieces[1];
                    lastNum = parseInt(lastNum, 10);
                    lastNum++;
                    cust_code = "C" + ("000000000000" + lastNum).substr(-6);


                    connection.acquire(function(err, con) {
                        con.query('INSERT INTO customers (id,cust_code,branch_id,branch_code,cust_jenis,occupation_id,kerja_kode,cust_agama,poscode_id,kota_id,propinsi_id,cust_kelamin,pos_kode,pos_kota,pos_prop,pos_camat,pos_lurah,kota_id2,propinsi_id2,pos_kode2,pos_kota2,pos_prop2,pos_camat2,pos_lurah2,cust_nama,cust_noktp,cust_npwp,cust_tgl_lahir,cust_alamat,cust_rt,cust_rw,alamat_lengkap,rt_stnk,rw_stnk,nama_stnk,alamat_stnk,cust_fax,cust_hp,cust_telp_rumah,cust_email,cust_alamat2,noktp_stnk,alamat_invoice,alamat_kirim,cust_telp_kantor,cust_sumber,salesman_id,assigned_to,cust_kendaraan,npwp_stnk,created,modified,id_motor,type_motor,kode_motor,deskripsi_motor,id_employee,employee_kode,employee_nama,employee_jabatan,id_event,kode_event,nama_event,lokasi_event,cust_pic,cust_type,company_id,company_code,status_generate_spk,doc_1,doc_2,doc_3,doc_4,doc_5,cust_hp_wa) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, cust_code, branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, poscode_id, kota_id, propinsi_id, cust_kelamin, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_nama, cust_noktp, cust_npwp, cust_tgl_lahir, cust_alamat, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_hp, cust_telp_rumah, cust_email, cust_alamat2, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, cust_sumber, salesman_id, assigned_to, cust_kendaraan, npwp_stnk, created, modified, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, cust_pic, cust_type, company_id, company_code, status_generate_spk, doc_1, doc_2, doc_3, doc_4, doc_5, cust_hp_wa],
                            function(err, result) {
                                con.release();
                                if (err) {
                                    res.send({ status: 400, message: 'creation failed' });
                                } else {
                                    res.send({ status: 200, message: 'created successfully' });
                                }
                            });
                    });
                }
            });
        });
    };

    //------------- DMS VALIDASI KTP (RATRI) ---------------------//

    //dari generate ktp lama
    this.updateCustomerOldDMS = function(req, res) {
        var id = req.body.id;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var poscode_id = req.body.poscode_id;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_npwp = req.body.cust_npwp;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;

        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;

        var cust_sumber = req.body.cust_sumber;
        var salesman_id = req.body.salesman_id;
        var assigned_to = req.body.assigned_to;
        var cust_kendaraan = req.body.cust_kendaraan;
        var npwp_stnk = req.body.npwp_stnk;

        var modified = new Date();

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var cust_pic = req.body.cust_pic;
        var cust_type = req.body.cust_type;
        var cust_hp_wa = req.body.cust_hp_wa;

        var doc_1 = req.body.doc_1;
        var doc_2 = req.body.doc_2;
        var doc_3 = req.body.doc_3;
        var doc_4 = req.body.doc_4;
        var doc_5 = req.body.doc_5;

        var status_generate_spk = "0";

        connection.acquire(function(err, con) {
            con.query('UPDATE customers SET branch_id = ?,branch_code = ?,cust_jenis = ?,occupation_id = ?,kerja_kode = ?,cust_agama = ?,poscode_id = ?,kota_id = ?,propinsi_id = ?,cust_kelamin = ?,pos_kode = ?,pos_kota = ?,pos_prop = ?,pos_camat = ?,pos_lurah = ?,kota_id2 =?,propinsi_id2 = ?,pos_kode2 = ?,pos_kota2 =?,pos_prop2 = ?,pos_camat2 = ?,pos_lurah2 = ?,cust_nama = ?,cust_noktp = ?,cust_npwp = ?,cust_tgl_lahir = ?,cust_alamat = ?,cust_rt = ?,cust_rw =?,alamat_lengkap = ?,rt_stnk = ?,rw_stnk =?,nama_stnk = ?,alamat_stnk = ?,cust_fax = ?,cust_hp = ?,cust_telp_rumah = ?,cust_email = ?,cust_alamat2 = ?,noktp_stnk = ?,alamat_invoice = ?,alamat_kirim = ?,cust_telp_kantor = ?,cust_sumber = ?,salesman_id = ?,assigned_to = ?,cust_kendaraan = ?,npwp_stnk =?,modified = ?,id_motor = ?,type_motor = ?,kode_motor = ?,deskripsi_motor = ?,id_employee = ?,employee_kode = ?,employee_nama = ?,employee_jabatan = ?,id_event = ?,kode_event =?,nama_event =?,lokasi_event =?,cust_pic =?, cust_type=?,cust_hp_wa=?, doc_1=?,doc_2=?,doc_3=?,doc_4=?,doc_5=?, status_generate_spk=? WHERE id = "' + id + '"', [branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, poscode_id, kota_id, propinsi_id, cust_kelamin, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_nama, cust_noktp, cust_npwp, cust_tgl_lahir, cust_alamat, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_hp, cust_telp_rumah, cust_email, cust_alamat2, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, cust_sumber, salesman_id, assigned_to, cust_kendaraan, npwp_stnk, modified, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, cust_pic, cust_type, cust_hp_wa, doc_1, doc_2, doc_3, doc_4, doc_5, status_generate_spk], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Update failed' });
                } else {
                    var no_ktp = req.body.cust_noktp
                    request({
                        uri: 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_customer_all.php?cust_noktp=' + no_ktp,
                        auth: {
                            'bearer': '71D55F9957529'
                        },
                        rejectUnauthorized: false, //add when working with https sites
                        requestCert: false, //add when working with https sites
                        agent: false, //add when working with https sites

                    }, function(error, rows) {
                        if (error) {
                            console.log(error);
                        } else {
                            const body = JSON.parse(rows.body); // parse string to JSON
                            console.log(rows.body);

                            if (body.status == "False") {
                                connection.acquire(function(err, con) {
                                    con.query('select id, cust_code, cust_noktp from customers order by modified desc limit 1', function(err, result) {
                                        con.release();
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            var id_cust = result[0].id;
                                            var code_cust = result[0].cust_code;

                                            console.log(code_cust);
                                            console.log(id_cust);
                                            var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_insert_customer.php';

                                            var requestData = {

                                                id: id_cust,
                                                cust_code: code_cust,
                                                poscode_id: req.body.poscode_id,
                                                pos_kode: req.body.pos_kode,
                                                pos_kota: req.body.pos_kota,
                                                pos_prop: req.body.pos_prop,
                                                pos_camat: req.body.pos_camat,
                                                pos_lurah: req.body.pos_lurah,
                                                cust_nama: req.body.cust_nama,
                                                cust_noktp: req.body.cust_noktp,
                                                cust_alamat: req.body.cust_alamat,
                                                cust_hp: req.body.cust_hp,
                                                cust_telp_rumah: req.body.cust_telp_rumah,
                                                noktp_stnk: req.body.noktp_stnk,
                                                alamat_invoice: req.body.alamat_invoice,
                                                alamat_kirim: req.body.alamat_kirim,
                                                cust_telp_kantor: req.body.cust_telp_kantor,
                                                salesman_id: req.body.salesman_id,
                                                cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                branch_id: req.body.branch_id,
                                                branch_code: req.body.branch_code,
                                                cust_jenis: req.body.cust_jenis,
                                                occupation_id: req.body.occupation_id,
                                                kerja_kode: req.body.kerja_kode,
                                                cust_agama: req.body.cust_agama,
                                                kota_id: req.body.kota_id,
                                                propinsi_id: req.body.propinsi_id,
                                                cust_kelamin: req.body.cust_kelamin,
                                                kota_id2: req.body.kota_id2,
                                                propinsi_id2: req.body.propinsi_id2,
                                                pos_kode2: req.body.pos_kode2,
                                                pos_kota2: req.body.pos_kota2,
                                                pos_prop2: req.body.pos_prop2,
                                                pos_camat2: req.body.pos_camat2,
                                                pos_lurah2: req.body.pos_lurah2,
                                                cust_npwp: req.body.cust_npwp,
                                                cust_rt: req.body.cust_rt,
                                                cust_rw: req.body.cust_rw,
                                                alamat_lengkap: req.body.alamat_lengkap,
                                                rt_stnk: req.body.rt_stnk,
                                                rw_stnk: req.body.rw_stnk,
                                                nama_stnk: req.body.nama_stnk,
                                                alamat_stnk: req.body.alamat_stnk,
                                                cust_fax: req.body.cust_fax,
                                                cust_email: req.body.cust_email,
                                                cust_alamat2: req.body.cust_alamat2,
                                                cust_kendaraan: req.body.cust_kendaraan,
                                                cust_type: req.body.cust_type,
                                                created: new Date(),
                                                modified: new Date(),
                                                npwp_stnk: req.body.npwp_stnk,
                                                lead_id: req.body.lead_id,
                                                lead_code: req.body.lead_code,
                                                dealer_id: req.body.dealer_id,
                                                dealer_code: req.body.dealer_code,
                                                cust_sumber: req.body.cust_sumber,
                                                doc_1: "",
                                                doc_2: "",
                                                doc_3: "",
                                                doc_4: "",
                                                doc_5: "",

                                                status_generate_spk: "0",

                                                cust_pic: req.body.cust_pic,
                                                cust_org: req.body.cust_org,
                                                assigned_to: req.body.assigned_to,
                                                cust_npk: req.body.cust_npk,

                                                id_motor: req.body.id_motor,
                                                type_motor: req.body.type_motor,
                                                kode_motor: req.body.kode_motor,
                                                deskripsi_motor: req.body.deskripsi_motor,

                                                id_employee: req.body.id_employee,
                                                employee_kode: req.body.employee_kode,
                                                employee_nama: req.body.employee_nama,
                                                employee_jabatan: req.body.employee_jabatan,

                                                id_event: req.body.id_event,
                                                kode_event: req.body.kode_event,
                                                nama_event: req.body.nama_event,
                                                lokasi_event: req.body.lokasi_event,
                                                cust_hp_wa: req.body.cust_hp_wa,

                                                ro_bd_id: "",
                                                sektor_kode: "",
                                                cust_wn: "",
                                                cust_kitas: "",
                                                cust_kk: "",
                                                cust_wn_stnk: "",
                                                nokitas_stnk: "",
                                                kk_stnk: "",
                                            }

                                            var data = {
                                                url: url,
                                                auth: {
                                                    'bearer': '71D55F9957529'
                                                },
                                                body: requestData,
                                                json: true,
                                                rejectUnauthorized: false, //add when working with https sites
                                                requestCert: false, //add when working with https sites
                                                agent: false, //add when working with https sites
                                            }

                                            console.log(data);
                                            request.post(data, function(error, httpResponse, body) {
                                                if (body.status == "Failed") {
                                                    console.log(error);
                                                    res.send({ status: 400, message: 'Generate Customer Failed' });
                                                } else {
                                                    console.log(body);
                                                    res.send({ status: 200, message: 'Generate Customer success' });
                                                }

                                            });

                                        }

                                    });
                                });
                            } else if (body.status == "True") {
                                var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_edit_customer2.php?cust_noktp=' + no_ktp;

                                var requestData = {

                                    // cust_code: result[0].cust_code,
                                    title: "",
                                    cust_nama: req.body.cust_nama,
                                    cust_pic: req.body.cust_pic,
                                    cust_org: "",
                                    cust_alamat: req.body.cust_alamat,
                                    noktp_stnk: req.body.noktp_stnk,
                                    poscode_id: req.body.poscode_id,
                                    pos_kode: req.body.pos_kode,
                                    pos_camat: req.body.pos_camat,
                                    pos_lurah: req.body.pos_lurah,
                                    pos_kota: req.body.pos_kota,
                                    pos_prop: req.body.pos_prop,
                                    alamat_invoice: req.body.alamat_invoice,
                                    alamat_kirim: req.body.alamat_kirim,
                                    cust_telp_rumah: req.body.cust_telp_rumah,
                                    cust_telp_kantor: req.body.cust_telp_kantor,
                                    cust_fax: req.body.cust_fax,
                                    cust_hp: req.body.cust_hp,
                                    cust_email: req.body.cust_email,
                                    cust_tgl_lahir: req.body.cust_tgl_lahir,
                                    cust_agama: req.body.cust_agama,
                                    occupation_id: req.body.occupation_id,
                                    kerja_kode: req.body.kerja_kode,
                                    cust_kelamin: req.body.cust_kelamin,
                                    cust_jenis: req.body.cust_jenis,
                                    cust_npwp: req.body.cust_npwp,

                                    salesman_id: req.body.salesman_id,
                                    assigned_to: req.body.assigned_to,
                                    cust_sumber: req.body.cust_sumber,
                                    cust_npk: req.body.cust_npk,
                                    cust_kendaraan: req.body.cust_kendaraan,
                                    branch_id: req.body.branch_id,
                                    branch_code: req.body.branch_code,
                                    company_id: req.body.company_id,
                                    company_code: req.body.company_code,
                                    cust_type: req.body.cust_type,
                                    modi_by: salesman_id,
                                    modified: new Date(),
                                    kunci: "",
                                    kota_id: req.body.kota_id,
                                    propinsi_id: req.body.propinsi_id,
                                    nama_stnk: req.body.nama_stnk,
                                    alamat_stnk: req.body.alamat_stnk,
                                    alamat_lengkap: req.body.alamat_lengkap,
                                    npwp_stnk: req.body.npwp_stnk,
                                    cust_alamat2: req.body.cust_alamat2,
                                    cek_alamat_kirim: "",
                                    pos_kota2: req.body.pos_kota2,
                                    pos_prop2: req.body.pos_prop2,
                                    propinsi_id2: req.body.propinsi_id2,
                                    pos_lurah2: req.body.pos_lurah2,
                                    pos_kode2: req.body.pos_kode2,
                                    kota_id2: req.body.kota_id2,
                                    pos_camat2: req.body.pos_camat2,

                                    // lead_code: result[0].lead_code,
                                    // lead_id: result[0].lead_id,
                                    cust_rt: req.body.cust_rt,
                                    cust_rw: req.body.cust_rw,

                                    dealer_id: req.body.dealer_id,
                                    dealer_code: req.body.dealer_code,

                                    rt_stnk: req.body.rt_stnk,
                                    rw_stnk: req.body.rw_stnk,

                                    status_generate_spk: "0",

                                    id_motor: req.body.id_motor,
                                    id_event: req.body.id_event,
                                    id_employee: req.body.id_employee,
                                    ro_bd_id: "",
                                    sektor_kode: "",
                                    cust_wn: "",
                                    cust_kitas: "",
                                    cust_kk: "",
                                    cust_wn_stnk: "",
                                    nokitas_stnk: "",
                                    kk_stnk: "",
                                    type_motor: req.body.type_motor,
                                    kode_motor: req.body.kode_motor,
                                    deskripsi_motor: req.body.deskripsi_motor,

                                    employee_kode: req.body.employee_kode,
                                    employee_nama: req.body.employee_nama,
                                    employee_jabatan: req.body.employee_jabatan,

                                    kode_event: req.body.kode_event,
                                    nama_event: req.body.nama_event,
                                    lokasi_event: req.body.lokasi_event,

                                    doc_1: "",
                                    doc_2: "",
                                    doc_3: "",
                                    doc_4: "",
                                    doc_5: "",

                                }

                                var data = {
                                    url: url,
                                    auth: {
                                        'bearer': '71D55F9957529'
                                    },
                                    json: true,
                                    body: requestData,
                                    rejectUnauthorized: false, //add when working with https sites
                                    requestCert: false, //add when working with https sites
                                    agent: false, //add when working with https sites
                                }

                                console.log(data);
                                request.post(data, function(error, httpResponse, body) {
                                    if (body.status == "Failed") {
                                        console.log(error);
                                        res.send({ status: 400, message: 'Generate Customer Failed' });
                                    } else {
                                        console.log(body);
                                        res.send({ status: 200, message: 'Generate Customer success' });
                                    }
                                });
                            }
                        }

                    });
                }
            });
        });
    };

    // gnereate jika ktp baru
    this.generateCustomerDMS = function(req, res) {
        var id = uuidv1();
        var cust_code = null;
        var poscode_id = req.body.poscode_id;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_alamat = req.body.cust_alamat;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;
        var salesman_id = req.body.salesman_id;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_npwp = req.body.cust_npwp;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;
        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var cust_kendaraan = req.body.cust_kendaraan;
        var cust_type = req.body.cust_type;
        var created = new Date();
        var modified = new Date();
        var npwp_stnk = req.body.npwp_stnk;
        var lead_id = req.body.lead_id;
        var lead_code = req.body.lead_code;
        var dealer_id = req.body.dealer_id;
        var dealer_code = req.body.dealer_code;
        var cust_sumber = req.body.cust_sumber;
        var doc_1 = req.body.doc_1;
        var doc_2 = req.body.doc_2;
        var doc_3 = req.body.doc_3;
        var doc_4 = req.body.doc_4;
        var doc_5 = req.body.doc_5;

        var status_generate_spk = "0";

        var cust_pic = req.body.cust_pic;
        var cust_org = req.body.cust_org;
        var assigned_to = req.body.assigned_to;
        var cust_npk = req.body.cust_npk;

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var company_id = req.body.company_id;
        var company_code = req.body.company_code;

        var cust_hp_wa = req.body.cust_hp_wa;

        connection.acquire(function(err, con) {
            con.query('select cust_code from customers order by created desc limit 1', function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Customer Failed' });
                } else {
                    // for CODE CP
                    if (result.length != 0) {
                        var test = result[0].cust_code;
                    } else {
                        var test = "C000000000000"
                    }

                    var pieces = test.split('C');
                    var prefix = pieces[0];
                    var lastNum = pieces[1];
                    lastNum = parseInt(lastNum, 10);
                    lastNum++;
                    cust_code = "C" + ("000000000000" + lastNum).substr(-6);

                    connection.acquire(function(err, con) {
                        con.query('INSERT INTO customers (id,cust_code,poscode_id,pos_kode,pos_kota,pos_prop,pos_camat,pos_lurah,cust_nama,cust_noktp,cust_alamat,cust_hp,cust_telp_rumah,noktp_stnk,alamat_invoice,alamat_kirim,cust_telp_kantor,salesman_id,branch_id,branch_code,cust_jenis,occupation_id,kerja_kode,cust_agama,kota_id,propinsi_id,cust_kelamin, kota_id2,propinsi_id2,pos_kode2,pos_kota2,pos_prop2,pos_camat2,pos_lurah2,cust_npwp,cust_rt,cust_rw,alamat_lengkap,rt_stnk,rw_stnk,nama_stnk,alamat_stnk,cust_fax,cust_email,cust_alamat2,cust_kendaraan,cust_type,created,modified,npwp_stnk,lead_id,lead_code,dealer_id,dealer_code,cust_sumber,doc_1,doc_2,doc_3,doc_4,doc_5,status_generate_spk,cust_pic,cust_org,assigned_to,cust_npk,cust_tgl_lahir,id_motor,type_motor,kode_motor,deskripsi_motor,id_employee,employee_kode,employee_nama,employee_jabatan,id_event,kode_event,nama_event,lokasi_event,company_id,company_code, cust_hp_wa) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, cust_code, poscode_id, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, cust_nama, cust_noktp, cust_alamat, cust_hp, cust_telp_rumah, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, salesman_id, branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, kota_id, propinsi_id, cust_kelamin, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_npwp, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_email, cust_alamat2, cust_kendaraan, cust_type, created, modified, npwp_stnk, lead_id, lead_code, dealer_id, dealer_code, cust_sumber, doc_1, doc_2, doc_3, doc_4, doc_5, status_generate_spk, cust_pic, cust_org, assigned_to, cust_npk, cust_tgl_lahir, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, company_id, company_code, cust_hp_wa],
                            function(err, result) {
                                con.release();
                                if (err) {
                                    res.send({ status: 400, message: 'creation failed' });
                                } else {
                                    var no_ktp = req.body.cust_noktp
                                    request({
                                        uri: 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_customer_all.php?cust_noktp=' + no_ktp,
                                        auth: {
                                            'bearer': '71D55F9957529'
                                        },
                                        rejectUnauthorized: false, //add when working with https sites
                                        requestCert: false, //add when working with https sites
                                        agent: false, //add when working with https sites

                                    }, function(error, rows) {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            const body = JSON.parse(rows.body); // parse string to JSON
                                            console.log(rows.body);

                                            if (body.status == "False") {
                                                connection.acquire(function(err, con) {
                                                    con.query('select id, cust_code, cust_noktp from customers order by modified desc limit 1', function(err, result) {
                                                        con.release();
                                                        if (err) {
                                                            console.log(err);
                                                        } else {
                                                            var id_cust = result[0].id;
                                                            var code_cust = result[0].cust_code;

                                                            console.log(code_cust);
                                                            console.log(id_cust);
                                                            var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_insert_customer.php';

                                                            var requestData = {

                                                                id: id_cust,
                                                                cust_code: code_cust,
                                                                poscode_id: req.body.poscode_id,
                                                                pos_kode: req.body.pos_kode,
                                                                pos_kota: req.body.pos_kota,
                                                                pos_prop: req.body.pos_prop,
                                                                pos_camat: req.body.pos_camat,
                                                                pos_lurah: req.body.pos_lurah,
                                                                cust_nama: req.body.cust_nama,
                                                                cust_noktp: req.body.cust_noktp,
                                                                cust_alamat: req.body.cust_alamat,
                                                                cust_hp: req.body.cust_hp,
                                                                cust_telp_rumah: req.body.cust_telp_rumah,
                                                                noktp_stnk: req.body.noktp_stnk,
                                                                alamat_invoice: req.body.alamat_invoice,
                                                                alamat_kirim: req.body.alamat_kirim,
                                                                cust_telp_kantor: req.body.cust_telp_kantor,
                                                                salesman_id: req.body.salesman_id,
                                                                cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                                branch_id: req.body.branch_id,
                                                                branch_code: req.body.branch_code,
                                                                cust_jenis: req.body.cust_jenis,
                                                                occupation_id: req.body.occupation_id,
                                                                kerja_kode: req.body.kerja_kode,
                                                                cust_agama: req.body.cust_agama,
                                                                kota_id: req.body.kota_id,
                                                                propinsi_id: req.body.propinsi_id,
                                                                cust_kelamin: req.body.cust_kelamin,
                                                                kota_id2: req.body.kota_id2,
                                                                propinsi_id2: req.body.propinsi_id2,
                                                                pos_kode2: req.body.pos_kode2,
                                                                pos_kota2: req.body.pos_kota2,
                                                                pos_prop2: req.body.pos_prop2,
                                                                pos_camat2: req.body.pos_camat2,
                                                                pos_lurah2: req.body.pos_lurah2,
                                                                cust_npwp: req.body.cust_npwp,
                                                                cust_rt: req.body.cust_rt,
                                                                cust_rw: req.body.cust_rw,
                                                                alamat_lengkap: req.body.alamat_lengkap,
                                                                rt_stnk: req.body.rt_stnk,
                                                                rw_stnk: req.body.rw_stnk,
                                                                nama_stnk: req.body.nama_stnk,
                                                                alamat_stnk: req.body.alamat_stnk,
                                                                cust_fax: req.body.cust_fax,
                                                                cust_email: req.body.cust_email,
                                                                cust_alamat2: req.body.cust_alamat2,
                                                                cust_kendaraan: req.body.cust_kendaraan,
                                                                cust_type: req.body.cust_type,
                                                                created: new Date(),
                                                                modified: new Date(),
                                                                npwp_stnk: req.body.npwp_stnk,
                                                                lead_id: req.body.lead_id,
                                                                lead_code: req.body.lead_code,
                                                                dealer_id: req.body.dealer_id,
                                                                dealer_code: req.body.dealer_code,
                                                                cust_sumber: req.body.cust_sumber,
                                                                doc_1: "",
                                                                doc_2: "",
                                                                doc_3: "",
                                                                doc_4: "",
                                                                doc_5: "",

                                                                status_generate_spk: "0",

                                                                cust_pic: req.body.cust_pic,
                                                                cust_org: req.body.cust_org,
                                                                assigned_to: req.body.assigned_to,
                                                                cust_npk: req.body.cust_npk,

                                                                id_motor: req.body.id_motor,
                                                                type_motor: req.body.type_motor,
                                                                kode_motor: req.body.kode_motor,
                                                                deskripsi_motor: req.body.deskripsi_motor,

                                                                id_employee: req.body.id_employee,
                                                                employee_kode: req.body.employee_kode,
                                                                employee_nama: req.body.employee_nama,
                                                                employee_jabatan: req.body.employee_jabatan,

                                                                id_event: req.body.id_event,
                                                                kode_event: req.body.kode_event,
                                                                nama_event: req.body.nama_event,
                                                                lokasi_event: req.body.lokasi_event,
                                                                cust_hp_wa: req.body.cust_hp_wa,

                                                                ro_bd_id: "",
                                                                sektor_kode: "",
                                                                cust_wn: "",
                                                                cust_kitas: "",
                                                                cust_kk: "",
                                                                cust_wn_stnk: "",
                                                                nokitas_stnk: "",
                                                                kk_stnk: "",
                                                            }

                                                            var data = {
                                                                url: url,
                                                                auth: {
                                                                    'bearer': '71D55F9957529'
                                                                },
                                                                body: requestData,
                                                                json: true,
                                                                rejectUnauthorized: false, //add when working with https sites
                                                                requestCert: false, //add when working with https sites
                                                                agent: false, //add when working with https sites
                                                            }

                                                            console.log(data);
                                                            request.post(data, function(error, httpResponse, body) {
                                                                if (body.status == "Failed") {
                                                                    console.log(error);
                                                                    res.send({ status: 400, message: 'Generate Customer Failed' });
                                                                } else {
                                                                    console.log(body);
                                                                    res.send({ status: 200, message: 'Generate Customer success' });
                                                                }

                                                            });

                                                        }

                                                    });
                                                });
                                            } else if (body.status == "True") {
                                                var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_edit_customer2.php?cust_noktp=' + no_ktp;

                                                var requestData = {

                                                    // cust_code: result[0].cust_code,
                                                    title: "",
                                                    cust_nama: req.body.cust_nama,
                                                    cust_pic: req.body.cust_pic,
                                                    cust_org: "",
                                                    cust_alamat: req.body.cust_alamat,
                                                    noktp_stnk: req.body.noktp_stnk,
                                                    poscode_id: req.body.poscode_id,
                                                    pos_kode: req.body.pos_kode,
                                                    pos_camat: req.body.pos_camat,
                                                    pos_lurah: req.body.pos_lurah,
                                                    pos_kota: req.body.pos_kota,
                                                    pos_prop: req.body.pos_prop,
                                                    alamat_invoice: req.body.alamat_invoice,
                                                    alamat_kirim: req.body.alamat_kirim,
                                                    cust_telp_rumah: req.body.cust_telp_rumah,
                                                    cust_telp_kantor: req.body.cust_telp_kantor,
                                                    cust_fax: req.body.cust_fax,
                                                    cust_hp: req.body.cust_hp,
                                                    cust_email: req.body.cust_email,
                                                    cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                    cust_agama: req.body.cust_agama,
                                                    occupation_id: req.body.occupation_id,
                                                    kerja_kode: req.body.kerja_kode,
                                                    cust_kelamin: req.body.cust_kelamin,
                                                    cust_jenis: req.body.cust_jenis,
                                                    cust_npwp: req.body.cust_npwp,

                                                    salesman_id: req.body.salesman_id,
                                                    assigned_to: req.body.assigned_to,
                                                    cust_sumber: req.body.cust_sumber,
                                                    cust_npk: req.body.cust_npk,
                                                    cust_kendaraan: req.body.cust_kendaraan,
                                                    branch_id: req.body.branch_id,
                                                    branch_code: req.body.branch_code,
                                                    company_id: req.body.company_id,
                                                    company_code: req.body.company_code,
                                                    cust_type: req.body.cust_type,
                                                    modi_by: salesman_id,
                                                    modified: new Date(),
                                                    kunci: "",
                                                    kota_id: req.body.kota_id,
                                                    propinsi_id: req.body.propinsi_id,
                                                    nama_stnk: req.body.nama_stnk,
                                                    alamat_stnk: req.body.alamat_stnk,
                                                    alamat_lengkap: req.body.alamat_lengkap,
                                                    npwp_stnk: req.body.npwp_stnk,
                                                    cust_alamat2: req.body.cust_alamat2,
                                                    cek_alamat_kirim: "",
                                                    pos_kota2: req.body.pos_kota2,
                                                    pos_prop2: req.body.pos_prop2,
                                                    propinsi_id2: req.body.propinsi_id2,
                                                    pos_lurah2: req.body.pos_lurah2,
                                                    pos_kode2: req.body.pos_kode2,
                                                    kota_id2: req.body.kota_id2,
                                                    pos_camat2: req.body.pos_camat2,

                                                    // lead_code: result[0].lead_code,
                                                    // lead_id: result[0].lead_id,
                                                    cust_rt: req.body.cust_rt,
                                                    cust_rw: req.body.cust_rw,

                                                    dealer_id: req.body.dealer_id,
                                                    dealer_code: req.body.dealer_code,

                                                    rt_stnk: req.body.rt_stnk,
                                                    rw_stnk: req.body.rw_stnk,

                                                    status_generate_spk: "0",

                                                    id_motor: req.body.id_motor,
                                                    id_event: req.body.id_event,
                                                    id_employee: req.body.id_employee,
                                                    ro_bd_id: "",
                                                    sektor_kode: "",
                                                    cust_wn: "",
                                                    cust_kitas: "",
                                                    cust_kk: "",
                                                    cust_wn_stnk: "",
                                                    nokitas_stnk: "",
                                                    kk_stnk: "",
                                                    type_motor: req.body.type_motor,
                                                    kode_motor: req.body.kode_motor,
                                                    deskripsi_motor: req.body.deskripsi_motor,

                                                    employee_kode: req.body.employee_kode,
                                                    employee_nama: req.body.employee_nama,
                                                    employee_jabatan: req.body.employee_jabatan,

                                                    kode_event: req.body.kode_event,
                                                    nama_event: req.body.nama_event,
                                                    lokasi_event: req.body.lokasi_event,

                                                    doc_1: "",
                                                    doc_2: "",
                                                    doc_3: "",
                                                    doc_4: "",
                                                    doc_5: "",

                                                }

                                                var data = {
                                                    url: url,
                                                    auth: {
                                                        'bearer': '71D55F9957529'
                                                    },
                                                    json: true,
                                                    body: requestData,
                                                    rejectUnauthorized: false, //add when working with https sites
                                                    requestCert: false, //add when working with https sites
                                                    agent: false, //add when working with https sites
                                                }

                                                console.log(data);
                                                request.post(data, function(error, httpResponse, body) {
                                                    if (body.status == "Failed") {
                                                        console.log(error);
                                                        res.send({ status: 400, message: 'Generate Customer Failed' });
                                                    } else {
                                                        console.log(body);
                                                        res.send({ status: 200, message: 'Generate Customer success' });
                                                    }
                                                });
                                            }
                                        }

                                    });
                                }
                            });
                    });
                }
            });
        });
    };

    // dari add ktp lama
    this.updateCustomerOldAddDMS = function(req, res) {

        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        var id = req.params.id;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var poscode_id = req.body.poscode_id;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_npwp = req.body.cust_npwp;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;

        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;

        var cust_sumber = req.body.cust_sumber;
        var salesman_id = req.body.salesman_id;
        var assigned_to = req.body.assigned_to;
        var cust_kendaraan = req.body.cust_kendaraan;
        var npwp_stnk = req.body.npwp_stnk;

        var modified = new Date();

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var cust_pic = req.body.cust_pic;
        var cust_type = req.body.cust_type;
        var cust_hp_wa = req.body.cust_hp_wa;

        var fileUploads = req.files;
        var docs = [];

        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT doc_1,doc_2,doc_3,doc_4,doc_5 FROM customers where id = "' + id + '"', function(err, data) {
                con.release();
                if (err) {
                    console.log("error");
                } else {

                    fileUploads.forEach(function(element, index) {
                        var image = data[0][element.fieldname];
                        if (image) {
                            fs.unlink(appRoot + image.replace("/files/", "/uploads/"), (err) => {
                                if (err) throw err;
                            });
                        }
                        docs[element.fieldname] = "/files/" + element.filename;
                    });
                    connection.acquire(function(err, conn) {
                        var queryStr = 'UPDATE customers SET id = ?, branch_id = ?,branch_code = ?,cust_jenis = ?,occupation_id = ?,kerja_kode = ?,cust_agama = ?,poscode_id = ?,kota_id = ?,propinsi_id = ?,cust_kelamin = ?,pos_kode = ?,pos_kota = ?,pos_prop = ?,pos_camat = ?,pos_lurah = ?,kota_id2 =?,propinsi_id2 = ?,pos_kode2 = ?,pos_kota2 =?,pos_prop2 = ?,pos_camat2 = ?,pos_lurah2 = ?,cust_nama = ?,cust_noktp = ?,cust_npwp = ?,cust_tgl_lahir = ?,cust_alamat = ?,cust_rt = ?,cust_rw =?,alamat_lengkap = ?,rt_stnk = ?,rw_stnk =?,nama_stnk = ?,alamat_stnk = ?,cust_fax = ?,cust_hp = ?,cust_telp_rumah = ?,cust_email = ?,cust_alamat2 = ?,noktp_stnk = ?,alamat_invoice = ?,alamat_kirim = ?,cust_telp_kantor = ?,cust_sumber = ?,salesman_id = ?,assigned_to = ?,cust_kendaraan = ?,npwp_stnk =?,modified = ?,id_motor = ?,type_motor = ?,kode_motor = ?,deskripsi_motor = ?,id_employee = ?,employee_kode = ?,employee_nama = ?,employee_jabatan = ?,id_event = ?,kode_event =?,nama_event =?,lokasi_event =?,cust_pic =?, cust_type=?,cust_hp_wa=?';

                        var queryParams = [id, branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, poscode_id, kota_id, propinsi_id, cust_kelamin, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_nama, cust_noktp, cust_npwp, cust_tgl_lahir, cust_alamat, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_hp, cust_telp_rumah, cust_email, cust_alamat2, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, cust_sumber, salesman_id, assigned_to, cust_kendaraan, npwp_stnk, modified, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, cust_pic, cust_type, cust_hp_wa];

                        for (var key in docs) {
                            var value = docs[key];
                            queryStr = queryStr + ", " + key + " = ?";
                            queryParams.push(value);
                        }

                        queryStr = queryStr + ' where id = "' + id + '"'

                        conn.query(queryStr, queryParams, function(err, result) {
                            conn.release();
                            if (err) {
                                res.send({ status: 400, message: 'Edit Customer Failed' });
                            } else {
                                var no_ktp = req.body.cust_noktp
                                request({
                                    uri: 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_customer_all.php?cust_noktp=' + no_ktp,
                                    auth: {
                                        'bearer': '71D55F9957529'
                                    },
                                    rejectUnauthorized: false, //add when working with https sites
                                    requestCert: false, //add when working with https sites
                                    agent: false, //add when working with https sites

                                }, function(error, rows) {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        const body = JSON.parse(rows.body); // parse string to JSON
                                        console.log(rows.body);

                                        if (body.status == "False") {
                                            connection.acquire(function(err, con) {
                                                con.query('select id, cust_code, cust_noktp from customers order by modified desc limit 1', function(err, result) {
                                                    con.release();
                                                    if (err) {
                                                        console.log(err);
                                                    } else {
                                                        var id_cust = result[0].id;
                                                        var code_cust = result[0].cust_code;

                                                        console.log(code_cust);
                                                        console.log(id_cust);
                                                        var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_insert_customer.php';

                                                        var requestData = {

                                                            id: id_cust,
                                                            cust_code: code_cust,
                                                            poscode_id: req.body.poscode_id,
                                                            pos_kode: req.body.pos_kode,
                                                            pos_kota: req.body.pos_kota,
                                                            pos_prop: req.body.pos_prop,
                                                            pos_camat: req.body.pos_camat,
                                                            pos_lurah: req.body.pos_lurah,
                                                            cust_nama: req.body.cust_nama,
                                                            cust_noktp: req.body.cust_noktp,
                                                            cust_alamat: req.body.cust_alamat,
                                                            cust_hp: req.body.cust_hp,
                                                            cust_telp_rumah: req.body.cust_telp_rumah,
                                                            noktp_stnk: req.body.noktp_stnk,
                                                            alamat_invoice: req.body.alamat_invoice,
                                                            alamat_kirim: req.body.alamat_kirim,
                                                            cust_telp_kantor: req.body.cust_telp_kantor,
                                                            salesman_id: req.body.salesman_id,
                                                            cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                            branch_id: req.body.branch_id,
                                                            branch_code: req.body.branch_code,
                                                            cust_jenis: req.body.cust_jenis,
                                                            occupation_id: req.body.occupation_id,
                                                            kerja_kode: req.body.kerja_kode,
                                                            cust_agama: req.body.cust_agama,
                                                            kota_id: req.body.kota_id,
                                                            propinsi_id: req.body.propinsi_id,
                                                            cust_kelamin: req.body.cust_kelamin,
                                                            kota_id2: req.body.kota_id2,
                                                            propinsi_id2: req.body.propinsi_id2,
                                                            pos_kode2: req.body.pos_kode2,
                                                            pos_kota2: req.body.pos_kota2,
                                                            pos_prop2: req.body.pos_prop2,
                                                            pos_camat2: req.body.pos_camat2,
                                                            pos_lurah2: req.body.pos_lurah2,
                                                            cust_npwp: req.body.cust_npwp,
                                                            cust_rt: req.body.cust_rt,
                                                            cust_rw: req.body.cust_rw,
                                                            alamat_lengkap: req.body.alamat_lengkap,
                                                            rt_stnk: req.body.rt_stnk,
                                                            rw_stnk: req.body.rw_stnk,
                                                            nama_stnk: req.body.nama_stnk,
                                                            alamat_stnk: req.body.alamat_stnk,
                                                            cust_fax: req.body.cust_fax,
                                                            cust_email: req.body.cust_email,
                                                            cust_alamat2: req.body.cust_alamat2,
                                                            cust_kendaraan: req.body.cust_kendaraan,
                                                            cust_type: req.body.cust_type,
                                                            created: new Date(),
                                                            modified: new Date(),
                                                            npwp_stnk: req.body.npwp_stnk,
                                                            lead_id: req.body.lead_id,
                                                            lead_code: req.body.lead_code,
                                                            dealer_id: req.body.dealer_id,
                                                            dealer_code: req.body.dealer_code,
                                                            cust_sumber: req.body.cust_sumber,
                                                            doc_1: "",
                                                            doc_2: "",
                                                            doc_3: "",
                                                            doc_4: "",
                                                            doc_5: "",

                                                            status_generate_spk: "0",

                                                            cust_pic: req.body.cust_pic,
                                                            cust_org: req.body.cust_org,
                                                            assigned_to: req.body.assigned_to,
                                                            cust_npk: req.body.cust_npk,

                                                            id_motor: req.body.id_motor,
                                                            type_motor: req.body.type_motor,
                                                            kode_motor: req.body.kode_motor,
                                                            deskripsi_motor: req.body.deskripsi_motor,

                                                            id_employee: req.body.id_employee,
                                                            employee_kode: req.body.employee_kode,
                                                            employee_nama: req.body.employee_nama,
                                                            employee_jabatan: req.body.employee_jabatan,

                                                            id_event: req.body.id_event,
                                                            kode_event: req.body.kode_event,
                                                            nama_event: req.body.nama_event,
                                                            lokasi_event: req.body.lokasi_event,
                                                            cust_hp_wa: req.body.cust_hp_wa,

                                                            ro_bd_id: "",
                                                            sektor_kode: "",
                                                            cust_wn: "",
                                                            cust_kitas: "",
                                                            cust_kk: "",
                                                            cust_wn_stnk: "",
                                                            nokitas_stnk: "",
                                                            kk_stnk: "",
                                                        }

                                                        var data = {
                                                            url: url,
                                                            auth: {
                                                                'bearer': '71D55F9957529'
                                                            },
                                                            body: requestData,
                                                            json: true,
                                                            rejectUnauthorized: false, //add when working with https sites
                                                            requestCert: false, //add when working with https sites
                                                            agent: false, //add when working with https sites
                                                        }

                                                        console.log(data);
                                                        request.post(data, function(error, httpResponse, body) {
                                                            if (body.status == "Failed") {
                                                                console.log(error);
                                                                res.send({ status: 400, message: 'Generate Customer Failed' });
                                                            } else {
                                                                console.log(body);
                                                                res.send({ status: 200, message: 'Generate Customer success' });
                                                            }

                                                        });

                                                    }

                                                });
                                            });
                                        } else if (body.status == "True") {
                                            var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_edit_customer2.php?cust_noktp=' + no_ktp;

                                            var requestData = {

                                                // cust_code: result[0].cust_code,
                                                title: "",
                                                cust_nama: req.body.cust_nama,
                                                cust_pic: req.body.cust_pic,
                                                cust_org: "",
                                                cust_alamat: req.body.cust_alamat,
                                                noktp_stnk: req.body.noktp_stnk,
                                                poscode_id: req.body.poscode_id,
                                                pos_kode: req.body.pos_kode,
                                                pos_camat: req.body.pos_camat,
                                                pos_lurah: req.body.pos_lurah,
                                                pos_kota: req.body.pos_kota,
                                                pos_prop: req.body.pos_prop,
                                                alamat_invoice: req.body.alamat_invoice,
                                                alamat_kirim: req.body.alamat_kirim,
                                                cust_telp_rumah: req.body.cust_telp_rumah,
                                                cust_telp_kantor: req.body.cust_telp_kantor,
                                                cust_fax: req.body.cust_fax,
                                                cust_hp: req.body.cust_hp,
                                                cust_email: req.body.cust_email,
                                                cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                cust_agama: req.body.cust_agama,
                                                occupation_id: req.body.occupation_id,
                                                kerja_kode: req.body.kerja_kode,
                                                cust_kelamin: req.body.cust_kelamin,
                                                cust_jenis: req.body.cust_jenis,
                                                cust_npwp: req.body.cust_npwp,

                                                salesman_id: req.body.salesman_id,
                                                assigned_to: req.body.assigned_to,
                                                cust_sumber: req.body.cust_sumber,
                                                cust_npk: req.body.cust_npk,
                                                cust_kendaraan: req.body.cust_kendaraan,
                                                branch_id: req.body.branch_id,
                                                branch_code: req.body.branch_code,
                                                company_id: req.body.company_id,
                                                company_code: req.body.company_code,
                                                cust_type: req.body.cust_type,
                                                modi_by: salesman_id,
                                                modified: new Date(),
                                                kunci: "",
                                                kota_id: req.body.kota_id,
                                                propinsi_id: req.body.propinsi_id,
                                                nama_stnk: req.body.nama_stnk,
                                                alamat_stnk: req.body.alamat_stnk,
                                                alamat_lengkap: req.body.alamat_lengkap,
                                                npwp_stnk: req.body.npwp_stnk,
                                                cust_alamat2: req.body.cust_alamat2,
                                                cek_alamat_kirim: "",
                                                pos_kota2: req.body.pos_kota2,
                                                pos_prop2: req.body.pos_prop2,
                                                propinsi_id2: req.body.propinsi_id2,
                                                pos_lurah2: req.body.pos_lurah2,
                                                pos_kode2: req.body.pos_kode2,
                                                kota_id2: req.body.kota_id2,
                                                pos_camat2: req.body.pos_camat2,

                                                // lead_code: result[0].lead_code,
                                                // lead_id: result[0].lead_id,
                                                cust_rt: req.body.cust_rt,
                                                cust_rw: req.body.cust_rw,

                                                dealer_id: req.body.dealer_id,
                                                dealer_code: req.body.dealer_code,

                                                rt_stnk: req.body.rt_stnk,
                                                rw_stnk: req.body.rw_stnk,

                                                status_generate_spk: "0",

                                                id_motor: req.body.id_motor,
                                                id_event: req.body.id_event,
                                                id_employee: req.body.id_employee,
                                                ro_bd_id: "",
                                                sektor_kode: "",
                                                cust_wn: "",
                                                cust_kitas: "",
                                                cust_kk: "",
                                                cust_wn_stnk: "",
                                                nokitas_stnk: "",
                                                kk_stnk: "",
                                                type_motor: req.body.type_motor,
                                                kode_motor: req.body.kode_motor,
                                                deskripsi_motor: req.body.deskripsi_motor,

                                                employee_kode: req.body.employee_kode,
                                                employee_nama: req.body.employee_nama,
                                                employee_jabatan: req.body.employee_jabatan,

                                                kode_event: req.body.kode_event,
                                                nama_event: req.body.nama_event,
                                                lokasi_event: req.body.lokasi_event,

                                                doc_1: "",
                                                doc_2: "",
                                                doc_3: "",
                                                doc_4: "",
                                                doc_5: "",

                                            }

                                            var data = {
                                                url: url,
                                                auth: {
                                                    'bearer': '71D55F9957529'
                                                },
                                                json: true,
                                                body: requestData,
                                                rejectUnauthorized: false, //add when working with https sites
                                                requestCert: false, //add when working with https sites
                                                agent: false, //add when working with https sites
                                            }

                                            console.log(data);
                                            request.post(data, function(error, httpResponse, body) {
                                                if (body.status == "Failed") {
                                                    console.log(error);
                                                    res.send({ status: 400, message: 'Generate Customer Failed' });
                                                } else {
                                                    console.log(body);
                                                    res.send({ status: 200, message: 'Generate Customer success' });
                                                }
                                            });
                                        }
                                    }

                                });
                            }
                        });
                    });
                }
            });
        });

    };

    // button add ktp baru
    this.addCustomerTestDMS = function(req, res) {

        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        var fileUploads = req.files;
        var doc_1 = null;
        var doc_2 = null;
        var doc_3 = null;
        var doc_4 = null;
        var doc_5 = null;

        fileUploads.forEach(function(element, index) {
            if (index == 0) {
                doc_1 = "/files/" + element.filename
            } else if (index == 1) {
                doc_2 = "/files/" + element.filename
            } else if (index == 2) {
                doc_3 = "/files/" + element.filename
            } else if (index == 3) {
                doc_4 = "/files/" + element.filename
            } else if (index == 4) {
                doc_5 = "/files/" + element.filename
            }
        });

        var id = uuidv1();
        var cust_code = null;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var poscode_id = req.body.poscode_id;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_npwp = req.body.cust_npwp;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;

        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;

        var cust_sumber = req.body.cust_sumber;
        var salesman_id = req.body.salesman_id;
        var assigned_to = req.body.assigned_to;
        var cust_kendaraan = req.body.cust_kendaraan;
        var npwp_stnk = req.body.npwp_stnk;
        var created = new Date();
        var modified = new Date();

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var cust_pic = req.body.cust_pic;
        var cust_type = req.body.cust_type;
        var company_id = req.body.company_id;
        var company_code = req.body.company_code;
        var status_generate_spk = "0";
        var cust_hp_wa = req.body.cust_hp_wa;
        // var doc_1 = req.body.doc_1;
        // var doc_2 = req.body.doc_2;
        // var doc_3 = req.body.doc_3;
        // var doc_4 = req.body.doc_4;
        // var doc_5 = req.body.doc_5;

        connection.acquire(function(err, con) {
            con.query('select cust_code from customers order by created desc limit 1', function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Customer Failed' });
                } else {
                    // for CODE CP
                    if (result.length != 0) {
                        var test = result[0].cust_code;
                    } else {
                        var test = "C000000000000"
                    }

                    var pieces = test.split('C');
                    var prefix = pieces[0];
                    var lastNum = pieces[1];
                    lastNum = parseInt(lastNum, 10);
                    lastNum++;
                    cust_code = "C" + ("000000000000" + lastNum).substr(-6);


                    connection.acquire(function(err, con) {
                        con.query('INSERT INTO customers (id,cust_code,branch_id,branch_code,cust_jenis,occupation_id,kerja_kode,cust_agama,poscode_id,kota_id,propinsi_id,cust_kelamin,pos_kode,pos_kota,pos_prop,pos_camat,pos_lurah,kota_id2,propinsi_id2,pos_kode2,pos_kota2,pos_prop2,pos_camat2,pos_lurah2,cust_nama,cust_noktp,cust_npwp,cust_tgl_lahir,cust_alamat,cust_rt,cust_rw,alamat_lengkap,rt_stnk,rw_stnk,nama_stnk,alamat_stnk,cust_fax,cust_hp,cust_telp_rumah,cust_email,cust_alamat2,noktp_stnk,alamat_invoice,alamat_kirim,cust_telp_kantor,cust_sumber,salesman_id,assigned_to,cust_kendaraan,npwp_stnk,created,modified,id_motor,type_motor,kode_motor,deskripsi_motor,id_employee,employee_kode,employee_nama,employee_jabatan,id_event,kode_event,nama_event,lokasi_event,cust_pic,cust_type,company_id,company_code,status_generate_spk,doc_1,doc_2,doc_3,doc_4,doc_5,cust_hp_wa) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, cust_code, branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, poscode_id, kota_id, propinsi_id, cust_kelamin, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_nama, cust_noktp, cust_npwp, cust_tgl_lahir, cust_alamat, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_hp, cust_telp_rumah, cust_email, cust_alamat2, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, cust_sumber, salesman_id, assigned_to, cust_kendaraan, npwp_stnk, created, modified, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, cust_pic, cust_type, company_id, company_code, status_generate_spk, doc_1, doc_2, doc_3, doc_4, doc_5, cust_hp_wa],
                            function(err, result) {
                                con.release();
                                if (err) {
                                    res.send({ status: 400, message: 'creation failed' });
                                } else {
                                    var no_ktp = req.body.cust_noktp
                                    request({
                                        uri: 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_customer_all.php?cust_noktp=' + no_ktp,
                                        auth: {
                                            'bearer': '71D55F9957529'
                                        },
                                        rejectUnauthorized: false, //add when working with https sites
                                        requestCert: false, //add when working with https sites
                                        agent: false, //add when working with https sites

                                    }, function(error, rows) {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            const body = JSON.parse(rows.body); // parse string to JSON
                                            console.log(rows.body);

                                            if (body.status == "False") {
                                                connection.acquire(function(err, con) {
                                                    con.query('select id, cust_code, cust_noktp from customers order by modified desc limit 1', function(err, result) {
                                                        con.release();
                                                        if (err) {
                                                            console.log(err);
                                                        } else {
                                                            var id_cust = result[0].id;
                                                            var code_cust = result[0].cust_code;

                                                            console.log(code_cust);
                                                            console.log(id_cust);
                                                            var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_insert_customer.php';

                                                            var requestData = {

                                                                id: id_cust,
                                                                cust_code: code_cust,
                                                                poscode_id: req.body.poscode_id,
                                                                pos_kode: req.body.pos_kode,
                                                                pos_kota: req.body.pos_kota,
                                                                pos_prop: req.body.pos_prop,
                                                                pos_camat: req.body.pos_camat,
                                                                pos_lurah: req.body.pos_lurah,
                                                                cust_nama: req.body.cust_nama,
                                                                cust_noktp: req.body.cust_noktp,
                                                                cust_alamat: req.body.cust_alamat,
                                                                cust_hp: req.body.cust_hp,
                                                                cust_telp_rumah: req.body.cust_telp_rumah,
                                                                noktp_stnk: req.body.noktp_stnk,
                                                                alamat_invoice: req.body.alamat_invoice,
                                                                alamat_kirim: req.body.alamat_kirim,
                                                                cust_telp_kantor: req.body.cust_telp_kantor,
                                                                salesman_id: req.body.salesman_id,
                                                                cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                                branch_id: req.body.branch_id,
                                                                branch_code: req.body.branch_code,
                                                                cust_jenis: req.body.cust_jenis,
                                                                occupation_id: req.body.occupation_id,
                                                                kerja_kode: req.body.kerja_kode,
                                                                cust_agama: req.body.cust_agama,
                                                                kota_id: req.body.kota_id,
                                                                propinsi_id: req.body.propinsi_id,
                                                                cust_kelamin: req.body.cust_kelamin,
                                                                kota_id2: req.body.kota_id2,
                                                                propinsi_id2: req.body.propinsi_id2,
                                                                pos_kode2: req.body.pos_kode2,
                                                                pos_kota2: req.body.pos_kota2,
                                                                pos_prop2: req.body.pos_prop2,
                                                                pos_camat2: req.body.pos_camat2,
                                                                pos_lurah2: req.body.pos_lurah2,
                                                                cust_npwp: req.body.cust_npwp,
                                                                cust_rt: req.body.cust_rt,
                                                                cust_rw: req.body.cust_rw,
                                                                alamat_lengkap: req.body.alamat_lengkap,
                                                                rt_stnk: req.body.rt_stnk,
                                                                rw_stnk: req.body.rw_stnk,
                                                                nama_stnk: req.body.nama_stnk,
                                                                alamat_stnk: req.body.alamat_stnk,
                                                                cust_fax: req.body.cust_fax,
                                                                cust_email: req.body.cust_email,
                                                                cust_alamat2: req.body.cust_alamat2,
                                                                cust_kendaraan: req.body.cust_kendaraan,
                                                                cust_type: req.body.cust_type,
                                                                created: new Date(),
                                                                modified: new Date(),
                                                                npwp_stnk: req.body.npwp_stnk,
                                                                lead_id: req.body.lead_id,
                                                                lead_code: req.body.lead_code,
                                                                dealer_id: req.body.dealer_id,
                                                                dealer_code: req.body.dealer_code,
                                                                cust_sumber: req.body.cust_sumber,
                                                                doc_1: "",
                                                                doc_2: "",
                                                                doc_3: "",
                                                                doc_4: "",
                                                                doc_5: "",

                                                                status_generate_spk: "0",

                                                                cust_pic: req.body.cust_pic,
                                                                cust_org: req.body.cust_org,
                                                                assigned_to: req.body.assigned_to,
                                                                cust_npk: req.body.cust_npk,

                                                                id_motor: req.body.id_motor,
                                                                type_motor: req.body.type_motor,
                                                                kode_motor: req.body.kode_motor,
                                                                deskripsi_motor: req.body.deskripsi_motor,

                                                                id_employee: req.body.id_employee,
                                                                employee_kode: req.body.employee_kode,
                                                                employee_nama: req.body.employee_nama,
                                                                employee_jabatan: req.body.employee_jabatan,

                                                                id_event: req.body.id_event,
                                                                kode_event: req.body.kode_event,
                                                                nama_event: req.body.nama_event,
                                                                lokasi_event: req.body.lokasi_event,
                                                                cust_hp_wa: req.body.cust_hp_wa,

                                                                ro_bd_id: "",
                                                                sektor_kode: "",
                                                                cust_wn: "",
                                                                cust_kitas: "",
                                                                cust_kk: "",
                                                                cust_wn_stnk: "",
                                                                nokitas_stnk: "",
                                                                kk_stnk: "",
                                                            }

                                                            var data = {
                                                                url: url,
                                                                auth: {
                                                                    'bearer': '71D55F9957529'
                                                                },
                                                                body: requestData,
                                                                json: true,
                                                                rejectUnauthorized: false, //add when working with https sites
                                                                requestCert: false, //add when working with https sites
                                                                agent: false, //add when working with https sites
                                                            }

                                                            console.log(data);
                                                            request.post(data, function(error, httpResponse, body) {
                                                                if (body.status == "Failed") {
                                                                    console.log(error);
                                                                    res.send({ status: 400, message: 'Generate Customer Failed' });
                                                                } else {
                                                                    console.log(body);
                                                                    res.send({ status: 200, message: 'Generate Customer success' });
                                                                }

                                                            });

                                                        }

                                                    });
                                                });
                                            } else if (body.status == "True") {
                                                var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_edit_customer2.php?cust_noktp=' + no_ktp;

                                                var requestData = {

                                                    // cust_code: result[0].cust_code,
                                                    title: "",
                                                    cust_nama: req.body.cust_nama,
                                                    cust_pic: req.body.cust_pic,
                                                    cust_org: "",
                                                    cust_alamat: req.body.cust_alamat,
                                                    noktp_stnk: req.body.noktp_stnk,
                                                    poscode_id: req.body.poscode_id,
                                                    pos_kode: req.body.pos_kode,
                                                    pos_camat: req.body.pos_camat,
                                                    pos_lurah: req.body.pos_lurah,
                                                    pos_kota: req.body.pos_kota,
                                                    pos_prop: req.body.pos_prop,
                                                    alamat_invoice: req.body.alamat_invoice,
                                                    alamat_kirim: req.body.alamat_kirim,
                                                    cust_telp_rumah: req.body.cust_telp_rumah,
                                                    cust_telp_kantor: req.body.cust_telp_kantor,
                                                    cust_fax: req.body.cust_fax,
                                                    cust_hp: req.body.cust_hp,
                                                    cust_email: req.body.cust_email,
                                                    cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                    cust_agama: req.body.cust_agama,
                                                    occupation_id: req.body.occupation_id,
                                                    kerja_kode: req.body.kerja_kode,
                                                    cust_kelamin: req.body.cust_kelamin,
                                                    cust_jenis: req.body.cust_jenis,
                                                    cust_npwp: req.body.cust_npwp,

                                                    salesman_id: req.body.salesman_id,
                                                    assigned_to: req.body.assigned_to,
                                                    cust_sumber: req.body.cust_sumber,
                                                    cust_npk: req.body.cust_npk,
                                                    cust_kendaraan: req.body.cust_kendaraan,
                                                    branch_id: req.body.branch_id,
                                                    branch_code: req.body.branch_code,
                                                    company_id: req.body.company_id,
                                                    company_code: req.body.company_code,
                                                    cust_type: req.body.cust_type,
                                                    modi_by: salesman_id,
                                                    modified: new Date(),
                                                    kunci: "",
                                                    kota_id: req.body.kota_id,
                                                    propinsi_id: req.body.propinsi_id,
                                                    nama_stnk: req.body.nama_stnk,
                                                    alamat_stnk: req.body.alamat_stnk,
                                                    alamat_lengkap: req.body.alamat_lengkap,
                                                    npwp_stnk: req.body.npwp_stnk,
                                                    cust_alamat2: req.body.cust_alamat2,
                                                    cek_alamat_kirim: "",
                                                    pos_kota2: req.body.pos_kota2,
                                                    pos_prop2: req.body.pos_prop2,
                                                    propinsi_id2: req.body.propinsi_id2,
                                                    pos_lurah2: req.body.pos_lurah2,
                                                    pos_kode2: req.body.pos_kode2,
                                                    kota_id2: req.body.kota_id2,
                                                    pos_camat2: req.body.pos_camat2,

                                                    // lead_code: result[0].lead_code,
                                                    // lead_id: result[0].lead_id,
                                                    cust_rt: req.body.cust_rt,
                                                    cust_rw: req.body.cust_rw,

                                                    dealer_id: req.body.dealer_id,
                                                    dealer_code: req.body.dealer_code,

                                                    rt_stnk: req.body.rt_stnk,
                                                    rw_stnk: req.body.rw_stnk,

                                                    status_generate_spk: "0",

                                                    id_motor: req.body.id_motor,
                                                    id_event: req.body.id_event,
                                                    id_employee: req.body.id_employee,
                                                    ro_bd_id: "",
                                                    sektor_kode: "",
                                                    cust_wn: "",
                                                    cust_kitas: "",
                                                    cust_kk: "",
                                                    cust_wn_stnk: "",
                                                    nokitas_stnk: "",
                                                    kk_stnk: "",
                                                    type_motor: req.body.type_motor,
                                                    kode_motor: req.body.kode_motor,
                                                    deskripsi_motor: req.body.deskripsi_motor,

                                                    employee_kode: req.body.employee_kode,
                                                    employee_nama: req.body.employee_nama,
                                                    employee_jabatan: req.body.employee_jabatan,

                                                    kode_event: req.body.kode_event,
                                                    nama_event: req.body.nama_event,
                                                    lokasi_event: req.body.lokasi_event,

                                                    doc_1: "",
                                                    doc_2: "",
                                                    doc_3: "",
                                                    doc_4: "",
                                                    doc_5: "",

                                                }

                                                var data = {
                                                    url: url,
                                                    auth: {
                                                        'bearer': '71D55F9957529'
                                                    },
                                                    json: true,
                                                    body: requestData,
                                                    rejectUnauthorized: false, //add when working with https sites
                                                    requestCert: false, //add when working with https sites
                                                    agent: false, //add when working with https sites
                                                }

                                                console.log(data);
                                                request.post(data, function(error, httpResponse, body) {
                                                    if (body.status == "Failed") {
                                                        console.log(error);
                                                        res.send({ status: 400, message: 'Generate Customer Failed' });
                                                    } else {
                                                        console.log(body);
                                                        res.send({ status: 200, message: 'Generate Customer success' });
                                                    }
                                                });
                                            }
                                        }

                                    });
                                }
                            });
                    });
                }
            });
        });
    };

    // detail customer
    this.updateCustomerTestDMS = function(req, res) {

        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        var id = req.params.id;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var poscode_id = req.body.poscode_id;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_npwp = req.body.cust_npwp;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;

        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;

        var cust_sumber = req.body.cust_sumber;
        var salesman_id = req.body.salesman_id;
        var assigned_to = req.body.assigned_to;
        var cust_kendaraan = req.body.cust_kendaraan;
        var npwp_stnk = req.body.npwp_stnk;

        var modified = new Date();

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var cust_pic = req.body.cust_pic;
        var cust_type = req.body.cust_type;
        var cust_hp_wa = req.body.cust_hp_wa;

        var fileUploads = req.files;
        var docs = [];

        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT doc_1,doc_2,doc_3,doc_4,doc_5 FROM customers where id = "' + id + '"', function(err, data) {
                con.release();
                if (err) {
                    console.log("error");
                } else {

                    fileUploads.forEach(function(element, index) {
                        var image = data[0][element.fieldname];
                        if (image) {
                            fs.unlink(appRoot + image.replace("/files/", "/uploads/"), (err) => {
                                if (err) throw err;
                            });
                        }
                        docs[element.fieldname] = "/files/" + element.filename;
                    });
                    connection.acquire(function(err, conn) {
                        var queryStr = 'UPDATE customers SET id = ?, branch_id = ?,branch_code = ?,cust_jenis = ?,occupation_id = ?,kerja_kode = ?,cust_agama = ?,poscode_id = ?,kota_id = ?,propinsi_id = ?,cust_kelamin = ?,pos_kode = ?,pos_kota = ?,pos_prop = ?,pos_camat = ?,pos_lurah = ?,kota_id2 =?,propinsi_id2 = ?,pos_kode2 = ?,pos_kota2 =?,pos_prop2 = ?,pos_camat2 = ?,pos_lurah2 = ?,cust_nama = ?,cust_noktp = ?,cust_npwp = ?,cust_tgl_lahir = ?,cust_alamat = ?,cust_rt = ?,cust_rw =?,alamat_lengkap = ?,rt_stnk = ?,rw_stnk =?,nama_stnk = ?,alamat_stnk = ?,cust_fax = ?,cust_hp = ?,cust_telp_rumah = ?,cust_email = ?,cust_alamat2 = ?,noktp_stnk = ?,alamat_invoice = ?,alamat_kirim = ?,cust_telp_kantor = ?,cust_sumber = ?,salesman_id = ?,assigned_to = ?,cust_kendaraan = ?,npwp_stnk =?,modified = ?,id_motor = ?,type_motor = ?,kode_motor = ?,deskripsi_motor = ?,id_employee = ?,employee_kode = ?,employee_nama = ?,employee_jabatan = ?,id_event = ?,kode_event =?,nama_event =?,lokasi_event =?,cust_pic =?, cust_type=?,cust_hp_wa=?';

                        var queryParams = [id, branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, poscode_id, kota_id, propinsi_id, cust_kelamin, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_nama, cust_noktp, cust_npwp, cust_tgl_lahir, cust_alamat, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_hp, cust_telp_rumah, cust_email, cust_alamat2, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, cust_sumber, salesman_id, assigned_to, cust_kendaraan, npwp_stnk, modified, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, cust_pic, cust_type, cust_hp_wa];

                        for (var key in docs) {
                            var value = docs[key];
                            queryStr = queryStr + ", " + key + " = ?";
                            queryParams.push(value);
                        }

                        queryStr = queryStr + ' where id = "' + id + '"'

                        conn.query(queryStr, queryParams, function(err, result) {
                            conn.release();
                            if (err) {
                                res.send({ status: 400, message: 'Edit Customer Failed' });
                            } else {
                                console.log(id);
                                var no_ktp = req.body.cust_noktp
                                connection.acquire(function(err, con) {
                                    con.query('select * from customers where cust_noktp="' + no_ktp + '" limit 1', function(err, result) {
                                        con.release();
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            console.log(result);
                                            var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_edit_customer2.php?cust_noktp=' + no_ktp;
                                            console.log(url);
                                            var requestData = {
                                                cust_code: result[0].cust_code,
                                                title: "",
                                                cust_nama: req.body.cust_nama,
                                                cust_pic: req.body.cust_pic,
                                                cust_org: "",
                                                cust_noktp: req.body.cust_noktp,
                                                cust_alamat: req.body.cust_alamat,
                                                noktp_stnk: req.body.noktp_stnk,
                                                poscode_id: req.body.poscode_id,
                                                pos_kode: req.body.pos_kode,
                                                pos_camat: req.body.pos_camat,
                                                pos_lurah: req.body.pos_lurah,
                                                pos_kota: req.body.pos_kota,
                                                pos_prop: req.body.pos_prop,
                                                alamat_invoice: req.body.alamat_invoice,
                                                alamat_kirim: req.body.alamat_kirim,
                                                cust_telp_rumah: req.body.cust_telp_rumah,
                                                cust_telp_kantor: req.body.cust_telp_kantor,
                                                cust_fax: req.body.cust_fax,
                                                cust_hp: req.body.cust_hp,
                                                cust_email: req.body.cust_email,
                                                cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                cust_agama: req.body.cust_agama,
                                                occupation_id: req.body.occupation_id,
                                                kerja_kode: req.body.kerja_kode,
                                                cust_kelamin: req.body.cust_kelamin,
                                                cust_jenis: req.body.cust_jenis,
                                                cust_npwp: req.body.cust_npwp,

                                                salesman_id: req.body.salesman_id,
                                                assigned_to: req.body.assigned_to,
                                                cust_sumber: req.body.cust_sumber,
                                                cust_npk: req.body.cust_npk,
                                                cust_kendaraan: req.body.cust_kendaraan,
                                                branch_id: req.body.branch_id,
                                                branch_code: req.body.branch_code,
                                                company_id: req.body.company_id,
                                                company_code: req.body.company_code,
                                                cust_type: req.body.cust_type,
                                                modi_by: salesman_id,
                                                modified: new Date(),
                                                kunci: "",
                                                kota_id: req.body.kota_id,
                                                propinsi_id: req.body.propinsi_id,
                                                nama_stnk: req.body.nama_stnk,
                                                alamat_stnk: req.body.alamat_stnk,
                                                alamat_lengkap: req.body.alamat_lengkap,
                                                npwp_stnk: req.body.npwp_stnk,
                                                cust_alamat2: req.body.cust_alamat2,
                                                cek_alamat_kirim: "",
                                                pos_kota2: req.body.pos_kota2,
                                                pos_prop2: req.body.pos_prop2,
                                                propinsi_id2: req.body.propinsi_id2,
                                                pos_lurah2: req.body.pos_lurah2,
                                                pos_kode2: req.body.pos_kode2,
                                                kota_id2: req.body.kota_id2,
                                                pos_camat2: req.body.pos_camat2,

                                                lead_code: result[0].lead_code,
                                                lead_id: result[0].lead_id,
                                                cust_rt: req.body.cust_rt,
                                                cust_rw: req.body.cust_rw,

                                                dealer_id: req.body.dealer_id,
                                                dealer_code: req.body.dealer_code,

                                                rt_stnk: req.body.rt_stnk,
                                                rw_stnk: req.body.rw_stnk,

                                                status_generate_spk: "0",

                                                id_motor: req.body.id_motor,
                                                id_event: req.body.id_event,
                                                id_employee: req.body.id_employee,
                                                ro_bd_id: "",
                                                sektor_kode: "",
                                                cust_wn: "",
                                                cust_kitas: "",
                                                cust_kk: "",
                                                cust_wn_stnk: "",
                                                nokitas_stnk: "",
                                                kk_stnk: "",
                                                type_motor: req.body.type_motor,
                                                kode_motor: req.body.kode_motor,
                                                deskripsi_motor: req.body.deskripsi_motor,

                                                employee_kode: req.body.employee_kode,
                                                employee_nama: req.body.employee_nama,
                                                employee_jabatan: req.body.employee_jabatan,

                                                kode_event: req.body.kode_event,
                                                nama_event: req.body.nama_event,
                                                lokasi_event: req.body.lokasi_event,

                                                doc_1: "",
                                                doc_2: "",
                                                doc_3: "",
                                                doc_4: "",
                                                doc_5: "",

                                            }

                                            var data = {
                                                url: url,
                                                auth: {
                                                    'bearer': '71D55F9957529'
                                                },
                                                json: true,
                                                body: requestData,
                                                rejectUnauthorized: false, //add when working with https sites
                                                requestCert: false, //add when working with https sites
                                                agent: false, //add when working with https sites
                                            }

                                            console.log(data);
                                            request.post(data, function(error, httpResponse, body) {
                                                if (body.status == "Failed") {
                                                    console.log(error);
                                                    res.send({ status: 400, message: 'Update Customer Failed' });
                                                } else {
                                                    console.log(body);
                                                    res.send({ status: 200, message: 'Update Customer success' });
                                                }

                                            });
                                        }

                                    });
                                });
                            }
                        });
                    });
                }
            });
        });

    };


    //TODMS NINA
    this.generateCustomerToDMS = function(req, res) {
        var id = uuidv1();
        var cust_code = null;
        var poscode_id = req.body.poscode_id;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_alamat = req.body.cust_alamat;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;
        var salesman_id = req.body.salesman_id;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_npwp = req.body.cust_npwp;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;
        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var cust_kendaraan = req.body.cust_kendaraan;
        var cust_type = req.body.cust_type;
        var created = new Date();
        var modified = new Date();
        var npwp_stnk = req.body.npwp_stnk;
        var lead_id = req.body.lead_id;
        var lead_code = req.body.lead_code;
        var dealer_id = req.body.dealer_id;
        var dealer_code = req.body.dealer_code;
        var cust_sumber = req.body.cust_sumber;
        var doc_1 = req.body.doc_1;
        var doc_2 = req.body.doc_2;
        var doc_3 = req.body.doc_3;
        var doc_4 = req.body.doc_4;
        var doc_5 = req.body.doc_5;

        var status_generate_spk = "0";

        var cust_pic = req.body.cust_pic;
        var cust_org = req.body.cust_org;
        var assigned_to = req.body.assigned_to;
        var cust_npk = req.body.cust_npk;

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var company_id = req.body.company_id;
        var company_code = req.body.company_code;

        var cust_hp_wa = req.body.cust_hp_wa;


        connection.acquire(function(err, con) {
            con.query('select cust_code from customers order by created desc limit 1', function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Customer Failed' });
                } else {
                    // for CODE CP
                    if (result.length != 0) {
                        var test = result[0].cust_code;
                    } else {
                        var test = "C000000000000"
                    }

                    var pieces = test.split('C');
                    var prefix = pieces[0];
                    var lastNum = pieces[1];
                    lastNum = parseInt(lastNum, 10);
                    lastNum++;
                    cust_code = "C" + ("000000000000" + lastNum).substr(-6);

                    console.log(cust_code);

                    connection.acquire(function(err, con) {
                        con.query('INSERT INTO customers (id,cust_code,poscode_id,pos_kode,pos_kota,pos_prop,pos_camat,pos_lurah,cust_nama,cust_noktp,cust_alamat,cust_hp,cust_telp_rumah,noktp_stnk,alamat_invoice,alamat_kirim,cust_telp_kantor,salesman_id,branch_id,branch_code,cust_jenis,occupation_id,kerja_kode,cust_agama,kota_id,propinsi_id,cust_kelamin, kota_id2,propinsi_id2,pos_kode2,pos_kota2,pos_prop2,pos_camat2,pos_lurah2,cust_npwp,cust_rt,cust_rw,alamat_lengkap,rt_stnk,rw_stnk,nama_stnk,alamat_stnk,cust_fax,cust_email,cust_alamat2,cust_kendaraan,cust_type,created,modified,npwp_stnk,lead_id,lead_code,dealer_id,dealer_code,cust_sumber,doc_1,doc_2,doc_3,doc_4,doc_5,status_generate_spk,cust_pic,cust_org,assigned_to,cust_npk,cust_tgl_lahir,id_motor,type_motor,kode_motor,deskripsi_motor,id_employee,employee_kode,employee_nama,employee_jabatan,id_event,kode_event,nama_event,lokasi_event,company_id,company_code,cust_hp_wa) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, cust_code, poscode_id, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, cust_nama, cust_noktp, cust_alamat, cust_hp, cust_telp_rumah, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, salesman_id, branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, kota_id, propinsi_id, cust_kelamin, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_npwp, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_email, cust_alamat2, cust_kendaraan, cust_type, created, modified, npwp_stnk, lead_id, lead_code, dealer_id, dealer_code, cust_sumber, doc_1, doc_2, doc_3, doc_4, doc_5, status_generate_spk, cust_pic, cust_org, assigned_to, cust_npk, cust_tgl_lahir, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, company_id, company_code, cust_hp_wa],
                            function(err, result) {
                                con.release();
                                if (err) {
                                    res.send({ status: 400, message: 'creation failed' });
                                } else {
                                    var no_ktp = req.body.cust_noktp
                                    request({
                                        uri: 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_customer_all.php?cust_noktp=' + no_ktp,
                                        auth: {
                                            'bearer': '71D55F9957529'
                                        },
                                        rejectUnauthorized: false, //add when working with https sites
                                        requestCert: false, //add when working with https sites
                                        agent: false, //add when working with https sites

                                    }, function(error, rows) {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            const body = JSON.parse(rows.body); // parse string to JSON
                                            console.log(rows.body);

                                            if (body.status == "False") {
                                                connection.acquire(function(err, con) {
                                                    con.query('select id, cust_code, cust_noktp from customers order by modified desc limit 1', function(err, result) {
                                                        con.release();
                                                        if (err) {
                                                            console.log(err);
                                                        } else {
                                                            var id_cust = result[0].id;
                                                            var code_cust = result[0].cust_code;

                                                            console.log(code_cust);
                                                            console.log(id_cust);
                                                            var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_insert_customer.php';

                                                            var requestData = {

                                                                id: id_cust,
                                                                cust_code: code_cust,
                                                                poscode_id: req.body.poscode_id,
                                                                pos_kode: req.body.pos_kode,
                                                                pos_kota: req.body.pos_kota,
                                                                pos_prop: req.body.pos_prop,
                                                                pos_camat: req.body.pos_camat,
                                                                pos_lurah: req.body.pos_lurah,
                                                                cust_nama: req.body.cust_nama,
                                                                cust_noktp: req.body.cust_noktp,
                                                                cust_alamat: req.body.cust_alamat,
                                                                cust_hp: req.body.cust_hp,
                                                                cust_telp_rumah: req.body.cust_telp_rumah,
                                                                noktp_stnk: req.body.noktp_stnk,
                                                                alamat_invoice: req.body.alamat_invoice,
                                                                alamat_kirim: req.body.alamat_kirim,
                                                                cust_telp_kantor: req.body.cust_telp_kantor,
                                                                salesman_id: req.body.salesman_id,
                                                                cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                                branch_id: req.body.branch_id,
                                                                branch_code: req.body.branch_code,
                                                                cust_jenis: req.body.cust_jenis,
                                                                occupation_id: req.body.occupation_id,
                                                                kerja_kode: req.body.kerja_kode,
                                                                cust_agama: req.body.cust_agama,
                                                                kota_id: req.body.kota_id,
                                                                propinsi_id: req.body.propinsi_id,
                                                                cust_kelamin: req.body.cust_kelamin,
                                                                kota_id2: req.body.kota_id2,
                                                                propinsi_id2: req.body.propinsi_id2,
                                                                pos_kode2: req.body.pos_kode2,
                                                                pos_kota2: req.body.pos_kota2,
                                                                pos_prop2: req.body.pos_prop2,
                                                                pos_camat2: req.body.pos_camat2,
                                                                pos_lurah2: req.body.pos_lurah2,
                                                                cust_npwp: req.body.cust_npwp,
                                                                cust_rt: req.body.cust_rt,
                                                                cust_rw: req.body.cust_rw,
                                                                alamat_lengkap: req.body.alamat_lengkap,
                                                                rt_stnk: req.body.rt_stnk,
                                                                rw_stnk: req.body.rw_stnk,
                                                                nama_stnk: req.body.nama_stnk,
                                                                alamat_stnk: req.body.alamat_stnk,
                                                                cust_fax: req.body.cust_fax,
                                                                cust_email: req.body.cust_email,
                                                                cust_alamat2: req.body.cust_alamat2,
                                                                cust_kendaraan: req.body.cust_kendaraan,
                                                                cust_type: req.body.cust_type,
                                                                created: new Date(),
                                                                modified: new Date(),
                                                                npwp_stnk: req.body.npwp_stnk,
                                                                lead_id: req.body.lead_id,
                                                                lead_code: req.body.lead_code,
                                                                dealer_id: req.body.dealer_id,
                                                                dealer_code: req.body.dealer_code,
                                                                cust_sumber: req.body.cust_sumber,
                                                                doc_1: "",
                                                                doc_2: "",
                                                                doc_3: "",
                                                                doc_4: "",
                                                                doc_5: "",

                                                                status_generate_spk: "0",

                                                                cust_pic: req.body.cust_pic,
                                                                cust_org: req.body.cust_org,
                                                                assigned_to: req.body.assigned_to,
                                                                cust_npk: req.body.cust_npk,

                                                                id_motor: req.body.id_motor,
                                                                type_motor: req.body.type_motor,
                                                                kode_motor: req.body.kode_motor,
                                                                deskripsi_motor: req.body.deskripsi_motor,

                                                                id_employee: req.body.id_employee,
                                                                employee_kode: req.body.employee_kode,
                                                                employee_nama: req.body.employee_nama,
                                                                employee_jabatan: req.body.employee_jabatan,

                                                                id_event: req.body.id_event,
                                                                kode_event: req.body.kode_event,
                                                                nama_event: req.body.nama_event,
                                                                lokasi_event: req.body.lokasi_event,
                                                                cust_hp_wa: req.body.cust_hp_wa,

                                                                ro_bd_id: "",
                                                                sektor_kode: "",
                                                                cust_wn: "",
                                                                cust_kitas: "",
                                                                cust_kk: "",
                                                                cust_wn_stnk: "",
                                                                nokitas_stnk: "",
                                                                kk_stnk: "",
                                                            }

                                                            var data = {
                                                                url: url,
                                                                auth: {
                                                                    'bearer': '71D55F9957529'
                                                                },
                                                                body: requestData,
                                                                json: true,
                                                                rejectUnauthorized: false, //add when working with https sites
                                                                requestCert: false, //add when working with https sites
                                                                agent: false, //add when working with https sites
                                                            }

                                                            console.log(data);
                                                            request.post(data, function(error, httpResponse, body) {
                                                                if (body.status == "Failed") {
                                                                    console.log(error);
                                                                    res.send({ status: 400, message: 'Generate Customer Failed' });
                                                                } else {
                                                                    console.log(body);
                                                                    res.send({ status: 200, message: 'Generate Customer success' });
                                                                }

                                                            });

                                                        }

                                                    });
                                                });
                                            } else if (body.status == "True") {
                                                var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_edit_customer2.php?cust_noktp=' + no_ktp;

                                                var requestData = {

                                                    // cust_code: result[0].cust_code,
                                                    title: "",
                                                    cust_nama: req.body.cust_nama,
                                                    cust_pic: req.body.cust_pic,
                                                    cust_org: "",
                                                    cust_alamat: req.body.cust_alamat,
                                                    noktp_stnk: req.body.noktp_stnk,
                                                    poscode_id: req.body.poscode_id,
                                                    pos_kode: req.body.pos_kode,
                                                    pos_camat: req.body.pos_camat,
                                                    pos_lurah: req.body.pos_lurah,
                                                    pos_kota: req.body.pos_kota,
                                                    pos_prop: req.body.pos_prop,
                                                    alamat_invoice: req.body.alamat_invoice,
                                                    alamat_kirim: req.body.alamat_kirim,
                                                    cust_telp_rumah: req.body.cust_telp_rumah,
                                                    cust_telp_kantor: req.body.cust_telp_kantor,
                                                    cust_fax: req.body.cust_fax,
                                                    cust_hp: req.body.cust_hp,
                                                    cust_email: req.body.cust_email,
                                                    cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                    cust_agama: req.body.cust_agama,
                                                    occupation_id: req.body.occupation_id,
                                                    kerja_kode: req.body.kerja_kode,
                                                    cust_kelamin: req.body.cust_kelamin,
                                                    cust_jenis: req.body.cust_jenis,
                                                    cust_npwp: req.body.cust_npwp,

                                                    salesman_id: req.body.salesman_id,
                                                    assigned_to: req.body.assigned_to,
                                                    cust_sumber: req.body.cust_sumber,
                                                    cust_npk: req.body.cust_npk,
                                                    cust_kendaraan: req.body.cust_kendaraan,
                                                    branch_id: req.body.branch_id,
                                                    branch_code: req.body.branch_code,
                                                    company_id: req.body.company_id,
                                                    company_code: req.body.company_code,
                                                    cust_type: req.body.cust_type,
                                                    modi_by: salesman_id,
                                                    modified: new Date(),
                                                    kunci: "",
                                                    kota_id: req.body.kota_id,
                                                    propinsi_id: req.body.propinsi_id,
                                                    nama_stnk: req.body.nama_stnk,
                                                    alamat_stnk: req.body.alamat_stnk,
                                                    alamat_lengkap: req.body.alamat_lengkap,
                                                    npwp_stnk: req.body.npwp_stnk,
                                                    cust_alamat2: req.body.cust_alamat2,
                                                    cek_alamat_kirim: "",
                                                    pos_kota2: req.body.pos_kota2,
                                                    pos_prop2: req.body.pos_prop2,
                                                    propinsi_id2: req.body.propinsi_id2,
                                                    pos_lurah2: req.body.pos_lurah2,
                                                    pos_kode2: req.body.pos_kode2,
                                                    kota_id2: req.body.kota_id2,
                                                    pos_camat2: req.body.pos_camat2,

                                                    // lead_code: result[0].lead_code,
                                                    // lead_id: result[0].lead_id,
                                                    cust_rt: req.body.cust_rt,
                                                    cust_rw: req.body.cust_rw,

                                                    dealer_id: req.body.dealer_id,
                                                    dealer_code: req.body.dealer_code,

                                                    rt_stnk: req.body.rt_stnk,
                                                    rw_stnk: req.body.rw_stnk,

                                                    status_generate_spk: "0",

                                                    id_motor: req.body.id_motor,
                                                    id_event: req.body.id_event,
                                                    id_employee: req.body.id_employee,
                                                    ro_bd_id: "",
                                                    sektor_kode: "",
                                                    cust_wn: "",
                                                    cust_kitas: "",
                                                    cust_kk: "",
                                                    cust_wn_stnk: "",
                                                    nokitas_stnk: "",
                                                    kk_stnk: "",
                                                    type_motor: req.body.type_motor,
                                                    kode_motor: req.body.kode_motor,
                                                    deskripsi_motor: req.body.deskripsi_motor,

                                                    employee_kode: req.body.employee_kode,
                                                    employee_nama: req.body.employee_nama,
                                                    employee_jabatan: req.body.employee_jabatan,

                                                    kode_event: req.body.kode_event,
                                                    nama_event: req.body.nama_event,
                                                    lokasi_event: req.body.lokasi_event,

                                                    doc_1: "",
                                                    doc_2: "",
                                                    doc_3: "",
                                                    doc_4: "",
                                                    doc_5: "",

                                                }

                                                var data = {
                                                    url: url,
                                                    auth: {
                                                        'bearer': '71D55F9957529'
                                                    },
                                                    json: true,
                                                    body: requestData,
                                                    rejectUnauthorized: false, //add when working with https sites
                                                    requestCert: false, //add when working with https sites
                                                    agent: false, //add when working with https sites
                                                }

                                                console.log(data);
                                                request.post(data, function(error, httpResponse, body) {
                                                    if (body.status == "Failed") {
                                                        console.log(error);
                                                        res.send({ status: 400, message: 'Generate Customer Failed' });
                                                    } else {
                                                        console.log(body);
                                                        res.send({ status: 200, message: 'Generate Customer success' });
                                                    }
                                                });
                                            }
                                        }

                                    });
                                }
                            });
                    });
                }
            });
        });
    };

    this.updateCustomerToDMS = function(req, res) {

        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        var id = req.params.id;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var poscode_id = req.body.poscode_id;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_npwp = req.body.cust_npwp;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;

        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;

        var cust_sumber = req.body.cust_sumber;
        var salesman_id = req.body.salesman_id;
        var assigned_to = req.body.assigned_to;
        var cust_kendaraan = req.body.cust_kendaraan;
        var npwp_stnk = req.body.npwp_stnk;

        var modified = new Date();

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var cust_pic = req.body.cust_pic;
        var cust_type = req.body.cust_type;
        var cust_hp_wa = req.body.cust_hp_wa;

        var fileUploads = req.files;
        var docs = [];

        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT doc_1,doc_2,doc_3,doc_4,doc_5 FROM customers where id = "' + id + '"', function(err, data) {
                con.release();
                if (err) {
                    console.log("error");
                } else {

                    fileUploads.forEach(function(element, index) {
                        var image = data[0][element.fieldname];
                        if (image) {
                            fs.unlink(appRoot + image.replace("/files/", "/uploads/"), (err) => {
                                if (err) throw err;
                            });
                        }
                        docs[element.fieldname] = "/files/" + element.filename;
                    });
                    connection.acquire(function(err, conn) {
                        var queryStr = 'UPDATE customers SET id = ?, branch_id = ?,branch_code = ?,cust_jenis = ?,occupation_id = ?,kerja_kode = ?,cust_agama = ?,poscode_id = ?,kota_id = ?,propinsi_id = ?,cust_kelamin = ?,pos_kode = ?,pos_kota = ?,pos_prop = ?,pos_camat = ?,pos_lurah = ?,kota_id2 =?,propinsi_id2 = ?,pos_kode2 = ?,pos_kota2 =?,pos_prop2 = ?,pos_camat2 = ?,pos_lurah2 = ?,cust_nama = ?,cust_noktp = ?,cust_npwp = ?,cust_tgl_lahir = ?,cust_alamat = ?,cust_rt = ?,cust_rw =?,alamat_lengkap = ?,rt_stnk = ?,rw_stnk =?,nama_stnk = ?,alamat_stnk = ?,cust_fax = ?,cust_hp = ?,cust_telp_rumah = ?,cust_email = ?,cust_alamat2 = ?,noktp_stnk = ?,alamat_invoice = ?,alamat_kirim = ?,cust_telp_kantor = ?,cust_sumber = ?,salesman_id = ?,assigned_to = ?,cust_kendaraan = ?,npwp_stnk =?,modified = ?,id_motor = ?,type_motor = ?,kode_motor = ?,deskripsi_motor = ?,id_employee = ?,employee_kode = ?,employee_nama = ?,employee_jabatan = ?,id_event = ?,kode_event =?,nama_event =?,lokasi_event =?,cust_pic =?, cust_type=?,cust_hp_wa=?';

                        var queryParams = [id, branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, poscode_id, kota_id, propinsi_id, cust_kelamin, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_nama, cust_noktp, cust_npwp, cust_tgl_lahir, cust_alamat, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_hp, cust_telp_rumah, cust_email, cust_alamat2, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, cust_sumber, salesman_id, assigned_to, cust_kendaraan, npwp_stnk, modified, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, cust_pic, cust_type, cust_hp_wa];

                        for (var key in docs) {
                            var value = docs[key];
                            queryStr = queryStr + ", " + key + " = ?";
                            queryParams.push(value);
                        }

                        queryStr = queryStr + ' where id = "' + id + '"'

                        conn.query(queryStr, queryParams, function(err, result) {
                            conn.release();
                            if (err) {
                                res.send({ status: 400, message: 'Edit Customer Failed' });
                            } else {
                                console.log(id);
                                var no_ktp = req.body.cust_noktp
                                connection.acquire(function(err, con) {
                                    con.query('select * from customers where cust_noktp="' + no_ktp + '" limit 1', function(err, result) {
                                        con.release();
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            console.log(result);
                                            var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_edit_customer2.php?cust_noktp=' + no_ktp;
                                            console.log(url);
                                            var requestData = {
                                                cust_code: result[0].cust_code,
                                                title: "",
                                                cust_nama: req.body.cust_nama,
                                                cust_pic: req.body.cust_pic,
                                                cust_org: "",
                                                cust_noktp: req.body.cust_noktp,
                                                cust_alamat: req.body.cust_alamat,
                                                noktp_stnk: req.body.noktp_stnk,
                                                poscode_id: req.body.poscode_id,
                                                pos_kode: req.body.pos_kode,
                                                pos_camat: req.body.pos_camat,
                                                pos_lurah: req.body.pos_lurah,
                                                pos_kota: req.body.pos_kota,
                                                pos_prop: req.body.pos_prop,
                                                alamat_invoice: req.body.alamat_invoice,
                                                alamat_kirim: req.body.alamat_kirim,
                                                cust_telp_rumah: req.body.cust_telp_rumah,
                                                cust_telp_kantor: req.body.cust_telp_kantor,
                                                cust_fax: req.body.cust_fax,
                                                cust_hp: req.body.cust_hp,
                                                cust_email: req.body.cust_email,
                                                cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                cust_agama: req.body.cust_agama,
                                                occupation_id: req.body.occupation_id,
                                                kerja_kode: req.body.kerja_kode,
                                                cust_kelamin: req.body.cust_kelamin,
                                                cust_jenis: req.body.cust_jenis,
                                                cust_npwp: req.body.cust_npwp,

                                                salesman_id: req.body.salesman_id,
                                                assigned_to: req.body.assigned_to,
                                                cust_sumber: req.body.cust_sumber,
                                                cust_npk: req.body.cust_npk,
                                                cust_kendaraan: req.body.cust_kendaraan,
                                                branch_id: req.body.branch_id,
                                                branch_code: req.body.branch_code,
                                                company_id: req.body.company_id,
                                                company_code: req.body.company_code,
                                                cust_type: req.body.cust_type,
                                                modi_by: salesman_id,
                                                modified: new Date(),
                                                kunci: "",
                                                kota_id: req.body.kota_id,
                                                propinsi_id: req.body.propinsi_id,
                                                nama_stnk: req.body.nama_stnk,
                                                alamat_stnk: req.body.alamat_stnk,
                                                alamat_lengkap: req.body.alamat_lengkap,
                                                npwp_stnk: req.body.npwp_stnk,
                                                cust_alamat2: req.body.cust_alamat2,
                                                cek_alamat_kirim: "",
                                                pos_kota2: req.body.pos_kota2,
                                                pos_prop2: req.body.pos_prop2,
                                                propinsi_id2: req.body.propinsi_id2,
                                                pos_lurah2: req.body.pos_lurah2,
                                                pos_kode2: req.body.pos_kode2,
                                                kota_id2: req.body.kota_id2,
                                                pos_camat2: req.body.pos_camat2,

                                                lead_code: result[0].lead_code,
                                                lead_id: result[0].lead_id,
                                                cust_rt: req.body.cust_rt,
                                                cust_rw: req.body.cust_rw,

                                                dealer_id: req.body.dealer_id,
                                                dealer_code: req.body.dealer_code,

                                                rt_stnk: req.body.rt_stnk,
                                                rw_stnk: req.body.rw_stnk,

                                                status_generate_spk: "0",

                                                id_motor: req.body.id_motor,
                                                id_event: req.body.id_event,
                                                id_employee: req.body.id_employee,
                                                ro_bd_id: "",
                                                sektor_kode: "",
                                                cust_wn: "",
                                                cust_kitas: "",
                                                cust_kk: "",
                                                cust_wn_stnk: "",
                                                nokitas_stnk: "",
                                                kk_stnk: "",
                                                type_motor: req.body.type_motor,
                                                kode_motor: req.body.kode_motor,
                                                deskripsi_motor: req.body.deskripsi_motor,

                                                employee_kode: req.body.employee_kode,
                                                employee_nama: req.body.employee_nama,
                                                employee_jabatan: req.body.employee_jabatan,

                                                kode_event: req.body.kode_event,
                                                nama_event: req.body.nama_event,
                                                lokasi_event: req.body.lokasi_event,

                                                doc_1: "",
                                                doc_2: "",
                                                doc_3: "",
                                                doc_4: "",
                                                doc_5: "",

                                            }

                                            var data = {
                                                url: url,
                                                auth: {
                                                    'bearer': '71D55F9957529'
                                                },
                                                json: true,
                                                body: requestData,
                                                rejectUnauthorized: false, //add when working with https sites
                                                requestCert: false, //add when working with https sites
                                                agent: false, //add when working with https sites
                                            }

                                            console.log(data);
                                            request.post(data, function(error, httpResponse, body) {
                                                if (body.status == "Failed") {
                                                    console.log(error);
                                                    res.send({ status: 400, message: 'Update Customer Failed' });
                                                } else {
                                                    console.log(body);
                                                    res.send({ status: 200, message: 'Update Customer success' });
                                                }

                                            });
                                        }

                                    });
                                });
                            }
                        });
                    });
                }
            });
        });

    };

    this.addCustomerToDMS = function(req, res) {

        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        var fileUploads = req.files;
        var doc_1 = null;
        var doc_2 = null;
        var doc_3 = null;
        var doc_4 = null;
        var doc_5 = null;

        fileUploads.forEach(function(element, index) {
            if (index == 0) {
                doc_1 = "/files/" + element.filename
            } else if (index == 1) {
                doc_2 = "/files/" + element.filename
            } else if (index == 2) {
                doc_3 = "/files/" + element.filename
            } else if (index == 3) {
                doc_4 = "/files/" + element.filename
            } else if (index == 4) {
                doc_5 = "/files/" + element.filename
            }
        });

        var id = uuidv1();
        var cust_code = null;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var cust_jenis = req.body.cust_jenis;
        var occupation_id = req.body.occupation_id;
        var kerja_kode = req.body.kerja_kode;
        var cust_agama = req.body.cust_agama;
        var poscode_id = req.body.poscode_id;
        var kota_id = req.body.kota_id;
        var propinsi_id = req.body.propinsi_id;
        var cust_kelamin = req.body.cust_kelamin;
        var pos_kode = req.body.pos_kode;
        var pos_kota = req.body.pos_kota;
        var pos_prop = req.body.pos_prop;
        var pos_camat = req.body.pos_camat;
        var pos_lurah = req.body.pos_lurah;
        var kota_id2 = req.body.kota_id2;
        var propinsi_id2 = req.body.propinsi_id2;
        var pos_kode2 = req.body.pos_kode2;
        var pos_kota2 = req.body.pos_kota2;
        var pos_prop2 = req.body.pos_prop2;
        var pos_camat2 = req.body.pos_camat2;
        var pos_lurah2 = req.body.pos_lurah2;
        var cust_nama = req.body.cust_nama;
        var cust_noktp = req.body.cust_noktp;
        var cust_npwp = req.body.cust_npwp;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;

        var alamat_lengkap = req.body.alamat_lengkap;
        var rt_stnk = req.body.rt_stnk;
        var rw_stnk = req.body.rw_stnk;
        var nama_stnk = req.body.nama_stnk;
        var alamat_stnk = req.body.alamat_stnk;
        var cust_fax = req.body.cust_fax;
        var cust_hp = req.body.cust_hp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_email = req.body.cust_email;
        var cust_alamat2 = req.body.cust_alamat2;
        var noktp_stnk = req.body.noktp_stnk;
        var alamat_invoice = req.body.alamat_invoice;
        var alamat_kirim = req.body.alamat_kirim;
        var cust_telp_kantor = req.body.cust_telp_kantor;

        var cust_sumber = req.body.cust_sumber;
        var salesman_id = req.body.salesman_id;
        var assigned_to = req.body.assigned_to;
        var cust_kendaraan = req.body.cust_kendaraan;
        var npwp_stnk = req.body.npwp_stnk;
        var created = new Date();
        var modified = new Date();

        var id_motor = req.body.id_motor;
        var type_motor = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event;
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var cust_pic = req.body.cust_pic;
        var status_generate_spk = "0";
        // var doc_1 = req.body.doc_1;
        // var doc_2 = req.body.doc_2;
        // var doc_3 = req.body.doc_3;
        // var doc_4 = req.body.doc_4;
        // var doc_5 = req.body.doc_5;

        connection.acquire(function(err, con) {
            con.query('select cust_code from customers order by created desc limit 1', function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Customer Failed' });
                } else {
                    // for CODE CP
                    if (result.length != 0) {
                        var test = result[0].cust_code;
                    } else {
                        var test = "C000000000000"
                    }

                    var pieces = test.split('C');
                    var prefix = pieces[0];
                    var lastNum = pieces[1];
                    lastNum = parseInt(lastNum, 10);
                    lastNum++;
                    cust_code = "C" + ("000000000000" + lastNum).substr(-6);


                    connection.acquire(function(err, con) {
                        con.query('INSERT INTO customers (id,cust_code,branch_id,branch_code,cust_jenis,occupation_id,kerja_kode,cust_agama,poscode_id,kota_id,propinsi_id,cust_kelamin,pos_kode,pos_kota,pos_prop,pos_camat,pos_lurah,kota_id2,propinsi_id2,pos_kode2,pos_kota2,pos_prop2,pos_camat2,pos_lurah2,cust_nama,cust_noktp,cust_npwp,cust_tgl_lahir,cust_alamat,cust_rt,cust_rw,alamat_lengkap,rt_stnk,rw_stnk,nama_stnk,alamat_stnk,cust_fax,cust_hp,cust_telp_rumah,cust_email,cust_alamat2,noktp_stnk,alamat_invoice,alamat_kirim,cust_telp_kantor,cust_sumber,salesman_id,assigned_to,cust_kendaraan,npwp_stnk,created,modified,id_motor,type_motor,kode_motor,deskripsi_motor,id_employee,employee_kode,employee_nama,employee_jabatan,id_event,kode_event,nama_event,lokasi_event,cust_pic,status_generate_spk,doc_1,doc_2,doc_3,doc_4,doc_5) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, cust_code, branch_id, branch_code, cust_jenis, occupation_id, kerja_kode, cust_agama, poscode_id, kota_id, propinsi_id, cust_kelamin, pos_kode, pos_kota, pos_prop, pos_camat, pos_lurah, kota_id2, propinsi_id2, pos_kode2, pos_kota2, pos_prop2, pos_camat2, pos_lurah2, cust_nama, cust_noktp, cust_npwp, cust_tgl_lahir, cust_alamat, cust_rt, cust_rw, alamat_lengkap, rt_stnk, rw_stnk, nama_stnk, alamat_stnk, cust_fax, cust_hp, cust_telp_rumah, cust_email, cust_alamat2, noktp_stnk, alamat_invoice, alamat_kirim, cust_telp_kantor, cust_sumber, salesman_id, assigned_to, cust_kendaraan, npwp_stnk, created, modified, id_motor, type_motor, kode_motor, deskripsi_motor, id_employee, employee_kode, employee_nama, employee_jabatan, id_event, kode_event, nama_event, lokasi_event, cust_pic, status_generate_spk, doc_1, doc_2, doc_3, doc_4, doc_5],
                            function(err, result) {
                                con.release();
                                if (err) {
                                    res.send({ status: 400, message: 'creation failed' });
                                } else {
                                    connection.acquire(function(err, con) {
                                        con.query('select cust_code from customers order by created desc limit 1', function(err, result) {
                                            con.release();
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                var no_ktp = req.body.cust_noktp
                                                request({
                                                    uri: 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_customer_all.php?cust_noktp=' + no_ktp,
                                                    auth: {
                                                        'bearer': '71D55F9957529'
                                                    },
                                                    rejectUnauthorized: false, //add when working with https sites
                                                    requestCert: false, //add when working with https sites
                                                    agent: false, //add when working with https sites

                                                }, function(error, rows) {
                                                    if (error) {
                                                        console.log(error);
                                                    } else {
                                                        const body = JSON.parse(rows.body); // parse string to JSON
                                                        console.log(rows.body);

                                                        if (body.status == "False") {
                                                            connection.acquire(function(err, con) {
                                                                con.query('select id, cust_code, cust_noktp from customers order by modified desc limit 1', function(err, result) {
                                                                    con.release();
                                                                    if (err) {
                                                                        console.log(err);
                                                                    } else {
                                                                        var id_cust = result[0].id;
                                                                        var code_cust = result[0].cust_code;

                                                                        console.log(code_cust);
                                                                        console.log(id_cust);
                                                                        var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_insert_customer.php';

                                                                        var requestData = {

                                                                            id: id_cust,
                                                                            cust_code: code_cust,
                                                                            poscode_id: req.body.poscode_id,
                                                                            pos_kode: req.body.pos_kode,
                                                                            pos_kota: req.body.pos_kota,
                                                                            pos_prop: req.body.pos_prop,
                                                                            pos_camat: req.body.pos_camat,
                                                                            pos_lurah: req.body.pos_lurah,
                                                                            cust_nama: req.body.cust_nama,
                                                                            cust_noktp: req.body.cust_noktp,
                                                                            cust_alamat: req.body.cust_alamat,
                                                                            cust_hp: req.body.cust_hp,
                                                                            cust_telp_rumah: req.body.cust_telp_rumah,
                                                                            noktp_stnk: req.body.noktp_stnk,
                                                                            alamat_invoice: req.body.alamat_invoice,
                                                                            alamat_kirim: req.body.alamat_kirim,
                                                                            cust_telp_kantor: req.body.cust_telp_kantor,
                                                                            salesman_id: req.body.salesman_id,
                                                                            cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                                            branch_id: req.body.branch_id,
                                                                            branch_code: req.body.branch_code,
                                                                            cust_jenis: req.body.cust_jenis,
                                                                            occupation_id: req.body.occupation_id,
                                                                            kerja_kode: req.body.kerja_kode,
                                                                            cust_agama: req.body.cust_agama,
                                                                            kota_id: req.body.kota_id,
                                                                            propinsi_id: req.body.propinsi_id,
                                                                            cust_kelamin: req.body.cust_kelamin,
                                                                            kota_id2: req.body.kota_id2,
                                                                            propinsi_id2: req.body.propinsi_id2,
                                                                            pos_kode2: req.body.pos_kode2,
                                                                            pos_kota2: req.body.pos_kota2,
                                                                            pos_prop2: req.body.pos_prop2,
                                                                            pos_camat2: req.body.pos_camat2,
                                                                            pos_lurah2: req.body.pos_lurah2,
                                                                            cust_npwp: req.body.cust_npwp,
                                                                            cust_rt: req.body.cust_rt,
                                                                            cust_rw: req.body.cust_rw,
                                                                            alamat_lengkap: req.body.alamat_lengkap,
                                                                            rt_stnk: req.body.rt_stnk,
                                                                            rw_stnk: req.body.rw_stnk,
                                                                            nama_stnk: req.body.nama_stnk,
                                                                            alamat_stnk: req.body.alamat_stnk,
                                                                            cust_fax: req.body.cust_fax,
                                                                            cust_email: req.body.cust_email,
                                                                            cust_alamat2: req.body.cust_alamat2,
                                                                            cust_kendaraan: req.body.cust_kendaraan,
                                                                            cust_type: req.body.cust_type,
                                                                            created: new Date(),
                                                                            modified: new Date(),
                                                                            npwp_stnk: req.body.npwp_stnk,
                                                                            lead_id: req.body.lead_id,
                                                                            lead_code: req.body.lead_code,
                                                                            dealer_id: req.body.dealer_id,
                                                                            dealer_code: req.body.dealer_code,
                                                                            cust_sumber: req.body.cust_sumber,
                                                                            doc_1: "",
                                                                            doc_2: "",
                                                                            doc_3: "",
                                                                            doc_4: "",
                                                                            doc_5: "",

                                                                            status_generate_spk: "0",

                                                                            cust_pic: req.body.cust_pic,
                                                                            cust_org: req.body.cust_org,
                                                                            assigned_to: req.body.assigned_to,
                                                                            cust_npk: req.body.cust_npk,

                                                                            id_motor: req.body.id_motor,
                                                                            type_motor: req.body.type_motor,
                                                                            kode_motor: req.body.kode_motor,
                                                                            deskripsi_motor: req.body.deskripsi_motor,

                                                                            id_employee: req.body.id_employee,
                                                                            employee_kode: req.body.employee_kode,
                                                                            employee_nama: req.body.employee_nama,
                                                                            employee_jabatan: req.body.employee_jabatan,

                                                                            id_event: req.body.id_event,
                                                                            kode_event: req.body.kode_event,
                                                                            nama_event: req.body.nama_event,
                                                                            lokasi_event: req.body.lokasi_event,
                                                                            cust_hp_wa: req.body.cust_hp_wa,

                                                                            ro_bd_id: "",
                                                                            sektor_kode: "",
                                                                            cust_wn: "",
                                                                            cust_kitas: "",
                                                                            cust_kk: "",
                                                                            cust_wn_stnk: "",
                                                                            nokitas_stnk: "",
                                                                            kk_stnk: "",
                                                                        }

                                                                        var data = {
                                                                            url: url,
                                                                            auth: {
                                                                                'bearer': '71D55F9957529'
                                                                            },
                                                                            body: requestData,
                                                                            json: true,
                                                                            rejectUnauthorized: false, //add when working with https sites
                                                                            requestCert: false, //add when working with https sites
                                                                            agent: false, //add when working with https sites
                                                                        }

                                                                        console.log(data);
                                                                        request.post(data, function(error, httpResponse, body) {
                                                                            if (body.status == "Failed") {
                                                                                console.log(error);
                                                                                res.send({ status: 400, message: 'Generate Customer Failed' });
                                                                            } else {
                                                                                console.log(body);
                                                                                res.send({ status: 200, message: 'Generate Customer success' });
                                                                            }

                                                                        });

                                                                    }

                                                                });
                                                            });
                                                        } else if (body.status == "True") {
                                                            var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_edit_customer2.php?cust_noktp=' + no_ktp;

                                                            var requestData = {

                                                                // cust_code: result[0].cust_code,
                                                                title: "",
                                                                cust_nama: req.body.cust_nama,
                                                                cust_pic: req.body.cust_pic,
                                                                cust_org: "",
                                                                cust_alamat: req.body.cust_alamat,
                                                                noktp_stnk: req.body.noktp_stnk,
                                                                poscode_id: req.body.poscode_id,
                                                                pos_kode: req.body.pos_kode,
                                                                pos_camat: req.body.pos_camat,
                                                                pos_lurah: req.body.pos_lurah,
                                                                pos_kota: req.body.pos_kota,
                                                                pos_prop: req.body.pos_prop,
                                                                alamat_invoice: req.body.alamat_invoice,
                                                                alamat_kirim: req.body.alamat_kirim,
                                                                cust_telp_rumah: req.body.cust_telp_rumah,
                                                                cust_telp_kantor: req.body.cust_telp_kantor,
                                                                cust_fax: req.body.cust_fax,
                                                                cust_hp: req.body.cust_hp,
                                                                cust_email: req.body.cust_email,
                                                                cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                                cust_agama: req.body.cust_agama,
                                                                occupation_id: req.body.occupation_id,
                                                                kerja_kode: req.body.kerja_kode,
                                                                cust_kelamin: req.body.cust_kelamin,
                                                                cust_jenis: req.body.cust_jenis,
                                                                cust_npwp: req.body.cust_npwp,

                                                                salesman_id: req.body.salesman_id,
                                                                assigned_to: req.body.assigned_to,
                                                                cust_sumber: req.body.cust_sumber,
                                                                cust_npk: req.body.cust_npk,
                                                                cust_kendaraan: req.body.cust_kendaraan,
                                                                branch_id: req.body.branch_id,
                                                                branch_code: req.body.branch_code,
                                                                company_id: req.body.company_id,
                                                                company_code: req.body.company_code,
                                                                cust_type: req.body.cust_type,
                                                                modi_by: salesman_id,
                                                                modified: new Date(),
                                                                kunci: "",
                                                                kota_id: req.body.kota_id,
                                                                propinsi_id: req.body.propinsi_id,
                                                                nama_stnk: req.body.nama_stnk,
                                                                alamat_stnk: req.body.alamat_stnk,
                                                                alamat_lengkap: req.body.alamat_lengkap,
                                                                npwp_stnk: req.body.npwp_stnk,
                                                                cust_alamat2: req.body.cust_alamat2,
                                                                cek_alamat_kirim: "",
                                                                pos_kota2: req.body.pos_kota2,
                                                                pos_prop2: req.body.pos_prop2,
                                                                propinsi_id2: req.body.propinsi_id2,
                                                                pos_lurah2: req.body.pos_lurah2,
                                                                pos_kode2: req.body.pos_kode2,
                                                                kota_id2: req.body.kota_id2,
                                                                pos_camat2: req.body.pos_camat2,

                                                                // lead_code: result[0].lead_code,
                                                                // lead_id: result[0].lead_id,
                                                                cust_rt: req.body.cust_rt,
                                                                cust_rw: req.body.cust_rw,

                                                                dealer_id: req.body.dealer_id,
                                                                dealer_code: req.body.dealer_code,

                                                                rt_stnk: req.body.rt_stnk,
                                                                rw_stnk: req.body.rw_stnk,

                                                                status_generate_spk: "0",

                                                                id_motor: req.body.id_motor,
                                                                id_event: req.body.id_event,
                                                                id_employee: req.body.id_employee,
                                                                ro_bd_id: "",
                                                                sektor_kode: "",
                                                                cust_wn: "",
                                                                cust_kitas: "",
                                                                cust_kk: "",
                                                                cust_wn_stnk: "",
                                                                nokitas_stnk: "",
                                                                kk_stnk: "",
                                                                type_motor: req.body.type_motor,
                                                                kode_motor: req.body.kode_motor,
                                                                deskripsi_motor: req.body.deskripsi_motor,

                                                                employee_kode: req.body.employee_kode,
                                                                employee_nama: req.body.employee_nama,
                                                                employee_jabatan: req.body.employee_jabatan,

                                                                kode_event: req.body.kode_event,
                                                                nama_event: req.body.nama_event,
                                                                lokasi_event: req.body.lokasi_event,

                                                                doc_1: "",
                                                                doc_2: "",
                                                                doc_3: "",
                                                                doc_4: "",
                                                                doc_5: "",

                                                            }

                                                            var data = {
                                                                url: url,
                                                                auth: {
                                                                    'bearer': '71D55F9957529'
                                                                },
                                                                json: true,
                                                                body: requestData,
                                                                rejectUnauthorized: false, //add when working with https sites
                                                                requestCert: false, //add when working with https sites
                                                                agent: false, //add when working with https sites
                                                            }

                                                            console.log(data);
                                                            request.post(data, function(error, httpResponse, body) {
                                                                if (body.status == "Failed") {
                                                                    console.log(error);
                                                                    res.send({ status: 400, message: 'Generate Customer Failed' });
                                                                } else {
                                                                    console.log(body);
                                                                    res.send({ status: 200, message: 'Generate Customer success' });
                                                                }
                                                            });
                                                        }
                                                    }

                                                });
                                            }

                                        });
                                    });
                                }
                            });
                    });
                }
            });
        });
    };










}

module.exports = new Todo();