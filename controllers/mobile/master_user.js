//Auth : Nina
var connection = require('../../config/db');

function Todo() {

    //GET
    this.get = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            if (id == null) {
                con.query('SELECT * FROM users', function(err, data) {
                    con.release();
                    if (err)
                        return res.json({ status: '400', message: 'Failed', result: [] });
                    return res.json({ status: '200', message: 'success', result: data });
                });
            } else {
                var query = 'SELECT * FROM users where id = "' + id + '" ';
                con.query(query, function(err, data) {
                    con.release();
                    if (err)
                        return res.json({ status: '400', message: 'Failed', result: [] });
                    return res.json({ status: '200', message: 'success', result: data });
                });
            }

        });
    };

    //CREATE
    this.post = function(req, res) {
        if (req.fileValidationError) {
            res.send(req.fileValidationError);
            return;
        }

        if (req.files.length == 0) {
            res.send("Image Can't Empty ");
            return;
        }

        var photo = req.files[0].filename;
        var url = '/files/' + photo;

        var id = uuidv1();
        var name = req.body.name;
        var username = req.body.username;
        var password = req.body.password;
        var validpass = md5(password);
        var email = req.body.email;
        var phone_number = req.body.phone_number;
        var birth_date = req.body.birth_date;
        var gender = req.body.gender;
        var is_login = req.body.is_login;
        var last_login = req.body.last_login;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_id;
        var group_id = req.body.group_id;
        var salesman_id = req.body.salesman_id;
        var created_at = new Date();
        var created_by = req.body.create_by;
        var modified_at = new Date();
        var modi_by = req.body.modi_by;

        connection.acquire(function(err, con) {
            con.query('INSERT INTO users (id, name, username,password,email,phone_number,birth_date,gender,image,is_login,last_login,branch_id,branch_code,group_id,salesman_id,created_at,created_by,modified_at,modi_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, name, username, validpass, email, phone_number, birth_date, gender, url, is_login, last_login, branch_id, branch_code, group_id, salesman_id, created_at, created_by, modified_at, modi_by],
                function(err, result) {
                    con.release();
                    if (err) {
                        res.send({ status: 400, message: 'creation failed' });
                    } else {
                        res.send({ status: 200, message: 'created successfully' });
                    }
                });
        });
    };

    //UPDATE
    this.put = function(req, res, next) {
        var id = req.body.id;
        var name = req.body.name;
        var phone_number = req.body.phone_number;
        var email = req.body.email;
        var birth_date = req.body.birth_date;
        var gender = req.body.gender;
        var modified_at = new Date();
        var modi_by = req.body.modi_by;

        connection.acquire(function(err, con, result, fields) {
            if (err) throw err;
            con.query('UPDATE users SET name = ?, phone_number = ?, email = ?, birth_date = ?, gender = ?, modified_at = ?, modi_by = ? WHERE id = "' + id + '"', [name, phone_number, email, birth_date, gender, modified_at, modi_by], function(err, results, fields) {
                con.release();
                connection.acquire(function(err, con) {
                    con.query('SELECT * FROM users where id = "' + id + '" ', [id], function(err, results, fields) {
                        con.release();
                        res.json(results[0]);

                    })
                })
            });
        });

    };

    this.putPhoto = function(req, res, next) {
        var id = req.body.id;
        if (req.fileValidationError) {
            res.send(req.fileValidationError);
            return;
        }
        connection.acquire(function(err, con) {
            con.query('SELECT image FROM users where id = "' + id + '"', function(err, data) {
                con.release();
                if (err) return res.json({ status: '400', message: 'Failed', result: [] });

                if (req.files.length != 0 && data.length > 0) {
                    const fs = require('fs');
                    fs.unlink(appRoot + data[0].image.replace("/files/", "/uploads/"), (err) => {});
                }

                if (req.files.length == 0) {
                    var url = data[0].image;
                } else {
                    var photo = req.files[0].filename;
                    var url = '/files/' + photo;
                }

                connection.acquire(function(err, con) {
                    con.query('UPDATE users SET image = ? WHERE id = "' + id + '"', [url], function(err, result) {
                        con.release();
                        if (err) {
                            res.send({ status: 400, message: 'Image update failed' });
                        } else {
                            res.send({ status: 200, message: 'Image update successfully' });
                        }
                    });
                });
            });
        });
    };

    this.putPassword = function(req, res) {
        var id = req.body.id;
        var password = req.body.password;
        var validpass = md5(password);

        connection.acquire(function(err, con) {
            con.query('UPDATE users SET password = ? WHERE id = "' + id + '"', [validpass], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Update password failed' });
                } else {
                    res.send({ status: 200, message: 'Update password successfully' });
                }
            });
        });
    };

    this.updatePhotoProfile = function(req, res) {
        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        var id = req.params.id;
        var name = req.body.name;
        var photo_profile = null;

        var fileUploads = req.files;
        var docs = [];

        console.log(id);

        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT image FROM users where id = "' + id + '"', function(err, data) {
                con.release();
                if (err) {
                    console.log("error");
                } else {

                    fileUploads.forEach(function(element, index) {
                        var image = data[0][element.fieldname];
                        if (image) {
                            fs.unlink(appRoot + image.replace("/files/", "/uploads/"), (err) => {
                                if (err) throw err;
                            });
                        }
                        photo_profile = "/files/" + element.filename;
                        console.log(photo_profile);
                    });
                    connection.acquire(function(err, conn) {
                        var queryStr = 'UPDATE users set image =?';

                        var queryParams = [photo_profile];

                        // for (var key in docs) {
                        //     var value = docs[key];

                        //     queryStr = queryStr + ", " + key + " = ?";
                        //     queryParams.push(value);
                        // }

                        queryStr = queryStr + ' where id = "' + id + '"'
                        console.log(queryStr);

                        conn.query(queryStr, queryParams, function(err, result) {
                            conn.release();
                            if (err) {
                                res.send({ status: 400, message: 'Failed' });
                            } else {
                                res.send({ status: 200, message: 'Success' });
                            }
                        });
                    });
                }
            });
        });

    };

}

module.exports = new Todo();