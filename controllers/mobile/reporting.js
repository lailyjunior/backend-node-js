//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    const fs = require('fs');
    var request = require('request');

    //------------------- For Report Customer ---------------------//
    //salesman by branch
    this.selectSalesmanByBranch = function(req, res, next) {
        var branch = req.query.branch_code;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT s.id, sls_nama,supervisor_nama FROM salesmen s, supervisors sp WHERE s.supervisor_id = sp.id and s.branch_code = "' + branch + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    // untuk admin cabang all
    this.reportCustomerByBranch = function(req, res, next) {
        var branch_id = req.query.branch_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.id,cust_code,title,cust_nama name,cust_pic,cust_org,cust_noktp,cust_alamat,noktp_stnk, poscode_id,pos_kode,pos_camat,pos_lurah,pos_kota,pos_prop,alamat_invoice,alamat_kirim,cust_telp_rumah, cust_telp_kantor,cust_fax,cust_hp no_hp, cust_email,cust_tgl_lahir,cust_agama,occupation_id,kerja_kode,cust_kelamin,cust_jenis,cust_npwp,salesman_id,assigned_to,cust_sumber,cust_npk,cust_kendaraan,c.branch_id,c.branch_code,c.company_id, c.company_code,cust_type,c.create_by,c.modi_by,c.created,c.modified,c.kunci,kota_id,propinsi_id,nama_stnk,alamat_stnk,alamat_lengkap,npwp_stnk,cust_alamat2,cek_alamat_kirim,pos_kota2,pos_prop2,propinsi_id2,pos_lurah2,pos_kode2,kota_id2,pos_camat2,lead_code,lead_id,cust_rt,cust_rw,dealer_id,dealer_code,rw_stnk,rt_stnk,id_motor,kode_motor,deskripsi_motor,doc_1,doc_2,doc_3,doc_4,doc_5,id_event,kode_event,nama_event,lokasi_event,id_employee,employee_kode,employee_nama,employee_jabatan,s.sls_nama sales, mr.religion, mo.occupation, mt.type type_customer, mj.jenis jenis_customer, ms.deskripsi sumber, sp.supervisor_nama FROM customers c left join salesmen s ON c.salesman_id = s.id left join master_religion mr ON c.cust_agama = mr.code left join master_occupation mo ON c.kerja_kode = mo.code left join master_jenis_customer mj ON c.cust_jenis = mj.code left join master_type_customer mt ON c.cust_type = mt.code left join master_sumber ms ON c.cust_sumber = ms.code   left join supervisors sp ON s.supervisor_id = sp.id where c.branch_id = "' + branch_id + '" order by cust_code asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    // untuk admin cabang by salesman
    this.reportCustomerByBranchAndSalesman = function(req, res, next) {
        var branch_id = req.query.branch_id;
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.id,cust_code,title,cust_nama name,cust_pic,cust_org,cust_noktp,cust_alamat,noktp_stnk, poscode_id,pos_kode,pos_camat,pos_lurah,pos_kota,pos_prop,alamat_invoice,alamat_kirim,cust_telp_rumah, cust_telp_kantor,cust_fax,cust_hp no_hp, cust_email,cust_tgl_lahir,cust_agama,occupation_id,kerja_kode,cust_kelamin,cust_jenis,cust_npwp,salesman_id,assigned_to,cust_sumber,cust_npk,cust_kendaraan,c.branch_id,c.branch_code,c.company_id, c.company_code,cust_type,c.create_by,c.modi_by,c.created,c.modified,c.kunci,kota_id,propinsi_id,nama_stnk,alamat_stnk,alamat_lengkap,npwp_stnk,cust_alamat2,cek_alamat_kirim,pos_kota2,pos_prop2,propinsi_id2,pos_lurah2,pos_kode2,kota_id2,pos_camat2,lead_code,lead_id,cust_rt,cust_rw,dealer_id,dealer_code,rw_stnk,rt_stnk,id_motor,kode_motor,deskripsi_motor,doc_1,doc_2,doc_3,doc_4,doc_5,id_event,kode_event,nama_event,lokasi_event,id_employee,employee_kode,employee_nama,employee_jabatan,s.sls_nama sales, mr.religion, mo.occupation, mt.type type_customer, mj.jenis jenis_customer, ms.deskripsi sumber FROM customers c left join salesmen s ON c.salesman_id = s.id left join master_religion mr ON c.cust_agama = mr.code left join master_occupation mo ON c.kerja_kode = mo.code left join master_jenis_customer mj ON c.cust_jenis = mj.code left join master_type_customer mt ON c.cust_type = mt.code left join master_sumber ms ON c.cust_sumber = ms.code where c.branch_id = "' + branch_id + '" and c.salesman_id = "' + salesman_id + '" order by cust_code asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    //untuk supervisor
    this.reportCustomerBySupervisor = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.id,cust_code,title,cust_nama name,cust_pic,cust_org,cust_noktp,cust_alamat,noktp_stnk, poscode_id,pos_kode,pos_camat,pos_lurah,pos_kota,pos_prop,alamat_invoice,alamat_kirim,cust_telp_rumah, cust_telp_kantor,cust_fax,cust_hp no_hp, cust_email,cust_tgl_lahir,cust_agama,occupation_id,kerja_kode,cust_kelamin,cust_jenis,cust_npwp,salesman_id,assigned_to,cust_sumber,cust_npk,cust_kendaraan,c.branch_id,c.branch_code,c.company_id, c.company_code,cust_type,c.create_by,c.modi_by,c.created,c.modified,c.kunci,kota_id,propinsi_id,nama_stnk,alamat_stnk,alamat_lengkap,npwp_stnk,cust_alamat2,cek_alamat_kirim,pos_kota2,pos_prop2,propinsi_id2,pos_lurah2,pos_kode2,kota_id2,pos_camat2,lead_code,lead_id,cust_rt,cust_rw,dealer_id,dealer_code,rw_stnk,rt_stnk,id_motor,kode_motor,deskripsi_motor,doc_1,doc_2,doc_3,doc_4,doc_5,id_event,kode_event,nama_event,lokasi_event,id_employee,employee_kode,employee_nama,employee_jabatan,s.sls_nama sales, mr.religion, mo.occupation, mt.type type_customer, mj.jenis jenis_customer, ms.deskripsi sumber FROM customers c left join salesmen s ON c.salesman_id = s.id left join master_religion mr ON c.cust_agama = mr.code left join master_occupation mo ON c.kerja_kode = mo.code left join master_jenis_customer mj ON c.cust_jenis = mj.code left join master_type_customer mt ON c.cust_type = mt.code left join master_sumber ms ON c.cust_sumber = ms.code where c.salesman_id IN (select id from salesmen where supervisor_id = "' + supervisor_id + '") order by cust_code asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    //untuk supervisor by salesman
    this.reportCustomerBySalesman = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.id,cust_code,title,cust_nama name,cust_pic,cust_org,cust_noktp,cust_alamat,noktp_stnk, poscode_id,pos_kode,pos_camat,pos_lurah,pos_kota,pos_prop,alamat_invoice,alamat_kirim,cust_telp_rumah, cust_telp_kantor,cust_fax,cust_hp no_hp, cust_email,cust_tgl_lahir,cust_agama,occupation_id,kerja_kode,cust_kelamin,cust_jenis,cust_npwp,salesman_id,assigned_to,cust_sumber,cust_npk,cust_kendaraan,c.branch_id,c.branch_code,c.company_id, c.company_code,cust_type,c.create_by,c.modi_by,c.created,c.modified,c.kunci,kota_id,propinsi_id,nama_stnk,alamat_stnk,alamat_lengkap,npwp_stnk,cust_alamat2,cek_alamat_kirim,pos_kota2,pos_prop2,propinsi_id2,pos_lurah2,pos_kode2,kota_id2,pos_camat2,lead_code,lead_id,cust_rt,cust_rw,dealer_id,dealer_code,rw_stnk,rt_stnk,id_motor,kode_motor,deskripsi_motor,doc_1,doc_2,doc_3,doc_4,doc_5,id_event,kode_event,nama_event,lokasi_event,id_employee,employee_kode,employee_nama,employee_jabatan,s.sls_nama sales, mr.religion, mo.occupation, mt.type type_customer, mj.jenis jenis_customer, ms.deskripsi sumber FROM customers c left join salesmen s ON c.salesman_id = s.id left join master_religion mr ON c.cust_agama = mr.code left join master_occupation mo ON c.kerja_kode = mo.code left join master_jenis_customer mj ON c.cust_jenis = mj.code left join master_type_customer mt ON c.cust_type = mt.code left join master_sumber ms ON c.cust_sumber = ms.code where salesman_id = "' + salesman_id + '" order by cust_code asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    //------------------- For Report SPK ---------------------//

    // untuk admin cabang all
    this.reportSPKByBranch = function(req, res, next) {
        var branch_id = req.query.branch_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT kode_spk,no_spk, code_customer,cust_nama, cust_alamat, cust_hp, cust_telp, cust_email,cust_npwp, cust_stnk,cust_alamat_stnk,cust_tgl_lahir, cust_ktp, cust_jenis_kelamin, cust_hp2, cust_telp2, cust_sumber,cara_bayar,leasing_nama, jumlah_tenor, dp_po, dp_bayar, angsuran_per_bulan, asuransi, denda_daerah, denda_jumlah, pajak_progresif, salesman_id,mtr_type, mtr_kode, gd.deskripsi, harga, ms.deskripsi sumber,s.sls_nama, sp.supervisor_nama FROM generate_spks g INNER JOIN generate_spk_details gd ON g.id = gd.id_generate_spk LEFT JOIN master_sumber ms ON g.cust_sumber = ms.code LEFT JOIN salesmen s ON g.salesman_id = s.id LEFT JOIN supervisors sp ON s.supervisor_id = sp.id where s.branch_id = "' + branch_id + '" ORDER BY kode_spk', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    //untuk cabang per salesman 
    this.reportSPKByBranchSalesman = function(req, res, next) {
        var branch_id = req.query.branch_id;
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT kode_spk,no_spk, code_customer,cust_nama, cust_alamat, cust_hp, cust_telp, cust_email,cust_npwp, cust_stnk,cust_alamat_stnk,cust_tgl_lahir, cust_ktp, cust_jenis_kelamin, cust_hp2, cust_telp2, cust_sumber,cara_bayar,leasing_nama, jumlah_tenor, dp_po, dp_bayar, angsuran_per_bulan, asuransi, denda_daerah, denda_jumlah, pajak_progresif, salesman_id,mtr_type, mtr_kode, gd.deskripsi, harga, ms.deskripsi sumber,s.sls_nama, sp.supervisor_nama FROM generate_spks g INNER JOIN generate_spk_details gd ON g.id = gd.id_generate_spk LEFT JOIN master_sumber ms ON g.cust_sumber = ms.code LEFT JOIN salesmen s ON g.salesman_id = s.id LEFT JOIN supervisors sp ON s.supervisor_id = sp.id where s.branch_id = "' + branch_id + '" and g.salesman_id = "' + salesman_id + '" ORDER BY kode_spk', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    //untuk cabang supervisor all 
    this.reportSPKBySupervisor = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT kode_spk,no_spk, code_customer,cust_nama, cust_alamat, cust_hp, cust_telp, cust_email,cust_npwp, cust_stnk,cust_alamat_stnk,cust_tgl_lahir, cust_ktp, cust_jenis_kelamin, cust_hp2, cust_telp2, cust_sumber,cara_bayar,leasing_nama, jumlah_tenor, dp_po, dp_bayar, angsuran_per_bulan, asuransi, denda_daerah, denda_jumlah, pajak_progresif, salesman_id,mtr_type, mtr_kode, gd.deskripsi, harga, ms.deskripsi sumber,s.sls_nama, sp.supervisor_nama FROM generate_spks g INNER JOIN generate_spk_details gd ON g.id = gd.id_generate_spk LEFT JOIN master_sumber ms ON g.cust_sumber = ms.code LEFT JOIN salesmen s ON g.salesman_id = s.id LEFT JOIN supervisors sp ON s.supervisor_id = sp.id where g.salesman_id IN (select id from salesmen where supervisor_id = "' + supervisor_id + '") ORDER BY kode_spk', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    //untuk cabang supervisor by salesman 
    this.reportSPKBySalesman = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT kode_spk,no_spk, code_customer,cust_nama, cust_alamat, cust_hp, cust_telp, cust_email,cust_npwp, cust_stnk,cust_alamat_stnk,cust_tgl_lahir, cust_ktp, cust_jenis_kelamin, cust_hp2, cust_telp2, cust_sumber,cara_bayar,leasing_nama, jumlah_tenor, dp_po, dp_bayar, angsuran_per_bulan, asuransi, denda_daerah, denda_jumlah, pajak_progresif, salesman_id,mtr_type, mtr_kode, gd.deskripsi, harga, ms.deskripsi sumber,s.sls_nama, sp.supervisor_nama FROM generate_spks g INNER JOIN generate_spk_details gd ON g.id = gd.id_generate_spk LEFT JOIN master_sumber ms ON g.cust_sumber = ms.code LEFT JOIN salesmen s ON g.salesman_id = s.id LEFT JOIN supervisors sp ON s.supervisor_id = sp.id where g.salesman_id = "' + salesman_id + '" ORDER BY kode_spk', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    //------------------- For Report Customer Prospect ---------------------//

    // untuk admin cabang all
    this.reportCustomerProspectByBranch = function(req, res, next) {
        var branch_id = req.query.branch_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.id,cust_code,cust_nama name,cust_noktp,cust_alamat,pos_kode,pos_camat,pos_lurah,pos_kota,pos_prop,cust_telp_rumah, cust_telp_kantor,cust_fax,cust_hp no_hp, cust_email,cust_tgl_lahir,cust_agama,kerja_kode,cust_kelamin,cust_jenis,cust_npwp,cust_sumber,cust_kendaraan,cust_type,c.create_by,c.modi_by,c.created,c.modified,c.kunci,kota_id,propinsi_id,cust_rt,cust_rw,rw_stnk,rt_stnk,id_motor,kode_motor,deskripsi_motor,id_event,kode_event,nama_event,lokasi_event,id_employee,employee_kode,employee_nama,employee_jabatan,s.sls_nama sales, mr.religion, mo.occupation, mt.type type_customer, mj.jenis jenis_customer, ms.deskripsi sumber, sp.supervisor_nama FROM customer_prospect c left join salesmen s ON c.salesman_id = s.id left join master_religion mr ON c.cust_agama = mr.code left join master_occupation mo ON c.kerja_kode = mo.code left join master_jenis_customer mj ON c.cust_jenis = mj.code left join master_type_customer mt ON c.cust_type = mt.code left join master_sumber ms ON c.cust_sumber = ms.code left join supervisors sp ON s.supervisor_id = sp.id where c.branch_id = "' + branch_id + '" order by cust_code asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    // untuk admin cabang all by salesman 
    this.reportCustomerProspectByBranchSalesman = function(req, res, next) {
        var branch_id = req.query.branch_id;
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.id,cust_code,cust_nama name,cust_noktp,cust_alamat,pos_kode,pos_camat,pos_lurah,pos_kota,pos_prop,cust_telp_rumah, cust_telp_kantor,cust_fax,cust_hp no_hp, cust_email,cust_tgl_lahir,cust_agama,kerja_kode,cust_kelamin,cust_jenis,cust_npwp,cust_sumber,cust_kendaraan,cust_type,c.create_by,c.modi_by,c.created,c.modified,c.kunci,kota_id,propinsi_id,cust_rt,cust_rw,rw_stnk,rt_stnk,id_motor,kode_motor,deskripsi_motor,id_event,kode_event,nama_event,lokasi_event,id_employee,employee_kode,employee_nama,employee_jabatan,s.sls_nama sales, mr.religion, mo.occupation, mt.type type_customer, mj.jenis jenis_customer, ms.deskripsi sumber, sp.supervisor_nama FROM customer_prospect c left join salesmen s ON c.salesman_id = s.id left join master_religion mr ON c.cust_agama = mr.code left join master_occupation mo ON c.kerja_kode = mo.code left join master_jenis_customer mj ON c.cust_jenis = mj.code left join master_type_customer mt ON c.cust_type = mt.code left join master_sumber ms ON c.cust_sumber = ms.code left join supervisors sp ON s.supervisor_id = sp.id where c.branch_id = "' + branch_id + '" and c.salesman_id = "' + salesman_id + '"order by cust_code asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    // untuk supervisor 
    this.reportCustomerProspectBySupervisor = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.id,cust_code,cust_nama name,cust_noktp,cust_alamat,pos_kode,pos_camat,pos_lurah,pos_kota,pos_prop,cust_telp_rumah, cust_telp_kantor,cust_fax,cust_hp no_hp, cust_email,cust_tgl_lahir,cust_agama,kerja_kode,cust_kelamin,cust_jenis,cust_npwp,cust_sumber,cust_kendaraan,cust_type,c.create_by,c.modi_by,c.created,c.modified,c.kunci,kota_id,propinsi_id,cust_rt,cust_rw,rw_stnk,rt_stnk,id_motor,kode_motor,deskripsi_motor,id_event,kode_event,nama_event,lokasi_event,id_employee,employee_kode,employee_nama,employee_jabatan,s.sls_nama sales, mr.religion, mo.occupation, mt.type type_customer, mj.jenis jenis_customer, ms.deskripsi sumber, sp.supervisor_nama FROM customer_prospect c left join salesmen s ON c.salesman_id = s.id left join master_religion mr ON c.cust_agama = mr.code left join master_occupation mo ON c.kerja_kode = mo.code left join master_jenis_customer mj ON c.cust_jenis = mj.code left join master_type_customer mt ON c.cust_type = mt.code left join master_sumber ms ON c.cust_sumber = ms.code left join supervisors sp ON s.supervisor_id = sp.id where c.salesman_id IN (select id from salesmen where supervisor_id = "' + supervisor_id + '")  order by cust_code asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    // untuk supervisor 
    this.reportCustomerProspectBySupervisorSalesman = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.id,cust_code,cust_nama name,cust_noktp,cust_alamat,pos_kode,pos_camat,pos_lurah,pos_kota,pos_prop,cust_telp_rumah, cust_telp_kantor,cust_fax,cust_hp no_hp, cust_email,cust_tgl_lahir,cust_agama,kerja_kode,cust_kelamin,cust_jenis,cust_npwp,cust_sumber,cust_kendaraan,cust_type,c.create_by,c.modi_by,c.created,c.modified,c.kunci,kota_id,propinsi_id,cust_rt,cust_rw,rw_stnk,rt_stnk,id_motor,kode_motor,deskripsi_motor,id_event,kode_event,nama_event,lokasi_event,id_employee,employee_kode,employee_nama,employee_jabatan,s.sls_nama sales, mr.religion, mo.occupation, mt.type type_customer, mj.jenis jenis_customer, ms.deskripsi sumber, sp.supervisor_nama FROM customer_prospect c left join salesmen s ON c.salesman_id = s.id left join master_religion mr ON c.cust_agama = mr.code left join master_occupation mo ON c.kerja_kode = mo.code left join master_jenis_customer mj ON c.cust_jenis = mj.code left join master_type_customer mt ON c.cust_type = mt.code left join master_sumber ms ON c.cust_sumber = ms.code left join supervisors sp ON s.supervisor_id = sp.id where c.salesman_id = "' + salesman_id + '" order by cust_code asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };
}
module.exports = new Todo();