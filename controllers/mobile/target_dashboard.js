//Auth : Lely
var connection = require('../../config/db');
var request = require('request');

function Todo() {
    const fs = require('fs');

    this.selectMasterTargetProspect = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        var date = new Date();
        var periode = dateFormat(date, "yyyy-mm");
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT sum(target) target, sum(now) now, (sum(target) - sum(now)) balance from (SELECT target_prospect target, 0 as now from master_targets m where salesman_id = "' + salesman_id + '" and periode ="' + periode + '" UNION ALL SELECT 0 as target, count(id) as now from customer_prospect c where salesman_id = "' + salesman_id + '" and SUBSTR(created, 1, 7) = "' + periode + '") x', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'failed', result: [] });
                } else {
                    if (data[0] != null) {
                        // return res.json(data[0]);
                        var datas = data[0];
                        var target = datas.target;
                        var now = datas.now;
                        if (now > target) {
                            var balance = 0;
                        } else {
                            var balance = datas.balance;
                        }
                        return res.json({ target: target, now: now, balance: balance })
                    } else {
                        return res.json({ target: 0, now: 0, balance: 0 })
                    }
                }
            });
        });
    };

    this.selectMasterTargetDeal = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        var date = new Date();
        var periode = dateFormat(date, "yyyy-mm");
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT sum(target) target, sum(now) now, (sum(target) - sum(now)) balance from (SELECT target_deal target, 0 as now from master_targets m where salesman_id = "' + salesman_id + '" and periode ="' + periode + '" UNION ALL SELECT 0 as target, count(id) as now from generate_spks g where salesman_id = "' + salesman_id + '" and SUBSTR(created_at, 1, 7) = "' + periode + '") x', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'failed', result: [] });
                } else {
                    if (data[0] != null) {
                        // return res.json(data[0]);
                        var datas = data[0];
                        var target = datas.target;
                        var now = datas.now;
                        if (now > target) {
                            var balance = 0;
                        } else {
                            var balance = datas.balance;
                        }
                        return res.json({ target: target, now: now, balance: balance })
                    } else {
                        return res.json({ target: 0, now: 0, balance: 0 })
                    }
                }
            });
        });
    };

    this.selectMasterTargetProspectAll = function(req, res, next) {
        var id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select sum(target_prospect) target, count(c.id) now, (sum(target_prospect) - count(c.id)) balance from master_targets m LEFT JOIN customer_prospect c ON m.salesman_id = c.salesman_id WHERE m.salesman_id IN (SELECT id from salesmen where supervisor_id = "' + id + '") group by m.supervisor_id', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'failed', result: [] });
                } else {
                    if (data[0] != null) {
                        return res.json(data[0]);
                    } else {
                        return res.json({ target: 0, now: 0, balance: 0 })
                    }
                }
            });
        });
    };

    //NINA
    this.prospectDealAll = function(req, res, next) {
        var id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT sum(prospect) prospect, sum(deal) deal, salesman_id, sls_nama, sls_kode from (SELECT count(c.id) prospect, 0 as deal, salesman_id, s.sls_nama, s.sls_kode from customer_prospect c, salesmen s where c.salesman_id = s.id and MONTH(c.created) = MONTH(CURRENT_DATE()) and salesman_id IN (SELECT id from salesmen where supervisor_id = "' + id + '") group by c.salesman_id UNION ALL SELECT 0 as prospect, count(g.id) deal, salesman_id, s.sls_nama, s.sls_kode from generate_spks g, salesmen s where g.salesman_id = s.id and MONTH(g.created_at) = MONTH(CURRENT_DATE()) and salesman_id IN (SELECT id from salesmen where supervisor_id= "' + id + '") group by g.salesman_id) x group by salesman_id order by deal DESC, prospect', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                // console.log(err);
                return res.json(data);
            });
        });
    };

    this.selectDealProspectSalesDMS = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        var dataSales = [];
        var dataNamaSales = [];
        var dataKodeSales = [];

        request.get({
            uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
            auth: {
                'bearer': '71D55F9957529'
            },
            rejectUnauthorized: false, //add when working with https sites
            requestCert: false, //add when working with https sites
            agent: false, //add when working with https sites
        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                obj = JSON.parse(body);

                obj.forEach(function(item) {
                    dataSales.push("'" + item.id + "'");
                    dataNamaSales.push("'" + item.sls_nama + "', ");
                    dataKodeSales.push("'" + item.sls_kode + "'");
                });

                if (dataSales.length === 0) {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT sum(prospect) prospect, sum(deal) deal, salesman_id from (SELECT count(id) prospect, 0 as deal, salesman_id from customer_prospect where salesman_id IN (" + dataSales + ") and MONTH(created) = MONTH(CURRENT_DATE()) group by salesman_id UNION ALL SELECT 0 as prospect, count(id) deal, salesman_id from generate_spks where salesman_id IN (" + dataSales + ") and MONTH(created_at) = MONTH(CURRENT_DATE()) group by salesman_id) x group by salesman_id order by deal DESC, prospect", function(err, data) {
                            con.release();
                            // console.log(obj);
                            // obj.concat(data);
                            // console.log(obj);
                            if (err)
                                return res.json({ status: 400, message: 'failed', result: [] });
                            return res.json(dataSet);
                        });
                    });
                } else {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT sum(prospect) prospect, sum(deal) deal, salesman_id from (SELECT count(id) prospect, 0 as deal, salesman_id from customer_prospect where salesman_id IN (" + dataSales + ") and MONTH(created) = MONTH(CURRENT_DATE()) group by salesman_id UNION ALL SELECT 0 as prospect, count(id) deal, salesman_id from generate_spks where salesman_id IN (" + dataSales + ") and MONTH(created_at) = MONTH(CURRENT_DATE()) group by salesman_id) x group by salesman_id order by deal DESC, prospect", function(err, data) {
                            con.release();

                            data.forEach(function(element, index) {
                                var id_sales = element.salesman_id;

                                obj.forEach(function(detail, i) {
                                    var id_sls = detail.id;
                                    if (id_sls == id_sales) {
                                        data[index].slsNama = detail.sls_nama;
                                        data[index].slsKode = detail.sls_kode;
                                    }
                                });

                            });
                            console.log(data);

                            if (err)
                                return res.json({ status: 400, message: 'failed', result: [] });
                            return res.json(data);
                        });
                    });
                }
            }
        });
    };

    // PROSPECT ALL
    this.selectProspectAll = function(req, res, next) {
        var id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT count(id) prospect_all from customer_prospect where salesman_id IN (SELECT id from salesmen where supervisor_id = "' + id + '") and MONTH(created) = MONTH(CURRENT_DATE())', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ prospect_all: 0 });
                    // console.log(err);

                } else {
                    if (data[0] != null) {
                        var prospect = data[0].prospect_all;
                        var data_prospect = prospect.toString();
                        return res.json({ prospect_all: data_prospect });
                    } else {
                        return res.json({ prospect_all: "0" });
                    }

                }

            });
        });
    };

    this.selectProspectAllDMS = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        var idSales = [];
        request.get({
            uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
            auth: {
                'bearer': '71D55F9957529'
            },
            rejectUnauthorized: false, //add when working with https sites
            requestCert: false, //add when working with https sites
            agent: false, //add when working with https sites
        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                obj = JSON.parse(body);

                obj.forEach(function(item) {
                    idSales.push("'" + item.id + "'");
                });

                // console.log(supervisor_id);
                // console.log(idSales);

                if (idSales.length === 0) {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT count(id) prospect_all from customer_prospect where salesman_id IN ('" + idSales + "') and MONTH(created) = MONTH(CURRENT_DATE())", function(err, data) {
                            con.release();
                            if (err) {
                                // return res.json({ status: 400, message: 'failed', result: [] });
                                return res.json({ prospect_all: 0 });
                            } else {
                                if (data[0] != null) {
                                    var prospect = data[0].prospect_all;
                                    var data_prospect = prospect.toString();
                                    return res.json({ prospect_all: data_prospect });
                                } else {
                                    return res.json({ prospect_all: "0" });
                                }
                            }


                        });
                    });
                } else {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT count(id) prospect_all from customer_prospect where salesman_id IN (" + idSales + ") and MONTH(created) = MONTH(CURRENT_DATE())", function(err, data) {
                            con.release();
                            if (err) {
                                // return res.json({ status: 400, message: 'failed', result: [] });
                                return res.json({ prospect_all: 0 });
                            } else {
                                if (data[0] != null) {
                                    var prospect = data[0].prospect_all;
                                    var data_prospect = prospect.toString();
                                    return res.json({ prospect_all: data_prospect });
                                } else {
                                    return res.json({ prospect_all: "0" });
                                }
                            }
                        });
                    });
                }
            }
        });
    };

    // DEAL ALL
    this.selectDealAll = function(req, res, next) {
        var id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT count(id) deal_all from generate_spks where salesman_id IN (SELECT id from salesmen where supervisor_id = "' + id + '") and MONTH(created_at) = MONTH(CURRENT_DATE())', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ deal_all: 0 });
                } else {
                    if (data[0] != null) {
                        var deal = data[0].deal_all;
                        var data_deal = deal.toString();
                        return res.json({ deal_all: data_deal });
                    } else {
                        return res.json({ deal_all: "0" });
                    }
                }
            });
        });
    };

    this.selectDealAllDMS = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        var idSales = [];
        request.get({
            uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
            auth: {
                'bearer': '71D55F9957529'
            },
            rejectUnauthorized: false, //add when working with https sites
            requestCert: false, //add when working with https sites
            agent: false, //add when working with https sites
        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                obj = JSON.parse(body);

                obj.forEach(function(item) {
                    idSales.push("'" + item.id + "'");
                });

                // console.log(supervisor_id);
                // console.log(idSales);

                if (idSales.length === 0) {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT count(id) deal_all from generate_spks where salesman_id IN ('" + idSales + "') and MONTH(created_at) = MONTH(CURRENT_DATE())", function(err, data) {
                            con.release();
                            if (err) {
                                return res.json({ deal_all: 0 });
                            } else {
                                if (data[0] != null) {
                                    var deal = data[0].deal_all;
                                    var data_deal = deal.toString();
                                    return res.json({ deal_all: data_deal });
                                } else {
                                    return res.json({ deal_all: "0" });
                                }
                            }
                        });
                    });
                } else {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT count(id) deal_all from generate_spks where salesman_id IN (" + idSales + ") and MONTH(created_at) = MONTH(CURRENT_DATE())", function(err, data) {
                            con.release();
                            if (err) {
                                return res.json({ deal_all: 0 });
                            } else {
                                if (data[0] != null) {
                                    var deal = data[0].deal_all;
                                    var data_deal = deal.toString();
                                    return res.json({ deal_all: data_deal });
                                } else {
                                    return res.json({ deal_all: "0" });
                                }
                            }
                        });
                    });
                }
            }
        });
    };

    // DETAIL PROSPECT ALL
    this.selectDetailProspectAll = function(req, res, next) {
        var id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT sum(prospect) prospect, salesman_id, sls_nama, supervisor_nama from (SELECT count(c.id) prospect, salesman_id, s.sls_nama, spv.supervisor_nama from customer_prospect c, salesmen s, supervisors spv where c.salesman_id = s.id and c.supervisor_id = spv.id and MONTH(c.created) = MONTH(CURRENT_DATE()) and salesman_id IN (SELECT id from salesmen where supervisor_id = "' + id + '") group by c.salesman_id)x group by salesman_id ', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectDetailProspectAllDMS = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        var getId = [];
        var getNama = [];
        var getkode = [];
        request.get({
            uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
            auth: {
                'bearer': '71D55F9957529'
            },
            rejectUnauthorized: false, //add when working with https sites
            requestCert: false, //add when working with https sites
            agent: false, //add when working with https sites
        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                obj = JSON.parse(body);

                obj.forEach(function(item) {
                    getId.push("'" + item.id + "'");
                    getNama.push("'" + item.sls_nama + "', ");
                    getkode.push("'" + item.sls_kode + "'");
                });
                if (getId.length === 0) {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT sum(prospect) prospect, salesman_id from (SELECT count(id) prospect, salesman_id from customer_prospect where salesman_id IN (" + getId + ") and MONTH(created) = MONTH(CURRENT_DATE()) group by salesman_id)x group by salesman_id", function(err, data) {
                            con.release();
                            if (err)
                                return res.json({ status: 400, message: 'failed', result: [] });
                            return res.json(data);
                        });
                    });
                } else {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT sum(prospect) prospect, salesman_id from (SELECT count(id) prospect, salesman_id from customer_prospect where salesman_id IN (" + getId + ") and MONTH(created) = MONTH(CURRENT_DATE()) group by salesman_id)x group by salesman_id", function(err, data) {
                            con.release();

                            data.forEach(function(element, index) {
                                var id_sales = element.salesman_id;

                                obj.forEach(function(detail, i) {
                                    var id_sls = detail.id;
                                    if (id_sls == id_sales) {
                                        data[index].slsNama = detail.sls_nama;
                                        data[index].slsKode = detail.sls_kode;
                                    }
                                });

                            });
                            console.log(data);

                            if (err)
                                return res.json({ status: 400, message: 'failed', result: [] });
                            return res.json(data);
                        });
                    });
                }
            }
        });
    };

    // DETAIL Deal ALL
    this.selectDetailDealAll = function(req, res, next) {
        var id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT sum(deal) deal, salesman_id, sls_nama, supervisor_nama from (SELECT count(g.id) deal, salesman_id, s.sls_nama, spv.supervisor_nama from generate_spks g, salesmen s, supervisors spv where g.salesman_id = s.id and s.supervisor_id = spv.id and MONTH(g.created_at) = MONTH(CURRENT_DATE()) and salesman_id IN (SELECT id from salesmen where supervisor_id ="' + id + '") group by g.salesman_id)x group by salesman_id', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectDetailDealAllDMS = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        var getId = [];
        var getNama = [];
        var getkode = [];
        request.get({
            uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
            auth: {
                'bearer': '71D55F9957529'
            },
            rejectUnauthorized: false, //add when working with https sites
            requestCert: false, //add when working with https sites
            agent: false, //add when working with https sites
        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                obj = JSON.parse(body);

                obj.forEach(function(item) {
                    getId.push("'" + item.id + "'");
                    getNama.push("'" + item.sls_nama + "', ");
                    getkode.push("'" + item.sls_kode + "'");
                });
                if (getId.length === 0) {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT sum(deal) deal, salesman_id from (SELECT count(id) deal, salesman_id from generate_spks where  salesman_id IN (" + getId + ") and MONTH(created_at) = MONTH(CURRENT_DATE()) group by salesman_id)x group by salesman_id", function(err, data) {
                            con.release();
                            if (err)
                                return res.json({ status: 400, message: 'failed', result: [] });
                            return res.json(data);
                        });
                    });
                } else {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT sum(deal) deal, salesman_id from (SELECT count(id) deal, salesman_id from generate_spks where  salesman_id IN (" + getId + ") and MONTH(created_at) = MONTH(CURRENT_DATE()) group by salesman_id)x group by salesman_id", function(err, data) {
                            con.release();

                            data.forEach(function(element, index) {
                                var id_sales = element.salesman_id;

                                obj.forEach(function(detail, i) {
                                    var id_sls = detail.id;
                                    if (id_sls == id_sales) {
                                        data[index].slsNama = detail.sls_nama;
                                        data[index].slsKode = detail.sls_kode;
                                    }
                                });

                            });
                            console.log(data);

                            if (err)
                                return res.json({ status: 400, message: 'failed', result: [] });
                            return res.json(data);
                        });
                    });
                }
            }
        });
    };

}
module.exports = new Todo();