//Auth : Lely
var connection = require('../../config/db');
var request = require('request');

function Todo() {
    const fs = require('fs');

    this.selectMasterTargetProspect = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        var date = new Date();
        var periode = dateFormat(date, "yyyy-mm");
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT sum(target) target, sum(now) now from (SELECT target_prospect target, 0 as now from master_targets m where salesman_id = "' + salesman_id + '" and periode ="' + periode + '" UNION ALL SELECT 0 as target, count(id) as now from customer_prospect c where salesman_id = "' + salesman_id + '" and SUBSTR(created, 1, 7) = "' + periode + '") x', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'failed', result: [] });
                } else {
                    if (data[0] != null) {
                        var datas = data[0];
                        var target = datas.target;
                        var now = datas.now;
                        return res.json({ target_prospect: target, prospect: now })
                    } else {
                        return res.json({ target_prospect: 0, prospect: 0 })
                    }
                }
            });
        });
    };

    this.selectMasterTargetDeal = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        var date = new Date();
        var periode = dateFormat(date, "yyyy-mm");
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT sum(target) target, sum(now) now from (SELECT target_deal target, 0 as now from master_targets m where salesman_id = "' + salesman_id + '" and periode ="' + periode + '" UNION ALL SELECT 0 as target, count(id) as now from generate_spks g where salesman_id = "' + salesman_id + '" and SUBSTR(created_at, 1, 7) = "' + periode + '") x', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'failed', result: [] });
                } else {
                    if (data[0] != null) {
                        // return res.json(data[0]);
                        var datas = data[0];
                        var target = datas.target;
                        var now = datas.now;
                        return res.json({ target_deal: target, deal: now })
                    } else {
                        return res.json({ target_deal: 0, deal: 0 })
                    }
                }
            });
        });
    };

    this.selectMasterTargetSales = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        var date = new Date();
        var periode = dateFormat(date, "yyyy-mm");
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select target_sales, 0 as sales from master_targets where salesman_id = "' + salesman_id + '" and periode = "' + periode + '"', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'failed', result: [] });
                } else {
                    if (data[0] != null) {
                        var datas = data[0];
                        var target = datas.target_sales;
                        var sales = datas.sales;
                        return res.json({ target_sales: target, sales: sales })
                    } else {
                        return res.json({ target_sales: 0, sales: 0 })
                    }
                }
            });
        });
    };
}
module.exports = new Todo();