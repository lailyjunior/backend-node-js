//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    const fs = require('fs');
    var request = require('request');

    this.selectReligion = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, code, religion FROM master_religion order by code', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectPosCode = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, pos_kode, pos_lurah, pos_camat, pos_kota, propinsi_name pos_prop, kota_id, propinsi_id FROM master_poscodes order by pos_kode', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectJenisCustomer = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, code, jenis FROM master_jenis_customer order by code', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectTypeCustomer = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, code, type FROM master_type_customer order by code desc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectBranches = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id branch_id, branch_code, name branch_name, company_id, company_code FROM branches', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectOccupation = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, code,occupation FROM master_occupation', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectSumber = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, code, deskripsi FROM master_sumber order by code desc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    this.selectPameran = function(req, res, next) {
        var sls_kode = req.query.sls_kode;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, event_code, title, detail description,start start_date,end end_date, location_name FROM transaksi_new_events where versi = 0 and sls_kode = "' + sls_kode + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    this.selectEmployee = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, employee_kode, employee_nama, employee_jabatan FROM employees', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    this.selectCustomerProspect = function(req, res, next) {
        var salesman_id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT customer_prospect.id id,cust_code,title,cust_nama name,cust_pic,cust_org,cust_noktp,cust_alamat alamat,noktp_stnk,poscode_id,pos_kode,pos_camat,pos_lurah,pos_kota,pos_prop,alamat_invoice,alamat_kirim,cust_telp_rumah,cust_telp_kantor,cust_fax,cust_hp no_hp,cust_hp_wa,cust_email,cust_tgl_lahir,cust_agama,occupation_id,kerja_kode,cust_kelamin,cust_jenis,cust_npwp,salesman_id,assigned_to,cust_sumber,cust_npk,cust_kendaraan,branch_id,branch_code,company_id,company_code,cust_type,create_by,modi_by,created,modified,kunci,kota_id,propinsi_id,nama_stnk,alamat_stnk,alamat_lengkap,npwp_stnk,cust_alamat2,cek_alamat_kirim,pos_kota2,pos_prop2,propinsi_id2,pos_lurah2,pos_kode2,kota_id2,pos_camat2,lead_code,lead_id,cust_rt,cust_rw,dealer_id,dealer_code,rw_stnk,rt_stnk,status_deal,id_motor,kode_motor,deskripsi_motor,doc_1,doc_2,doc_3,doc_4,doc_5,id_event,kode_event,nama_event,lokasi_event,id_employee,employee_kode,employee_nama,employee_jabatan,status_generate_customer, UPPER(master_status.status) status_profile FROM customer_prospect, master_status WHERE master_status.code = customer_prospect.status_profile AND salesman_id = "' + salesman_id + '" order by cust_nama asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectCustomerProspectLov = function(req, res, next) {
        var branch_code = req.query.branch_code;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * from customers where branch_code = "' + branch_code + '" order by cust_nama asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.insertCustomerProspect = function(req, res) {
        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }
        var fileUploads = req.files;
        var doc_1 = null;
        var doc_2 = null;
        var doc_3 = null;
        var doc_4 = null;
        var doc_5 = null;

        fileUploads.forEach(function(element, index) {
            if (index == 0) {
                doc_1 = "/files/" + element.filename
            } else if (index == 1) {
                doc_2 = "/files/" + element.filename
            } else if (index == 2) {
                doc_3 = "/files/" + element.filename
            } else if (index == 3) {
                doc_4 = "/files/" + element.filename
            } else if (index == 4) {
                doc_5 = "/files/" + element.filename
            }
        });

        var id = uuidv1();
        // var cust_code = req.body.cust_code;
        var cust_nama = req.body.cust_nama;
        var cust_hp = req.body.cust_hp;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;
        var cust_alamat_lengkap = req.body.alamat_lengkap;

        var cust_pos_id = req.body.poscode_id;
        var cust_pos_code = req.body.pos_kode;
        var cust_pos_lurah = req.body.pos_lurah;
        var cust_pos_camat = req.body.pos_camat;
        var cust_pos_kota = req.body.pos_kota;
        var cust_pos_propinsi = req.body.pos_prop;
        var cust_pos_kotaID = req.body.kota_id;
        var cust_pos_propinsiID = req.body.propinsi_id;

        var cust_email = req.body.cust_email;
        var cust_jenis_kelamin = req.body.cust_kelamin;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_agama = req.body.cust_agama;
        var cust_pekerjaan = req.body.kerja_kode;
        var cust_npwp = req.body.cust_npwp;
        var cust_jenis = req.body.cust_jenis;
        var cust_type = req.body.cust_type;
        var cust_pic = req.body.cust_pic;
        var cust_noktp = req.body.cust_noktp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_telp_kantor = req.body.cust_telp_kantor;
        var cust_fax = req.body.cust_fax;

        var id_motor = req.body.id_motor;
        var cust_kendaraan = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var created = new Date();
        var modified = new Date();
        var create_by = req.body.id;
        var modi_by = req.body.id;
        var salesman_id = req.body.salesman_id;
        var branch_id = req.body.branch_id;
        var occupation_id = req.body.occupation_id;
        var cust_hp_wa = req.body.cust_hp_wa;
        var cust_code = null;
        var cust_sumber = req.body.cust_sumber;

        var status_profile = '1';
        var status_deal = '1';
        var status_generate_customer = '0';

        request.get({
            uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_branch.php?id=' + branch_id,
            auth: {
                'bearer': '71D55F9957529'
            },
            rejectUnauthorized: false, //add when working with https sites
            requestCert: false, //add when working with https sites
            agent: false, //add when working with https sites
        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                obj = JSON.parse(body);
                console.log(obj);

                var branch_code = obj.branch_code;
                var company_id = obj.company_id;
                var company_code = obj.company_code;

                connection.acquire(function(err, con) {
                    con.query('select cust_code from customer_prospect order by created desc limit 1', function(err, result) {
                        con.release();
                        if (err) {
                            res.send({ status: 400, message: 'Customer Prospect Failed' });
                        } else {
                            // for CODE CP
                            if (result.length != 0) {
                                var test = result[0].cust_code;
                            } else {
                                var test = "CP-000000000000"
                            }

                            var pieces = test.split('-');
                            var prefix = pieces[0];
                            var lastNum = pieces[1];
                            lastNum = parseInt(lastNum, 10);
                            lastNum++;
                            cust_code = "CP-" + ("000000000000" + lastNum).substr(-11);

                            // return;
                            connection.acquire(function(err, con) {
                                con.query('INSERT INTO customer_prospect (id, cust_code, cust_nama, cust_hp, cust_alamat, cust_rt, cust_rw, alamat_lengkap, poscode_id, pos_kode, pos_lurah, pos_camat, pos_kota, pos_prop, kota_id, propinsi_id, cust_email, cust_kelamin, cust_tgl_lahir, cust_agama, kerja_kode, cust_npwp, cust_jenis, cust_type, cust_pic, cust_noktp, cust_telp_rumah, cust_telp_kantor, cust_fax, branch_id, branch_code, company_id, company_code, status_profile, created, modified, create_by, modi_by, salesman_id, occupation_id,cust_hp_wa, doc_1,doc_2,doc_3,doc_4,doc_5,id_motor,cust_kendaraan, kode_motor, deskripsi_motor, id_event, nama_event, kode_event, lokasi_event, status_deal, cust_sumber, id_employee, employee_kode, employee_nama, employee_jabatan, status_generate_customer) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, cust_code, cust_nama, cust_hp, cust_alamat, cust_rt, cust_rw, cust_alamat_lengkap, cust_pos_id, cust_pos_code, cust_pos_lurah, cust_pos_camat, cust_pos_kota, cust_pos_propinsi, cust_pos_kotaID, cust_pos_propinsiID, cust_email, cust_jenis_kelamin, cust_tgl_lahir, cust_agama, cust_pekerjaan, cust_npwp, cust_jenis, cust_type, cust_pic, cust_noktp, cust_telp_rumah, cust_telp_kantor, cust_fax, branch_id, branch_code, company_id, company_code, status_profile, created, modified, create_by, modi_by, salesman_id, occupation_id, cust_hp_wa, doc_1, doc_2, doc_3, doc_4, doc_5, id_motor, cust_kendaraan, kode_motor, deskripsi_motor, id_event, nama_event, kode_event, lokasi_event, status_deal, cust_sumber, id_employee, employee_kode, employee_nama, employee_jabatan, status_generate_customer], function(err, result) {
                                    con.release();
                                    if (err) {
                                        res.send({ status: 400, message: 'Customer Prospect Failed' });
                                    } else {
                                        res.send({ status: 200, message: 'Customer Prospect Success' });
                                    }
                                });
                            });
                        }
                    });
                });
            }
        });
    };

    this.insertCustomerProspectNonDms = function(req, res) {
        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }
        var fileUploads = req.files;
        var doc_1 = null;
        var doc_2 = null;
        var doc_3 = null;
        var doc_4 = null;
        var doc_5 = null;

        fileUploads.forEach(function(element, index) {
            if (index == 0) {
                doc_1 = "/files/" + element.filename
            } else if (index == 1) {
                doc_2 = "/files/" + element.filename
            } else if (index == 2) {
                doc_3 = "/files/" + element.filename
            } else if (index == 3) {
                doc_4 = "/files/" + element.filename
            } else if (index == 4) {
                doc_5 = "/files/" + element.filename
            }
        });

        var id = uuidv1();
        // var cust_code = req.body.cust_code;
        var cust_nama = req.body.cust_nama;
        var cust_hp = req.body.cust_hp;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;
        var cust_alamat_lengkap = req.body.alamat_lengkap;

        var cust_pos_id = req.body.poscode_id;
        var cust_pos_code = req.body.pos_kode;
        var cust_pos_lurah = req.body.pos_lurah;
        var cust_pos_camat = req.body.pos_camat;
        var cust_pos_kota = req.body.pos_kota;
        var cust_pos_propinsi = req.body.pos_prop;
        var cust_pos_kotaID = req.body.kota_id;
        var cust_pos_propinsiID = req.body.propinsi_id;

        var cust_email = req.body.cust_email;
        var cust_jenis_kelamin = req.body.cust_kelamin;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_agama = req.body.cust_agama;
        var cust_pekerjaan = req.body.kerja_kode;
        var cust_npwp = req.body.cust_npwp;
        var cust_jenis = req.body.cust_jenis;
        var cust_type = req.body.cust_type;
        var cust_pic = req.body.cust_pic;
        var cust_noktp = req.body.cust_noktp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_telp_kantor = req.body.cust_telp_kantor;
        var cust_fax = req.body.cust_fax;

        var id_motor = req.body.id_motor;
        var cust_kendaraan = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var created = new Date();
        var modified = new Date();
        var create_by = req.body.id;
        var modi_by = req.body.id;
        var salesman_id = req.body.salesman_id;
        var branch_id = req.body.branch_id;
        var occupation_id = req.body.occupation_id;
        var cust_hp_wa = req.body.cust_hp_wa;
        var cust_code = null;
        var cust_sumber = req.body.cust_sumber;

        var status_profile = '1';
        var status_deal = '1';
        var status_generate_customer = '0';

        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, branch_code, name, company_id, company_code from branch_nondms where id = "' + branch_id + '"', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'failed', result: [] });
                } else {
                    var branch_code = data[0].branch_code;
                    var company_id = data[0].company_id;
                    var company_code = data[0].company_code;

                    connection.acquire(function(err, con) {
                        con.query('select cust_code from customer_prospect order by created desc limit 1', function(err, result) {
                            con.release();
                            if (err) {
                                res.send({ status: 400, message: 'Customer Prospect Failed' });
                            } else {
                                // for CODE CP
                                if (result.length != 0) {
                                    var test = result[0].cust_code;
                                } else {
                                    var test = "CP-000000000000"
                                }

                                var pieces = test.split('-');
                                var prefix = pieces[0];
                                var lastNum = pieces[1];
                                lastNum = parseInt(lastNum, 10);
                                lastNum++;
                                cust_code = "CP-" + ("000000000000" + lastNum).substr(-11);

                                // return;
                                connection.acquire(function(err, con) {
                                    con.query('INSERT INTO customer_prospect (id, cust_code, cust_nama, cust_hp, cust_alamat, cust_rt, cust_rw, alamat_lengkap, poscode_id, pos_kode, pos_lurah, pos_camat, pos_kota, pos_prop, kota_id, propinsi_id, cust_email, cust_kelamin, cust_tgl_lahir, cust_agama, kerja_kode, cust_npwp, cust_jenis, cust_type, cust_pic, cust_noktp, cust_telp_rumah, cust_telp_kantor, cust_fax, branch_id, branch_code, company_id, company_code, status_profile, created, modified, create_by, modi_by, salesman_id, occupation_id,cust_hp_wa, doc_1,doc_2,doc_3,doc_4,doc_5,id_motor,cust_kendaraan, kode_motor, deskripsi_motor, id_event, nama_event, kode_event, lokasi_event, status_deal, cust_sumber, id_employee, employee_kode, employee_nama, employee_jabatan, status_generate_customer) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, cust_code, cust_nama, cust_hp, cust_alamat, cust_rt, cust_rw, cust_alamat_lengkap, cust_pos_id, cust_pos_code, cust_pos_lurah, cust_pos_camat, cust_pos_kota, cust_pos_propinsi, cust_pos_kotaID, cust_pos_propinsiID, cust_email, cust_jenis_kelamin, cust_tgl_lahir, cust_agama, cust_pekerjaan, cust_npwp, cust_jenis, cust_type, cust_pic, cust_noktp, cust_telp_rumah, cust_telp_kantor, cust_fax, branch_id, branch_code, company_id, company_code, status_profile, created, modified, create_by, modi_by, salesman_id, occupation_id, cust_hp_wa, doc_1, doc_2, doc_3, doc_4, doc_5, id_motor, cust_kendaraan, kode_motor, deskripsi_motor, id_event, nama_event, kode_event, lokasi_event, status_deal, cust_sumber, id_employee, employee_kode, employee_nama, employee_jabatan, status_generate_customer], function(err, result) {
                                        con.release();
                                        if (err) {
                                            res.send({ status: 400, message: 'Customer Prospect Failed' });
                                        } else {
                                            res.send({ status: 200, message: 'Customer Prospect Success' });
                                        }
                                    });
                                });
                            }
                        });
                    });
                }
            });
        });
    };

    this.selectDetailCP = function(req, res, next) {

        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM customer_prospect where id = "' + id + '"', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'failed', result: [] });
                } else {
                    if (data.length > 0) {
                        return res.json(data[0]);
                    }
                }
            });
        });
    };

    this.updateCustomerProspect = function(req, res) {
        // res.send(req.body.data);
        // return;
        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        var idcp = req.params.id;
        var cust_nama = req.body.cust_nama;
        var cust_hp = req.body.cust_hp;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;

        var cust_pos_id = req.body.poscode_id;
        var cust_pos_code = req.body.pos_kode;
        var cust_pos_lurah = req.body.pos_lurah;
        var cust_pos_camat = req.body.pos_camat;
        var cust_pos_kota = req.body.pos_kota;
        var cust_pos_propinsi = req.body.pos_prop;
        var cust_pos_kotaID = req.body.kota_id;
        var cust_pos_propinsiID = req.body.propinsi_id;

        var cust_email = req.body.cust_email;
        var cust_jenis_kelamin = req.body.cust_kelamin;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_agama = req.body.cust_agama;
        var cust_pekerjaan = req.body.kerja_kode;
        var cust_npwp = req.body.cust_npwp;
        var cust_jenis = req.body.cust_jenis;
        var cust_type = req.body.cust_type;
        var cust_pic = req.body.cust_pic;
        var cust_noktp = req.body.cust_noktp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_telp_kantor = req.body.cust_telp_kantor;
        var cust_fax = req.body.cust_fax;
        var modified = new Date();
        var modi_by = req.body.id;
        var salesman_id = req.body.salesman_id;
        var occupation_id = req.body.occupation_id;
        var cust_hp_wa = req.body.cust_hp_wa;

        var fileUploads = req.files;
        var docs = [];

        var id_motor = req.body.id_motor;
        var cust_kendaraan = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var cust_sumber = req.body.cust_sumber;
        var id_event = req.body.id_event;
        var nama_event = req.body.nama_event;
        var kode_event = req.body.kode_event;
        var lokasi_event = req.body.lokasi_event;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT doc_1,doc_2,doc_3,doc_4,doc_5 FROM customer_prospect where id = "' + idcp + '"', function(err, data) {
                con.release();
                if (err) {
                    console.log("error");
                } else {
                    fileUploads.forEach(function(element, index) {
                        var image = data[0][element.fieldname];
                        if (image) {
                            fs.unlink(appRoot + image.replace("/files/", "/uploads/"), (err) => {
                                if (err) throw err;
                            });
                        }
                        docs[element.fieldname] = "/files/" + element.filename;
                    });
                    connection.acquire(function(err, conn) {
                        var queryStr = 'UPDATE customer_prospect set cust_nama =?, cust_hp = ?, cust_alamat = ?, cust_rt = ?, cust_rw = ? , poscode_id =? , pos_kode = ?, pos_lurah = ?, pos_camat = ?, pos_kota = ? , pos_prop = ?, kota_id = ?, propinsi_id = ?, cust_email =?, cust_kelamin =?, cust_tgl_lahir=?, cust_agama=?, kerja_kode=?, cust_npwp=?, cust_jenis=?, cust_type=?, cust_pic=?, cust_noktp=?, cust_telp_rumah=?, cust_telp_kantor =?, cust_fax=?, modified=?, modi_by=?, salesman_id=?, occupation_id=?, cust_hp_wa=?, id_motor =?, cust_kendaraan=?, kode_motor=?, deskripsi_motor=?, id_event=?, nama_event=?, kode_event=?, lokasi_event=?, cust_sumber=?, id_employee=?, employee_kode=?, employee_nama=?, employee_jabatan=?';

                        var queryParams = [cust_nama, cust_hp, cust_alamat, cust_rt, cust_rw, cust_pos_id, cust_pos_code, cust_pos_lurah, cust_pos_camat, cust_pos_kota, cust_pos_propinsi, cust_pos_kotaID, cust_pos_propinsiID, cust_email, cust_jenis_kelamin, cust_tgl_lahir, cust_agama, cust_pekerjaan, cust_npwp, cust_jenis, cust_type, cust_pic, cust_noktp, cust_telp_rumah, cust_telp_kantor, cust_fax, modified, modi_by, salesman_id, occupation_id, cust_hp_wa, id_motor, cust_kendaraan, kode_motor, deskripsi_motor, id_event, nama_event, kode_event, lokasi_event, cust_sumber, id_employee, employee_kode, employee_nama, employee_jabatan];

                        for (var key in docs) {
                            var value = docs[key];
                            queryStr = queryStr + ", " + key + " = ?";
                            queryParams.push(value);
                        }

                        queryStr = queryStr + ' where id = "' + idcp + '"'

                        conn.query(queryStr, queryParams, function(err, result) {
                            conn.release();
                            if (err) {
                                res.send({ status: 400, message: 'Edit Customer Prospect Failed' });
                            } else {
                                res.send({ status: 200, message: 'Edit Customer Prospect Success' });
                            }
                        });
                    });
                }
            });
        });

    };

    this.notifyActivitySalesman = function(req, res, next) {
        var id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select a.id id_cp, a.cust_code, a.cust_nama, a.cust_alamat, a.cust_hp, b.id id_act,b.activity_kode, b.activity_tgl, b.activity_subject from customer_prospect a, activities b where b.lead_id = a.id AND a.salesman_id = "' + id + '" AND b.activity_tgl >= CONVERT_TZ(NOW(),"+00:00","-07:00") order by b.activity_tgl', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getLovTypeMotor = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT m.id, m.deskripsi, m.mtr_type, m.mtr_kode product_kode FROM motors m', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    // FOR SUPERVISOR

    this.insertCustomerProspectSpv = function(req, res) {
        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }
        var fileUploads = req.files;
        var doc_1 = null;
        var doc_2 = null;
        var doc_3 = null;
        var doc_4 = null;
        var doc_5 = null;

        fileUploads.forEach(function(element, index) {
            if (index == 0) {
                doc_1 = "/files/" + element.filename
            } else if (index == 1) {
                doc_2 = "/files/" + element.filename
            } else if (index == 2) {
                doc_3 = "/files/" + element.filename
            } else if (index == 3) {
                doc_4 = "/files/" + element.filename
            } else if (index == 4) {
                doc_5 = "/files/" + element.filename
            }
        });

        var id = uuidv1();
        // var cust_code = req.body.cust_code;
        var cust_nama = req.body.cust_nama;
        var cust_hp = req.body.cust_hp;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;
        var cust_alamat_lengkap = req.body.alamat_lengkap;

        var cust_pos_id = req.body.poscode_id;
        var cust_pos_code = req.body.pos_kode;
        var cust_pos_lurah = req.body.pos_lurah;
        var cust_pos_camat = req.body.pos_camat;
        var cust_pos_kota = req.body.pos_kota;
        var cust_pos_propinsi = req.body.pos_prop;
        var cust_pos_kotaID = req.body.kota_id;
        var cust_pos_propinsiID = req.body.propinsi_id;

        var cust_email = req.body.cust_email;
        var cust_jenis_kelamin = req.body.cust_kelamin;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_agama = req.body.cust_agama;
        var cust_pekerjaan = req.body.kerja_kode;
        var cust_npwp = req.body.cust_npwp;
        var cust_jenis = req.body.cust_jenis;
        var cust_type = req.body.cust_type;
        var cust_pic = req.body.cust_pic;
        var cust_noktp = req.body.cust_noktp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_telp_kantor = req.body.cust_telp_kantor;
        var cust_fax = req.body.cust_fax;

        var id_motor = req.body.id_motor;
        var cust_kendaraan = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var created = new Date();
        var modified = new Date();
        var create_by = req.body.id;
        var modi_by = req.body.id;
        var salesman_id = req.body.salesman_id;
        var supervisor_id = req.body.supervisor_id;
        var branch_id = req.body.branch_id;
        var occupation_id = req.body.occupation_id;
        var cust_hp_wa = req.body.cust_hp_wa;
        var cust_code = null;
        var cust_sumber = req.body.cust_sumber;

        var status_profile = '1';
        var status_deal = '1';
        var status_generate_customer = '0';

        request.get({
            uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_branch.php?id=' + branch_id,
            auth: {
                'bearer': '71D55F9957529'
            },
            rejectUnauthorized: false, //add when working with https sites
            requestCert: false, //add when working with https sites
            agent: false, //add when working with https sites
        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                obj = JSON.parse(body);
                console.log(obj);

                var branch_code = obj.branch_code;
                var company_id = obj.company_id;
                var company_code = obj.company_code;

                connection.acquire(function(err, con) {
                    con.query('select cust_code from customer_prospect order by created desc limit 1', function(err, result) {
                        con.release();
                        if (err) {
                            res.send({ status: 400, message: 'Customer Prospect Failed' });
                        } else {
                            // for CODE CP
                            if (result.length != 0) {
                                var test = result[0].cust_code;
                            } else {
                                var test = "CP-000000000000"
                            }

                            var pieces = test.split('-');
                            var prefix = pieces[0];
                            var lastNum = pieces[1];
                            lastNum = parseInt(lastNum, 10);
                            lastNum++;
                            cust_code = "CP-" + ("000000000000" + lastNum).substr(-11);

                            // return;
                            connection.acquire(function(err, con) {
                                con.query('INSERT INTO customer_prospect (id, cust_code, cust_nama, cust_hp, cust_alamat, cust_rt, cust_rw, alamat_lengkap, poscode_id, pos_kode, pos_lurah, pos_camat, pos_kota, pos_prop, kota_id, propinsi_id, cust_email, cust_kelamin, cust_tgl_lahir, cust_agama, kerja_kode, cust_npwp, cust_jenis, cust_type, cust_pic, cust_noktp, cust_telp_rumah, cust_telp_kantor, cust_fax, branch_id, branch_code, company_id, company_code, status_profile, created, modified, create_by, modi_by, salesman_id, supervisor_id, occupation_id,cust_hp_wa, doc_1,doc_2,doc_3,doc_4,doc_5,id_motor,cust_kendaraan, kode_motor, deskripsi_motor, id_event, nama_event, kode_event, lokasi_event, status_deal, cust_sumber, id_employee, employee_kode, employee_nama, employee_jabatan, status_generate_customer) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, cust_code, cust_nama, cust_hp, cust_alamat, cust_rt, cust_rw, cust_alamat_lengkap, cust_pos_id, cust_pos_code, cust_pos_lurah, cust_pos_camat, cust_pos_kota, cust_pos_propinsi, cust_pos_kotaID, cust_pos_propinsiID, cust_email, cust_jenis_kelamin, cust_tgl_lahir, cust_agama, cust_pekerjaan, cust_npwp, cust_jenis, cust_type, cust_pic, cust_noktp, cust_telp_rumah, cust_telp_kantor, cust_fax, branch_id, branch_code, company_id, company_code, status_profile, created, modified, create_by, modi_by, salesman_id, supervisor_id, occupation_id, cust_hp_wa, doc_1, doc_2, doc_3, doc_4, doc_5, id_motor, cust_kendaraan, kode_motor, deskripsi_motor, id_event, nama_event, kode_event, lokasi_event, status_deal, cust_sumber, id_employee, employee_kode, employee_nama, employee_jabatan, status_generate_customer], function(err, result) {
                                    con.release();
                                    if (err) {
                                        res.send({ status: 400, message: 'Customer Prospect Failed' });
                                    } else {
                                        res.send({ status: 200, message: 'Customer Prospect Success' });
                                    }
                                });
                            });
                        }
                    });
                });
            }
        });
    };

    this.insertCustomerProspectNonDmsSpv = function(req, res) {
        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }
        var fileUploads = req.files;
        var doc_1 = null;
        var doc_2 = null;
        var doc_3 = null;
        var doc_4 = null;
        var doc_5 = null;

        fileUploads.forEach(function(element, index) {
            if (index == 0) {
                doc_1 = "/files/" + element.filename
            } else if (index == 1) {
                doc_2 = "/files/" + element.filename
            } else if (index == 2) {
                doc_3 = "/files/" + element.filename
            } else if (index == 3) {
                doc_4 = "/files/" + element.filename
            } else if (index == 4) {
                doc_5 = "/files/" + element.filename
            }
        });

        var id = uuidv1();
        // var cust_code = req.body.cust_code;
        var cust_nama = req.body.cust_nama;
        var cust_hp = req.body.cust_hp;
        var cust_alamat = req.body.cust_alamat;
        var cust_rt = req.body.cust_rt;
        var cust_rw = req.body.cust_rw;
        var cust_alamat_lengkap = req.body.alamat_lengkap;

        var cust_pos_id = req.body.poscode_id;
        var cust_pos_code = req.body.pos_kode;
        var cust_pos_lurah = req.body.pos_lurah;
        var cust_pos_camat = req.body.pos_camat;
        var cust_pos_kota = req.body.pos_kota;
        var cust_pos_propinsi = req.body.pos_prop;
        var cust_pos_kotaID = req.body.kota_id;
        var cust_pos_propinsiID = req.body.propinsi_id;

        var cust_email = req.body.cust_email;
        var cust_jenis_kelamin = req.body.cust_kelamin;
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_agama = req.body.cust_agama;
        var cust_pekerjaan = req.body.kerja_kode;
        var cust_npwp = req.body.cust_npwp;
        var cust_jenis = req.body.cust_jenis;
        var cust_type = req.body.cust_type;
        var cust_pic = req.body.cust_pic;
        var cust_noktp = req.body.cust_noktp;
        var cust_telp_rumah = req.body.cust_telp_rumah;
        var cust_telp_kantor = req.body.cust_telp_kantor;
        var cust_fax = req.body.cust_fax;

        var id_motor = req.body.id_motor;
        var cust_kendaraan = req.body.type_motor;
        var kode_motor = req.body.kode_motor;
        var deskripsi_motor = req.body.deskripsi_motor;

        var id_event = req.body.id_event;
        var kode_event = req.body.kode_event
        var nama_event = req.body.nama_event;
        var lokasi_event = req.body.lokasi_event;

        var id_employee = req.body.id_employee;
        var employee_kode = req.body.employee_kode;
        var employee_nama = req.body.employee_nama;
        var employee_jabatan = req.body.employee_jabatan;

        var created = new Date();
        var modified = new Date();
        var create_by = req.body.id;
        var modi_by = req.body.id;
        var salesman_id = req.body.salesman_id;
        var supervisor_id = req.body.supervisor_id;
        var branch_id = req.body.branch_id;
        var occupation_id = req.body.occupation_id;
        var cust_hp_wa = req.body.cust_hp_wa;
        var cust_code = null;
        var cust_sumber = req.body.cust_sumber;

        var status_profile = '1';
        var status_deal = '1';
        var status_generate_customer = '0';

        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, branch_code, name, company_id, company_code from branch_nondms where id = "' + branch_id + '"', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'failed', result: [] });
                } else {
                    var branch_code = data[0].branch_code;
                    var company_id = data[0].company_id;
                    var company_code = data[0].company_code;

                    connection.acquire(function(err, con) {
                        con.query('select cust_code from customer_prospect order by created desc limit 1', function(err, result) {
                            con.release();
                            if (err) {
                                res.send({ status: 400, message: 'Customer Prospect Failed' });
                            } else {
                                // for CODE CP
                                if (result.length != 0) {
                                    var test = result[0].cust_code;
                                } else {
                                    var test = "CP-000000000000"
                                }

                                var pieces = test.split('-');
                                var prefix = pieces[0];
                                var lastNum = pieces[1];
                                lastNum = parseInt(lastNum, 10);
                                lastNum++;
                                cust_code = "CP-" + ("000000000000" + lastNum).substr(-11);

                                // return;
                                connection.acquire(function(err, con) {
                                    con.query('INSERT INTO customer_prospect (id, cust_code, cust_nama, cust_hp, cust_alamat, cust_rt, cust_rw, alamat_lengkap, poscode_id, pos_kode, pos_lurah, pos_camat, pos_kota, pos_prop, kota_id, propinsi_id, cust_email, cust_kelamin, cust_tgl_lahir, cust_agama, kerja_kode, cust_npwp, cust_jenis, cust_type, cust_pic, cust_noktp, cust_telp_rumah, cust_telp_kantor, cust_fax, branch_id, branch_code, company_id, company_code, status_profile, created, modified, create_by, modi_by, salesman_id, supervisor_id, occupation_id,cust_hp_wa, doc_1,doc_2,doc_3,doc_4,doc_5,id_motor,cust_kendaraan, kode_motor, deskripsi_motor, id_event, nama_event, kode_event, lokasi_event, status_deal, cust_sumber, id_employee, employee_kode, employee_nama, employee_jabatan, status_generate_customer) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, cust_code, cust_nama, cust_hp, cust_alamat, cust_rt, cust_rw, cust_alamat_lengkap, cust_pos_id, cust_pos_code, cust_pos_lurah, cust_pos_camat, cust_pos_kota, cust_pos_propinsi, cust_pos_kotaID, cust_pos_propinsiID, cust_email, cust_jenis_kelamin, cust_tgl_lahir, cust_agama, cust_pekerjaan, cust_npwp, cust_jenis, cust_type, cust_pic, cust_noktp, cust_telp_rumah, cust_telp_kantor, cust_fax, branch_id, branch_code, company_id, company_code, status_profile, created, modified, create_by, modi_by, salesman_id, supervisor_id, occupation_id, cust_hp_wa, doc_1, doc_2, doc_3, doc_4, doc_5, id_motor, cust_kendaraan, kode_motor, deskripsi_motor, id_event, nama_event, kode_event, lokasi_event, status_deal, cust_sumber, id_employee, employee_kode, employee_nama, employee_jabatan, status_generate_customer], function(err, result) {
                                        con.release();
                                        if (err) {
                                            res.send({ status: 400, message: 'Customer Prospect Failed' });
                                        } else {
                                            res.send({ status: 200, message: 'Customer Prospect Success' });
                                        }
                                    });
                                });
                            }
                        });
                    });
                }
            });
        });
    };
}
module.exports = new Todo();