//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    this.selectListTraining = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('Select t.id,kuota, remaining_quota, training_schedule_id, training_code, training_name,training_schedule_start_date, training_schedule_end_date, sls_kode, sls_nama, t.created_at tgl_daftar, salesman_id, case when (salesman_id is not null and salesman_id = "' + salesman_id + '") then "true" else "false" end as hasRegistered,status_approve from training_schedules t LEFT JOIN training_salesman s ON t.id = s.training_schedule_id group by id', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectTrainingSalesman = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM training_salesman', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.insertTrainingSalesman = function(req, res) {
        var id = uuidv1();
        var salesmanId = req.body.salesman_id
        var hondaid = req.body.hondaid
        var trainingScheduleId = req.body.training_schedule_id
        var slsNama = req.body.sls_nama
        var email = req.body.email
        var phoneNumber = req.body.phone_number
        var slsKode = req.body.sls_kode
        var gender = req.body.gender
        var branchId = req.body.branch_id
        var branchCode = req.body.branch_code
        var supervisorId = req.body.supervisor_id
        var created_at = new Date();
        var modified_at = new Date();
        var created_by = req.body.salesman_id;
        var modi_by = req.body.salesman_id;
        var versi = req.body.versi;
        var status_approve = "0";

        connection.acquire(function(err, con) {
            con.query('select kuota from training_schedules where id = "' + trainingScheduleId + '"',
                function(err, row) {
                    con.release();
                    if (err) {
                        console.log(err)
                    } else {
                        var kuota = row[0].kuota;
                        var now_quota = kuota - 1;

                        con.query('INSERT INTO training_salesman (id, salesman_id, hondaid,training_schedule_id,sls_nama,sls_kode,email,phone_number,gender,branch_id,branch_code,supervisor_id, status_approve, created_at, modified_at, created_by, modified_by,versi) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, salesmanId, hondaid, trainingScheduleId, slsNama, slsKode, email, phoneNumber, gender, branchId, branchCode, supervisorId, status_approve, created_at, modified_at, created_by, modi_by, versi],
                            function(err, result) {
                                if (err) {
                                    res.send({ status: 400, message: 'Master training creation failed' });
                                } else {
                                    con.query('UPDATE training_schedules SET remaining_quota = "' + now_quota + '" where id = "' + trainingScheduleId + '"',
                                        function(err, result) {
                                            if (err) {
                                                res.send({ status: 400, message: 'failed' });
                                            } else {
                                                res.send({ status: 200, message: 'success' });
                                            }
                                        });
                                }
                            });


                    }
                });
        });
    };

    this.selectTrainingregister = function(req, res, next) {
        var id = req.query.id;
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id FROM training_salesman where training_schedule_id = "' + id + '" and salesman_id = "' + salesman_id + '" ', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: '400', message: 'Failed', result: [] });
                } else {
                    if (data.length > 0) {
                        return res.json({ status: "Terdaftar" });
                    }
                }
            });
        });
    };
}
module.exports = new Todo();