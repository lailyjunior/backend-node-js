//Auth : Anggi
var connection = require('../../config/db');

var request = require('request');

function Todo() {

    this.getAllSupervisorCustomer = function(req, res, next) {
        var id = req.query.supervisor_id;
        var dataSales = [];
        request.get({
            uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + id,
            auth: {
                'bearer': '71D55F9957529'
            },
            rejectUnauthorized: false, //add when working with https sites
            requestCert: false, //add when working with https sites
            agent: false, //add when working with https sites

        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                console.log(body);
                obj = JSON.parse(body);

                obj.forEach(function(item) {
                    dataSales.push("'" + item.id + "'");
                });

                console.log(id);
                console.log(dataSales);

                connection.acquire(function(err, con) {
                    if (err) throw err;
                    con.query("SELECT * FROM customers WHERE salesman_id IN (" + dataSales + ") GROUP BY cust_nama asc", function(err, data) {
                        con.release();
                        if (err)
                                return res.json({ status: 400, message: 'failed', result: [] });
                            return res.json(data);
                    });
                });
            }
        });

    };

    this.getSupervisorCustomerBySalesman = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM customers WHERE salesman_id= "' + salesman_id + '" GROUP BY cust_nama asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getAllSupervisorCustomerNonDMS = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.*, sl.sls_nama , sp.id supervisor_id, sp.supervisor_nama from customers c, salesmen sl, supervisors sp WHERE c.salesman_id = sl.id and sl.supervisor_id=sp.id and sp.id="' + supervisor_id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

}

module.exports = new Todo();