//Auth : Ratri
var connection = require('../../config/db');
 
function Todo() {
  this.selectImageMotor = function(req,res,next) {
     connection.acquire(function(err,con){
        if (err) throw err;
          con.query('SELECT * FROM galerys GROUP BY mtr_grp_segment', function(err,data){
            con.release();
            if(err)
              return res.json({status:400,message: 'failed',result:[]});
              return res.json(data);
        });
     });
  };

  this.selectTypeGroup = function(req,res,next) {
    connection.acquire(function(err,con){
      var mtr_grp_segment = req.query.mtr_grp_segment;
       if (err) throw err;
         con.query('SELECT mtr_type_group FROM galerys WHERE mtr_grp_segment = "'+mtr_grp_segment+'" group by mtr_type_group', function(err,data){
           con.release();
           if(err)
             return res.json({status:400,message: 'failed',result:[]});
             return res.json(data);
       });
    });
 };

 this.selectTypeGroupDetail = function(req,res,next) {
  connection.acquire(function(err,con){
    var mtr_type_group = req.query.mtr_type_group;
     if (err) throw err;
       con.query('SELECT m.id, m.mtr_kode, m.mtr_type as motor_type, m.mtr_grp_segment, m.mtr_type_group, m.mtr_type_desc, mw.product_nama as description, mw.product_kode, mw.wrn_kode, mwpd.mtr_hrgbeli, mwpd.mtr_hrgjual as price FROM motors m, motorwarnas mw, motorwarnaprice_details mwpd WHERE m.id=mw.motor_id and mw.id = mwpd.motorwarna_id and mtr_type_group = "'+mtr_type_group+'" GROUP BY mtr_type_desc', function(err,data){
         con.release();
         if(err)
           return res.json({status:400,message: 'failed',result:[]});
           return res.json(data);
     });
  });
};

this.selectDetail = function(req,res,next) {
  connection.acquire(function(err,con){
    var mtr_type_group = req.query.mtr_type_group;
     if (err) throw err;
       con.query('SELECT * FROM galerys WHERE mtr_type_group="'+mtr_type_group+'"', function(err,data){
         con.release();
         if(err)
           return res.json({status:400,message: 'failed',result:[]});
           return res.json(data);
     });
  });
};

this.selectCatalogPrice = function(req,res,next) {
  connection.acquire(function(err,con){
    var mtr_type_desc = req.query.mtr_type_desc;
     if (err) throw err;
       con.query('SELECT id, mtr_type_desc, mtr_hrgjual FROM motorprice_details GROUP BY mtr_type_desc', function(err,data){
         con.release();
         if(err)
           return res.json({status:400,message: 'failed',result:[]});
           return res.json(data);
     });
  });
};
}
module.exports = new Todo();
