//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    const fs = require('fs');

    this.selectMasterTraining = function(req, res, next) {
        var id = req.query.id;
        if (id != null) {
            detailMasterTraining(req, res, next);
            return;
        }
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM master_training order by modified_at desc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json({ status: 200, message: 'success', result: data });
            });
        });
    };

    this.insertMasterTraining = function(req, res) {
        var id = uuidv1();
        var code = req.body.code;
        var title = req.body.title;
        var description = req.body.description;
        var type = req.body.type;
        var level = req.body.level;
        var created_at = new Date();
        var modified_at = new Date();
        var created_by = req.body.created_by;
        var modi_by = req.body.modi_by;

        var attachment = req.files;

        connection.acquire(function(err, con) {
            con.query('INSERT INTO master_training (id, code, title, description, type, level, created_at, modified_at, created_by, modi_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [id, code, title, description, type, level, created_at, modified_at, created_by, modi_by], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Master training creation failed' });
                } else {
                    if (attachment.length > 0) {
                        var values = [];
                        attachment.forEach(element => {
                            var id_detail = uuidv1();
                            var fileTraining = "/files/" + element.filename;
                            values.push([id_detail, id, fileTraining, created_at, modified_at, created_by, modi_by]);
                        });
                        connection.acquire(function(err, con) {
                            if (err) throw err;
                            con.query("INSERT INTO master_training_detail VALUES ?", [values], function(err, rows) {
                                con.release();
                                if (err) {
                                    console.log(err);
                                } else {
                                    res.send({ status: 200, message: 'Master training created successfully' });
                                }
                            });
                        });
                    } else {
                        res.send({ status: 200, message: 'Master training created successfully' });
                    }
                }
            });
        });
    };

    this.updateMasterTraining = function(req, res) {
        var id = req.params.id;
        var code = req.body.code;
        var title = req.body.title;
        var description = req.body.description;
        var type = req.body.type;
        var level = req.body.level;
        var created_at = new Date();
        var created_by = req.body.created_by;
        var modified_at = new Date();
        var modi_by = req.body.modi_by;

        var fileTraining = req.files;

        connection.acquire(function(err, con) {
            con.query('UPDATE master_training SET code = ?, title = ?, description = ?, type = ?, level = ?, modified_at = ?, modi_by = ? WHERE id = "' + id + '"', [code, title, description, type, level, modified_at, modi_by], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Master training update failed' });
                } else {
                    if (fileTraining.length > 0) {
                        var valuesAdd = [];
                        var valuesUpdate = [];
                        fileTraining.forEach(element => {
                            var flag = element.fieldname;
                            if (flag == 'file') {
                                var id_detail = uuidv1();
                                id = req.params.id;
                                var attachment = "/files/" + element.filename;
                                valuesAdd.push([id_detail, id, attachment, created_at, modified_at, created_by, modi_by]);
                            } else {
                                var id = flag
                                var attachment = "/files/" + element.filename;
                                valuesUpdate.push([id, attachment, modified_at, modi_by]);
                            }
                        });

                        if (valuesAdd.length > 0) {
                            //query add
                            connection.acquire(function(err, con) {
                                if (err) throw err;
                                con.query("INSERT INTO master_training_detail VALUES ?", [valuesAdd], function(err, rows) {
                                    con.release();
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        if (valuesUpdate.length > 0) {
                                            var ids = [];
                                            fileTraining.forEach(element => {
                                                var flag = element.fieldname;
                                                if (flag != 'file') {
                                                    var id = element.fieldname;
                                                    var idInput = "'" + id + "'";
                                                    ids.push([idInput]);
                                                }
                                            });
                                            connection.acquire(function(err, con) {
                                                if (err) throw err;
                                                con.query("SELECT attachment from master_training_detail where id IN (" + ids + ")", function(err, data) {
                                                    con.release();
                                                    if (err) {
                                                        console.log(err);
                                                    } else {
                                                        //query update
                                                        connection.acquire(function(err, con) {
                                                            if (err) throw err;
                                                            con.query("INSERT INTO master_training_detail(id, attachment, modified_at, modi_by) VALUES ? ON DUPLICATE KEY UPDATE attachment = VALUES(attachment) ", [valuesUpdate], function(err, rows) {
                                                                con.release();
                                                                if (err) {
                                                                    console.log(err);
                                                                } else {
                                                                    data.forEach(element => {
                                                                        var image = element.attachment;
                                                                        fs.unlink(appRoot + image.replace("/files/", "/uploads/"), (err) => {
                                                                            if (err) throw err;
                                                                            console.log('successfully deleted /tmp/hello');
                                                                        });
                                                                    });
                                                                    res.send({ status: 200, message: 'Master training updated successfully' });
                                                                }
                                                            });
                                                        });
                                                    }
                                                });
                                            });
                                        } else {
                                            res.send({ status: 200, message: 'Master training updated successfully' });
                                        }
                                    }
                                });
                            });
                        } else if (valuesUpdate.length > 0) {
                            var ids = [];
                            fileTraining.forEach(element => {
                                var flag = element.fieldname;
                                if (flag != 'file') {
                                    var id = element.fieldname;
                                    var idInput = "'" + id + "'";
                                    ids.push([idInput]);
                                }
                            });
                            connection.acquire(function(err, con) {
                                if (err) throw err;
                                con.query("SELECT attachment from master_training_detail where id IN (" + ids + ")", function(err, data) {
                                    con.release();
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        //query update
                                        connection.acquire(function(err, con) {
                                            if (err) throw err;
                                            con.query("INSERT INTO master_training_detail(id, attachment, modified_at, modi_by) VALUES ? ON DUPLICATE KEY UPDATE attachment = VALUES(attachment) ", [valuesUpdate], function(err, rows) {
                                                con.release();
                                                if (err) {
                                                    console.log(err);
                                                } else {
                                                    data.forEach(element => {
                                                        var image = element.attachment;
                                                        fs.unlink(appRoot + image.replace("/files/", "/uploads/"), (err) => {
                                                            if (err) throw err;
                                                            console.log('successfully deleted /tmp/hello');
                                                        });
                                                    });
                                                    res.send({ status: 200, message: 'Master training updated successfully' });
                                                }
                                            });
                                        });
                                    }
                                });
                            });
                        }
                    } else {
                        res.send({ status: 200, message: 'Master training updated successfully' });
                    }
                }
            });
        });
    };

    this.deleteMasterTraining = function(req, res) {
        var id = req.params.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT attachment from master_training_detail where id_training = ?', [id], function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    if (rows.length > 0) {
                        var values = [];
                        rows.forEach(element => {
                            var image = element.attachment;
                            values.push(image);
                        });

                        connection.acquire(function(err, con) {
                            if (err) throw err;
                            con.query('DELETE FROM master_training_detail WHERE attachment IN (?)', [values], function(err, result) {
                                con.release();
                                if (err) {
                                    console.log(err);
                                } else {
                                    rows.forEach(element => {
                                        var image = element.attachment;
                                        fs.unlink(appRoot + image.replace("/files/", "/uploads/"), (err) => {
                                            if (err) throw err;
                                            console.log('successfully deleted /tmp/hello');
                                        });
                                    });
                                    connection.acquire(function(err, con) {
                                        con.query('DELETE FROM master_training WHERE id = "' + id + '"', function(err, result) {
                                            con.release();
                                            if (err) {
                                                res.send({ status: 400, message: 'Failed to delete master training' });
                                            } else {
                                                res.send({ status: 200, message: 'Deleted successfully master training' });
                                            }
                                        });
                                    });
                                }
                            });

                        });
                    } else {
                        connection.acquire(function(err, con) {
                            con.query('DELETE FROM master_training WHERE id = "' + id + '"', function(err, result) {
                                con.release();
                                if (err) {
                                    res.send({ status: 400, message: 'Failed to delete master training' });
                                } else {
                                    res.send({ status: 200, message: 'Deleted successfully master training' });
                                }
                            });
                        });
                    }
                }
            });
        });
    };

    function detailMasterTraining(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM master_training WHERE id = "' + id + '" order by modified_at desc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json({ status: 200, message: 'success', result: data });
            });
        });
    };
}
module.exports = new Todo();