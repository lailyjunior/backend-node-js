//Auth : Nina
var connection = require('../../config/db');


function Todo() {

    this.selectMasterActivity = function(req, res, next) {
        var id = req.query.lead_id;
        //  connection.acquire(function(err,con){
        //     if (err) throw err;
        //     // if(id == null){
        //       con.query('SELECT a.id, a.activity_subject, a.activity_tgl, a.activity_time, a.activity_note, a.sls_nama, s.status as activity_status, t.type as activity_type, c.cust_nama as lead_nama FROM activities a LEFT OUTER JOIN master_status s ON a.activity_status = s.code LEFT OUTER JOIN master_type_activity t ON a.activity_type = t.code LEFT OUTER JOIN customer_prospect c ON a.lead_id = c.id WHERE a.lead_id ="'+id+'"', function(err,data){
        //         con.release();
        //         if(err)
        //           return res.json({status:'400',message: 'Failed',result:[]});
        //           return res.json(data);
        //       });
        //  });
        connection.acquire(function(err, con) {
            con.query('SELECT a.id, a.activity_subject, a.activity_tgl,a.activity_note, s.status as activity_status, t.type as activity_type, c.cust_nama as lead_nama FROM activities a LEFT OUTER JOIN master_status s ON a.activity_status = s.code LEFT OUTER JOIN master_type_activity t ON a.activity_type = t.code LEFT OUTER JOIN customer_prospect c ON a.lead_id = c.id WHERE a.lead_id ="' + id + '"', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'Failed', result: [] });
                } else {
                    connection.acquire(function(err, con) {
                        con.query('SELECT salesman_id FROM customer_prospect WHERE id = "' + id + '"', function(err, data) {
                            con.release();
                            if (err) {
                                res.send({ status: 400, message: 'Master activity failed' });
                            } else {
                                res.send({ status: 200, message: 'Master activity successfully' });
                                return res.json(data);
                            }
                        });
                    });
                }
            });
        });
    };

    this.selectAllActivityBySalesman = function(req, res, next) {
        var id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            // if(id == null){
            con.query('SELECT a.id, a.activity_subject, a.activity_tgl, a.activity_note,a.activity_status, t.type as activity_type, c.cust_nama as lead_nama, c.cust_code as lead_code FROM activities a LEFT OUTER JOIN master_status s ON a.activity_status = s.code LEFT OUTER JOIN master_type_activity t ON a.activity_type = t.code LEFT OUTER JOIN customer_prospect c ON a.lead_id = c.id WHERE a.salesman_id ="' + id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    }

    this.selectAllActivityToday = function(req, res, next) {
        var id = req.query.salesman_id;

        connection.acquire(function(err, con) {
            con.query('select a.id, a.activity_subject, a.activity_tgl, t.type as activity_type, a.activity_status, a.activity_note FROM activities a LEFT JOIN master_type_activity t ON a.activity_type = t.code WHERE DATE(activity_tgl) = DATE(CURDATE()) and a.activity_status = "Planned" and a.salesman_id = "' + id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    }

    this.selectAllActivityTomorrow = function(req, res, next) {
        var id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select a.id, a.activity_subject, a.activity_tgl, t.type as activity_type, a.activity_status, a.activity_note FROM activities a LEFT JOIN master_type_activity t ON a.activity_type = t.code WHERE DATE(activity_tgl) = DATE(CURDATE()+INTERVAL 1 DAY) and a.activity_status = "Planned" and a.salesman_id ="' + id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    }

    this.selectAllActivityDone = function(req, res, next) {
        var id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT a.id, a.activity_subject, a.activity_tgl, t.type as activity_type, a.activity_status, a.activity_note FROM activities a LEFT JOIN master_type_activity t ON a.activity_type = t.code WHERE a.activity_status = "Done" and a.salesman_id ="' + id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    }

    this.selectStatus = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM master_status', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            })
        });
    };

    this.selectAllActivity = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM activities', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            })
        });
    };

    this.selectType = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM master_type_activity', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            })
        });
    };

    this.selectStatusGenerateCustomer = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT act.lead_id, cp.status_generate_customer FROM activities act, customer_prospect cp WHERE act.lead_id = cp.id', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            })
        });
    };

    this.insertMasterActivity = function(req, res) {
        // connection.acquire(function(err, con) {
        //   var queryStr = 'SELECT activity_kode FROM activities ORDER BY activity_kode DESC LIMIT 1';

        //   console.log(queryStr);
        // });

        // // var str = "SELECT activity_kode FROM activities ORDER BY activity_kode DESC LIMIT 1";
        // // var str2 = str.substring(12);
        // // var str3 = parseInt(str2);
        // // var jumlah = str3+1;
        // // // var kode = str.substring(0, 12)+jumlah;
        // var kode = queryStr;

        // var someVar = [];
        // function kodeActivity(val_id, val_char, cb){
        //   var result = "";
        //   connection.acquire(function(err,con){
        //     if(err) throw err;
        //     var d = new Date();
        //     var n = d.getDate();
        //     var m = d.getMonth()+1;
        //     var y = d.getFullYear();
        //     var z = '00000';

        //   con.query("SELECT * FROM mst_run_nums WHERE val_id = 'LA' AND val_char = '"+n+m+y+z+"' LIMIT 1", function(err, rows){
        //     // console.log(1); 
        //     con.release(); 
        //     // console.log("SELECT * FROM mst_run_nums WHERE val_id = 'LA' AND val_char = '"+n+m+y+"' LIMIT 1");
        //     // console.log(rows);
        //     if(err) {
        //       throw err;
        //       console.log(err);
        //     }
        //       if(rows.length >= 1){
        //         console.log(1);
        //         connection.acquire(function(err, con) {
        //           // var data = JSON.parse(JSON.stringify(rows[0]));
        //           // console.log(data[0]);
        //           rows.forEach((row) => {
        //             console.log(row.val_value);
        //             var value = Number(row.val_value)+1;
        //             console.log(value);
        //             // value = sprintf('%0'.)
        //           var val_id = "LA";
        //           var val_char = "2342018";

        //           con.query("UPDATE mst_run_nums SET val_value = ? WHERE val_id = ? AND val_char = ?",[value, val_id,val_char], function(err, result) {
        //               con.release();
        //               if (err) {
        //                result = -1;
        //                console.log(err);
        //               } else {
        //                 result = val_id+val_char+value;
        //               }
        //               cb(result);
        //           });
        //           });
        //         });
        //       }


        //   });
        // });

        // }
        // kodeActivity("LA", "2342018", function(id){
        //     console.log(id);
        var id = uuidv1();
        var activity_tgl = req.body.activity_tgl;
        var activity_kode = null;
        var activity_subject = req.body.activity_subject;
        var activity_type = req.body.activity_type;
        var activity_note = req.body.activity_note;
        var activity_status = req.body.activity_status;
        var lead_id = req.body.lead_id;
        var lead_code = req.body.lead_code;
        var lead_status_old = '1';
        var lead_status = '1';
        var create_by = req.body.create_by;
        var modi_by = req.body.modi_by;
        var created = new Date();
        var modified = new Date();
        var is_delete = '0';
        var kunci = req.body.kunci;
        var salesman_id = req.body.salesman_id;
        // var sls_kode = req.body.sls_kode;
        // var sls_nama = req.body.sls_nama;

        connection.acquire(function(err, con) {
            con.query('select activity_kode from activities order by created desc limit 1', function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Activities Failed' });
                } else {
                    // for CODE CP
                    if (result.length != 0) {
                        var test = result[0].activity_kode;
                    } else {
                        var test = "LA00000000000"
                    }

                    var pieces = test.split('LA');
                    var prefix = pieces[0];
                    var lastNum = pieces[1];
                    lastNum = parseInt(lastNum, 10);
                    lastNum++;
                    activity_kode = "LA" + ("00000000000" + lastNum).substr(-11);

                    connection.acquire(function(err, con) {
                        con.query('INSERT INTO activities (id, activity_kode, activity_tgl,activity_subject,activity_type,activity_note,activity_status,lead_id,lead_code,lead_status_old,lead_status,create_by, modi_by,created,modified, is_delete, kunci, salesman_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, activity_kode, activity_tgl, activity_subject, activity_type, activity_note, activity_status, lead_id, lead_code, lead_status_old, lead_status, create_by, modi_by, created, modified, is_delete, kunci, salesman_id], function(err, result) {
                            con.release();
                            console.log(err);
                            if (err) {
                                res.send({ status: 400, message: 'creation failed' });
                            } else {
                                res.send({ status: 200, message: 'created successfully' });
                            }
                        });
                    });
                };
            });
        });
    }

    // function getMstum(){
    //   return 1;
    // }
    // function setValue(value) {
    //   someVar = getMstum();
    //   console.log(someVar);
    // }

    this.updateMasterActivity = function(req, res, next) {
        var id = req.body.id;
        var activity_kode = req.body.activity_kode;
        var activity_tgl = req.body.activity_tgl;
        var activity_subject = req.body.activity_subject;
        var activity_type = req.body.activity_type;
        var activity_note = req.body.activity_note;
        var activity_status = req.body.activity_status;
        var lead_status_old = req.body.lead_status_old;
        var lead_status = req.body.lead_status;
        var modi_by = req.body.modi_by;
        var modified = new Date();
        var is_delete = req.body.is_delete;
        var kunci = req.body.kunci;
        var id_lead = req.body.lead_id;
        var status_profile = req.body.lead_status;

        connection.acquire(function(err, con) {
            con.query('UPDATE activities SET activity_kode = ?, activity_tgl = ?, activity_subject = ?, activity_type = ?, activity_note = ?, activity_status = ?,lead_status_old = ?, lead_status = ?, modi_by = ?, modified = ?, is_delete = ?, kunci = ? WHERE id = "' + id + '"', [activity_kode, activity_tgl, activity_subject, activity_type, activity_note, activity_status, lead_status_old, lead_status, modi_by, modified, is_delete, kunci], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Activity Failed' });
                } else {

                    connection.acquire(function(err, con) {
                        con.query('UPDATE customer_prospect SET status_profile = ? WHERE id = "' + id_lead + '"', [status_profile], function(err, result) {
                            con.release();
                            if (err) {
                                res.send({ status: 400, message: 'UPDATE failed' });
                            } else {
                                res.send({ status: 200, message: 'UPDATE successfully' });
                            }
                        });
                    });
                }
            });
        });
    };



    this.deleteMasterActivity = function(req, res) {
        var id = req.params.id;
        connection.acquire(function(err, con) {
            con.query('DELETE FROM activities WHERE id = ?', [id], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Failed to delete master activity' });
                } else {
                    res.send({ status: 200, message: 'Deleted successfully master activity' });
                }
            });
        });
    };

    this.getSalesmenbySupervisor = function(req, res) {
        var supervisor_id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select id, sls_nama from salesmen where supervisor_id="' + supervisor_id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            })
        });
    }

    this.detailActivity = function(req, res) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT a.id, a.activity_kode, a.activity_status, a.activity_tgl, a.activity_subject, a.activity_note, t.code as activity_type, a.lead_id, a.lead_code, a.lead_status_old, a.lead_status, a.create_by, a.modi_by, a.created, a.modified, a.is_delete, a.kunci, a.salesman_id, c.cust_nama as lead_nama, s.sls_nama as salesmen_name FROM activities a LEFT JOIN master_type_activity t ON a.activity_type = t.code LEFT JOIN customer_prospect c ON a.lead_id = c.id LEFT JOIN salesmen s ON a.salesman_id = s.id WHERE a.id= "' + id + '"', function(err, data) {
                con.release();
                if (err) {
                    return res.json({ status: 400, message: 'failed', result: [] });
                } else {
                    if (data.length > 0) {
                        return res.json(data[0]);
                    }
                }
            });
        });
    }

    // ANGGI
    this.getAllActivitySalesman = function(req, res, next) {
        var id = req.query.salesman_id;
        var lead_id = req.query.lead_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT a.id,a.activity_kode,a.activity_tgl,a.activity_subject,t.code activity_type,a.activity_note,activity_status,c.id lead_id,a.lead_code,a.lead_status_old,a.lead_status,a.salesman_id FROM activities a, master_type_activity t, customer_prospect c WHERE a.activity_type=t.code and a.lead_id=c.id and a.lead_id="' + lead_id + '" and a.salesman_id="' + id + '" ORDER BY a.activity_tgl', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getTypeActivitySalesman = function(req, res, next) {
        var id = req.query.salesman_id;
        var lead_id = req.query.lead_id;
        var activity_type = req.query.activity_type;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT a.id,a.activity_kode,a.activity_tgl,a.activity_subject,t.code activity_type,a.activity_note,activity_status,a.lead_id,a.lead_code,a.lead_status_old,a.lead_status,a.salesman_id FROM activities a, master_type_activity t, customer_prospect c WHERE a.activity_type=t.code and a.lead_id=c.id and a.salesman_id="' + id + '" and a.activity_type="' + activity_type + '" and a.lead_id="' + lead_id + '" ORDER BY a.activity_tgl', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getDropdownSalesmen = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, sls_kode, sls_nama FROM salesmen WHERE supervisor_id =  "' + supervisor_id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    }


    // FOR LOGIN SUPERVISOR

    this.getActivityForSupervisor = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT a.id, a.activity_kode, a.activity_tgl, a.activity_subject, a.activity_type, a.activity_note, a.activity_status, a.lead_id, a.lead_code, s.id as salesman_id, a.lead_status, sp.id as supervisor_id, sp.supervisor_nama FROM activities a, salesmen s, supervisors sp WHERE a.salesman_id = s.id and s.supervisor_id = sp.id and a.salesman_id = "' + salesman_id + '" and s.supervisor_id = "' + supervisor_id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    }

    this.getIndexActivityForSupervisor = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT a.id, a.activity_kode, a.activity_tgl, a.activity_subject, a.activity_type, a.activity_note, a.activity_status, a.lead_id, a.lead_code, s.id as salesman_id, a.lead_status, sp.id as supervisor_id, sp.supervisor_nama FROM activities a, salesmen s, supervisors sp WHERE a.salesman_id = s.id and s.supervisor_id = sp.id and s.supervisor_id = "' + supervisor_id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    }

    this.getActivityDoingSupervisor = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;

        connection.acquire(function(err, con) {
            con.query('SELECT a.id, a.activity_kode, a.activity_tgl, a.activity_subject, a.activity_type, a.activity_note, a.activity_status, a.lead_id, a.lead_code, s.id as salesman_id, a.lead_status, sp.id as supervisor_id, sp.supervisor_nama FROM activities a, salesmen s, supervisors sp WHERE a.salesman_id = s.id and s.supervisor_id = sp.id and s.supervisor_id  = "' + supervisor_id + '" and  DATE(activity_tgl) = DATE(CURDATE()) and a.activity_status = "Planned"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    }

    this.getActivityTomorrowSupervisor = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;

        connection.acquire(function(err, con) {
            con.query('SELECT a.id, a.activity_kode, a.activity_tgl, a.activity_subject, a.activity_type, a.activity_note, a.activity_status, a.lead_id, a.lead_code, s.id as salesman_id, a.lead_status, sp.id as supervisor_id, sp.supervisor_nama FROM activities a, salesmen s, supervisors sp WHERE a.salesman_id = s.id and s.supervisor_id = sp.id and s.supervisor_id = "' + supervisor_id + '" and  DATE(activity_tgl) = DATE(CURDATE()+INTERVAL 1 DAY) and a.activity_status = "Planned"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    }

    this.getActivityDoneSupervisor = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;

        connection.acquire(function(err, con) {
            con.query('SELECT a.id, a.activity_kode, a.activity_tgl, a.activity_subject, a.activity_type, a.activity_note, a.activity_status, a.lead_id, a.lead_code, s.id as salesman_id, a.lead_status, sp.id as supervisor_id, sp.supervisor_nama FROM activities a, salesmen s, supervisors sp WHERE a.salesman_id = s.id and s.supervisor_id = sp.id and s.supervisor_id = "' + supervisor_id + '" and a.activity_status = "Done"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });

    }

    this.getAllActivitySalesmanSpv = function(req, res, next) {
        var lead_id = req.query.lead_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT a.id,a.activity_kode,a.activity_tgl,a.activity_subject,t.code activity_type,a.activity_note,activity_status,c.id lead_id,a.lead_code,a.lead_status_old,a.lead_status,a.salesman_id,s.sls_nama FROM activities a, master_type_activity t, customer_prospect c, salesmen s WHERE a.activity_type=t.code and a.lead_id=c.id and s.id = a.salesman_id AND a.lead_id="' + lead_id + '" ORDER BY a.activity_tgl', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getTypeActivitySalesmanSpv = function(req, res, next) {
        var lead_id = req.query.lead_id;
        var activity_type = req.query.activity_type;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT a.id,a.activity_kode,a.activity_tgl,a.activity_subject,t.code activity_type,a.activity_note,activity_status,a.lead_id,a.lead_code,a.lead_status_old,a.lead_status,a.salesman_id,s.sls_nama FROM activities a, master_type_activity t, customer_prospect c, salesmen s WHERE a.activity_type=t.code and a.lead_id=c.id and s.id=a.salesman_id and a.activity_type="' + activity_type + '" and a.lead_id="' + lead_id + '" ORDER BY a.activity_tgl', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    //  ANGGI LISDAYANTI

    this.getActivityCalendar = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        var start_date = req.query.start_date;
        var end_date = req.query.end_date;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM activities WHERE salesman_id="' + salesman_id + '" and activity_tgl between "' + start_date + '" and "' + end_date + '" ORDER BY activity_tgl desc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    //ACTIVITY
    this.getActivityByYear = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        var year = req.query.year;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM activities WHERE  YEAR(activity_tgl) = "' + year + '" and salesman_id="' + salesman_id + '" ORDER BY activity_tgl desc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getActivityByMonth = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        var year = req.query.year;
        var month = req.query.month;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM activities WHERE  YEAR(activity_tgl) = "' + year + '" AND MONTH(activity_tgl) = "' + month + '" and salesman_id="' + salesman_id + '" ORDER BY activity_tgl desc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getActivityByDay = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        var year = req.query.year;
        var month = req.query.month;
        var day = req.query.day;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM activities WHERE  YEAR(activity_tgl) = "' + year + '" AND MONTH(activity_tgl) = "' + month + '" AND DAY(activity_tgl) = "' + day + '" and salesman_id="' + salesman_id + '" ORDER BY activity_tgl desc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };


    //EVENT
    this.getEventByYear = function(req, res, next) {
        var sls_kode = req.query.sls_kode;
        var year = req.query.year;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM transaksi_new_events WHERE  YEAR(start) = "' + year + '" and sls_kode="' + sls_kode + '" ORDER BY start desc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getEventByMonth = function(req, res, next) {
        var sls_kode = req.query.sls_kode;
        var year = req.query.year;
        var month = req.query.month;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM transaksi_new_events WHERE  YEAR(start) = "' + year + '" AND MONTH(start) = "' + month + '" and sls_kode="' + sls_kode + '" ORDER BY start desc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getEventByDay = function(req, res, next) {
        var sls_kode = req.query.sls_kode;
        var year = req.query.year;
        var month = req.query.month;
        var day = req.query.day;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM transaksi_new_events WHERE  YEAR(start) = "' + year + '" AND MONTH(start) = "' + month + '" AND DAY(start) = "' + day + '" and sls_kode="' + sls_kode + '" ORDER BY start desc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getAllActivityBySalesman = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM activities WHERE salesman_id="' + salesman_id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getAllEventBySalesman = function(req, res, next) {
        var sls_kode = req.query.sls_kode;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM transaksi_new_events WHERE sls_kode="' + sls_kode + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    //supervisor
    this.getEventBySupervisor = function(req, res, next) {
        var sls_kode = req.query.sls_kode;
        var supervisor_id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT e.*, sls.sls_nama FROM transaksi_new_events e, salesmen sls, supervisors spv WHERE e.sls_kode = sls.sls_kode and sls.supervisor_id = spv.id and sls.sls_kode = "' + sls_kode + '" and spv.id = "' + supervisor_id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getAllEventBySupervisor = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT e.*, sls.sls_nama FROM transaksi_new_events e, salesmen sls, supervisors spv WHERE e.sls_kode = sls.sls_kode and sls.supervisor_id = spv.id', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.getEventPerDayBySupervisor = function(req, res, next) {
        var year = req.query.year;
        var month = req.query.month;
        var day = req.query.day;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT e.*, sls.sls_nama FROM transaksi_new_events e, salesmen sls, supervisors spv WHERE e.sls_kode = sls.sls_kode and sls.supervisor_id = spv.id and YEAR(e.`start`)="' + year + '" and MONTH(e.`start`)="' + month + '" and DAY(e.`start`)="' + day + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: '400', message: 'Failed', result: [] });
                return res.json(data);
            });
        });
    };
}

module.exports = new Todo();