//Auth : Lely
var connection = require('../../config/db');

function Todo() {

    var request = require('request');

    this.selectCaraBayar = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, code, title, is_installment FROM master_cara_bayar order by code', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    this.selectDpPo = function(req, res, next) {
        var leas_kode = req.query.leas_kode;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT m.id, i.id id_schema,i.leas_kode, m.mtr_kode, m.deskripsi, m.mtr_type ,ids.dp_maks1 pricelist FROM motors m, insentifschemes i, insentifscheme_details ids where m.mtr_kode = i.mtr_kode and i.id = ids.insentifscheme_id and i.leas_kode = "' + leas_kode + '" order by mtr_kode', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    this.selectLeasing = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, leas_kode, leas_nama FROM leasings order by leas_kode', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    this.selectDetailMotorAll = function(req, res, next) {
        var branch_code = req.query.branch_code;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT m.id, md.motorwarna_id, md.product_kode mtr_kode, md.product_nama deskripsi, count(sm.product_kode) stock, md.mtr_hrgjual harga , m.mtr_type mtr_type from stock_motors sm inner join motorwarnaprice_details md on sm.product_kode = md.product_kode inner join motors m on md.motor_id = m.id where md.branch_code = "' + branch_code + '" and sm.branch_code = "' + branch_code + '" GROUP BY md.product_kode', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    this.selectDetailMotorByType = function(req, res, next) {
        var branch_code = req.query.branch_code;
        var type_motor = req.query.type;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT m.id, md.motorwarna_id, md.product_kode mtr_kode, md.product_nama deskripsi, count(sm.product_kode) stock, md.mtr_hrgjual harga , m.mtr_type mtr_type from stock_motors sm inner join motorwarnaprice_details md on sm.product_kode = md.product_kode inner join motors m on md.motor_id = m.id where md.branch_code = "' + branch_code + '" and sm.branch_code = "' + branch_code + '" and m.mtr_type = "' + type_motor + '" group by md.product_kode', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    this.selectNoSpkBySalesmanID = function(req, res, next) {
        var branch_code = req.query.branch_code;
        var sls_kode = req.query.sales;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT n.id idnospk, n.register_nomor,n.branch_code, nd.id idnospkdetail, nd.nomor_spk from nomorspks n, nomorspk_details nd where nd.nomorspk_id = n.id and nd.aktivasi ="Y" and nd.used ="N" and nd.branch_code = "' + branch_code + '" and nd.sls_kode = "' + sls_kode + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    this.selectJumlahTenor = function(req, res, next) {
        var mtr_kode = req.query.mtr_kode;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT m.id, i.id id_schema,i.leas_kode, m.mtr_kode, m.deskripsi, m.mtr_type ,ids.dp_maks1 pricelist , ids.tenor FROM motors m, insentifschemes i, insentifscheme_details ids where m.mtr_kode = i.mtr_kode and i.id = ids.insentifscheme_id and i.leas_kode = "WOM" and ids.mtr_kode ="' + mtr_kode + '" order by m.mtr_kode', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    this.selectDendaWilayah = function(req, res, next) {
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * from wilayahs', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data);
            });
        });
    };

    this.selectCustomerDetails = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * from customers where id = "' + id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data[0]);
            });
        });
    };

    this.selectCustomers = function(req, res, next) {
        var salesman_id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM customers WHERE salesman_id="' + salesman_id + '" order by cust_nama asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.insertGenerateSpk = function(req, res) {
        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        // var fileUploads = req.files;
        var doc1 = req.body.doc_1;
        var doc2 = req.body.doc_2;
        var doc3 = req.body.doc_3;
        var doc4 = req.body.doc_4;
        var doc5 = req.body.doc_5;

        var id = uuidv1();

        //data spk
        var id_spk = req.body.id_spk;
        var id_detail_spk = req.body.id_detail_spk;
        var no_spk = req.body.no_spk;

        //data pembeli
        var id_customer = req.body.id_customer;
        var code_customer = req.body.code_customer;
        var cust_nama = req.body.cust_nama;
        var cust_alamat = req.body.cust_alamat;
        var cust_hp = req.body.cust_hp;
        var cust_telp = req.body.cust_telp;
        var cust_email = req.body.cust_email;
        var cust_npwp = req.body.cust_npwp;

        var cust_stnk = req.body.cust_stnk;
        var cust_alamat_stnk = req.body.cust_alamat_stnk
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_ktp = req.body.cust_ktp;
        var cust_hp_stnk = req.body.cust_hp2;
        var cust_telp_stnk = req.body.cust_telp2;
        var cust_jenis_kelamin = req.body.cust_jenis_kelamin;
        var cust_sumber = req.body.cust_sumber;

        //cara bayar
        var cara_bayar_code = req.body.cara_bayar_kode;
        var cara_bayar = req.body.cara_bayar;

        // pembelian kredit
        var id_leasing = req.body.id_leasing;
        var leasing_nama = req.body.leasing_nama;
        var tenor = req.body.jumlah_tenor;
        var dp_po = req.body.dp_po;
        var dp_bayar = req.body.dp_bayar;
        var angsuran = req.body.angsuran_per_bulan;

        // denda & asuransi
        var asuransi = req.body.asuransi;
        var id_denda = req.body.id_denda;
        var denda_daerah = req.body.denda_daerah;
        var denda_jumlah = req.body.denda_jumlah;
        var pajak_progresif = req.body.pajak_progresif;

        // data created modified

        var created = new Date();
        var modified = new Date();
        var create_by = req.body.created_by;
        var modi_by = req.body.modi_by;
        var salesman_id = req.body.salesman_id

        // data detail 
        var detail = req.body.detail;

        connection.acquire(function(err, con) {
            con.query('select kode_spk from generate_spks order by created_at desc limit 1', function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Error' });
                } else {
                    var kode_spk = null;
                    if (result.length != 0) {
                        var test = result[0].kode_spk;
                    } else {
                        var test = "CP-000000000000"
                    }

                    var pieces = test.split('-');
                    var prefix = pieces[0];
                    var lastNum = pieces[1];
                    lastNum = parseInt(lastNum, 10);
                    lastNum++;
                    kode_spk = "SPK-" + ("000000000000" + lastNum).substr(-11);

                    connection.acquire(function(err, con) {
                        con.query('INSERT INTO generate_spks (id, id_spk, id_detail_spk, kode_spk, no_spk, id_customer, code_customer, cust_nama, cust_alamat, cust_hp, cust_telp, cust_email, cust_npwp, cust_stnk, cust_alamat_stnk, cust_tgl_lahir, cust_ktp, cust_jenis_kelamin, cust_telp2, cust_hp2, doc_1, doc_2, doc_3, doc_4, doc_5, id_leasing, leasing_nama, jumlah_tenor, dp_po, dp_bayar, angsuran_per_bulan, asuransi, id_denda, denda_daerah, denda_jumlah, pajak_progresif, created_at, modified_at, created_by, modi_by, cust_sumber, cara_bayar_code, cara_bayar, salesman_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)', [id, id_spk, id_detail_spk, kode_spk, no_spk, id_customer, code_customer, cust_nama, cust_alamat, cust_hp, cust_telp, cust_email, cust_npwp, cust_stnk, cust_alamat_stnk, cust_tgl_lahir, cust_ktp, cust_jenis_kelamin, cust_telp_stnk, cust_hp_stnk, doc1, doc2, doc3, doc4, doc5, id_leasing, leasing_nama, tenor, dp_po, dp_bayar, angsuran, asuransi, id_denda, denda_daerah, denda_jumlah, pajak_progresif, created, modified, create_by, modi_by, cust_sumber, cara_bayar_code, cara_bayar, salesman_id], function(err, result) {
                            con.release();
                            if (err) {
                                res.send({ status: 400, message: 'Create SPK Failed' });
                            } else {
                                var values = [];
                                if (detail.length > 0) {
                                    detail.forEach(element => {
                                        var id_detail = uuidv1();
                                        var motor_id = element.motor_id;
                                        var motorwarna_id = element.motorwarna_id;
                                        var mtr_type = element.mtr_type;
                                        var mtr_kode = element.mtr_kode;
                                        var deskripsi = element.deskripsi;
                                        var harga = element.harga;
                                        var stock = element.stock;
                                        values.push([id_detail, id, motor_id, motorwarna_id, mtr_type, mtr_kode, deskripsi, harga, created, modified, create_by, modi_by]);
                                    });
                                    connection.acquire(function(err, con) {
                                        if (err) throw err;
                                        con.query("INSERT INTO generate_spk_details VALUES ?", [values], function(err, rows) {
                                            con.release();
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                connection.acquire(function(err, con) {
                                                    con.query('UPDATE customers SET status_generate_spk="1" WHERE id = "' + id_customer + '"', function(err, result) {
                                                        con.release();
                                                        if (err) {
                                                            console.log(err);
                                                        } else {
                                                            res.send({ status: 200, message: 'add success' });
                                                        }
                                                    });
                                                });
                                            }
                                        });
                                    });
                                }
                            }
                        });
                    });
                }
            });
        });
    }

    this.insertGenerateSpkDms = function(req, res) {
        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        // var fileUploads = req.files;
        var doc1 = req.body.doc_1;
        var doc2 = req.body.doc_2;
        var doc3 = req.body.doc_3;
        var doc4 = req.body.doc_4;
        var doc5 = req.body.doc_5;

        var id = uuidv1();

        //data spk
        var id_spk = req.body.id_spk;
        var id_detail_spk = req.body.id_detail_spk;
        var no_spk = req.body.no_spk;

        //data pembeli
        var id_customer = req.body.id_customer;
        var code_customer = req.body.code_customer;
        var cust_nama = req.body.cust_nama;
        var cust_alamat = req.body.cust_alamat;
        var cust_hp = req.body.cust_hp;
        var cust_telp = req.body.cust_telp;
        var cust_email = req.body.cust_email;
        var cust_npwp = req.body.cust_npwp;

        var cust_stnk = req.body.cust_stnk;
        var cust_alamat_stnk = req.body.cust_alamat_stnk
        var cust_tgl_lahir = req.body.cust_tgl_lahir;
        var cust_ktp = req.body.cust_ktp;
        var cust_hp_stnk = req.body.cust_hp2;
        var cust_telp_stnk = req.body.cust_telp2;
        var cust_jenis_kelamin = req.body.cust_jenis_kelamin;
        var cust_sumber = req.body.cust_sumber;

        //cara bayar
        var cara_bayar_code = req.body.cara_bayar_kode;
        var cara_bayar = req.body.cara_bayar;

        // pembelian kredit
        var id_leasing = req.body.id_leasing;
        var leasing_nama = req.body.leasing_nama;
        var tenor = req.body.jumlah_tenor;
        var dp_po = req.body.dp_po;
        var dp_bayar = req.body.dp_bayar;
        var angsuran = req.body.angsuran_per_bulan;

        // denda & asuransi
        var asuransi = req.body.asuransi;
        var id_denda = req.body.id_denda;
        var denda_daerah = req.body.denda_daerah;
        var denda_jumlah = req.body.denda_jumlah;
        var pajak_progresif = req.body.pajak_progresif;

        // data created modified

        var created = new Date();
        var modified = new Date();
        var create_by = req.body.created_by;
        var modi_by = req.body.modi_by;
        var salesman_id = req.body.salesman_id

        // data detail 
        var detail = req.body.detail;

        console.log(detail);

        connection.acquire(function(err, con) {
            con.query('select kode_spk from generate_spks order by created_at desc limit 1', function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Error' });
                } else {
                    var kode_spk = null;
                    if (result.length != 0) {
                        var test = result[0].kode_spk;
                    } else {
                        var test = "CP-000000000000"
                    }

                    var pieces = test.split('-');
                    var prefix = pieces[0];
                    var lastNum = pieces[1];
                    lastNum = parseInt(lastNum, 10);
                    lastNum++;
                    kode_spk = "SPK-" + ("000000000000" + lastNum).substr(-11);
                    kode_spk2 = kode_spk;

                    connection.acquire(function(err, con) {
                        con.query('INSERT INTO generate_spks (id, id_spk, id_detail_spk, kode_spk, no_spk, id_customer, code_customer, cust_nama, cust_alamat, cust_hp, cust_telp, cust_email, cust_npwp, cust_stnk, cust_alamat_stnk, cust_tgl_lahir, cust_ktp, cust_jenis_kelamin, cust_telp2, cust_hp2, doc_1, doc_2, doc_3, doc_4, doc_5, id_leasing, leasing_nama, jumlah_tenor, dp_po, dp_bayar, angsuran_per_bulan, asuransi, id_denda, denda_daerah, denda_jumlah, pajak_progresif, created_at, modified_at, created_by, modi_by, cust_sumber, cara_bayar_code, cara_bayar, salesman_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)', [id, id_spk, id_detail_spk, kode_spk, no_spk, id_customer, code_customer, cust_nama, cust_alamat, cust_hp, cust_telp, cust_email, cust_npwp, cust_stnk, cust_alamat_stnk, cust_tgl_lahir, cust_ktp, cust_jenis_kelamin, cust_telp_stnk, cust_hp_stnk, doc1, doc2, doc3, doc4, doc5, id_leasing, leasing_nama, tenor, dp_po, dp_bayar, angsuran, asuransi, id_denda, denda_daerah, denda_jumlah, pajak_progresif, created, modified, create_by, modi_by, cust_sumber, cara_bayar_code, cara_bayar, salesman_id], function(err, result) {
                            con.release();
                            if (err) {
                                res.send({ status: 400, message: 'Create SPK Failed' });
                            } else {
                                var values = [];
                                if (detail.length > 0) {
                                    detail.forEach(element => {
                                        var id_detail = uuidv1();
                                        var motor_id = element.motor_id;
                                        var motorwarna_id = element.motorwarna_id;
                                        var mtr_type = element.mtr_type;
                                        var mtr_kode = element.mtr_kode;
                                        var deskripsi = element.deskripsi;
                                        var harga = element.harga;
                                        var stock = element.stock;
                                        values.push([id_detail, id, motor_id, motorwarna_id, mtr_type, mtr_kode, deskripsi, harga, created, modified, create_by, modi_by]);
                                    });
                                    connection.acquire(function(err, con) {
                                        if (err) throw err;
                                        con.query("INSERT INTO generate_spk_details VALUES ?", [values], function(err, rows) {
                                            con.release();
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                connection.acquire(function(err, con) {
                                                    con.query('UPDATE customers SET status_generate_spk="1" WHERE id = "' + id_customer + '"', function(err, result) {
                                                        con.release();
                                                        if (err) {
                                                            console.log(err);
                                                        } else {
                                                            connection.acquire(function(err, con) {
                                                                con.query('SELECT id FROM generate_spks order by created_at desc limit 1', function(err, resultID) {
                                                                    con.release();
                                                                    if (err) {
                                                                        console.log(err);
                                                                    } else {

                                                                        // FOR SPK TO DMS
                                                                        var idSPK = resultID[0].id;
                                                                        var url = 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_insert_generate_spk.php?id=' + idSPK;

                                                                        var requestData = {
                                                                            id_spk: req.body.id_spk,
                                                                            id_detail_spk: req.body.id_detail_spk,
                                                                            kode_spk: kode_spk2,
                                                                            no_spk: req.body.no_spk,

                                                                            //data pembeli
                                                                            id_customer: req.body.id_customer,
                                                                            code_customer: req.body.code_customer,
                                                                            cust_nama: req.body.cust_nama,
                                                                            cust_alamat: req.body.cust_alamat,
                                                                            cust_hp: req.body.cust_hp,
                                                                            cust_telp: req.body.cust_telp,
                                                                            cust_email: req.body.cust_email,
                                                                            cust_npwp: req.body.cust_npwp,

                                                                            cust_stnk: req.body.cust_stnk,
                                                                            cust_alamat_stnk: req.body.cust_alamat_stnk,
                                                                            cust_tgl_lahir: req.body.cust_tgl_lahir,
                                                                            cust_ktp: req.body.cust_ktp,
                                                                            cust_jenis_kelamin: req.body.cust_jenis_kelamin,
                                                                            cust_telp2: req.body.cust_telp2,
                                                                            cust_hp2: req.body.cust_hp2,
                                                                            cust_sumber: req.body.cust_sumber,

                                                                            //cara bayar
                                                                            cara_bayar_code: req.body.cara_bayar_kode,
                                                                            cara_bayar: req.body.cara_bayar,

                                                                            // pembelian kredit
                                                                            id_leasing: req.body.id_leasing,
                                                                            leasing_nama: req.body.leasing_nama,
                                                                            jumlah_tenor: req.body.jumlah_tenor,
                                                                            dp_po: req.body.dp_po,
                                                                            dp_bayar: req.body.dp_bayar,
                                                                            angsuran_per_bulan: req.body.angsuran_per_bulan,

                                                                            // denda & asuransi
                                                                            asuransi: req.body.asuransi,
                                                                            id_denda: req.body.id_denda,
                                                                            denda_daerah: req.body.denda_daerah,
                                                                            denda_jumlah: req.body.denda_jumlah,
                                                                            pajak_progresif: req.body.pajak_progresif,

                                                                            // data created modified

                                                                            salesman_id: req.body.salesman_id,
                                                                            created_at: new Date(),
                                                                            modified_at: new Date(),
                                                                            created_by: req.body.created_by,
                                                                            modi_by: req.body.modi_by,

                                                                        }

                                                                        var data = {
                                                                            url: url,
                                                                            auth: {
                                                                                'bearer': '71D55F9957529'
                                                                            },
                                                                            json: true,
                                                                            form: requestData,
                                                                            rejectUnauthorized: false, //add when working with https sites
                                                                            requestCert: false, //add when working with https sites
                                                                            agent: false, //add when working with https sites
                                                                        }

                                                                        console.log(data);
                                                                        request.post(data, function(error, httpResponse, body) {
                                                                            if (body.status == "Failed") {
                                                                                console.log(error);
                                                                                res.send({ status: 400, message: 'Create SPK Failed' });
                                                                            } else {
                                                                                console.log(body);
                                                                                connection.acquire(function(err, con) {
                                                                                    if (err) throw err;
                                                                                    con.query('SELECT * FROM generate_spk_details order by created_at desc', function(err, result) {
                                                                                        con.release();
                                                                                        if (err) {
                                                                                            res.send({ status: 400, message: 'Create SPK Failed' });
                                                                                        } else {

                                                                                            console.log(result[0]);
                                                                                            console.log(result[1]);
                                                                                        }
                                                                                    })
                                                                                })
                                                                                connection.acquire(function(err, con) {
                                                                                    if (err) throw err;
                                                                                    con.query('SELECT no_spk FROM generate_spks ORDER BY created_at limit 1', function(err, result_no_spk) {
                                                                                        con.release();
                                                                                        if (err) {
                                                                                            res.send({ status: 400, message: 'Create SPK Failed' });
                                                                                        } else {
                                                                                            var noSPK = result_no_spk[0].no_spk;
                                                                                            var url = 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_edit_no_spk_details.php?nomor_spk=' + noSPK;

                                                                                            var data = {
                                                                                                url: url,
                                                                                                auth: {
                                                                                                    'bearer': '71D55F9957529'
                                                                                                },
                                                                                                json: true,
                                                                                                rejectUnauthorized: false, //add when working with https sites
                                                                                                requestCert: false, //add when working with https sites
                                                                                                agent: false, //add when working with https sites
                                                                                            }

                                                                                            request.post(data, function(error, httpResponse, body) {
                                                                                                if (body.status == "Failed") {
                                                                                                    console.log(error);
                                                                                                    res.send({ status: 400, message: 'Create SPK Failed' });
                                                                                                } else {
                                                                                                    console.log(body);
                                                                                                    res.send({ status: 200, message: 'add success' });
                                                                                                }
                                                                                            });
                                                                                        }
                                                                                    });
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            });

                                                        }
                                                    });
                                                });
                                            }
                                        });
                                    });
                                }
                            }
                        });
                    });
                }
            });
        });
    }

    this.getDataEvent = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('Select end end_date, latitude,created created_at,detail description, modi_by,event_type_id id_type,created created_by, location_name, event_code,title event_name, id, modified modified_at, radius, start start_date, longitude FROM transaksi_new_events where id="' + id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ message: 'failed' });
                return res.json(data[0]);
            });
        });
    };

    this.selectIndexSpk = function(req, res, next) {
        var salesman_id = req.query.salesman_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select id, cust_nama, no_spk, cust_alamat from generate_spks where salesman_id = "' + salesman_id + '" order by cust_nama asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectIndexSpkAll = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        var dataSales = [];
        request.get({
            uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
            auth: {
                'bearer': '71D55F9957529'
            },
            rejectUnauthorized: false, //add when working with https sites
            requestCert: false, //add when working with https sites
            agent: false, //add when working with https sites
        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                obj = JSON.parse(body);

                obj.forEach(function(item) {
                    dataSales.push("'" + item.id + "'");
                });

                console.log(supervisor_id);
                console.log(dataSales);

                if (dataSales.length === 0) {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT id, cust_nama, no_spk, cust_alamat FROM generate_spks WHERE salesman_id IN ('" + dataSales + "') order by cust_nama asc", function(err, data) {
                            con.release();
                            if (err)
                                return res.json({ status: 400, message: 'failed', result: [] });
                            return res.json(data);
                        });
                    });
                } else {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query("SELECT id, cust_nama, no_spk, cust_alamat FROM generate_spks WHERE salesman_id IN (" + dataSales + ") order by cust_nama asc", function(err, data) {
                            con.release();
                            if (err)
                                return res.json({ status: 400, message: 'failed', result: [] });
                            return res.json(data);
                        });
                    });
                }
            }
        });
    };

    this.selectIndexSpkDetail = function(req, res, next) {
        var id_spk = req.query.id_spk;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select g.*, s.deskripsi sumber from generate_spks g, master_sumber s where g.id = "' + id_spk + '" and g.cust_sumber = s.code', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data[0]);
            });
        });
    };

    this.selectIndexSpkDetailMotor = function(req, res, next) {
        var id_spk = req.query.id_spk;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('select * from generate_spk_details where id_generate_spk = "' + id_spk + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectSalesmanBySupervisor = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, sls_nama FROM salesmen WHERE supervisor_id = "' + supervisor_id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.selectIndexSpkAllNonDms = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query("SELECT id, cust_nama, no_spk, cust_alamat FROM generate_spks WHERE salesman_id IN (SELECT id from salesmen where supervisor_id = '" + supervisor_id + "') order by cust_nama asc", function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

}
module.exports = new Todo();