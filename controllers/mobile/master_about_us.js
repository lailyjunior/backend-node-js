//Auth : Lely
var connection = require('../../config/db');
 
function Todo() {
  this.selectAboutUs = function(req,res,next) {
    var id = req.query.id;
    if (id != null) {
      detailAboutUs(req, res, next);
      return;
    }
     connection.acquire(function(err,con){
        if (err) throw err;
          con.query('SELECT * FROM master_about_us', function(err,data){
            con.release();
            if(err)
              return res.json({status:400,message: 'failed',result:[]});
              return res.json({status:200,message:'success',result:data});
        });
     });
  };

  this.insertAboutUs = function(req, res) {
    if (req.fileValidationError) {
      res.send(req.fileValidationError);
      return;
    }

    if(req.files.length == 0){
      res.send("Image Can't Empty ");
      return;
    }

    var photo = req.files[0].filename;
    var url = '/files/' + photo;

    var id_user = uuidv1();
    var title = req.body.title;
    var content = req.body.content;
    var created_at = new Date();
    var modified_at = new Date();
    var created_by = req.body.created_by;
    var modi_by = req.body.modi_by;

    connection.acquire(function(err, con) {
      con.query('INSERT INTO master_about_us (id, title, content, image, created_at, modified_at, created_by, modi_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)', 
        [id_user, title, content, url, created_at, modified_at, created_by, modi_by], 
        function(err, result){
          con.release();
          if (err) {
            res.send({status: 400, message: 'Master about us creation failed'});
          } else {
            res.send({status: 200, message: 'Master about us created successfully'});
          }
      });
    });
  };

  this.updateAboutUs = function(req, res) {
      var id = req.params.id;
      if (req.fileValidationError) {
        res.send(req.fileValidationError);
        return;
      }
      connection.acquire(function(err, con) {  
        con.query('SELECT image FROM master_about_us where id = "'+id+'"', function(err,data){
          con.release();
          if(err) return res.json({status:400,message: 'Failed',result:[]});
          
          if(req.files.length != 0 && data.length > 0){
              const fs = require('fs');
              fs.unlink(appRoot + data[0].image.replace("/files/", "/uploads/"), (err) => {
                if (err) throw err;
                console.log('successfully deleted /tmp/hello');
              });
          }

          if(req.files.length == 0){
            var url = data[0].image;
          }else{
            var photo = req.files[0].filename;
            var url = '/files/' + photo;
          }
          var title = req.body.title;
          var content = req.body.content;
          var modified_at = new Date();
          var modi_by = req.body.modi_by;
      
          connection.acquire(function(err, con) {
            con.query('UPDATE master_about_us SET title = ?, content = ?, image = ?, modified_at = ?, modi_by = ? WHERE id = "'+id+'"',[title, content, url, modified_at, modi_by], function(err, result) {
                con.release();
                if (err) {
                  res.send({status: 400, message: 'Master about us update failed'});
                } else {
                  res.send({status: 200, message: 'Master about us update successfully'});
                }
            });
          });
          // return res.json({status:'200',message:'success',result:data});
        });
      });
  };

  this.deleteAboutUs = function(req, res) {
    var id = req.params.id;
      if (req.fileValidationError) {
        res.send(req.fileValidationError);
        return;
      }
      connection.acquire(function(err, con) {  
        con.query('SELECT image FROM master_about_us where id = "'+id+'"', function(err,data){
          con.release();
          if(err) return res.json({status:400,message: 'Failed',result:[]});
          
          if(data.length > 0){
              const fs = require('fs');
              fs.unlink(appRoot + data[0].image.replace("/files/", "/uploads/"), (err) => {
                if (err) throw err;
                console.log('successfully deleted /tmp/hello');
              });
          }
          
          connection.acquire(function(err, con) {
            con.query('DELETE FROM master_about_us WHERE id = ?',[id], function(err, result) {
                con.release();
                if (err) {
                  res.send({status: 400, message: 'Master about us delete failed'});
                } else {
                  res.send({status: 200, message: 'Master about us delete successfully'});
                }
            });
          });
          // return res.json({status:'200',message:'success',result:data});
        });
      });
  };

  function detailAboutUs(req,res,next) {
    var id = req.query.id;
    connection.acquire(function(err,con){
       if (err) throw err;
         con.query('SELECT * FROM master_about_us WHERE id = "'+id+'"', function(err,data){
           con.release();
           if(err)
             return res.json({status:400,message: 'failed',result:[]});
             return res.json({status:200,message:'success',result:data});
       });
    });
  };

}
module.exports = new Todo();
