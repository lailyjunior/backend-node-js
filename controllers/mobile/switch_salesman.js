//Auth : Ratri
var connection = require('../../config/db');

var request = require('request');

function Todo() {

    this.selectCustomerProspect = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        var salesmen_id = req.query.salesmen_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.id, c.cust_nama, c.cust_alamat, c.cust_hp, c.salesman_id, c.status_profile, UPPER(ms.status) status,c.status_generate_customer FROM customer_prospect c, master_status ms WHERE c.status_profile = ms.`code` AND c.salesman_id= "' + salesmen_id + '" GROUP BY c.cust_nama asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.filterCustomerProspectNonDms = function(req, res, next) {
        var id = req.query.id;
        // var supervisor_id = req.body.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.id, c.cust_nama, c.cust_alamat, c.cust_hp, c.salesman_id, c.status_profile, UPPER(ms.status) status, u.supervisor_id, s.sls_nama, c.status_generate_customer FROM customer_prospect c, salesmen s, users u, master_status ms WHERE c.status_profile = ms.`code` AND c.salesman_id = s.id AND u.supervisor_id= "' + id + '" GROUP BY c.cust_nama asc', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

    this.filterCustomerProspect = function(req, res, next) {
        var id = req.query.id;
        var dataSales = [];
        // var supervisor_id = req.body.supervisor_id;
        request.get({
            uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + id,
            auth: {
                'bearer': '71D55F9957529'
            },
            rejectUnauthorized: false, //add when working with https sites
            requestCert: false, //add when working with https sites
            agent: false, //add when working with https sites

        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                obj = JSON.parse(body);

                obj.forEach(function(item) {
                    dataSales.push("'" + item.id + "'");
                });

                console.log(id);
                console.log(dataSales);

                connection.acquire(function(err, con) {
                    if (err) throw err;
                    con.query("SELECT c.id, c.cust_nama, c.cust_alamat, c.cust_hp, c.salesman_id, c.status_profile, UPPER(ms.status) status, c.status_generate_customer FROM customer_prospect c, master_status ms WHERE c.status_profile = ms.`code` AND c.salesman_id IN (" + dataSales + ") GROUP BY c.cust_nama asc", function(err, data) {
                        con.release();
                        if (err)
                            return res.json({ status: 400, message: 'failed', result: [] });
                        return res.json(data);
                    });
                });
            }
        });

    };

    this.put = function(req, res, next) {
        var id = req.body.id;
        var assigned_to = req.body.assigned_to;
        var salesman_id = req.body.salesman_id;
        var modified_at = new Date();
        var modi_by = req.body.modi_by;

        connection.acquire(function(err, con, result, fields) {
            if (err) throw err;
            con.query('UPDATE customer_prospect SET assigned_to =?, salesman_id = ?, modified = ?, modi_by =? WHERE id = "' + id + '"', [assigned_to, salesman_id, modified_at, modi_by], function(err, results, fields) {
                con.release();
                connection.acquire(function(err, con) {
                    con.query('SELECT * FROM customer_prospect where id = "' + id + '" ', [id], function(err, results, fields) {
                        con.release();
                        res.json(results[0]);

                    })
                })
            });
        });

    };

    this.detailCustomerProspect = function(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT c.* FROM customer_prospect c where c.id = "' + id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data[0]);
            });
        });
    };

    //drpdown
    this.selectSalesman = function(req, res, next) {
        var supervisor_id = req.query.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT id, sls_nama, supervisor_id, supervisor_kode FROM salesmen WHERE supervisor_id = "' + supervisor_id + '"', function(err, data) {
                con.release();
                if (err)
                    return res.json({ status: 400, message: 'failed', result: [] });
                return res.json(data);
            });
        });
    };

}
module.exports = new Todo();