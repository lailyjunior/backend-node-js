//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    const fs = require('fs');
    this.selectMasterTraining = function(req, res, next) {
        var id = req.query.id;
        var toast = req.query.toast;
        if (id != null) {
            detailMasterTraining(req, res, next);
            return;
        }
        connection.acquire(function(err, con) {
            con.query('SELECT * FROM master_training order by modified_at desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    if (toast != null) {
                        if (toast == 'add') {
                            res.render('master_training/index', { obj: obj, message: 'Add Data Success' });
                        } else if (toast == 'edit') {
                            res.render('master_training/index', { obj: obj, message: 'Update Data Success' });
                        } else {
                            res.render('master_training/index', { obj: obj, message: 'Delete Data Success' });
                        }
                    } else {
                        res.render('master_training/index', { obj: obj, message: null });
                    }
                }
            });
        });
    };

    this.insertMasterTraining = function(req, res) {
        connection.acquire(function(err, con) {
            con.query('SELECT * FROM master_training_category order by category asc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    res.render('master_training/add', { obj: obj });
                }
            });
        });
    };

    Object.byString = function(o, s, d) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, ''); // strip a leading dot
        var a = s.split('.');
        var newO = o;
        for (var i = 0, n = a.length; i < n; ++i) {
            var k = a[i];
            if (i == a.length - 1) {
                newO[k] = d
            }
            newO = newO[k];
        }
        return o;
    }

    this.submitInsertMasterTraining = function(req, res) {

        var attachment = req.files;
        attachment.forEach(x => Object.byString(req.body, x.fieldname, x));
        var detail = req.body.detail;
        // res.send(detail);
        // return;

        //data header
        var id = uuidv1();
        var code = req.body.code;
        var title = req.body.title;
        var description = req.body.description;
        var category_id = req.body.category_id;
        var type = req.body.type;
        var level = req.body.level;
        var created_at = new Date();
        var modified_at = new Date();
        var created_by = req.session.user.id;
        var modi_by = req.session.user.id;

        connection.acquire(function(err, con) {
            con.query('SELECT category FROM master_training_category where id = "' + category_id + '"', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    var category = rows[0].category;
                    con.query('INSERT INTO master_training (id, code, title, description, category_id, category, type, level, created_at, modified_at, created_by, modi_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [id, code, title, description, category_id, category, type, level, created_at, modified_at, created_by, modi_by], function(err, result) {
                        if (err) {
                            res.send({ status: 400, message: 'Master training creation failed' });
                        } else {
                            if (attachment.length > 0) {
                                var values = [];

                                detail.forEach(element => {
                                    var detail_title = element.title;
                                    var kategori = element.kategori_detail;
                                    if (kategori == "dokumen") {
                                        var detail_kategori = 0;
                                    } else {
                                        var detail_kategori = 1;
                                    }

                                    var cover = "/files/" + escape(element.cover.filename);
                                    var document = "/files/" + escape(element.document.filename);

                                    var id_detail = uuidv1();
                                    values.push([id_detail, id, detail_title, detail_kategori, cover, document, created_at, modified_at, created_by, modi_by]);
                                });
                                // attachment.forEach(element => {

                                //     var cover = "/files/" + element.filename;
                                //     var fileTraining = "/files/" + element.filename;
                                //     var filename = element.filename;

                                // });
                                connection.acquire(function(err, con) {
                                    if (err) throw err;
                                    con.query("INSERT INTO master_training_detail(id, id_training, title_attachment, category_detail, cover, attachment, created_at,modified_at,created_by,modi_by) VALUES ?", [values], function(err, rows) {
                                        con.release();
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            res.redirect('/MasterTraining/Index?toast=' + 'add');
                                        }
                                    });
                                });
                            } else {
                                res.redirect('/MasterTraining/Index?toast=' + 'add');
                            }
                        }
                    });
                }
            });
        });
    }

    this.editMasterTraining = function(req, res) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            con.query('SELECT * FROM master_training_category', function(err, result) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    var category = JSON.parse(JSON.stringify(result));
                    con.query('SELECT * FROM master_training WHERE id = "' + id + '" order by modified_at desc', function(err, rows) {
                        if (err) {
                            console.log(err);
                        } else {
                            obj = JSON.parse(JSON.stringify(rows));
                            connection.acquire(function(err, con) {
                                con.query('SELECT * FROM master_training_detail WHERE id_training = ?', [id], function(err, result) {
                                    con.release();
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        objDetail = JSON.parse(JSON.stringify(result));
                                        res.render('master_training/edit', { obj: obj, objdet: objDetail, category: category });
                                    }
                                });
                            });
                        }
                    });
                }
            });
        });
    };

    this.submitEditMasterTraining = function(req, res) {

        var id = req.query.id;
        var code = req.body.code;
        var title = req.body.title;
        var description = req.body.description;
        var category_id = req.body.category_id;
        var type = req.body.type;
        var level = req.body.level;
        var created_at = new Date();
        var created_by = req.session.user.id;
        var modified_at = new Date();
        var modi_by = req.session.user.id;

        var fileTraining = req.files;
        fileTraining.forEach(x => Object.byString(req.body, x.fieldname, x));
        var detail = req.body.detail;
        // res.send(detail);
        // return;

        connection.acquire(function(err, con) {
            con.query('SELECT * FROM master_training_category WHERE id = ?', [category_id], function(err, result) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    var category = result[0].category;
                    con.query('UPDATE master_training SET code = ?, title = ?, description = ?, category_id = ?, category =?, type = ?, level = ?, modified_at = ?, modi_by = ? WHERE id = "' + id + '"', [code, title, description, category_id, category, type, level, modified_at, modi_by], function(err, result) {
                        if (err) {
                            res.send({ status: 400, message: 'Master training update failed' });
                        } else {
                            if (detail.length > 0) {
                                var valuesAdd = [];
                                var valuesUpdate = [];

                                detail.forEach(element => {
                                    var detail_title = element.title;
                                    var kategori = element.kategori_detail;
                                    if (kategori == "dokumen") {
                                        var detail_kategori = 0;
                                    } else {
                                        var detail_kategori = 1;
                                    }

                                    if (element.id == "0") {
                                        var id_detail = uuidv1();
                                        var cover = "/files/" + escape(element.cover.filename);
                                        var document = "/files/" + escape(element.document.filename);
                                        valuesAdd.push([id_detail, id, detail_title, cover, detail_kategori, document, created_at, modified_at, created_by, modi_by]);
                                    } else {
                                        var id_detail = element.id;
                                        var cover = "/files/" + element.cover.filename;
                                        var document = "/files/" + element.document.filename;

                                        valuesUpdate.push([id_detail, id, detail_title, cover, detail_kategori, document, created_at, modified_at, created_by, modi_by]);

                                    }
                                });
                                if (valuesAdd.length > 0) {
                                    //query add
                                    connection.acquire(function(err, con) {
                                        if (err) throw err;
                                        con.query("INSERT INTO master_training_detail VALUES ?", [valuesAdd], function(err, rows) {
                                            con.release();
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                if (valuesUpdate.length > 0) {
                                                    var ids = [];
                                                    detail.forEach(element => {
                                                        var flag = element.id;
                                                        if (flag != '0') {
                                                            var id = flag;
                                                            var idInput = "'" + id + "'";
                                                            ids.push([idInput]);
                                                        }
                                                    });
                                                    connection.acquire(function(err, con) {
                                                        if (err) throw err;
                                                        con.query("SELECT cover, attachment from master_training_detail where id IN (" + ids + ")", function(err, data) {
                                                            con.release();
                                                            if (err) {
                                                                console.log(err);
                                                            } else {
                                                                //query update
                                                                connection.acquire(function(err, con) {
                                                                    if (err) throw err;
                                                                    con.query("INSERT INTO master_training_detail(id, id_training, title_attachment, cover, category_detail, attachment, created_at,modified_at,created_by,modi_by)  VALUES ? ON DUPLICATE KEY UPDATE attachment = VALUES(attachment) ,title_attachment = VALUES(title_attachment),cover = VALUES(cover),category_detail = VALUES(category_detail)", [valuesUpdate], function(err, rows) {
                                                                        con.release();
                                                                        if (err) {
                                                                            console.log(err);
                                                                        } else {
                                                                            data.forEach(element => {
                                                                                var cover = unescape(element.cover);
                                                                                var doc = unescape(element.attachment);

                                                                                // fs.unlink(appRoot + cover.replace("/files/", "/uploads/"), (err) => {
                                                                                //     if (err) throw err;
                                                                                //     fs.unlink(appRoot + doc.replace("/files/", "/uploads/"), (err) => {
                                                                                //         if (err) throw err;
                                                                                //         console.log('successfully deleted /tmp/hello');
                                                                                //     });
                                                                                // });
                                                                            });
                                                                            res.redirect('/MasterTraining/Index?toast=' + 'edit');
                                                                        }
                                                                    });
                                                                });
                                                            }
                                                        });
                                                    });
                                                } else {
                                                    res.redirect('/MasterTraining/Index?toast=' + 'edit');
                                                }
                                            }
                                        });
                                    });
                                } else if (valuesUpdate.length > 0) {
                                    var ids = [];
                                    detail.forEach(element => {
                                        var flag = element.id;
                                        if (flag != '0') {
                                            var id = flag;
                                            var idInput = "'" + id + "'";
                                            ids.push([idInput]);
                                        }
                                    });
                                    connection.acquire(function(err, con) {
                                        if (err) throw err;
                                        con.query("SELECT cover, attachment from master_training_detail where id IN (" + ids + ")", function(err, data) {
                                            con.release();
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                //query update
                                                connection.acquire(function(err, con) {
                                                    if (err) throw err;
                                                    con.query("INSERT INTO master_training_detail(id, id_training, title_attachment, cover, category_detail, attachment, created_at,modified_at,created_by,modi_by)  VALUES ? ON DUPLICATE KEY UPDATE attachment = VALUES(attachment) ,title_attachment = VALUES(title_attachment),cover = VALUES(cover),category_detail = VALUES(category_detail)", [valuesUpdate], function(err, rows) {
                                                        con.release();
                                                        if (err) {
                                                            console.log(err);
                                                        } else {
                                                            data.forEach(element => {
                                                                var cover = unescape(element.cover);
                                                                var doc = unescape(element.attachment);
                                                                // fs.unlink(appRoot + cover.replace("/files/", "/uploads/"), (err) => {
                                                                //     if (err) throw err;
                                                                //     fs.unlink(appRoot + doc.replace("/files/", "/uploads/"), (err) => {
                                                                //         if (err) throw err;
                                                                //         console.log('successfully deleted /tmp/hello');
                                                                //     });
                                                                // });
                                                            });
                                                            res.redirect('/MasterTraining/Index?toast=' + 'edit');
                                                        }
                                                    });
                                                });
                                            }
                                        });
                                    });
                                }
                            } else {
                                res.redirect('/MasterTraining/Index?toast=' + 'edit');
                            }
                        }
                    });
                }
            });
        });
    };

    this.deleteMasterTraining = function(req, res) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT cover,attachment from master_training_detail where id_training = ?', [id], function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    if (rows.length > 0) {
                        var values = [];
                        var valueCover = [];
                        rows.forEach(element => {
                            var image = element.attachment;
                            var cover = element.cover;
                            values.push(image);
                            valueCover.push(cover);
                        });

                        connection.acquire(function(err, con) {
                            if (err) throw err;
                            con.query('DELETE FROM master_training_detail WHERE attachment IN ("' + [values] + '") and cover IN ("' + [valueCover] + '")', function(err, result) {
                                con.release();
                                if (err) {
                                    console.log(err);
                                } else {
                                    rows.forEach(element => {
                                        var cover = unescape(element.cover);
                                        var document = unescape(element.attachment);
                                        fs.unlink(appRoot + cover.replace("/files/", "/uploads/"), (err) => {
                                            if (err) throw err;
                                            fs.unlink(appRoot + document.replace("/files/", "/uploads/"), (err) => {
                                                if (err) throw err;
                                                console.log('successfully deleted /tmp/hello');
                                            });
                                        });
                                    });
                                    connection.acquire(function(err, con) {
                                        con.query('DELETE FROM master_training WHERE id = "' + id + '"', function(err, result) {
                                            con.release();
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                res.redirect('/MasterTraining/Index?toast=' + 'delete');
                                            }
                                        });
                                    });
                                }
                            });

                        });
                    } else {
                        connection.acquire(function(err, con) {
                            con.query('DELETE FROM master_training WHERE id = "' + id + '"', function(err, result) {
                                con.release();
                                if (err) {
                                    console.log(err);
                                } else {
                                    res.redirect('/MasterTraining/Index?toast=' + 'delete');
                                }
                            });
                        });
                    }
                }
            });
        });
    };

    function detailMasterTraining(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM master_training WHERE id = "' + id + '" order by modified_at desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    connection.acquire(function(err, con) {
                        con.query('SELECT * FROM master_training_detail WHERE id_training = ? order by modified_at desc', [id], function(err, result) {
                            con.release();
                            if (err) {
                                console.log(err);
                            } else {
                                objDetail = JSON.parse(JSON.stringify(result));
                                // res.send(objDetail);
                                // return;
                                res.render('master_training/view', { obj: obj, objdet: objDetail });
                            }
                        });
                    });
                }
            });
        });
    };
}
module.exports = new Todo();