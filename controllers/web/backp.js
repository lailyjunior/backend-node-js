//Auth : Lely
var connection = require('../config/db');

function Todo() {

    this.home = function(req, res) {
        store.db.all('SELECT * FROM ' + store.table,
            function(err, rows) {
                if (err) return;
                if (!rows) return;
                rows.forEach(element => {
                    var sess = JSON.parse(element.sess);
                    if (sess.user != null) {
                        var user = sess.user;
                    }
                });
            }
        );
        connection.acquire(function(err, con) {
            res.render('index');
        });
    };

    this.login = function(req, res) {
        var toast = req.query.toast;
        connection.acquire(function(err, con) {
            if (toast != null) {
                if (toast == 'success') {
                    res.render('./index', { message: 'Login Success' });
                } else if (toast == 'duplicate') {
                    res.render('./login', { message: 'Cant Login' });
                } else if (toast == 'password') {
                    res.render('./login', { message: 'Wrong Password' });
                } else if (toast == 'passuser') {
                    res.render('./login', { message: 'Wrong Username Or Password' });
                } else if (toast == 'logout') {
                    res.render('./login', { message: 'Logout Success' });
                }
            } else {
                res.render('./login', { message: null });
            }
        });
    };

    this.submitLogin = function(req, res) {
        var username = req.body.username;
        var password = req.body.password;
        var validpass = md5(password);
        var last_login = new Date();

        connection.acquire(function(err, con) {
            var islogin = "0"
            con.query('SELECT u.id, u.username, u.name, u.password, u.group_id, u.salesman_id, g.title, u.is_login FROM users u, groups g WHERE g.group_id = u.group_id AND u.username = ? ', [username], function(error, results, fields) {
                con.release();
                if (error) throw error;
                if (results.length > 0) {
                    var is_login = results[0].is_login;
                    req.session.user = results[0];
                    req.session.isLogin = true;

                    store.db.all('SELECT * FROM ' + store.table,
                        function(err, rows) {
                            if (err) return;
                            if (!rows) return;
                            // for (var i = 0; i < rows.length; i++) {
                            //     var element = rows[1];
                            //     var sess = JSON.parse(element.sess);

                            //     if (sess.user != null) {
                            //         var user = sess.user;
                            //         if (user.id == results[0].id) {
                            //             res.redirect('/login?toast=' + 'duplicate');
                            //             return;
                            //         }
                            //     }
                            // }
                            rows.forEach(element => {
                                var sess = JSON.parse(element.sess);
                                if (sess.user != null) {
                                    var user = sess.user;
                                    if (user.id == results[0].id) {
                                        return;
                                    }
                                }
                            });

                            res.redirect('/login?toast=' + 'success');

                        }
                    );
                    return;
                    if (validpass == results[0].password) {
                        if (results[0].is_login == "0") {
                            connection.acquire(function(err, con) {
                                con.query('UPDATE users set is_login = ?, last_login = ? where username= "' + username + '"', [islogin, last_login], function(error, results, fields) {
                                    con.release();
                                    connection.acquire(function(err, con) {
                                        con.query('SELECT u.id, u.username, u.name, u.password, u.group_id, u.salesman_id, g.title, u.is_login FROM users u, groups g WHERE g.group_id = u.group_id AND u.username = ?', [username], function(error, results, fields) {
                                            con.release();
                                            req.session.user = results[0];
                                            req.session.isLogin = true;

                                            store.db.all('SELECT * FROM ' + store.table,
                                                function(err, rows) {
                                                    if (err) return;
                                                    if (!rows) return;
                                                    rows.forEach(element => {
                                                        var sess = JSON.parse(element.sess);
                                                        if (sess.user != null) {
                                                            var user = sess.user;
                                                            if (user.id == results[0].id) {
                                                                return;
                                                            }
                                                        }
                                                    });
                                                }
                                            );
                                            res.redirect('/login?toast=' + 'success');
                                        });
                                    });
                                });
                            });
                        } else {
                            res.redirect('/login?toast=' + 'duplicate');
                        }
                    } else {
                        res.redirect('/login?toast=' + 'password');
                    }
                } else {
                    res.redirect('/login?toast=' + 'passuser');
                }
            });
        });
    };

    this.logout = function(req, res) {

        if (req.session == null && req.session.user == null) {
            res.redirect('/login');
            return;
        }
        // return;
        var id = req.session.user.id;
        var is_login = '0';
        var modified_at = new Date();
        var modi_by = 'lely';
        connection.acquire(function(err, con) {
            con.query('UPDATE users SET is_login = ?, modified_at = ?, modi_by = ? WHERE id = "' + id + '"', [is_login, modified_at, modi_by], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'logout failed' });
                } else {
                    // Destroy session when logout
                    req.session.destroy(function(err) {
                        if (err) console.log(err);
                    });
                    res.redirect('/login?toast=' + 'logout');
                }
            });
        });
    };
}
module.exports = new Todo();