//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    var request = require('request');
    var xlstojson = require("xls-to-json-lc");
    var xlsxtojson = require("xlsx-to-json-lc");

    this.selectCustomerProspect = function(req, res, next) {
        var toast = req.query.toast;
        var supervisor_id = req.session.user.supervisor_id;
        var versi = req.session.user.versi;

        connection.acquire(function(err, con) {
            if (versi == '0') {
                con.query("select c.cust_code, c.cust_nama, c.cust_alamat, c.cust_hp, c.cust_kelamin, c.status_profile, u.name from customer_prospect c JOIN users u  ON u.id = c.create_by where u.salesman_id IN (SELECT id from salesmen where supervisor_id = '" + supervisor_id + "') OR u.supervisor_id = '" + supervisor_id + "' group by c.cust_code order by c.cust_code desc", function(err, rows) {
                    console.log('this.sql ', this.sql);
                    con.release();
                    if (err) {
                        console.log(err);
                    } else {
                        obj = JSON.parse(JSON.stringify(rows));
                        if (toast != null) {
                            if (toast == 'upload') {
                                res.render('master_customer_prospect/index', { obj: obj, message: 'Upload Data Success' });
                            }
                        } else {
                            res.render('master_customer_prospect/index', { obj: obj, message: null });
                        }
                    }
                });
            } else if (versi == '1') {
                var dataSales = [];
                request.get({
                    uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
                    auth: {
                        'bearer': '71D55F9957529'
                    },
                    rejectUnauthorized: false, //add when working with https sites
                    requestCert: false, //add when working with https sites
                    agent: false, //add when working with https sites
                }, function(error, response, body) {
                    if (error) {
                        console.log(error);
                    } else {
                        obj = JSON.parse(body);

                        obj.forEach(function(item) {
                            dataSales.push("'" + item.id + "'");
                        });

                        console.log(supervisor_id);
                        console.log(dataSales);

                        if (dataSales.length === 0) {
                            connection.acquire(function(err, con) {
                                if (err) throw err;
                                con.query("select c.cust_code, c.cust_nama, c.cust_alamat, c.cust_hp, c.cust_kelamin, c.status_profile, u.name from customer_prospect c JOIN users u  ON u.id = c.create_by where u.salesman_id IN ('" + dataSales + "') OR u.supervisor_id = '" + supervisor_id + "' group by c.cust_code order by c.cust_code desc", function(err, data) {
                                    con.release();
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        obj = JSON.parse(JSON.stringify(data));
                                        if (toast != null) {
                                            if (toast == 'upload') {
                                                res.render('master_customer_prospect/index', { obj: obj, message: 'Upload Data Success' });
                                            }
                                        } else {
                                            res.render('master_customer_prospect/index', { obj: obj, message: null });
                                        }
                                    }
                                });
                            });
                        } else {
                            connection.acquire(function(err, con) {
                                if (err) throw err;
                                con.query("select c.cust_code, c.cust_nama, c.cust_alamat, c.cust_hp, c.cust_kelamin, c.status_profile, u.name from customer_prospect c JOIN users u  ON u.id = c.create_by where u.salesman_id IN (" + dataSales + ") OR u.supervisor_id = '" + supervisor_id + "' group by c.cust_code order by c.cust_code desc", function(err, data) {

                                    con.release();
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        obj = JSON.parse(JSON.stringify(data));
                                        if (toast != null) {
                                            if (toast == 'upload') {
                                                res.render('master_customer_prospect/index', { obj: obj, message: 'Upload Data Success' });
                                            }
                                        } else {
                                            res.render('master_customer_prospect/index', { obj: obj, message: null });
                                        }
                                    }
                                });
                            });
                        }
                    }
                });
            } else {
                con.query("select c.cust_code, c.cust_nama, c.cust_alamat, c.cust_hp, c.cust_kelamin, c.status_profile, u.name from customer_prospect c JOIN users u ON u.id = c.create_by group by c.cust_code order by c.cust_code desc", function(err, rows) {
                    console.log('this.sql ', this.sql);
                    con.release();
                    if (err) {
                        console.log(err);
                    } else {
                        obj = JSON.parse(JSON.stringify(rows));
                        if (toast != null) {
                            if (toast == 'upload') {
                                res.render('master_customer_prospect/index', { obj: obj, message: 'Upload Data Success' });
                            }
                        } else {
                            res.render('master_customer_prospect/index', { obj: obj, message: null });
                        }
                    }
                });
            }
        });
    };

    this.insertCustomerProspect = function(req, res) {
        var toast = req.query.toast;
        if (toast != null) {
            if (toast == 'error') {
                res.render('master_customer_prospect/add', { message: 'File Tidak Sesuai (Harus file .xls/.xlsx)' });
            }
            if (toast == 'kosong') {
                res.render('master_customer_prospect/add', { message: 'Nama, alamat, hp tidak boleh kosong' });
            }
            if (toast == 'nama') {
                res.render('master_customer_prospect/add', { message: 'Nama Customer tidak boleh kosong' });
            }
            if (toast == 'alamat') {
                res.render('master_customer_prospect/add', { message: 'Alamat Customer tidak boleh kosong' });
            }
            if (toast == 'hp') {
                res.render('master_customer_prospect/add', { message: 'Np Hp Customer tidak boleh kosong' });
            }
        } else {
            res.render('master_customer_prospect/add', { message: null });
        }
    };

    this.uploadCustomerProspect = function(req, res) {
        var exceltojson;
        // if (err) {
        //     res.json({ error_code: 1, err_desc: err });
        //     return;
        // }
        /** Multer gives us file info in req.file object */
        if ((!req.files && req.files.length == 0) && !req.file) {
            console.log({ error_code: 1, err_desc: "No file passed" });
            return;
        }
        var size = req.files.length;
        if (req.files.length == 0) {
            res.redirect('/MasterCustomerProspect/Add?toast=' + 'error');
        } else {
            if (req.files[0].originalname.split('.')[req.files[0].originalname.split('.').length - 1] === 'xlsx') {
                exceltojson = xlsxtojson;
            } else {
                exceltojson = xlstojson;
            }
            console.log(req.files[0].path);
            try {
                exceltojson({
                    input: req.files[0].path,
                    output: null, //since we don't need output.json
                    lowerCaseHeaders: true
                }, function(err, excelJson) {
                    if (err) {
                        return res.status(400);
                    }

                    connection.acquire(function(err, con) {
                        con.query('select cust_code from customer_prospect order by cust_code desc limit 1', function(err, result) {
                            if (err) {
                                res.send({ status: 400, message: 'Failed' });
                            } else {
                                // for CODE CP
                                if (result.length != 0) {
                                    var test = result[0].cust_code;
                                } else {
                                    var test = "CP-000000000000"
                                }
                                var pieces = test.split('-');
                                var prefix = pieces[0];
                                var lastNum = pieces[1];
                                lastNum = parseInt(lastNum, 10);

                                var insertedData = [];
                                // excelJson.forEach(element => {

                                //     if (!element.nama || !element.alamat || !element.hp) {
                                //         res.redirect('/MasterCustomerProspect/Add?toast=' + 'kosong');
                                //         return;
                                //     }

                                //     element.id = uuidv1();
                                //     lastNum++;
                                //     cust_code = "CP-" + ("000000000000" + lastNum).substr(-11);
                                //     element.cust_code = cust_code;
                                //     element.created = new Date();
                                //     element.modified = new Date();
                                //     element.create_by = req.session.user.id;
                                //     element.modi_by = req.session.user.id;
                                //     element.supervisor_id = req.session.user.supervisor_id;
                                //     element.branch_id = req.session.user.branch_id;
                                //     element.branch_code = req.session.user.branch_code;
                                //     element.status_profile = '1';
                                //     element.status_deal = '1';
                                //     element.status_generate_customer = '0';
                                //     element.company_id = req.session.user.company_id;
                                //     element.company_code = req.session.user.company_code;
                                //     element.sumber = '3';

                                //     var temp = []
                                //     temp.push(element.id);
                                //     temp.push(element.cust_code);
                                //     temp.push(element.nama);
                                //     temp.push(element.alamat);
                                //     temp.push(element.rt);
                                //     temp.push(element.rw);
                                //     temp.push(element.hp);
                                //     temp.push(element.whatsapp);
                                //     temp.push(element.jenis_kelamin);
                                //     temp.push(element.email);
                                //     temp.push(element.ktp);
                                //     temp.push(element.telpon_rumah);
                                //     temp.push(element.telpon_kantor);
                                //     temp.push(element.fax);
                                //     temp.push(element.tanggal_lahir);
                                //     temp.push(element.npwp);
                                //     temp.push(element.created);
                                //     temp.push(element.modified);
                                //     temp.push(element.create_by);
                                //     temp.push(element.modi_by);
                                //     temp.push(element.supervisor_id);
                                //     temp.push(element.branch_id);
                                //     temp.push(element.branch_code);
                                //     temp.push(element.status_profile);
                                //     temp.push(element.status_deal);
                                //     temp.push(element.status_generate_customer);
                                //     temp.push(element.company_id);
                                //     temp.push(element.company_code);
                                //     temp.push(element.sumber);
                                //     insertedData.push(temp);
                                // });
                                var element = excelJson;
                                for (var i = 0; i < element.length; i++) {
                                    console.log(element[i].nama);
                                    if (!element[i].nama) {
                                        res.redirect('/MasterCustomerProspect/Add?toast=' + 'nama');
                                        return;
                                    }

                                    if (!excelJson[i].alamat) {
                                        res.redirect('/MasterCustomerProspect/Add?toast=' + 'alamat');
                                        return;
                                    }

                                    if (!excelJson[i].hp) {
                                        res.redirect('/MasterCustomerProspect/Add?toast=' + 'hp');
                                        return;
                                    }

                                    element[i].id = uuidv1();
                                    lastNum++;
                                    cust_code = "CP-" + ("000000000000" + lastNum).substr(-11);
                                    element[i].cust_code = cust_code;
                                    element[i].created = new Date();
                                    element[i].modified = new Date();
                                    element[i].create_by = req.session.user.id;
                                    element[i].modi_by = req.session.user.id;
                                    element[i].supervisor_id = req.session.user.supervisor_id;
                                    element[i].branch_id = req.session.user.branch_id;
                                    element[i].branch_code = req.session.user.branch_code;
                                    element[i].status_profile = '1';
                                    element[i].status_deal = '1';
                                    element[i].status_generate_customer = '0';
                                    element[i].company_id = req.session.user.company_id;
                                    element[i].company_code = req.session.user.company_code;
                                    element[i].sumber = '3';

                                    var temp = []
                                    temp.push(element[i].id);
                                    temp.push(element[i].cust_code);
                                    temp.push(element[i].nama);
                                    temp.push(element[i].alamat);
                                    temp.push(element[i].rt);
                                    temp.push(element[i].rw);
                                    temp.push(element[i].hp);
                                    temp.push(element[i].whatsapp);
                                    temp.push(element[i].jenis_kelamin);
                                    temp.push(element[i].email);
                                    temp.push(element[i].ktp);
                                    temp.push(element[i].telpon_rumah);
                                    temp.push(element[i].telpon_kantor);
                                    temp.push(element[i].fax);
                                    temp.push(element[i].tanggal_lahir);
                                    temp.push(element[i].npwp);
                                    temp.push(element[i].created);
                                    temp.push(element[i].modified);
                                    temp.push(element[i].create_by);
                                    temp.push(element[i].modi_by);
                                    temp.push(element[i].supervisor_id);
                                    temp.push(element[i].branch_id);
                                    temp.push(element[i].branch_code);
                                    temp.push(element[i].status_profile);
                                    temp.push(element[i].status_deal);
                                    temp.push(element[i].status_generate_customer);
                                    temp.push(element[i].company_id);
                                    temp.push(element[i].company_code);
                                    temp.push(element[i].sumber);
                                    insertedData.push(temp);
                                }

                                con.query("INSERT INTO customer_prospect(id, cust_code, cust_nama, cust_alamat, cust_rt, cust_rw, cust_hp, cust_hp_wa, cust_kelamin, cust_email, cust_noktp, cust_telp_rumah, cust_telp_kantor, cust_fax, cust_tgl_lahir, cust_npwp, created, modified, create_by, modi_by, supervisor_id, branch_id, branch_code, status_profile, status_deal, status_generate_customer, company_id, company_code, cust_sumber) VALUES ?", [insertedData], function(err, rows) {
                                    con.release();
                                    if (err) {
                                        console.log(err);
                                        res.json(err);
                                    } else {
                                        res.redirect('/MasterCustomerProspect/Index?toast=' + 'upload');
                                    }
                                });
                            }
                        });
                    });
                });
            } catch (e) {
                res.redirect('/MasterCustomerProspect/Add?toast=' + 'error');
            }
        }
    }
}
module.exports = new Todo()