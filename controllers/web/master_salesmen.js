//Auth : Nina
var connection = require('../../config/db');

function Todo() {
    this.selectMasterSalesmen = function(req, res, next) {
        var id = req.query.id;
        var toast = req.query.toast;
        if (id != null) {
            detailMasterSalesmen(req, res, next);
            return;
        }
        connection.acquire(function(err, con) {
            con.query('SELECT * FROM salesmen', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query('SELECT sv.id, sv.supervisor_nama, sv.supervisor_kode, sv.branch_id, bc.name as branch_name, sv.branch_code FROM supervisors sv, branch_nondms bc WHERE  sv.branch_id = bc.id COLLATE utf8_unicode_ci order by sv.supervisor_nama desc', function(err, dataSupervisor) {
                            con.release();
                            if (err) {
                                console.log(err);
                            } else {
                                objSupervisor = JSON.parse(JSON.stringify(dataSupervisor));
                                obj = JSON.parse(JSON.stringify(rows));
                                if (toast != null) {
                                    if (toast == 'add') {
                                        res.render('master_salesmen/index', { obj: obj, message: 'Add Data Success' });
                                    } else if (toast == 'edit') {
                                        res.render('master_salesmen/index', { obj: obj, message: 'Update Data Success' });
                                    } else {
                                        res.render('master_salesmen/index', { obj: obj, message: 'Delete Data Success' });
                                    }
                                } else {
                                    res.render('master_salesmen/index', { obj: obj, message: null });
                                }
                            }
                        });
                    });
                }
            });
        });
    };

    this.insertMasterSalesmen = function(req, res) {
        connection.acquire(function(err, con) {
            res.render('master_salesmen/add');
        });
    };

    this.submitInsertMasterSalesmen = function(req, res) {
        // res.send(req.body);
        // return;
        var id = uuidv1();
        var sls_kode = req.body.sls_kode;
        var sls_nama = req.body.sls_nama;
        var sls_alamat = req.body.sls_alamat;
        var sls_alamat_ktp = req.body.sls_alamat_ktp;
        var sls_hp = req.body.sls_hp;
        var sls_email = req.body.sls_email;
        var supervisor_id = req.body.supervisor_id;
        var supervisor_kode = req.body.supervisor_kode;
        var sls_status = req.body.sls_status;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var created = new Date();
        var modified = new Date();
        var create_by = '1';
        var modi_by = '1';

        connection.acquire(function(err, con) {
            con.query('INSERT INTO salesmen (id, sls_kode, sls_nama, sls_alamat, sls_alamat_ktp, sls_hp, sls_email,supervisor_id, supervisor_kode, sls_status, branch_id, branch_code, created, modified, create_by, modi_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [id, sls_kode, sls_nama, sls_alamat, sls_alamat_ktp, sls_hp, sls_email, supervisor_id, supervisor_kode, sls_status, branch_id, branch_code, created, modified, create_by, modi_by], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Master salesmen creation failed' });
                    console.log(err);
                } else {
                    res.redirect('/MasterSalesmen/Index?toast=' + 'add');
                }
            });
        });
    }

    this.editMasterSalesmen = function(req, res) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT s.id, s.sls_kode, s.sls_nama, s.sls_alamat, s.sls_alamat_ktp, s.sls_hp, s.sls_email, s.supervisor_id, s.supervisor_kode, s.sls_status, s.branch_id, s.branch_code, sv.supervisor_nama as supervisor_name, bc.name as branch_name FROM salesmen s, supervisors sv, branch_nondms bc WHERE bc.id = s.branch_id COLLATE utf8_unicode_ci and sv.id = s.supervisor_id COLLATE utf8_unicode_ci and s.id = "' + id + '"', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query('SELECT sv.id, sv.supervisor_nama, sv.supervisor_kode, sv.branch_id, bc.name as branch_name, sv.branch_code FROM supervisors sv, branch_nondms bc WHERE bc.id = sv.branch_id COLLATE utf8_unicode_ci order by supervisor_nama desc', function(err, dataSupervisor) {
                            con.release();
                            if (err) {
                                console.log(err);
                            } else {
                                objSupervisor = JSON.parse(JSON.stringify(dataSupervisor));
                                obj = JSON.parse(JSON.stringify(rows));
                                res.render('master_salesmen/edit', { obj: obj });
                            }
                        });
                    });
                }
            });
        });
    };

    this.submitEditMasterSalesmen = function(req, res) {
        var id = req.query.id;
        var sls_kode = req.body.sls_kode;
        var sls_nama = req.body.sls_nama;
        var sls_alamat = req.body.sls_alamat;
        var sls_alamat_ktp = req.body.sls_alamat_ktp;
        var sls_hp = req.body.sls_hp;
        var sls_email = req.body.sls_email;
        var supervisor_id = req.body.supervisor_id;
        var supervisor_kode = req.body.supervisor_kode;
        var sls_status = req.body.sls_status;
        var branch_id = req.body.branch_id;
        var branch_code = req.body.branch_code;
        var modified = new Date();
        var modi_by = '1';

        connection.acquire(function(err, con) {
            con.query('UPDATE salesmen SET sls_kode = ?, sls_nama = ?, sls_alamat = ?, sls_alamat_ktp = ?, sls_hp = ?, sls_email = ?, supervisor_id = ?, supervisor_kode = ?, sls_status = ?, branch_id = ?, branch_code = ?, modified = ?, modi_by = ? WHERE id = "' + id + '"', [sls_kode, sls_nama, sls_alamat, sls_alamat_ktp, sls_hp, sls_email, supervisor_id, supervisor_kode, sls_status, branch_id, branch_code, modified, modi_by], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Master salesmen update failed' });
                } else {
                    res.redirect('/MasterSalesmen/Index?toast=' + 'edit');
                }
            });
        });
    };

    this.deleteMasterSalesmen = function(req, res) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            con.query('DELETE FROM salesmen WHERE id = ?', [id], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Failed to delete master salesmen' });
                } else {
                    res.redirect('/MasterSalesmen/Index?toast=' + 'delete');
                }
            });
        });
    };

    function detailMasterSalesmen(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT s.id, s.sls_tipe, s.sls_grade, s.sls_kode, s.sls_nama, s.sls_alamat, s.sls_alamat_ktp, s.sls_hp, s.sls_email, s.sls_status, b.name as branch_name FROM salesmen s, branch_nondms b WHERE b.id=s.branch_id COLLATE utf8_unicode_ci and s.id =  "' + id + '"', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    res.render('master_salesmen/view', { obj: obj });
                }
            });
        });
    };
}
module.exports = new Todo();