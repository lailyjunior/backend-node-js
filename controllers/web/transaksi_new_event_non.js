//Auth : Ratri

var connection = require('../../config/db');

var request = require('request');

function Todo() {

    this.selectNewEvent = function(req, res, next) {
        var id = req.query.id;
        var toast = req.query.toast;
        var versi = "0";

        if (id != null) {
            detailNewEvent(req, res, next);
                return;
        }

        connection.acquire(function(err, con) {
            con.query('SELECT * FROM transaksi_new_events WHERE versi = "'+versi+'" order by modified desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    console.log(obj);
                    console.log(rows);
                    if (toast != null) {
                        if (toast == 'add') {
                            res.render('transaksi_new_event_non/index', { obj: obj, message: 'Add Data Success' });
                        } else if (toast == 'edit') {
                            res.render('transaksi_new_event_non/index', { obj: obj, message: 'Update Data Success' });
                        } else {
                            res.render('transaksi_new_event_non/index', { obj: obj, message: 'Delete Data Success' });
                        }
                    } else {
                        res.render('transaksi_new_event_non/index', { obj: obj, message: null });
                    }
                }
            });
        });
    };

    this.insertNewEvent = function(req, res) {
        connection.acquire(function(err, con) {
            con.query('SELECT id, event_type, description FROM master_event', function(err, lovEvent) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'failed' });
                } else {
                    connection.acquire(function(err, con) {
                        con.query('select event_code from transaksi_new_events order by created desc limit 1', function(err, result) {
                            con.release();
                            if (err) {
                                res.send({ status: 400, message: ' Failed' });
                            } else {
                                connection.acquire(function(err, con) {
                                    con.query('SELECT * FROM salesmen', function(err, lovSalesmen) {
                                        con.release();
                                        if (err) {
                                            res.send({ status: 400, message: ' Failed' });
                                        } else {
                                            // for CODE EVENT
                                            if (result.length != 0) {
                                                var date = new Date().toISOString().replace(/\T.+/, '').replace(/-/, '').replace(/-/, '')
                                                var test = result[0].event_code;
                                            } else {
                                                var date = new Date().toISOString().replace(/\T.+/, '').replace(/-/, '').replace(/-/, '')
                                                console.log(date);
                                                var test = "EV-" + date + "00"
                                                console.log(test);
                                            }

                                            var pieces = test.split('-');
                                            var prefix = pieces[0];
                                            var lastNum = pieces[1];
                                            lastNum = parseInt(lastNum, 10);
                                            lastNum++;
                                            code = "EV-" + date + ("00" + lastNum).substr(-2);
                                            console.log(code);

                                            objCode = code;
                                            objLOVSls = JSON.parse(JSON.stringify(lovSalesmen));
                                            objLOV = JSON.parse(JSON.stringify(lovEvent));
                                            res.render('transaksi_new_event_non/add');
                                        }
                                    });
                                });
                            }  
                        });
                    }); 
                    // obj = JSON.parse(body);
                    
                }
            });
        });
        // connection.acquire(function(err, con) {

        //     res.render('transaksi_new_event/add');
        // });
    };

    this.submitInsertNewEvent = function(req, res) {
        var id = uuidv1();
        var event_code = req.body.event_code;
        var title = req.body.title;
        var event_type_id = req.body.event_type_id;
        var detail = req.body.detail;
        var start = req.body.start;
        var end = req.body.end;
        var all_day = req.body.all_day;
        var status = req.body.status;
        var active = req.body.active;
        var sls_kode = req.body.sls_kode;
        var activity_code = req.body.activity_code;
        var location_name = req.body.location_name;
        var longitude = req.body.longitude;
        var latitude = req.body.latitude;
        var radius = req.body.radius;
        var created_at = new Date();
        var modified_at = new Date();
        var created_by = req.session.user.id;
        var modi_by = req.session.user.id;
        var versi = "0"

        connection.acquire(function(err, con) {
            con.query('INSERT INTO transaksi_new_events (id, event_code, title, event_type_id, detail, all_day, status, active, sls_kode, activity_code, start, end,location_name, longitude, latitude, radius, created, modified, create_by, modi_by, versi) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [id, event_code, title, event_type_id, detail, all_day, status, active, sls_kode, activity_code, start, end, location_name, longitude, latitude, radius, created_at, modified_at, created_by, modi_by, versi], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'New Event creation failed' });
                    console.log(err);
                } else {
                    res.redirect('/TransaksiNewEventNon/Index?toast=' + 'add');
                }
            });
        });
    }

    this.editNewEvent = function(req, res) {
        var id = req.query.id;
        // var start_date = ;
        // var dateFormat = require('dateFormat');
        // var day_start = dateFormat(start_date, "dd/mm/yyyy");
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT transaksi_new_events.*, master_event.event_type as event_type FROM transaksi_new_events INNER JOIN master_event ON transaksi_new_events.event_type_id=master_event.id COLLATE utf8_unicode_ci', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    connection.acquire(function(err, con) {
                        if(err) throw err;
                        con.query('SELECT id, event_type, description FROM master_event order by event_type desc', function(err, dataLOV){
                            con.release();
                            if(err) {
                                console.log(err);
                            } else {
                                connection.acquire(function(err, con) {
                                    if(err) throw err;
                                    con.query('SELECT * FROM salesmen', function(err, dataLOVsls){
                                        con.release();
                                        if(err) {
                                            console.log(err);
                                        } else {
                                            obj = JSON.parse(JSON.stringify(rows));
                                            objLOV = JSON.parse(JSON.stringify(dataLOV));
                                            objLOVsls = JSON.parse(JSON.stringify(dataLOVsls));

                                            res.render('transaksi_new_event_non/edit', { obj: obj, objLOV: objLOV });
                                        }
                                    })
                                });
                            }
                        })
                    });
                }
            });
        });
    };

    this.submitEditNewEvent = function(req, res) {
        var id = req.query.id;
        var sls_kode = req.body.sls_kode;
        var event_code = req.body.event_code;
        var activity_code = req.body.activity_code;
        var title = req.body.title;
        var id_type = req.body.id_type;
        var status = req.body.status;
        var active = req.body.active;
        var all_day = req.body.all_day;
        var detail = req.body.detail;
        var start = req.body.start;
        var end = req.body.end;
        var location_name = req.body.location_name;
        var longitude = req.body.longitude;
        var latitude = req.body.latitude;
        var radius = req.body.radius;
        var modified_at = new Date();
        var modi_by = req.session.user.id;
        connection.acquire(function(err, con) {
            con.query('UPDATE transaksi_new_events SET sls_kode = ?, event_code = ?, activity_code = ?, title = ?, id_type = ?, status = ?, detail = ?, active = ?, all_day = ?, start = ?, end = ?, location_name = ?, longitude = ?, latitude = ?, radius = ?, modified_at = ?, modi_by = ? WHERE id = "'+ id +'"', [sls_kode, event_code, activity_code, title, id_type, status, detail, active, all_day, start, end, location_name, longitude, latitude, radius, modified_at, modi_by], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Transaksi New Event update failed' });
                } else {
                    res.redirect('/TransaksiNewEventNon/Index?toast=' + 'edit');
                }
            });
        });
    };

    this.deleteNewEvent = function(req, res) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            con.query('DELETE FROM transaksi_new_events WHERE id = ?', [id], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Failed to delete transaksi new event' });
                } else {
                    res.redirect('/TransaksiNewEventNon/Index?toast=' + 'delete');
                }
            });
        });
    }; 

    function detailNewEvent(req, res, next) {
        var id = req.query.id;
        console.log(id);
        connection.acquire(function(err, con) {
            if (err) throw err;
             
            con.query('SELECT * FROM transaksi_new_events WHERE id ="'+id+'" ORDER BY modified desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    console.log(obj);
                    res.render('transaksi_new_event_non/view', { obj: obj });
                }
            });
        });
    };


}
module.exports = new Todo();