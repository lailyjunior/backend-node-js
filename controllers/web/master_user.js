//Auth : Nina
var connection = require('../../config/db');

var request = require('request');

function Todo() {

    this.selectMasterUser = function(req, res, next) {
        var id = req.query.id;
        var toast = req.query.toast;

        if (id != null) {
            detailMasterUser(req, res, next);
            return;
        }
        connection.acquire(function(err, con) {
            con.query('SELECT u.id, u.name, u.email, u.phone_number, u.branch_name FROM users u WHERE versi = "1" order by created_at desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    if (toast != null) {
                        if (toast == 'add') {
                            res.render('master_user/index', { obj: obj, message: 'Add Data Success' });
                        } else if (toast == 'edit') {
                            res.render('master_user/index', { obj: obj, message: 'Update Data Success' });
                        } else {
                            res.render('master_user/index', { obj: obj, message: 'Delete Data Success' });
                        }
                    } else {
                        res.render('master_user/index', { obj: obj, message: null });
                    }
                }
            }); 
        });
    }

    this.insertMasterUser = function(req, res) {
        
        var id_branch = req.body.branch_id;
        console.log(id_branch);
        
        connection.acquire(function(err, con) {
            if(err) throw err;
            con.query('SELECT group_id, title FROM groups', function(err, dataGroup){
                con.release();
                if(err) {
                    console.log(err);
                } else {
                    request({
                        uri : 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_branch_all.php',
                        auth: {
                        'bearer': '71D55F9957529'
                          },
                          rejectUnauthorized: false,//add when working with https sites
                          requestCert: false,//add when working with https sites
                          agent: false,//add when working with https sites
                    
                    }, function(error, LOVBranch) {
                        if (error) {
                            console.log(error);
                        } else {
                            request({
                                uri : 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_supervisor_all.php',
                                auth: {
                                'bearer': '71D55F9957529'
                                  },
                                  rejectUnauthorized: false,//add when working with https sites
                                  requestCert: false,//add when working with https sites
                                  agent: false,//add when working with https sites
                            
                            }, function(error, LOVSpv) {
                                if (error) {
                                    console.log(error);
                                } else {
                                    request({
                                        uri : 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesmen_all.php',
                                        auth: {
                                        'bearer': '71D55F9957529'
                                          },
                                          rejectUnauthorized: false,//add when working with https sites
                                          requestCert: false,//add when working with https sites
                                          agent: false,//add when working with https sites
                                    
                                    }, function(error, LOVSlsmn) {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            request({
                                                uri : 'http://intranet.wahanaartha.com/ws_webservices/salesmens/getSalesman',
                                                json: true,
                                            
                                            }, function(error, LovCoba) {
                                                if (error) {
                                                    console.log(error);
                                                } else {
                                                    // console.log(LOVBranch);
                                                    
                                                    objBranch = JSON.parse(LOVBranch.body);
                                                    objSpv = JSON.parse(LOVSpv.body);
                                                    console.log(objSpv);
                                                    objSls = JSON.parse(LOVSlsmn.body);
                                                    objGroup = JSON.parse(JSON.stringify(dataGroup));
                                                    objCoba = JSON.parse(JSON.stringify(LovCoba.body));
                                                    objLagi = objCoba.listSalesman;

                                                    console.log(objCoba.listSalesman[0].Hondaid.honda_id);
                                                    res.render('master_user/add');
                                                }
                                            });
                                        }
                                            
                                    });
                                }
                            });
                        }
                    });
                }
            })
        });
    };

    this.submitInsertMasterUser = function(req, res) {
        // res.send(req.body);
        // return;
             var id = uuidv1();
             var name = req.body.name;
             var username = req.body.username;
             var password = req.body.password;
             var validpass = md5(password);
             var email = req.body.email;
             var phone_number = req.body.phone_number;
             var birth_date = req.body.birth_date;
             var gender = req.body.gender;
            //  var is_login = req.body.is_login;
            //  var last_login = req.body.last_login;
             var branch_id = req.body.branch_id;
             var branch_name = req.body.branch_name;
             var branch_code = req.body.branch_code;
             var supervisor_nama = req.body.supervisor_nama;
             var sls_nama = req.body.sls_nama;
             var sls_kode = req.body.sls_kode;
             var status_user = req.body.status_user;
             var group_id = req.body.group_id;
             var salesman_id = req.body.salesman_id;
             var supervisor_id = req.body.supervisor_id;
             var created_at = new Date();
             var created_by = req.session.user.id;
             var modified_at = new Date();
             var modi_by = req.session.user.id;

             var versi = "1";
         
             connection.acquire(function(err, con) {
               con.query('INSERT INTO users (id, name, username,password,email,phone_number,birth_date,gender,branch_id,branch_name,branch_code, group_id,supervisor_id,supervisor_nama, salesman_id, sls_nama,sls_kode, status_user, created_at,created_by,modified_at,modi_by, versi) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, name, username, validpass, email, phone_number, birth_date, gender,branch_id,branch_name, branch_code,group_id,supervisor_id,supervisor_nama, salesman_id,sls_nama, sls_kode,status_user,created_at,created_by,modified_at,modi_by,versi],function(err, result){
                con.release();
                if (err) {
                    console.log(err);
                    res.send({ status: 400, message: 'Master user creation failed' });
                } else {
                    res.redirect('/MasterUser/Index?toast=' + 'add');
                }
            });
        });
    }

    this.editMasterUser = function(req, res) {
        var id = req.query.id;
        const dateformat = require('dateformat');
        let now = new Date();
        dateformat(now, 'dddd, mmmm dS, yyyy, h:MM:ss TT');
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT u.id, u.name, u.username, u.email, u.phone_number, u.gender, u.birth_date, u.supervisor_nama, u.status_user, u.group_id, g.title as group_name, u.sls_nama, u.sls_kode,u.branch_code, u.branch_name FROM users u LEFT OUTER JOIN groups g ON u.group_id = g.group_id WHERE u.id ="'+id+'" ORDER BY modified_at desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    connection.acquire(function(err, con) {
                        if(err) throw err;
                        con.query('SELECT group_id, title FROM groups', function(err, dataGroup){
                            con.release();
                            if(err) {
                                console.log(err);
                            } else {
                                connection.acquire(function(err, con){
                                    if(err) throw err;
                                    con.query('SELECT sv.id, sv.supervisor_nama, bc.name as branch_name FROM supervisors sv, branches bc WHERE bc.id = sv.branch_id order by supervisor_nama desc', function(err, dataSupervisor){
                                        con.release();
                                        if(err){
                                            console.log(err);
                                        } else {
                                            connection.acquire(function(err, con){
                                                if(err) throw err;
                                                con.query('SELECT s.id, s.sls_nama, sp.supervisor_nama, b.name as branch_name FROM salesmen s LEFT OUTER JOIN supervisors sp ON s.supervisor_id = sp.id LEFT OUTER JOIN branches b ON s.branch_id = b.id order by sls_nama desc', function(err, dataLOV){
                                                    con.release();
                                                    if(err){
                                                        console.log(err);
                                                    } else {
                                                        connection.acquire(function(err, con) {
                                                            if(err) throw err;
                                                            con.query('SELECT id, name FROM branches', function(err, dataBranch){
                                                                con.release();
                                                                if(err) {
                                                                    console.log(err);
                                                                } else {
                                                                    objBranch = JSON.parse(JSON.stringify(dataBranch));
                                                                    obj = JSON.parse(JSON.stringify(rows));
                                                                    objLOV = JSON.parse(JSON.stringify(dataLOV));
                                                                    objSupervisor = JSON.parse(JSON.stringify(dataSupervisor));
                                                                    objGroup = JSON.parse(JSON.stringify(dataGroup));
                                                                    res.render('master_user/edit', {obj: obj, objLOV: objLOV, objBranch: objBranch, objSupervisor: objSupervisor, objGroup: objGroup});
                                                                }
                                                            });
                                                        });
                                                    }
                                                });
                                            });
                                        }
                                    });
                                });
                                        
                            }
                        })
                    });
                }
            });
        });
    };

    this.submitEditMasterUser = function(req, res) {
        var id = req.query.id;
        var name = req.body.name;
        var username = req.body.username;
        var password = req.body.password;
        var validpass = md5(password);
        var email = req.body.email;
        var phone_number = req.body.phone_number;
        var birth_date = req.body.birth_date;
        var gender = req.body.gender;
        var branch_id = req.body.branch_id;
        var branch_name = req.body.branch_name;
        var branch_code = req.body.branch_code;
        var supervisor_nama = req.body.supervisor_nama;
        var sls_nama = req.body.sls_nama;
        var sls_kode = req.body.sls_kode;
        var status_user = req.body.status_user;
        var group_id = req.body.group_id;
        var salesman_id = req.body.salesman_id;
        var supervisor_id = req.body.supervisor_id;
        var created_at = new Date();
        var created_by = req.session.user.id;
        var modified_at = new Date();
        var modi_by = req.session.user.id;

        connection.acquire(function(err, con) {
            con.query('UPDATE users set name = ?, username = ?, password = ?, phone_number = ?, birth_date =?, email = ?, gender = ?, branch_name = ?, supervisor_nama = ?, sls_nama = ?, sls_kode = ?, status_user = ?,  modified_at = ?, modi_by = ? WHERE id = "' + id + '"', [name, username, validpass,phone_number,birth_date,email,gender, branch_name, supervisor_nama, sls_nama, sls_kode, status_user, modified_at, modi_by], function(err, result) {
                con.release();
                if (err) {
                    console.log(err);
                    res.send({ status: 400, message: 'Master user update failed' });
                } else {
                    res.redirect('/MasterUser/Index?toast=' + 'edit');
                }
            });
        });
    };

    this.deleteMasterUser = function(req, res) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            con.query('DELETE FROM users WHERE id = ?', [id], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Failed to delete master user' });
                } else {
                    res.redirect('/MasterUser/Index?toast=' + 'delete');
                }
            });
        });
    };

    function detailMasterUser(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
             
            con.query('SELECT u.id, u.name, u.username, u.email, u.phone_number, u.gender, u.birth_date, sp.supervisor_nama, g.title as group_name, sl.sls_nama, b.name as branch_name, u.status_user FROM users u LEFT OUTER JOIN supervisors sp ON u.supervisor_id = sp.id LEFT OUTER JOIN groups g ON u.group_id = g.group_id LEFT OUTER JOIN salesmen sl ON u.salesman_id = sl.id LEFT OUTER JOIN branches b ON u.branch_id = b.id WHERE u.id ="'+id+'" ORDER BY modified_at desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    res.render('master_user/view', { obj: obj });
                }
            });
        });
    };
}
module.exports = new Todo();