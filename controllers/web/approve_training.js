//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    const fs = require('fs');
    var request = require('request');
    this.selectTrainingApprove = function(req, res, next) {
        var id = req.query.id;
        var toast = req.query.toast;

        connection.acquire(function(err, con) {
            con.query('Select t.id, training_schedule_id, training_code, training_name,training_schedule_start_date, training_schedule_end_date, sls_kode, salesman_id, sls_nama, t.created_at tgl_daftar, status_approve from training_salesman t, training_schedules ts WHERE t.training_schedule_id = ts.id', function(err, rows) {

                con.release();
                if (err) {
                    console.log(err);
                } else {
                    var datas = [];

                    rows.forEach(function(element, index) {
                        var start_date = dateFormat(element.training_schedule_start_date, "yyyy-mm-dd");
                        var end_date = dateFormat(element.training_schedule_end_date, "yyyy-mm-dd");
                        var created = dateFormat(element.training_schedule_date, "yyyy-mm-dd");
                        var date = new Date()
                        var d2 = new Date(start_date);

                        if (d2.getTime() <= date.getTime()) {
                            var compare = "1"
                        } else {
                            var compare = "0"
                        }

                        rows[index].tgl_daftar = created;
                        rows[index].training_schedule_start_date = start_date;
                        rows[index].training_schedule_end_date = end_date;
                        rows[index].compare = compare;

                        datas.push(rows[index]);
                    });

                    obj = datas;
                    if (toast != null) {
                        if (toast == 'add') {
                            res.render('approve_training/index', { obj: obj, message: 'Approval Success' });
                        } else if (toast == 'failedApprove') {
                            res.render('approve_training/index', { obj: obj, message: 'Approval Failed' });
                        } else if (toast == 'kuotaHabis') {
                            res.render('approve_training/index', { obj: obj, message: 'Empty Quota' });
                        } else if (toast == 'reject') {
                            res.render('approve_training/index', { obj: obj, message: 'Reject Success' });
                        } else if (toast == 'rejectFailed') {
                            res.render('approve_training/index', { obj: obj, message: 'Reject Failed' });
                        }
                    } else {
                        res.render('approve_training/index', { obj: obj, message: null });
                    }
                }
            });
        });
    };

    this.submitApprove = function(req, res) {

        var id = req.query.id;
        var training_schedule_id = req.query.training_schedule_id;
        var modified_at = new Date();
        var modified_by = req.session.user.id;

        connection.acquire(function(err, con) {
            con.query('SELECT remaining_quota from training_schedules where id = "' + training_schedule_id + '"', function(err, rows) {
                // con.release();
                if (err) {
                    console.log(err);
                } else {
                    var kuota = rows[0].remaining_quota;
                    if (kuota > 0) {
                        var status_approve = "1";
                        con.query('UPDATE training_salesman SET status_approve = "' + status_approve + '" WHERE id = "' + id + '" ', function(err, result) {
                            // con.release();
                            if (err) {
                                res.redirect('/ApproveTraining/Index?toast=' + 'failedApprove');
                            } else {
                                con.query('select id,event_type from master_event WHERE event_type = "training" ', function(err, resultEvent) {
                                    // con.release();
                                    var id_mast_ev = resultEvent[0]["id"];
                                    var master_ev = resultEvent[0]["event_type"];

                                    con.query('Select mt.title, mt.description, training_schedule_start_date start_date, training_schedule_end_date end_date, salesman_id, sls_kode from training_salesman t, training_schedules ts, master_training mt WHERE t.training_schedule_id = ts.id and ts.training_id = mt.id COLLATE utf8_unicode_ci and t.id = "' + id + '" ', function(err, result) {
                                        // con.release();
                                        var salesman_id = result[0]["salesman_id"];

                                        con.query('select b.versi from branches b, users u where b.id = u.branch_id and u.salesman_id = "' + salesman_id + '" ', function(err, resultVersi) {
                                            // con.release();
                                            var versi = resultVersi[0]["versi"];

                                            var id_ev = uuidv1();
                                            var title = result[0]["title"];
                                            var description = result[0]["description"];
                                            var sls_kode = result[0]["sls_kode"];
                                            var start = result[0]["start_date"];
                                            var end = result[0]["end_date"];

                                            var created_at = new Date();
                                            var modified_at = new Date();
                                            var created_by = req.session.user.id;
                                            var modi_by = req.session.user.id;

                                            if (versi == 0) {
                                                con.query('INSERT INTO transaksi_new_events (id, event_type_id, event_type,title, detail,start,end,sls_kode,versi, created, modified, create_by, modi_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)', [id_ev, id_mast_ev, master_ev, title, description, start, end, sls_kode, versi, created_at, modified_at, created_by, modi_by], function(err, result) {
                                                    con.release();
                                                    if (err) {
                                                        res.redirect('/ApproveTraining/Index?toast=' + 'failedApprove');
                                                    } else {
                                                        res.redirect('/ApproveTraining/Index?toast=' + 'add');
                                                    }
                                                });

                                            } else if (versi == 1) {
                                                con.query('INSERT INTO transaksi_new_events (id, event_type_id, event_type,title, detail,start,end,sls_kode,versi, created, modified, create_by, modi_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)', [id_ev, id_mast_ev, master_ev, title, description, start, end, sls_kode, versi, created_at, modified_at, created_by, modi_by], function(err, result) {
                                                    con.release();
                                                    if (err) {
                                                        res.redirect('/ApproveTraining/Index?toast=' + 'failedApprove');
                                                    } else {

                                                        var url = 'https://evodms-dev.clientsolve.com/evoDMSTest/api/api_insert_event.php';

                                                        var periode_start = dateFormat(start, "yyyy-mm-dd HH:MM:ss");
                                                        var periode_end = dateFormat(end, "yyyy-mm-dd HH:MM:ss");

                                                        var requestData = {
                                                            id: id_ev,
                                                            event_type: master_ev,
                                                            title: title,
                                                            details: description,
                                                            start: periode_start,
                                                            end: periode_end,
                                                            sls_kode: sls_kode,
                                                        }
                                                        var data = {
                                                            url: url,
                                                            auth: {
                                                                'bearer': '71D55F9957529'
                                                            },
                                                            json: true,
                                                            body: requestData,
                                                            rejectUnauthorized: false, //add when working with https sites
                                                            requestCert: false, //add when working with https sites
                                                            agent: false, //add when working with https sites
                                                        }


                                                        request.post(data, function(error, httpResponse, body) {
                                                            console.log(data);
                                                            // return;
                                                            var status = body.status;
                                                            if (status == "Failed") {
                                                                console.log(error);
                                                                res.redirect('/ApproveTraining/Index?toast=' + 'failedApprove');
                                                            } else {
                                                                res.redirect('/ApproveTraining/Index?toast=' + 'add');
                                                            }

                                                        });
                                                    }
                                                });
                                            }

                                        });
                                    });
                                });
                            }
                        });
                    } else {
                        res.redirect('/ApproveTraining/Index?toast=' + 'kuotaHabis');
                    }
                }
            });
        });
    }

    this.submitReject = function(req, res) {
        var id = req.query.id;
        var training_schedule_id = req.query.training_schedule_id;
        var modified_at = new Date();
        var modified_by = req.session.user.id;

        connection.acquire(function(err, con) {
            con.query('SELECT remaining_quota from training_schedules where id = "' + training_schedule_id + '"', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    var kuota = rows[0].remaining_quota;
                    if (kuota > 0) {
                        var status_approve = "2";
                        con.query('UPDATE training_salesman SET status_approve = "' + status_approve + '" WHERE id = "' + id + '" ', function(err, result) {
                            // con.release();
                            if (err) {
                                res.redirect('/ApproveTraining/Index?toast=' + 'failedReject');
                            } else {
                                res.redirect('/ApproveTraining/Index?toast=' + 'reject');
                            }
                        });
                    } else {
                        res.redirect('/ApproveTraining/Index?toast=' + 'kuotaHabis');
                    }
                }
            });
        });
    }
}
module.exports = new Todo();