//Auth : Ratri

var connection = require('../../config/db');

var request = require('request');

function Todo() {

    this.selectNewEvent = function(req, res, next) {
        var id = req.query.id;
        var toast = req.query.toast;

        if (id != null) {
            detailNewEvent(req, res, next);
                return;
        }
        request({
            uri : 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_event_all.php',
            auth: {
            'bearer': '71D55F9957529'
              },
              rejectUnauthorized: false,//add when working with https sites
              requestCert: false,//add when working with https sites
              agent: false,//add when working with https sites
        
        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                request({
                    uri : 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_event_all.php',
                    auth: {
                    'bearer': '71D55F9957529'
                      },
                      rejectUnauthorized: false,//add when working with https sites
                      requestCert: false,//add when working with https sites
                      agent: false,//add when working with https sites
                
                }, function(error, rows) {
                    if (error) {
                        console.log(error);
                    } else {
                        obj = JSON.parse(body);
                        objLOV = JSON.parse(rows.body);
                        // console.log(obj);
                        if (toast != null) {
                            if (toast == 'add') {
                                res.render('transaksi_new_event/index', { obj: obj, message: 'Add Data Success' });
                            } else if (toast == 'edit') {
                                res.render('transaksi_new_event/index', { obj: obj, message: 'Update Data Success' });
                            } else {
                                res.render('transaksi_new_event/index', { obj: obj, message: 'Delete Data Success' });
                            }
                        } else {
                            res.render('transaksi_new_event/index', { obj: obj, message: null });
                        }
                    }
                    
                });
                
            }
        } );
        
    };

    this.insertNewEvent = function(req, res) {
        connection.acquire(function(err, con) {
            con.query('SELECT id, event_type, description FROM master_event', function(err, lovEvent) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'failed' });
                } else {
                    connection.acquire(function(err, con) {
                        con.query('select event_code from transaksi_new_events order by created desc limit 1', function(err, result) {
                            con.release();
                            if (err) {
                                res.send({ status: 400, message: ' Failed' });
                            } else {
                                // for CODE EVENT
                                if (result.length != 0) {
                                    var date = new Date().toISOString().replace(/\T.+/, '').replace(/-/, '').replace(/-/, '')
                                    var test = result[0].event_code;
                                } else {
                                    var date = new Date().toISOString().replace(/\T.+/, '').replace(/-/, '').replace(/-/, '')
                                    console.log(date);
                                    var test = "EV-" + date + "00"
                                    console.log(test);
                                }

                                var pieces = test.split('-');
                                var prefix = pieces[0];
                                var lastNum = pieces[1];
                                lastNum = parseInt(lastNum, 10);
                                lastNum++;
                                code = "EV-" + date + ("00" + lastNum).substr(-2);
                                console.log(code);
                                }  

                                objCode = code;
                                // obj = JSON.parse(body);
                                objLOV = JSON.parse(JSON.stringify(lovEvent));
                                res.render('transaksi_new_event/add');
                        });
                    }); 
                    // obj = JSON.parse(body);
                    
                }
            });
        });
        // connection.acquire(function(err, con) {

        //     res.render('transaksi_new_event/add');
        // });
    };

    this.submitInsertNewEvent = function(req, res) {
        // res.send(req.body);
        // return;
        var id = uuid();
        var event_code = req.body.event_code;
        var title = req.body.title;
        var event_type_id = req.body.event_type_id;
        var detail = req.body.detail;
        var start = req.body.start;
        var end = req.body.end;
        var all_day = req.body.all_day;
        var status = req.body.status;
        var active = req.body.active;
        var sls_kode = req.body.sales_code;
        var lead_code = req.body.lead_code;
        var activity_code = req.body.activity_code;
        var location_name = req.body.location_name;
        var longitude = req.body.longitude;
        var latitude = req.body.latitude;
        var radius = req.body.radius;
        var created_at = new Date();
        var modified_at = new Date();
        // var created_by = req.body.created_by;
        // var modi_by = req.body.modi_by;
        var created_by = req.session.user.id;
        var modi_by = req.session.user.id;
        var versi = "0"

        connection.acquire(function(err, con) {
            con.query('INSERT INTO transaksi_new_events (id, event_code, title, event_type_id, detail, all_day, status, active, sls_kode, lead_code, activity_code, start, end,location_name, longitude, latitude, radius, created, modified, created_by, modi_by, versi) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [id, event_code, title, event_type_id, detail, all_day, status, active, sls_kode, lead_code, activity_code, start, end, location_name, longitude, latitude, radius, created_at, modified_at, created_by, modi_by, versi], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'New Event creation failed' });
                } else {
                    res.redirect('/TransaksiNewEvent/Index?toast=' + 'add');
                }
            });
        });
    }

    this.editNewEvent = function(req, res) {
        var id = req.query.id;
        var event_kode = null;
        request.get({
            uri : 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_event.php?id='+id,
            auth: {
            'bearer': '71D55F9957529'
              },
              rejectUnauthorized: false,//add when working with https sites
              requestCert: false,//add when working with https sites
              agent: false,//add when working with https sites
        }, function(error, response, body) {
            if (error) {
                console.log(error);
            } else {
                connection.acquire(function(err, con) {
                    con.query('SELECT id FROM transaksi_new_events WHERE id="' + id + '"', function(err, result) {
                        con.release();
                        if (err) {
                            res.send({ status: 400, message: 'failed' });
                        } else {
                            if (result == 0) {
                                //INSERT
                                connection.acquire(function(err, con) {
                                    con.query('select event_code from transaksi_new_events order by created desc limit 1', function(err, result) {
                                        con.release();
                                        if (err) {
                                            res.send({ status: 400, message: ' Failed' });
                                        } else {
                                            // for CODE EVENT
                                            if (result.length != 0) {
                                                var date = new Date().toISOString().replace(/\T.+/, '').replace(/-/, '').replace(/-/, '')
                                                var test = result[0].event_code;
                                            } else {
                                                var date = new Date().toISOString().replace(/\T.+/, '').replace(/-/, '').replace(/-/, '')
                                                console.log(date);
                                                var test = "EV-" + date + "00"
                                                console.log(test);
                                            }
        
                                            var pieces = test.split('-');
                                            var prefix = pieces[0];
                                            var lastNum = pieces[1];
                                            lastNum = parseInt(lastNum, 10);
                                            lastNum++;
                                            code = "EV-" + date + ("00" + lastNum).substr(-2);
                                            console.log(code);
                                            }  

                                            objCode = code;
                                            obj = JSON.parse(body);
                                            res.render('transaksi_new_event/edit', { obj: obj });
                                    });
                                });   
                            } else {
                                request.get({
                                    uri : 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_event.php?id='+id,
                                    auth: {
                                    'bearer': '71D55F9957529'
                                      },
                                      rejectUnauthorized: false,//add when working with https sites
                                      requestCert: false,//add when working with https sites
                                      agent: false,//add when working with https sites
                                }, function(error, response, body) {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        obj = JSON.parse(body);
                                        objCode = obj.event_code;
                                        res.render('transaksi_new_event/edit', { obj: obj });
                                    }
                                } );
                            }
                        }
                    });
                });
            }
        } );
    };

    
    this.submitEditNewEvent = function(req, res) {
        var id = req.query.id;
        // var event_kode = req.body.event_code;
        var event_kode = null;
        var event_code = req.body.event_code;
        var title = req.body.title;
        var event_type_id = req.body.event_type_id;
        var sls_kode = req.body.sls_kode;
        var lead_code = req.body.lead_code;
        var activity_code = req.body.activity_code;
        var details = req.body.details;
        var start = req.body.start;
        var end = req.body.end;
        var all_day = req.body.all_day;
        var active = req.body.active;
        var status = req.body.status;
        var location_name = req.body.location_name;
        var longitude = req.body.longitude;
        var latitude = req.body.latitude;
        var radius = req.body.radius;
        var modified_at = new Date();
        var modi_by = req.session.user.id;
        var create_by = req.session.user.id;
        var created = new Date();
        var versi = "1";

        connection.acquire(function(err, con) {
            con.query('SELECT id FROM transaksi_new_events WHERE id="' + id + '"', function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'failed' });
                } else {
                    if (result == 0) {
                        //INSERT
                        //FOR DMS
                        var url = 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_edit_event_by_id.php?id='+id;

                        var requestData = {
                            event_code : req.body.event_code,
                            location_name : req.body.location_name,
                            longitude : req.body.longitude,
                            latitude : req.body.latitude,
                            radius : req.body.radius
                        }
                        
                        var data = {
                            url: url,
                            auth: {
                                'bearer' : '71D55F9957529'
                            },
                            json: true,
                            form : requestData,
                            rejectUnauthorized: false,//add when working with https sites
                            requestCert: false,//add when working with https sites
                            agent: false,//add when working with https sites
                        }

                        console.log(data);
                        request.post(data, function(error, httpResponse, body){
                            console.log(body);
                        });

                        //FOR 38
                        connection.acquire(function(err, con) {
                            con.query('INSERT INTO transaksi_new_events (id, event_code, title, detail, event_type_id, sls_kode, lead_code, activity_code, start, end, all_day, active, status, location_name, longitude, latitude, radius, created, modified, create_by, modi_by, versi) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [id, event_code, title, details, event_type_id, sls_kode, lead_code, activity_code, start, end, all_day, active, status, location_name, longitude, latitude, radius, created, modified_at, create_by, modi_by, versi], function(err, result) {
                                con.release();
                                if (err) {
                                    res.send({ status: 400, message: 'Transaksi New Event insert failed' });
                                } else {
                                    res.redirect('/TransaksiNewEvent/Index?toast=' + 'insert');
                                }
                            })
                        });


                    } else {
                        //EDIT
                        //FOR 38
                        connection.acquire(function(err, con) {
                            con.query('UPDATE transaksi_new_events SET event_code = ?, title = ?, detail = ?, event_type_id = ?, sls_kode = ?, lead_code = ?, activity_code = ?, start = ?, end = ?, all_day = ?, active = ?, status = ?,location_name = ?, longitude = ?, latitude = ?, radius = ?, modified = ?, modi_by = ? WHERE id = "' + id + '"', [event_code, title, details, event_type_id, sls_kode, lead_code, activity_code, start, end, all_day, active, status, location_name, longitude, latitude, radius, modified_at, modi_by], function(err, result) {
                                con.release();
                                if (err) {
                                    res.send({ status: 400, message: 'Transaksi New Event update failed' });
                                } else {
                                    res.redirect('/TransaksiNewEvent/Index?toast=' + 'edit');
                                }
                            })
                        })

                        //FOR DMS

                        var url = 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_edit_event_by_id.php?id='+id;

                        var requestData = {
                            event_code : req.body.event_code,
                            location_name : req.body.location_name,
                            longitude : req.body.longitude,
                            latitude : req.body.latitude,
                            radius : req.body.radius
                        }
                        
                        var data = {
                            url: url,
                            auth: {
                                'bearer' : '71D55F9957529'
                            },
                            json: true,
                            form : requestData,
                            rejectUnauthorized: false,//add when working with https sites
                            requestCert: false,//add when working with https sites
                            agent: false,//add when working with https sites
                        }

                        console.log(data);
                        request.post(data, function(error, httpResponse, body){
                            console.log(body);
                        });
                    }
                }
            });
        });
    };

    this.deleteNewEvent = function(req, res) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            con.query('DELETE FROM transaksi_new_events WHERE id = ?', [id], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Failed to delete transaksi new event' });
                } else {
                    res.redirect('/TransaksiNewEvent/Index?toast=' + 'delete');
                }
            });
        });
    }; 

    function detailNewEvent(req, res, next) {
        var id = req.query.id;
        // var start_date = new Date(dd-MM-YYYY);
        request.get({
            uri : 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_event.php?id='+id,
            auth: {
            'bearer': '71D55F9957529'
              },
              rejectUnauthorized: false,//add when working with https sites
              requestCert: false,//add when working with https sites
              agent: false,//add when working with https sites
        }, function(err, response, body) {
            if (err) {
                console.log(err);
            } else {
                obj = JSON.parse(body);
                console.log(obj);
                res.render('transaksi_new_event/view', { obj: obj });
            }
        });
    };

}
module.exports = new Todo();