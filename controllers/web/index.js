//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    var request = require('request');
    this.home = function(req, res) {
        connection.acquire(function(err, con) {
            res.render('index');
        });
    };

    this.login = function(req, res) {
        if (req.session.user) {
            res.redirect("/");
            return;
        }
        var toast = req.query.toast;
        connection.acquire(function(err, con) {
            if (toast != null) {
                if (toast == 'success') {
                    res.render('./index', { message: 'Login Success' });
                } else if (toast == 'password') {
                    res.render('./login', { message: 'Wrong Password' });
                } else if (toast == 'passuser') {
                    res.render('./login', { message: 'Wrong Username Or Password' });
                } else if (toast == 'logout') {
                    res.render('./login', { message: 'Logout Success' });
                }
            } else {
                res.render('./login', { message: null });
            }
        });
    };

    this.submitLogin = function(req, res) {
        var username = req.body.username;
        var password = req.body.password;
        var validpass = md5(password);
        var last_login = new Date();

        connection.acquire(function(err, con) {
            con.query('SELECT versi, branch_id from users where username = ?  ', [username], function(error, results, fields) {
                if (results[0].versi == 0) {
                    con.query('SELECT u.id, u.username, u.name, u.password, u.email,u.phone_number, u.birth_date, u.gender, u.group_id, u.salesman_id, g.title, u.is_login, u.branch_id, u.branch_code, u.supervisor_id, s.company_id, s.company_code, u.versi FROM users u JOIN groups g ON g.group_id = u.group_id LEFT JOIN supervisors s ON u.supervisor_id = s.id WHERE u.username = ? ', [username], function(error, results, fields) {
                        con.release();
                        if (error) throw error;
                        if (results.length > 0) {
                            if (validpass == results[0].password) {
                                req.session.user = results[0];
                                req.session.isLogin = true;

                                var now = new Date().getTime();
                                store.db.all('SELECT * FROM ' + store.table + ' WHERE ? <= expired', [now],
                                    function(err, rows) {
                                        if (err) return;
                                        if (!rows) return;
                                        for (var i = 0; i < rows.length; i++) {
                                            var element = rows[i];
                                            var sess = JSON.parse(element.sess);

                                            if (sess.user != null) {
                                                var user = sess.user;
                                            }
                                        }
                                        res.redirect('/login?toast=' + 'success');
                                    }
                                );
                            } else {
                                res.redirect('/login?toast=' + 'password');
                            }
                        } else {
                            res.redirect('/login?toast=' + 'passuser');
                        }
                    });
                } else if ((results[0].versi == 1)) {
                    var branch_id = results[0].branch_id;
                    request.get({
                        uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_branch.php?id=' + branch_id,
                        auth: {
                            'bearer': '71D55F9957529'
                        },
                        rejectUnauthorized: false, //add when working with https sites
                        requestCert: false, //add when working with https sites
                        agent: false, //add when working with https sites
                    }, function(error, response, body) {
                        if (error) {
                            console.log(error);
                        } else {
                            obj = JSON.parse(body);
                            console.log(obj);

                            var company_id = obj.company_id;
                            var company_code = obj.company_code;

                            con.query('SELECT u.id, u.username, u.name, u.password, u.email,u.phone_number, u.birth_date, u.gender, u.group_id, u.salesman_id, g.title, u.is_login, u.branch_id, u.branch_code, u.supervisor_id, versi FROM users u, groups g WHERE g.group_id = u.group_id AND u.username = ? ', [username], function(error, results, fields) {
                                con.release();
                                if (error) throw error;
                                if (results.length > 0) {
                                    if (validpass == results[0].password) {
                                        results[0].company_id = company_id
                                        results[0].company_code = company_code

                                        req.session.user = results[0];
                                        console.log(req.session.user);
                                        req.session.isLogin = true;

                                        var now = new Date().getTime();
                                        store.db.all('SELECT * FROM ' + store.table + ' WHERE ? <= expired', [now],
                                            function(err, rows) {
                                                if (err) return;
                                                if (!rows) return;
                                                for (var i = 0; i < rows.length; i++) {
                                                    var element = rows[i];
                                                    var sess = JSON.parse(element.sess);

                                                    if (sess.user != null) {
                                                        var user = sess.user;
                                                    }
                                                }
                                                res.redirect('/login?toast=' + 'success');
                                            }
                                        );
                                    } else {
                                        res.redirect('/login?toast=' + 'password');
                                    }
                                } else {
                                    res.redirect('/login?toast=' + 'passuser');
                                }
                            });
                        }
                    });
                } else if ((results[0].versi == null)) {
                    con.query('SELECT u.id, u.username, u.name, u.password, u.email,u.phone_number, u.birth_date, u.gender, u.group_id, u.salesman_id, g.title, u.is_login, u.branch_id, u.branch_code, u.supervisor_id, u.versi FROM users u, groups g, supervisors s WHERE g.group_id = u.group_id AND u.username = ? ', [username], function(error, results, fields) {
                        con.release();
                        if (error) throw error;
                        if (results.length > 0) {
                            if (validpass == results[0].password) {
                                req.session.user = results[0];
                                req.session.isLogin = true;

                                var now = new Date().getTime();
                                store.db.all('SELECT * FROM ' + store.table + ' WHERE ? <= expired', [now],
                                    function(err, rows) {
                                        if (err) return;
                                        if (!rows) return;
                                        for (var i = 0; i < rows.length; i++) {
                                            var element = rows[i];
                                            var sess = JSON.parse(element.sess);

                                            if (sess.user != null) {
                                                var user = sess.user;
                                            }
                                        }
                                        res.redirect('/login?toast=' + 'success');
                                    }
                                );
                            } else {
                                res.redirect('/login?toast=' + 'password');
                            }
                        } else {
                            res.redirect('/login?toast=' + 'passuser');
                        }
                    });
                }
            });
        });
    };

    this.logout = function(req, res) {
        if (req.session == null && req.session.user == null) {
            res.redirect('/login');
            return;
        }
        var id = req.session.user.id;
        var modified_at = new Date();
        var modi_by = 'lely';
        store.destroy(req.sessionID, function(err) {
            if (err) console.log(err);
            req.session.destroy(function(err) {
                console.log(err);
                res.redirect('/login?toast=' + 'logout');
            })
        });
    };

    this.error404 = function(req, res) {
        connection.acquire(function(err, con) {
            res.render('error404');
        });
    };
}
module.exports = new Todo();