//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    const fs = require('fs');
    this.selectTrainingSchedule = function(req, res, next) {
        var id = req.query.id;
        var toast = req.query.toast;
        if (id != null) {
            detailTrainingSchedule(req, res, next);
            return;
        }
        connection.acquire(function(err, con) {
            con.query('SELECT * FROM training_schedules order by modified_at desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    var datas = [];

                    rows.forEach(function(element, index) {
                        var start_date = dateFormat(element.training_schedule_start_date, "yyyy-mm-dd");
                        var end_date = dateFormat(element.training_schedule_end_date, "yyyy-mm-dd");
                        var created = dateFormat(element.training_schedule_date, "yyyy-mm-dd");

                        rows[index].training_schedule_date = created;
                        rows[index].training_schedule_start_date = start_date;
                        rows[index].training_schedule_end_date = end_date;
                        datas.push(rows[index]);
                    });

                    obj = datas;
                    if (toast != null) {
                        if (toast == 'add') {
                            res.render('training_schedule/index', { obj: obj, message: 'Add Data Success' });
                        } else if (toast == 'edit') {
                            res.render('training_schedule/index', { obj: obj, message: 'Update Data Success' });
                        } else if (toast == 'failedDelete') {
                            res.render('training_schedule/index', { obj: obj, message: 'Delete Data Failed' });
                        } else {
                            res.render('training_schedule/index', { obj: obj, message: 'Delete Data Success' });
                        }
                    } else {
                        res.render('training_schedule/index', { obj: obj, message: null });
                    }
                }
            });
        });
    };

    this.insertTrainingSchedule = function(req, res) {
        var toast = req.query.toast;
        connection.acquire(function(err, con) {
            con.query('SELECT * FROM master_training order by title asc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    // res.render('training_schedule/add', { obj: obj });

                    if (toast != null) {
                        if (toast == 'failed') {
                            res.render('training_schedule/add', { obj: obj, message: 'Add Data Failed' });
                        }
                    } else {
                        res.render('training_schedule/add', { obj: obj, message: null });
                    }
                }
            });
        });
    };

    this.submitInsertTrainingSchedule = function(req, res) {

        // res.send(req.body);
        // return;

        //data header
        var id = uuidv1();
        var code = req.body.code;
        var training_id = req.body.training_id;
        var start_date = req.body.start_date;
        var end_date = req.body.end_date;
        var kuota = req.body.quota;
        var remaining_quota = req.body.quota;
        var training_schedule_date = new Date();
        var created_at = new Date();
        var modified_at = new Date();
        var created_by = req.session.user.id;
        var modi_by = req.session.user.id;

        connection.acquire(function(err, con) {
            con.query('SELECT code, title FROM master_training where id = "' + training_id + '"', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    var training_code = rows[0].code;
                    var training_name = rows[0].title;

                    con.query('INSERT INTO training_schedules (id, training_schedule_code, training_schedule_date, training_schedule_start_date, training_schedule_end_date, training_id, training_code, training_name,kuota,remaining_quota, created_at, modified_at, created_by, modified_by) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [id, code, training_schedule_date, start_date, end_date, training_id, training_code, training_name, kuota, remaining_quota, created_at, modified_at, created_by, modi_by], function(err, result) {
                        // con.release();
                        if (err) {
                            res.redirect('/TrainingSchedule/Add?toast=' + 'failed');
                        } else {
                            res.redirect('/TrainingSchedule/Index?toast=' + 'add');
                        }
                    });
                }
            });
        });
    }

    function detailTrainingSchedule(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM training_schedules WHERE id = "' + id + '"', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    var datas = [];

                    rows.forEach(function(element, index) {
                        var start_date = dateFormat(element.training_schedule_start_date, "yyyy-mm-dd");
                        var end_date = dateFormat(element.training_schedule_end_date, "yyyy-mm-dd");
                        var created = dateFormat(element.training_schedule_date, "yyyy-mm-dd");

                        rows[index].training_schedule_date = created;
                        rows[index].training_schedule_start_date = start_date;
                        rows[index].training_schedule_end_date = end_date;
                        datas.push(rows[index]);
                    });

                    obj = datas;
                    res.render('training_schedule/view', { obj: obj });
                }
            });
        });
    };

    this.editTrainingSchedule = function(req, res) {
        var id = req.query.id;
        var toast = req.query.toast;
        var versi = req.session.user.versi;
        var supervisor_id = req.session.user.supervisor_id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM training_schedules WHERE id = "' + id + '"', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    con.query('SELECT * FROM master_training order by title asc', function(err, result) {
                        if (err) {
                            console.log(err);
                        } else {
                            var datas = [];
                            rows.forEach(function(element, index) {
                                var start_date = dateFormat(element.training_schedule_start_date, "yyyy-mm-dd");
                                var end_date = dateFormat(element.training_schedule_end_date, "yyyy-mm-dd");
                                var created = dateFormat(element.training_schedule_date, "yyyy-mm-dd");

                                rows[index].training_schedule_date = created;
                                rows[index].training_schedule_start_date = start_date;
                                rows[index].training_schedule_end_date = end_date;
                                datas.push(rows[index]);
                            });

                            obj = datas;
                            master = JSON.parse(JSON.stringify(result));
                            if (toast != null) {
                                if (toast == 'failedEdit') {
                                    res.render('training_schedule/edit', { obj: obj, master: master, message: 'Edit Data Failed' });
                                }
                            } else {
                                res.render('training_schedule/edit', { obj: obj, master: master, message: null });
                            }
                        }
                    });
                }
            });
        });
    };

    this.submitEditTrainingSchedule = function(req, res) {

        var id = req.query.id;
        var code = req.body.code;
        var training_id = req.body.training_id;
        var start_date = req.body.start_date;
        var end_date = req.body.end_date;
        var kuota = req.body.quota;

        var modified_at = new Date();
        var modi_by = req.session.user.id;

        connection.acquire(function(err, con) {
            con.query('SELECT code, title FROM master_training where id = "' + training_id + '"', function(err, rows) {
                if (err) {
                    console.log(err);
                } else {
                    if (rows.length != 0) {
                        var training_code = rows[0].code;
                        var training_name = rows[0].title;
                        if (err) {
                            console.log(err);
                        } else {
                            con.query('UPDATE training_schedules SET training_schedule_code = ?, training_id = ?, training_name = ?, training_code = ?, training_schedule_start_date = ?, training_schedule_end_date = ?,kuota = ?,modified_at = ?, modified_by = ? WHERE id = "' + id + '"', [code, training_id, training_name, training_code, start_date, end_date, kuota, modified_at, modi_by], function(err, result) {
                                con.release();
                                if (err) {
                                    res.redirect('/TrainingSchedule/Edit?id=' + id + '&toast=' + 'failedEdit');
                                } else {
                                    res.redirect('/TrainingSchedule/Index?toast=' + 'edit');
                                }
                            });
                        }
                    }
                }
            });
        });
    };

    this.deleteTrainingSchedule = function(req, res) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            con.query('DELETE FROM training_schedules WHERE id = ?', [id], function(err, result) {
                con.release();
                if (err) {
                    res.redirect('/TrainingSchedule/Index?toast=' + 'failedDelete');
                } else {
                    res.redirect('/TrainingSchedule/Index?toast=' + 'delete');
                }
            });
        });
    };
}
module.exports = new Todo();