//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    const fs = require('fs');
    this.selectMasterCicilanKredit = function(req, res, next) {
        var id = req.query.id;
        var toast = req.query.toast;
        if (id != null) {
            detailMasterTraining(req, res, next);
            return;
        }
        connection.acquire(function(err, con) {
            con.query('SELECT * FROM motors order by deskripsi', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    if (toast != null) {
                        if (toast == 'add') {
                            res.render('master_cicilan_kredit/index', { obj: obj, message: 'Add Data Success' });
                        } else if (toast == 'edit') {
                            res.render('master_cicilan_kredit/index', { obj: obj, message: 'Update Data Success' });
                        } else {
                            res.render('master_cicilan_kredit/index', { obj: obj, message: 'Delete Data Success' });
                        }
                    } else {
                        res.render('master_cicilan_kredit/index', { obj: obj, message: null });
                    }
                }
            });
        });
    };
}
module.exports = new Todo()