//Auth : Lely
var connection = require('../../config/db');

function Todo() {
    const fs = require('fs');
    var request = require('request');

    this.selectMasterTarget = function(req, res, next) {
        var id = req.query.id;
        var toast = req.query.toast;
        var supervisor_id = req.session.user.supervisor_id;

        if (id != null) {
            detailMasterTarget(req, res, next);
            return;
        }
        connection.acquire(function(err, con) {
            con.query('SELECT * FROM master_targets where supervisor_id = "' + supervisor_id + '" order by modified_at desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    if (toast != null) {
                        if (toast == 'add') {
                            res.render('master_target/index', { obj: obj, message: 'Add Data Success' });
                        } else if (toast == 'edit') {
                            res.render('master_target/index', { obj: obj, message: 'Update Data Success' });
                        } else if (toast == 'failedDelete') {
                            res.render('master_target/index', { obj: obj, message: 'Delete Data Failed' });
                        } else {
                            res.render('master_target/index', { obj: obj, message: 'Delete Data Success' });
                        }
                    } else {
                        res.render('master_target/index', { obj: obj, message: null });
                    }
                }
            });
        });
    };

    this.insertMasterTarget = function(req, res) {
        var toast = req.query.toast;
        var versi = req.session.user.versi;
        var supervisor_id = req.session.user.supervisor_id;
        if (versi == '0') {
            connection.acquire(function(err, con) {
                con.query('SELECT * FROM salesmen where supervisor_id = "' + supervisor_id + '" order by sls_nama', function(err, rows) {
                    con.release();
                    if (err) {
                        console.log(err);
                    } else {
                        obj = JSON.parse(JSON.stringify(rows));
                        if (toast != null) {
                            if (toast == 'double') {
                                res.render('master_target/add', { obj: obj, message: 'Target Pada Periode Tersebut Sudah Ada' });
                            } else if (toast == 'addFailed') {
                                res.render('master_target/add', { obj: obj, message: 'Add data Failed' });
                            }
                        } else {
                            res.render('master_target/add', { obj: obj, message: null });
                        }
                    }
                });
            });
        } else if (versi == '1') {
            var dataSales = [];
            request.get({
                uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
                auth: {
                    'bearer': '71D55F9957529'
                },
                rejectUnauthorized: false, //add when working with https sites
                requestCert: false, //add when working with https sites
                agent: false, //add when working with https sites
            }, function(error, response, body) {
                if (error) {
                    console.log(error);
                } else {
                    obj = JSON.parse(body);

                    if (toast != null) {
                        if (toast == 'double') {
                            res.render('master_target/add', { obj: obj, message: 'Target Pada Periode Tersebut Sudah Ada' });
                        } else if (toast == 'addFailed') {
                            res.render('master_target/add', { obj: obj, message: 'Add data Failed' });
                        }
                    } else {
                        res.render('master_target/add', { obj: obj, message: null });
                    }
                }
            });
        }
    };

    this.submitInsertMasterTarget = function(req, res) {

        //data header
        var id = uuidv1();
        var salesman_id = req.body.salesman;
        var prospect = req.body.prospect;
        var deal = req.body.deal;
        var sales = req.body.sales;
        var rasio = req.body.rasio;
        var periode = req.body.periode;
        var created_at = new Date();
        var modified_at = new Date();
        var created_by = req.session.user.id;
        var modi_by = req.session.user.id;

        var date = new Date();
        var pers = dateFormat(date, "yyyymm");

        var target_code = null;

        var versi = req.session.user.versi;
        var supervisor_id = req.session.user.supervisor_id;

        if (versi == '1') {
            connection.acquire(function(err, con) {
                con.query('SELECT id, periode FROM master_targets where salesman_id = "' + salesman_id + '" and periode = "' + periode + '"', function(err, rows) {
                    con.release();
                    if (err) {
                        console.log(err);
                    } else {
                        if (rows.length != 0) {
                            res.redirect('/MasterTarget/Add?toast=' + 'double');
                        } else {
                            request.get({
                                uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
                                auth: {
                                    'bearer': '71D55F9957529'
                                },
                                rejectUnauthorized: false, //add when working with https sites
                                requestCert: false, //add when working with https sites
                                agent: false, //add when working with https sites
                            }, function(error, response, body) {
                                if (error) {
                                    console.log(error);
                                } else {
                                    obj = JSON.parse(body);

                                    const results = obj.filter(function(value) {
                                        return value.id == salesman_id
                                    });

                                    if (results.length > 0) {
                                        const sls_nama = results[0].sls_nama;
                                        const sup_id = results[0].supervisor_id;

                                        connection.acquire(function(err, con) {
                                            con.query('select target_code from master_targets order by created_at desc limit 1', function(err, result) {
                                                con.release();
                                                if (err) {
                                                    res.send({ status: 400, message: 'Failed' });
                                                } else {

                                                    if (result.length != 0) {
                                                        var test = result[0].target_code;
                                                    } else {
                                                        var test = "TP-" + pers + "000"
                                                    }
                                                    var pieces = test.split('-');
                                                    var prefix = pieces[0];
                                                    var lastNum = pieces[1];
                                                    lastNum = parseInt(lastNum, 0);
                                                    lastNum++;
                                                    target_code = "TP-" + pers + ("000" + lastNum).substr(-3);

                                                    // return;
                                                    connection.acquire(function(err, con) {
                                                        con.query('INSERT INTO master_targets (id, target_code, salesman_id, salesman_nama, supervisor_id, target_rasio,target_sales, target_prospect, target_deal, periode, created_at, modified_at, created_by, modified_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?)', [id, target_code, salesman_id, sls_nama, sup_id, rasio, sales, prospect, deal, periode, created_at, modified_at, created_by, modi_by], function(err, result) {
                                                            con.release();
                                                            if (err) {
                                                                res.redirect('/MasterTarget/Add?toast=' + 'addFailed');
                                                            } else {
                                                                res.redirect('/MasterTarget/Index?toast=' + 'add');
                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                        });
                                    }
                                }
                            });

                        }
                    }
                });
            })
        } else if (versi == '0') {
            connection.acquire(function(err, con) {
                con.query('SELECT id FROM master_targets where salesman_id = "' + salesman_id + '" and periode = "' + periode + '"', function(err, rows) {
                    con.release();
                    if (err) {
                        console.log(err);
                    } else {
                        if (rows.length != 0) {
                            res.redirect('/MasterTarget/Add?toast=' + 'double');
                        } else {
                            con.query('select sls_nama, supervisor_id from salesmen where id = "' + salesman_id + '" ', function(err, rows) {
                                connection.acquire(function(err, con) {
                                    con.query('select target_code from master_targets order by created_at desc limit 1', function(err, result) {
                                        con.release();
                                        if (err) {
                                            res.send({ status: 400, message: 'Failed' });
                                        } else {
                                            var sls_nama = rows[0].sls_nama;
                                            var sup_id = rows[0].supervisor_id;
                                            // for CODE CP
                                            if (result.length != 0) {
                                                var test = result[0].target_code;
                                            } else {
                                                var test = "TP-" + pers + "000"
                                            }
                                            var pieces = test.split('-');
                                            var prefix = pieces[0];
                                            var lastNum = pieces[1];
                                            lastNum = parseInt(lastNum, 0);
                                            lastNum++;
                                            target_code = "TP-" + pers + ("000" + lastNum).substr(-3);

                                            // return;
                                            connection.acquire(function(err, con) {
                                                con.query('INSERT INTO master_targets (id, target_code, salesman_id, salesman_nama, supervisor_id, target_rasio,target_sales, target_prospect, target_deal, periode, created_at, modified_at, created_by, modified_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?)', [id, target_code, salesman_id, sls_nama, sup_id, rasio, sales, prospect, deal, periode, created_at, modified_at, created_by, modi_by], function(err, result) {
                                                    con.release();
                                                    if (err) {
                                                        res.redirect('/MasterTarget/Add?toast=' + 'addFailed');
                                                    } else {
                                                        res.redirect('/MasterTarget/Index?toast=' + 'add');
                                                    }
                                                });
                                            });
                                        }
                                    });
                                });
                            });
                        }
                    }
                });
            })
        }
    }

    this.editMasterTarget = function(req, res) {
        var id = req.query.id;
        var toast = req.query.toast;
        var versi = req.session.user.versi;
        var supervisor_id = req.session.user.supervisor_id;
        if (versi == '0') {
            connection.acquire(function(err, con) {
                if (err) throw err;
                con.query('SELECT * FROM master_targets WHERE id = "' + id + '"', function(err, result) {
                    con.release();
                    if (err) {
                        console.log(err);
                    } else {
                        con.query('SELECT * FROM salesmen where supervisor_id = "' + supervisor_id + '" order by sls_nama', function(err, rows) {
                            if (err) {
                                console.log(err);
                            } else {
                                obj = JSON.parse(JSON.stringify(result));
                                sales = JSON.parse(JSON.stringify(rows));
                                // res.render('master_target/edit', { obj: obj, sales: sales });

                                if (toast != null) {
                                    if (toast == 'double') {
                                        res.render('master_target/edit', { obj: obj, sales: sales, message: 'Target Pada Periode Tersebut Sudah Ada' });
                                    } else if (toast == 'editFailed') {
                                        res.render('master_target/edit', { obj: obj, sales: sales, message: 'Edit Data failed' });
                                    }
                                } else {
                                    res.render('master_target/edit', { obj: obj, sales: sales, message: null });
                                }
                            }
                        });

                    }
                });
            });
        } else if (versi == '1') {
            connection.acquire(function(err, con) {
                if (err) throw err;
                con.query('SELECT * FROM master_targets WHERE id = "' + id + '"', function(err, result) {
                    con.release();
                    if (err) {
                        console.log(err);
                    } else {
                        request.get({
                            uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
                            auth: {
                                'bearer': '71D55F9957529'
                            },
                            rejectUnauthorized: false, //add when working with https sites
                            requestCert: false, //add when working with https sites
                            agent: false, //add when working with https sites
                        }, function(error, response, body) {
                            if (error) {
                                console.log(error);
                            } else {
                                sales = JSON.parse(body);
                                obj = JSON.parse(JSON.stringify(result));

                                if (toast != null) {
                                    if (toast == 'double') {
                                        res.render('master_target/edit', { obj: obj, sales: sales, message: 'Target Pada Periode Tersebut Sudah Ada' });
                                    } else if (toast == 'editFailed') {
                                        res.render('master_target/edit', { obj: obj, sales: sales, message: 'Edit Data failed' });
                                    }
                                } else {
                                    res.render('master_target/edit', { obj: obj, sales: sales, message: null });
                                }
                            }
                        });
                    }
                });
            });
        }
    };

    this.submitEditMasterTarget = function(req, res) {

        // console.log(req.body);
        // return;
        var id = req.query.id;
        var salesman_id = req.body.salesman;
        var prospect = req.body.prospect;
        var rasio = req.body.rasio;
        var sales = req.body.sales;
        var deal = req.body.deal;
        var periode = req.body.periode;
        var per_hide = req.body.per_hide;
        var modified_at = new Date();
        var modi_by = req.session.user.id;

        var versi = req.session.user.versi;
        var supervisor_id = req.session.user.supervisor_id;

        if (versi == '0') {
            connection.acquire(function(err, con) {
                con.query('SELECT id, periode FROM master_targets where salesman_id = "' + salesman_id + '" and periode = "' + periode + '"', function(err, rows) {
                    con.release();
                    if (err) {
                        console.log(err);
                    } else {
                        if (rows.length != 0) {
                            if (per_hide == rows[0].periode) {
                                connection.acquire(function(err, con) {
                                    con.query('select sls_nama from salesmen where id = "' + salesman_id + '"', function(err, rows) {
                                        var salesman_nama = rows[0].sls_nama;
                                        if (err) {
                                            console.log(err);
                                        } else {
                                            con.query('UPDATE master_targets SET salesman_id = ?, salesman_nama = ?, target_prospect = ?, target_deal = ?, target_sales=?, target_rasio =?, periode = ?, modified_at = ?, modified_by = ? WHERE id = "' + id + '"', [salesman_id, salesman_nama, prospect, deal, sales, rasio, periode, modified_at, modi_by], function(err, result) {
                                                con.release();
                                                if (err) {
                                                    res.redirect('/MasterTarget/Edit?id=' + id + '&toast=' + 'editFailed');
                                                } else {
                                                    res.redirect('/MasterTarget/Index?toast=' + 'edit');
                                                }
                                            });
                                        }
                                    });
                                });
                            } else {
                                res.redirect('/MasterTarget/Edit?id=' + id + '&toast=' + 'double');
                            }

                        } else {
                            connection.acquire(function(err, con) {
                                con.query('select sls_nama from salesmen where id = "' + salesman_id + '"', function(err, rows) {
                                    var salesman_nama = rows[0].sls_nama;
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        con.query('UPDATE master_targets SET salesman_id = ?, salesman_nama = ?, target_prospect = ?, target_deal = ?, target_sales=?, target_rasio =?, periode = ?, modified_at = ?, modified_by = ? WHERE id = "' + id + '"', [salesman_id, salesman_nama, prospect, deal, sales, rasio, periode, modified_at, modi_by], function(err, result) {
                                            con.release();
                                            if (err) {
                                                res.redirect('/MasterTarget/Edit?id=' + id + '&toast=' + 'editFailed');
                                            } else {
                                                res.redirect('/MasterTarget/Index?toast=' + 'edit');
                                            }
                                        });
                                    }
                                });
                            });
                        }
                    }
                });
            });
        } else if (versi == '1') {
            connection.acquire(function(err, con) {
                con.query('SELECT id,periode FROM master_targets where salesman_id = "' + salesman_id + '" and periode = "' + periode + '"', function(err, rows) {
                    con.release();
                    if (err) {
                        console.log(err);
                    } else {
                        if (rows.length != 0) {
                            if (per_hide == rows[0].periode) {
                                request.get({
                                    uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
                                    auth: {
                                        'bearer': '71D55F9957529'
                                    },
                                    rejectUnauthorized: false, //add when working with https sites
                                    requestCert: false, //add when working with https sites
                                    agent: false, //add when working with https sites
                                }, function(error, response, body) {
                                    if (error) {
                                        console.log(error);
                                    } else {
                                        obj = JSON.parse(body);

                                        const results = obj.filter(function(value) {
                                            return value.id == salesman_id
                                        });

                                        if (results.length > 0) {
                                            const sls_nama = results[0].sls_nama;
                                            const sup_id = results[0].supervisor_id;

                                            con.query('UPDATE master_targets SET salesman_id = ?, salesman_nama = ?, target_prospect = ?, target_deal = ?, target_sales=?, target_rasio =?, periode = ?, modified_at = ?, modified_by = ? WHERE id = "' + id + '"', [salesman_id, sls_nama, prospect, deal, sales, rasio, periode, modified_at, modi_by], function(err, result) {
                                                // con.release();
                                                if (err) {
                                                    res.redirect('/MasterTarget/Edit?id=' + id + '&toast=' + 'editFailed');
                                                } else {
                                                    res.redirect('/MasterTarget/Index?toast=' + 'edit');
                                                }
                                            });
                                        }
                                    }
                                });
                            } else {
                                res.redirect('/MasterTarget/Edit?id=' + id + '&toast=' + 'double');
                            }
                        } else {
                            request.get({
                                uri: 'https://evodms-dev.clientsolve.com/evoDMSDev/api/api_salesman.php?supervisor_id=' + supervisor_id,
                                auth: {
                                    'bearer': '71D55F9957529'
                                },
                                rejectUnauthorized: false, //add when working with https sites
                                requestCert: false, //add when working with https sites
                                agent: false, //add when working with https sites
                            }, function(error, response, body) {
                                if (error) {
                                    console.log(error);
                                } else {
                                    obj = JSON.parse(body);

                                    const results = obj.filter(function(value) {
                                        return value.id == salesman_id
                                    });

                                    if (results.length > 0) {
                                        const sls_nama = results[0].sls_nama;
                                        const sup_id = results[0].supervisor_id;

                                        con.query('UPDATE master_targets SET salesman_id = ?, salesman_nama = ?, target_prospect = ?, target_deal = ?, target_sales=?, target_rasio =?, periode = ?, modified_at = ?, modified_by = ? WHERE id = "' + id + '"', [salesman_id, sls_nama, prospect, deal, sales, rasio, periode, modified_at, modi_by], function(err, result) {
                                            // con.release();
                                            if (err) {
                                                res.redirect('/MasterTarget/Edit?id=' + id + '&toast=' + 'editFailed');
                                            } else {
                                                res.redirect('/MasterTarget/Index?toast=' + 'edit');
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            });
        }
    };

    this.deleteTarget = function(req, res) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            con.query('DELETE FROM master_targets WHERE id = ?', [id], function(err, result) {
                con.release();
                if (err) {
                    res.redirect('/MasterTarget/Index?toast=' + 'failedDelete');
                } else {
                    res.redirect('/MasterTarget/Index?toast=' + 'delete');
                }
            });
        });
    };

    function detailMasterTarget(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM master_targets WHERE id = "' + id + '"', function(err, rows) {
                if (err) {
                    console.log(err);
                } else {
                    // rows[0].periode
                    var dates = new Date(rows[0].periode);
                    var periode = dates.getFullYear() + "-" + dates.getMonth();
                    var salesman_id = rows[0].salesman_id;
                    con.query('select sum(actual_prospect) prospect, sum(actual_spk) spk from (select count(c.id) actual_prospect, 0 as actual_spk from customer_prospect c where salesman_id = "' + salesman_id + '" and SUBSTR(created, 1, 7) = "' + periode + '" union select 0 as actual_prospect, count(s.id) actual_spk from generate_spks s where salesman_id = "' + salesman_id + '" and SUBSTR(created_at, 1, 7) = "' + periode + '")x', function(err, data) {
                        con.release();
                        var actual_prospect = data[0].prospect;
                        var actual_spk = data[0].spk;
                        var actual_rasio = actual_spk * 100 / actual_prospect
                        if (isNaN(actual_rasio)) {
                            var rasio = 0;
                        } else {
                            var rasio = Math.ceil(actual_rasio);
                        }
                        obj = JSON.parse(JSON.stringify(rows));
                        res.render('master_target/view', { obj: obj, rasio: rasio });
                    });
                }
            });
        });
    };
}
module.exports = new Todo()