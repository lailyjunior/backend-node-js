//Auth : Ratri
var connection = require('../../config/db');

function Todo() {
    this.selectGalery = function(req, res, next) {
        var id = req.query.id;
        var toast = req.query.toast;

        if (id != null) {
            detailGalery(req, res, next);
            return;
        }
        connection.acquire(function(err, con) {
            con.query('SELECT * FROM galerys order by modified_at desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query('SELECT m.id, m.mtr_kode, m.mtr_type as motor_type, m.mtr_grp_segment, m.mtr_type_group, mw.product_nama, mw.product_kode, mw.wrn_kode, mwpd.mtr_hrgbeli FROM motors m, motorwarnas mw, motorwarnaprice_details mwpd WHERE m.id=mw.motor_id and mw.id = mwpd.motorwarna_id', function(err, dataLOV) {
                            con.release();
                            if (err) {
                                console.log(err);
                            } else {
                                objLOV = JSON.parse(JSON.stringify(dataLOV));
                                obj = JSON.parse(JSON.stringify(rows));
                                console.log(obj);
                                if (toast != null) {
                                    if (toast == 'add') {
                                        res.render('upload_galery/index', { obj: obj, message: 'Add Data Success' });
                                    } else if (toast == 'edit') {
                                        res.render('upload_galery/index', { obj: obj, message: 'Update Data Success' });
                                    } else {
                                        res.render('upload_galery/index', { obj: obj, message: 'Delete Data Success' });
                                    }
                                } else {
                                    res.render('upload_galery/index', { obj: obj, message: null });
                                }
                            }
                        })
                    });
                }
            });
        });
    };

    this.insertGalery = function(req, res) {
        connection.acquire(function(err, con) {
            res.render('upload_galery/add');
        });
    };

    this.submitInsertGalery = function(req, res) {
        if (req.fileValidationError) {
            res.send(req.fileValidationError);
            return;
        }

        if (req.files.length == 0) {
            res.send("Image Can't Empty ");
            return;
        }

        var fileUploads = req.files;
        var image_header = null;
        var doc_1 = null;
        var doc_2 = null;
        var doc_3 = null;
        var doc_4 = null;
        var doc_5 = null;
        var doc_6 = null;
        var doc_7 = null;
        var doc_8 = null;
        var doc_9 = null;
        var doc_10 = null;

        fileUploads.forEach(function(element, index) {
            if (index == 0) {
                image_header = "/files/" + element.filename
            } else if (index == 1) {
                doc_1 = "/files/" + element.filename
            } else if (index == 2) {
                doc_2 = "/files/" + element.filename
            } else if (index == 3) {
                doc_3 = "/files/" + element.filename
            } else if (index == 4) {
                doc_4 = "/files/" + element.filename
            } else if (index == 5) {
                doc_5 = "/files/" + element.filename
            } else if (index == 6) {
                doc_6 = "/files/" + element.filename
            } else if (index == 7) {
                doc_7 = "/files/" + element.filename
            } else if (index == 8) {
                doc_8 = "/files/" + element.filename
            } else if (index == 9) {
                doc_9 = "/files/" + element.filename
            } else if (index == 10) {
                doc_10 = "/files/" + element.filename
            }
        });


        var id = uuidv1();
        var spesifikasi = req.body.spesifikasi;
        var mtr_grp_segment = req.body.mtr_grp_segment;
        var mtr_type_group = req.body.mtr_type_group;
        var link = req.body.link;
        var created_at = new Date();
        var modified_at = new Date();
        var created_by = req.session.user.id;
        var modi_by = req.session.user.id;

        connection.acquire(function(err, con) {
            con.query('INSERT INTO galerys (id, spesifikasi, mtr_grp_segment, mtr_type_group, link, image_header,image1, image2, image3, image4, image5, image6, image7, image8, image9, image10, created_at, modified_at, created_by, modi_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [id, spesifikasi, mtr_grp_segment, mtr_type_group, link, image_header, doc_1, doc_2, doc_3, doc_4, doc_5, doc_6, doc_7, doc_8, doc_9, doc_10, created_at, modified_at, created_by, modi_by], function(err, result) {
                con.release();
                if (err) {
                    res.send({ status: 400, message: 'Master galery creation failed' });
                } else {
                    res.redirect('/UploadGalery/Index?toast=' + 'add');
                }
            });
        });
    };

    this.editGalery = function(req, res) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM galerys WHERE id = "' + id + '" order by modified_at desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    connection.acquire(function(err, con) {
                        if (err) throw err;
                        con.query('SELECT m.id, m.mtr_kode, m.mtr_type as motor_type, m.mtr_grp_segment, m.mtr_type_group, mw.product_nama, mw.product_kode, mw.wrn_kode, mwpd.mtr_hrgbeli FROM motors m, motorwarnas mw, motorwarnaprice_details mwpd WHERE m.id=mw.motor_id and mw.id = mwpd.motorwarna_id', function(err, dataLOV) {
                            con.release();
                            if (err) {
                                console.log(err);
                            } else {
                                obj = JSON.parse(JSON.stringify(rows));
                                objLOV = JSON.parse(JSON.stringify(dataLOV));
                                res.render('upload_galery/edit', { obj: obj, objLOV: objLOV });
                            }
                        })
                    });
                }
            });
        });
    };

    this.submitEditGalery = function(req, res) {
        if (req.body.data) {
            req.body = req.body.data;
            req.body = JSON.parse(req.body);
        }

        var id = req.query.id;
        var mtr_grp_segment = req.body.mtr_grp_segment;
        var mtr_type_group = req.body.mtr_type_group;
        var spesifikasi = req.body.spesifikasi;
        var link = req.body.link;
        var modified_at = new Date();
        var modi_by = req.session.user.id;

        var fileUploads = req.files;
        var docs = [];

        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT image_header,image1,image2,image3,image4,image5,image6,image7,image8,image9,image10 FROM galerys where id = "' + id + '"', function(err, data) {
                con.release();
                if (err) {
                    console.log("error");
                } else {

                    fileUploads.forEach(function(element, index) {

                        var image = data[0][element.fieldname];
                        console.log(image);

                        if (image) {
                            const fs = require('fs');
                            fs.unlink(appRoot + image.replace("/files/", "/uploads/"), (err) => {
                                if (err) throw err;
                            });
                        }
                        docs[element.fieldname] = "/files/" + element.filename;
                        console.log(docs[[element.fieldname]])
                    });

                    connection.acquire(function(err, conn) {
                        var queryStr = 'UPDATE galerys SET spesifikasi = ?, mtr_grp_segment = ?, mtr_type_group = ?, link = ?, modified_at = ?, modi_by = ?';

                        var queryParams = [spesifikasi, mtr_grp_segment, mtr_type_group, link, modified_at, modi_by];

                        for (var key in docs) {
                            console.log(key);
                            var value = docs[key];
                            queryStr = queryStr + ", " + key + " = ?";
                            //console.log(queryStr);
                            queryParams.push(value);
                        }

                        queryStr = queryStr + ' where id = "' + id + '"'

                        conn.query(queryStr, queryParams, function(err, result) {
                            conn.release();
                            if (err) {
                                res.send({ status: 400, message: 'Master training update failed' });
                            } else {
                                res.redirect('/UploadGalery/Index?toast=' + 'edit');
                            }
                        });
                    });
                }
            });
        });

    };

    this.deleteGalery = function(req, res) {
        var id = req.query.id;
        if (req.fileValidationError) {
            res.send(req.fileValidationError);
            return;
        }
        connection.acquire(function(err, con) {
            con.query('SELECT image_header, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 FROM galerys where id = "' + id + '"', function(err, data) {
                con.release();
                if (undefined !== data && data.length > 0) {
                    const fs = require('fs');
                    fs.unlink(appRoot + data[0].image_header.replace("/files/", "/uploads/"), (err) => {
                        if (err) throw err;
                        console.log('successfully deleted /tmp/hello');
                    });
                }

                connection.acquire(function(err, con) {
                    con.query('SELECT image_header, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 FROM galerys where id = "' + id + '"', function(err, data) {
                        con.release();

                        if (data[1] == null) {
                            console.log('successfully deleted /tmp/hello');
                        } else if (data[1] > 0) {
                            const fs = require('fs');
                            fs.unlink(appRoot + data[0].image1.replace("/files/", "/uploads/"), (err) => {
                                if (err) throw err;
                                console.log('successfully deleted /tmp/hello');
                            });

                        }

                        connection.acquire(function(err, con) {
                            con.query('SELECT image_header, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 FROM galerys where id = "' + id + '"', function(err, data) {
                                con.release();

                                if (data[2] > 0) {
                                    const fs = require('fs');
                                    fs.unlink(appRoot + data[0].image2.replace("/files/", "/uploads/"), (err) => {
                                        if (err) throw err;
                                        console.log('successfully deleted /tmp/hello');
                                    });
                                } else if (data[2] == 0) {
                                    console.log('successfully deleted /tmp/hello');
                                }

                                connection.acquire(function(err, con) {
                                    con.query('SELECT image_header, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 FROM galerys where id = "' + id + '"', function(err, data) {
                                        con.release();

                                        if (data[3] > 0) {
                                            const fs = require('fs');
                                            fs.unlink(appRoot + data[0].image3.replace("/files/", "/uploads/"), (err) => {
                                                if (err) throw err;
                                                console.log('successfully deleted /tmp/hello');
                                            });
                                        } else if (data[3] == 0) {
                                            console.log('successfully deleted /tmp/hello');
                                        }

                                        connection.acquire(function(err, con) {
                                            con.query('SELECT image_header, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 FROM galerys where id = "' + id + '"', function(err, data) {
                                                con.release();

                                                if (data[4] > 0) {
                                                    const fs = require('fs');
                                                    fs.unlink(appRoot + data[0].image4.replace("/files/", "/uploads/"), (err) => {
                                                        if (err) throw err;
                                                        console.log('successfully deleted /tmp/hello');
                                                    });
                                                } else if (data[4] == 0) {
                                                    console.log('successfully deleted /tmp/hello');
                                                }

                                                connection.acquire(function(err, con) {
                                                    con.query('SELECT image_header, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 FROM galerys where id = "' + id + '"', function(err, data) {
                                                        con.release();

                                                        if (data[5] > 0) {
                                                            const fs = require('fs');
                                                            fs.unlink(appRoot + data[0].image5.replace("/files/", "/uploads/"), (err) => {
                                                                if (err) throw err;
                                                                console.log('successfully deleted /tmp/hello');
                                                            });
                                                        } else if (data[5] == 0) {
                                                            console.log('successfully deleted /tmp/hello');
                                                        }
                                                        connection.acquire(function(err, con) {
                                                            con.query('SELECT image_header, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 FROM galerys where id = "' + id + '"', function(err, data) {
                                                                con.release();

                                                                if (data[6] > 0) {
                                                                    const fs = require('fs');
                                                                    fs.unlink(appRoot + data[0].image6.replace("/files/", "/uploads/"), (err) => {
                                                                        if (err) throw err;
                                                                        console.log('successfully deleted /tmp/hello');
                                                                    });
                                                                } else if (data[6] == 0) {
                                                                    console.log('successfully deleted /tmp/hello');
                                                                }

                                                                connection.acquire(function(err, con) {
                                                                    con.query('SELECT image_header, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 FROM galerys where id = "' + id + '"', function(err, data) {
                                                                        con.release();

                                                                        if (data[7] > 0) {
                                                                            const fs = require('fs');
                                                                            fs.unlink(appRoot + data[0].image7.replace("/files/", "/uploads/"), (err) => {
                                                                                if (err) throw err;
                                                                                console.log('successfully deleted /tmp/hello');
                                                                            });
                                                                        } else if (data[7] == 0) {
                                                                            console.log('successfully deleted /tmp/hello');
                                                                        }
                                                                        connection.acquire(function(err, con) {
                                                                            con.query('SELECT image_header, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 FROM galerys where id = "' + id + '"', function(err, data) {
                                                                                con.release();

                                                                                if (data[8] > 0) {
                                                                                    const fs = require('fs');
                                                                                    fs.unlink(appRoot + data[0].image8.replace("/files/", "/uploads/"), (err) => {
                                                                                        if (err) throw err;
                                                                                        console.log('successfully deleted /tmp/hello');
                                                                                    });
                                                                                } else if (data[8] == 0) {
                                                                                    console.log('successfully deleted /tmp/hello');
                                                                                }
                                                                                connection.acquire(function(err, con) {
                                                                                    con.query('SELECT image_header, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 FROM galerys where id = "' + id + '"', function(err, data) {
                                                                                        con.release();

                                                                                        if (data[9] > 0) {
                                                                                            const fs = require('fs');
                                                                                            fs.unlink(appRoot + data[0].image9.replace("/files/", "/uploads/"), (err) => {
                                                                                                if (err) throw err;
                                                                                                console.log('successfully deleted /tmp/hello');
                                                                                            });
                                                                                        } else if (data[9] == 0) {
                                                                                            console.log('successfully deleted /tmp/hello');
                                                                                        }
                                                                                        connection.acquire(function(err, con) {
                                                                                            con.query('SELECT image_header, image1, image2, image3, image4, image5, image6, image7, image8, image9, image10 FROM galerys where id = "' + id + '"', function(err, data) {
                                                                                                con.release();

                                                                                                if (data[10] > 0) {
                                                                                                    const fs = require('fs');
                                                                                                    fs.unlink(appRoot + data[0].image10.replace("/files/", "/uploads/"), (err) => {
                                                                                                        if (err) throw err;
                                                                                                        console.log('successfully deleted /tmp/hello');
                                                                                                    });
                                                                                                } else if (data[10] == 0) {
                                                                                                    console.log('successfully deleted /tmp/hello');
                                                                                                }

                                                                                                connection.acquire(function(err, con) {
                                                                                                    con.query('DELETE FROM galerys WHERE id = ?', [id], function(err, result) {
                                                                                                        con.release();
                                                                                                        if (err) {
                                                                                                            res.send({ status: 400, message: 'Failed to delete galery' });
                                                                                                        } else {
                                                                                                            res.redirect('/UploadGalery/Index?toast=' + 'delete');
                                                                                                        }
                                                                                                    });
                                                                                                });
                                                                                                // return res.json({status:'200',message:'success',result:data});
                                                                                            });
                                                                                        });
                                                                                    });
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    };

    function detailGalery(req, res, next) {
        var id = req.query.id;
        connection.acquire(function(err, con) {
            if (err) throw err;
            con.query('SELECT * FROM galerys WHERE id = "' + id + '" order by modified_at desc', function(err, rows) {
                con.release();
                if (err) {
                    console.log(err);
                } else {
                    obj = JSON.parse(JSON.stringify(rows));
                    res.render('upload_galery/view', { obj: obj });
                }
            });
        });
    };
}
module.exports = new Todo();