// Auth: Nina
var master_user_non = require('./../../controllers/web/master_user_non');

module.exports = {
    configure: function(app) {
        app.route('/MasterUserNon/Index').get(master_user_non.selectMasterUser);
        app.route('/MasterUserNon/Add').get(master_user_non.insertMasterUser);
        app.route('/MasterUserNon/AddSubmit').post(master_user_non.submitInsertMasterUser);
        app.route('/MasterUserNon/Edit').get(master_user_non.editMasterUser);
        app.route('/MasterUserNon/EditSubmit').post(master_user_non.submitEditMasterUser);
        app.route('/MasterUserNon/Delete').get(master_user_non.deleteMasterUser);
    }
};