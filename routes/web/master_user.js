// Auth: Nina
var master_user = require('./../../controllers/web/master_user');

module.exports = {
    configure: function(app) {
        app.route('/MasterUser/Index').get(master_user.selectMasterUser);
        app.route('/MasterUser/Add').get(master_user.insertMasterUser);
        app.route('/MasterUser/AddSubmit').post(master_user.submitInsertMasterUser);
        app.route('/MasterUser/Edit').get(master_user.editMasterUser);
        app.route('/MasterUser/EditSubmit').post(master_user.submitEditMasterUser);
        app.route('/MasterUser/Delete').get(master_user.deleteMasterUser);
    }
};