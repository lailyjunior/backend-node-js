// Auth: Lely
var approve = require('../../controllers/web/approve_training');

module.exports = {
    configure: function(app) {
        app.route('/ApproveTraining/Index').get(approve.selectTrainingApprove);
        app.route('/ApproveTraining/SubmitApprove').get(approve.submitApprove);
        app.route('/ApproveTraining/SubmitReject').get(approve.submitReject);
    }
};