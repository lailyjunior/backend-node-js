// Auth: Lely
var training_schedule = require('./../../controllers/web/training_schedule');

module.exports = {
    configure: function(app) {
        app.route('/TrainingSchedule/Index').get(training_schedule.selectTrainingSchedule);
        app.route('/TrainingSchedule/Add').get(training_schedule.insertTrainingSchedule);
        app.route('/TrainingSchedule/AddSubmit').post(training_schedule.submitInsertTrainingSchedule);
        app.route('/TrainingSchedule/Delete').get(training_schedule.deleteTrainingSchedule);
        app.route('/TrainingSchedule/Edit').get(training_schedule.editTrainingSchedule);
        app.route('/TrainingSchedule/EditSubmit').post(training_schedule.submitEditTrainingSchedule);
    }
};