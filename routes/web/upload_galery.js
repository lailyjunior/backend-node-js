// Auth: Lely
var upload_galery = require('./../../controllers/web/upload_galery');

module.exports = {
    configure: function(app) {
        app.route('/UploadGalery/Index').get(upload_galery.selectGalery);
        app.route('/UploadGalery/Add').get(upload_galery.insertGalery);
        app.route('/UploadGalery/AddSubmit').post(upload_galery.submitInsertGalery);
        app.route('/UploadGalery/Edit').get(upload_galery.editGalery);
        app.route('/UploadGalery/EditSubmit').post(upload_galery.submitEditGalery);
        app.route('/UploadGalery/Delete').get(upload_galery.deleteGalery);
    }
};