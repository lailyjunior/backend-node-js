// Auth: Nina
var master_salesmen = require('./../../controllers/web/master_salesmen');

module.exports = {
    configure: function(app) {
        app.route('/MasterSalesmen/Index').get(master_salesmen.selectMasterSalesmen);
        app.route('/MasterSalesmen/Add').get(master_salesmen.insertMasterSalesmen);
        app.route('/MasterSalesmen/AddSubmit').post(master_salesmen.submitInsertMasterSalesmen);
        app.route('/MasterSalesmen/Edit').get(master_salesmen.editMasterSalesmen);
        app.route('/MasterSalesmen/EditSubmit').post(master_salesmen.submitEditMasterSalesmen);
        app.route('/MasterSalesmen/Delete').get(master_salesmen.deleteMasterSalesmen);
    }
};