// Auth: Ratri
var transaksi_new_event = require('./../../controllers/web/transaksi_new_event');

module.exports = {
    configure: function(app) {
        app.route('/TransaksiNewEvent/Index').get(transaksi_new_event.selectNewEvent);
        app.route('/TransaksiNewEvent/Add').get(transaksi_new_event.insertNewEvent);
        app.route('/TransaksiNewEvent/AddSubmit').post(transaksi_new_event.submitInsertNewEvent);
        app.route('/TransaksiNewEvent/Edit').get(transaksi_new_event.editNewEvent);
        app.route('/TransaksiNewEvent/EditSubmit').post(transaksi_new_event.submitEditNewEvent);
        app.route('/TransaksiNewEvent/Delete').get(transaksi_new_event.deleteNewEvent);
    }
};