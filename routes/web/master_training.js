// Auth: Lely
var master_training = require('./../../controllers/web/master_training');

module.exports = {
    configure: function(app) {
        app.route('/MasterTraining/Index').get(master_training.selectMasterTraining);
        app.route('/MasterTraining/Add').get(master_training.insertMasterTraining);
        app.route('/MasterTraining/AddSubmit').post(master_training.submitInsertMasterTraining);
        app.route('/MasterTraining/Edit').get(master_training.editMasterTraining);
        app.route('/MasterTraining/EditSubmit').post(master_training.submitEditMasterTraining);
        app.route('/MasterTraining/Delete').get(master_training.deleteMasterTraining);
    }
};