// Auth: Ratri
var transaksi_new_event_non = require('./../../controllers/web/transaksi_new_event_non');

module.exports = {
    configure: function(app) {
        app.route('/TransaksiNewEventNon/Index').get(transaksi_new_event_non.selectNewEvent);
        app.route('/TransaksiNewEventNon/Add').get(transaksi_new_event_non.insertNewEvent);
        app.route('/TransaksiNewEventNon/AddSubmit').post(transaksi_new_event_non.submitInsertNewEvent);
        app.route('/TransaksiNewEventNon/Edit').get(transaksi_new_event_non.editNewEvent);
        app.route('/TransaksiNewEventNon/EditSubmit').post(transaksi_new_event_non.submitEditNewEvent);
        app.route('/TransaksiNewEventNon/Delete').get(transaksi_new_event_non.deleteNewEvent);
    }
};