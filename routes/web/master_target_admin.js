// Auth: Lely
var master_target_admin = require('./../../controllers/web/master_target_admin');

module.exports = {
    configure: function(app) {
        app.route('/MasterTargetAdmin/Index').get(master_target_admin.selectMasterTargetAdmin);
        app.route('/MasterTargetAdmin/Add').get(master_target_admin.insertMasterTargetAdmin);
        app.route('/MasterTargetAdmin/AddSubmit').post(master_target_admin.submitInsertMasterTargetAdmin);
        app.route('/MasterTargetAdmin/Edit').get(master_target_admin.editMasterTargetAdmin);
        app.route('/MasterTargetAdmin/Delete').get(master_target_admin.deleteTargetAdmin);
        app.route('/MasterTargetAdmin/EditSubmit').post(master_target_admin.submitEditMasterTargetAdmin);

    }
};