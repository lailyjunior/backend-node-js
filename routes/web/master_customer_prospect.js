// Auth: Lely
var master_customer_prospect = require('./../../controllers/web/master_customer_prospect');

module.exports = {
    configure: function(app) {
        app.route('/MasterCustomerProspect/Index').get(master_customer_prospect.selectCustomerProspect);
        app.route('/MasterCustomerProspect/Add').get(master_customer_prospect.insertCustomerProspect);
        app.route('/MasterCustomerProspect/AddSubmit').post(master_customer_prospect.uploadCustomerProspect);
    }
};