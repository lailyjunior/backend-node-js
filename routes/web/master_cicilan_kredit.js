// Auth: Lely
var master_cicilan_kredit = require('./../../controllers/web/master_cicilan_kredit');

module.exports = {
    configure: function(app) {
        app.route('/MasterCicilanKredit/Index').get(master_cicilan_kredit.selectMasterCicilanKredit);
    }
};