// Auth: Lely
var master_target = require('./../../controllers/web/master_target');

module.exports = {
    configure: function(app) {
        app.route('/MasterTarget/Index').get(master_target.selectMasterTarget);
        app.route('/MasterTarget/Add').get(master_target.insertMasterTarget);
        app.route('/MasterTarget/AddSubmit').post(master_target.submitInsertMasterTarget);
        app.route('/MasterTarget/Edit').get(master_target.editMasterTarget);
        app.route('/MasterTarget/Delete').get(master_target.deleteTarget);
        app.route('/MasterTarget/EditSubmit').post(master_target.submitEditMasterTarget);

    }
};