//Auth : Lely
var target_dashboard = require('./../../controllers/mobile/target_dashboard');

module.exports = {
    configure: function(app) {
        app.route('/api_master_target_prospect').get(target_dashboard.selectMasterTargetProspect);
        app.route('/api_master_target_deal').get(target_dashboard.selectMasterTargetDeal);
        app.route('/api_master_target_prospect_all').get(target_dashboard.selectMasterTargetProspectAll);

        //NINA
        app.route('/api_prospect_deal_all').get(target_dashboard.prospectDealAll);
        app.route('/api_prospect_all').get(target_dashboard.selectProspectAll);
        app.route('/api_deal_all').get(target_dashboard.selectDealAll);
        app.route('/api_detail_prospect_all').get(target_dashboard.selectDetailProspectAll);
        app.route('/api_detail_deal_all').get(target_dashboard.selectDetailDealAll);

        // DMS
        app.route('/api_prospect_deal_all_DMS').get(target_dashboard.selectDealProspectSalesDMS); //Deal dan Prospect All
        app.route('/api_deal_all_DMS').get(target_dashboard.selectDealAllDMS); //Deal All
        app.route('/api_prospect_all_DMS').get(target_dashboard.selectProspectAllDMS); //Prospect All
        app.route('/api_detail_prospect_all_DMS').get(target_dashboard.selectDetailProspectAllDMS); //Detail Prospect All
        app.route('/api_detail_deal_all_DMS').get(target_dashboard.selectDetailDealAllDMS); //Detail Deal All
    }
};