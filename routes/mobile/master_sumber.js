//Auth : Anggi

var master_sumber = require('./../../controllers/mobile/master_sumber');

module.exports = {
    configure: function(app) {
        app.route('/api_master_sumber').get(master_sumber.getSumber);
        app.route('/api_master_employee').get(master_sumber.getKaryawan);
    }
};