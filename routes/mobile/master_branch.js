//Auth : Anggi

var master_branch = require('./../../controllers/mobile/master_branch');

module.exports = {
    configure: function(app) {
        app.route('/api_master_branch').get(master_branch.getBranch);
    }
};