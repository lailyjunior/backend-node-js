//Auth : nina
var about = require('./../../controllers/mobile/master_user');

module.exports = {
    configure: function(app) {
        app.route('/api_master_user').get(about.get);
        app.route('/api_master_user').post(about.post);
        app.route('/api_master_user').put(about.put);
        app.route('/api_master_userPhoto').put(about.putPhoto);
        app.route('/api_master_userPassword').put(about.putPassword);
        app.route('/api_photo_profile/:id').put(about.updatePhotoProfile);

    }
};