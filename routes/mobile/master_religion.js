//Auth : Anggi

var master_religion = require('./../../controllers/mobile/master_religion');

module.exports = {
    configure: function(app) {
        app.route('/api_master_religion').get(master_religion.getReligion);
    }
};