//Auth : Ratri

var transaksi_new_event = require('./../../controllers/mobile/transaksi_new_event');

module.exports = {
    configure: function(app) {
        app.route('/api_transaksi_new_event').get(transaksi_new_event.get);

    }
};