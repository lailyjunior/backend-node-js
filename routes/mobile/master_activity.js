//Auth : Nina
var master_activity = require('./../../controllers/mobile/master_activity');

module.exports = {
    configure: function(app) {
        app.route('/api_master_activity').get(master_activity.selectMasterActivity);
        app.route('/api_master_activity_all').get(master_activity.selectAllActivityBySalesman);
        app.route('/api_master_activity_today').get(master_activity.selectAllActivityToday);
        app.route('/api_master_activity_tomorrow').get(master_activity.selectAllActivityTomorrow);
        app.route('/api_master_activity_done').get(master_activity.selectAllActivityDone);
        app.route('/api_master_activity_detail').get(master_activity.detailActivity);
        app.route('/api_all_activity').get(master_activity.selectAllActivity);
        app.route('/api_master_activity_status').get(master_activity.selectStatus);
        app.route('/api_master_activity_type').get(master_activity.selectType);
        app.route('/api_status_generate_customer').get(master_activity.selectStatusGenerateCustomer);
        app.route('/api_master_activity').post(master_activity.insertMasterActivity);
        app.route('/api_master_activity').put(master_activity.updateMasterActivity);
        app.route('/api_master_activity/:id').delete(master_activity.deleteMasterActivity);
        app.route('/api_get_salesmen_by_supervisor').get(master_activity.getSalesmenbySupervisor);

        app.route('/api_master_activity_salesman_all').get(master_activity.getAllActivitySalesman);
        app.route('/api_master_activity_salesman_type').get(master_activity.getTypeActivitySalesman);


        //FOR SUPERVISOR
        app.route('/api_dropdown_salesmen').get(master_activity.getDropdownSalesmen);
        app.route('/api_activity_supervisor').get(master_activity.getActivityForSupervisor);
        app.route('/api_index_activity_supervisor').get(master_activity.getIndexActivityForSupervisor);
        app.route('/api_activity_supervisor_doing').get(master_activity.getActivityDoingSupervisor);
        app.route('/api_activity_supervisor_tomorrow').get(master_activity.getActivityTomorrowSupervisor);
        app.route('/api_activity_supervisor_done').get(master_activity.getActivityDoneSupervisor);
        app.route('/api_master_activity_salesman_all_spv').get(master_activity.getAllActivitySalesmanSpv);
        app.route('/api_master_activity_salesman_type_spv').get(master_activity.getTypeActivitySalesmanSpv);

        app.route('/api_calendar').get(master_activity.getActivityCalendar);
        app.route('/api_activity_by_salesman').get(master_activity.getAllActivityBySalesman);
        app.route('/api_event_by_salesman').get(master_activity.getAllEventBySalesman);

        //activity
        app.route('/api_activity_byyear').get(master_activity.getActivityByYear);
        app.route('/api_activity_bymonth').get(master_activity.getActivityByMonth);
        app.route('/api_activity_byday').get(master_activity.getActivityByDay);

        //event
        app.route('/api_event_byyear').get(master_activity.getEventByYear);
        app.route('/api_event_bymonth').get(master_activity.getEventByMonth);
        app.route('/api_event_byday').get(master_activity.getEventByDay);

        //event supervisor
        app.route('/api_event_by_spv').get(master_activity.getEventBySupervisor);
        app.route('/api_all_event_by_spv').get(master_activity.getAllEventBySupervisor);
        app.route('/api_event_byday_spv').get(master_activity.getEventPerDayBySupervisor);

    }
};