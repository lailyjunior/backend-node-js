//Auth : Anggi

var master_occupation = require('./../../controllers/mobile/master_occupation');

module.exports = {
    configure: function(app) {
        app.route('/api_master_occupation').get(master_occupation.getOccupation);
    }
};