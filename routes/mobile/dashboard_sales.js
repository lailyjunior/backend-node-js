//Auth : Lely
var d_sales = require('../../controllers/mobile/dashboard_Sales');

module.exports = {
    configure: function(app) {
        app.route('/api_dashboard_prospect_target').get(d_sales.selectMasterTargetProspect);
        app.route('/api_dashboard_deal_target').get(d_sales.selectMasterTargetDeal);
        app.route('/api_dashboard_sales_target').get(d_sales.selectMasterTargetSales);

    }
};