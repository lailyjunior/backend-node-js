//Auth : Lely
var report = require('./../../controllers/mobile/reporting');

module.exports = {
    configure: function(app) {

        //------------------- For Report Customer ---------------------//
        app.route('/api_salesman_bybranch').get(report.selectSalesmanByBranch);
        app.route('/report_customer_excel_bybranch').get(report.reportCustomerByBranch);
        app.route('/report_customer_excel_bybranch_salesman').get(report.reportCustomerByBranchAndSalesman);
        app.route('/report_customer_excel_bysupervisor').get(report.reportCustomerBySupervisor);
        app.route('/report_customer_excel_bysalesman').get(report.reportCustomerBySalesman);

        //------------------- For Report SPK ---------------------//
        app.route('/report_spk_excel_bybranch').get(report.reportSPKByBranch);
        app.route('/report_spk_excel_bybranch_salesman').get(report.reportSPKByBranchSalesman);
        app.route('/report_spk_excel_bysupervisor').get(report.reportSPKBySupervisor);
        app.route('/report_spk_excel_bysalesman').get(report.reportSPKBySalesman);

        //------------------- For Report Customer Prospect ---------------------//
        app.route('/report_customer_prospect_excel_bybranch').get(report.reportCustomerProspectByBranch);
        app.route('/report_customer_prospect_excel_bybranch_salesman').get(report.reportCustomerProspectByBranchSalesman);
        app.route('/report_customer_prospect_excel_bysupervisor').get(report.reportCustomerProspectBySupervisor);
        app.route('/report_customer_prospect_excel_bysupervisor_salesman').get(report.reportCustomerProspectBySupervisorSalesman);
    }
};