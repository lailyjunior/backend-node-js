//Auth : Lely
var about = require('./../../controllers/mobile/master_about_us');

module.exports = {
    configure: function(app) {
        app.route('/api_master_about_us').get(about.selectAboutUs);
        app.route('/api_about_us').post(about.insertAboutUs);
        app.route('/api_master_about_us/:id').put(about.updateAboutUs);
        app.route('/api_master_about_us/:id').delete(about.deleteAboutUs);
    }
};