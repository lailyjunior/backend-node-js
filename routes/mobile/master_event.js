//Auth : Lely
var event = require('./../../controllers/mobile/master_event');

module.exports = {
    configure: function(app) {
        app.route('/api_master_event').get(event.selectEvent); //index
        app.route('/api_master_event').post(event.insertEvent); //add
        app.route('/api_master_event/:id').put(event.updateEvent); //edit
        app.route('/api_master_event/:id').delete(event.deleteEvent); //delete
    }
};