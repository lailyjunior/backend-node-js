//Auth : Anggi

var master_jenis_customer = require('./../../controllers/mobile/master_jenis_customer');

module.exports = {
    configure: function(app) {
        app.route('/api_master_jenis_customer').get(master_jenis_customer.getJenisCust);
    }
};