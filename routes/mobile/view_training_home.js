//Auth : Lely
var training = require('./../../controllers/mobile/view_training_home');

module.exports = {
    configure: function(app) {
        app.route('/api_mag_training_home_category').get(training.selectCategoryTraining);
        app.route('/api_mag_training_by_category').get(training.selectTrainingAllByCategory);

        app.route('/api_mag_training_home_this_month_limit').get(training.selectThisMonthTraining);
        app.route('/api_mag_training_home_this_month').get(training.selectThisMonthTrainingAll);

        app.route('/api_mag_training_home_video_limit').get(training.selectVideoTraining);
        app.route('/api_mag_training_home_video').get(training.selectVideoTrainingAll);

        app.route('/api_mag_training_viewers').get(training.selectViewersTraining);
    }
};