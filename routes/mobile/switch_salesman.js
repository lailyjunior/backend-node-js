//Auth : Ratri
var switch_salesman = require('./../../controllers/mobile/switch_salesman');

module.exports = {
    configure: function(app) {
        app.route('/api_switch_salesman').get(switch_salesman.selectCustomerProspect); 
        app.route('/api_detail_customer_prospect').get(switch_salesman.detailCustomerProspect) //detail
        app.route('/api_select_salesman').get(switch_salesman.selectSalesman); // dropdown
        app.route('/api_filter').get(switch_salesman.filterCustomerProspect);
        app.route('/api_edit_customer_prospect').post(switch_salesman.put); //edit
        app.route('/api_filter_non').get(switch_salesman.filterCustomerProspectNonDms); //filter non
    }
};