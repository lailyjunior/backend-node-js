//Auth : Lely
var list_training = require('./../../controllers/mobile/list_training');

module.exports = {
    configure: function(app) {
        app.route('/api_list_training').get(list_training.selectListTraining);
        app.route('/api_training_salesman').get(list_training.selectTrainingSalesman);
        app.route('/api_add_training_salesman').post(list_training.insertTrainingSalesman);
        app.route('/api_list_training_status').get(list_training.selectTrainingregister);
    }
};