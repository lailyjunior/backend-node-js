//Auth : Anggi
var master_type_motor = require('./../../controllers/mobile/master_type_motor');

module.exports = {
    configure: function(app) {
        app.route('/api_master_motor').get(master_type_motor.getMotor);
        app.route('/api_master_lovmotor').get(master_type_motor.getLovMotor);
    }
};