//Auth : Ratri
var motor = require('./../../controllers/mobile/catalog');
 
module.exports = {
  configure: function(app) {
    app.route('/api_index_motor').get(motor.selectImageMotor);
    app.route('/api_grp_segment_motor').get(motor.selectTypeGroup);
    app.route('/api_grp_type_motor_detail').get(motor.selectTypeGroupDetail);
    app.route('/api_motor_detail').get(motor.selectDetail);
    app.route('/api_catalog_price').get(motor.selectCatalogPrice);
  }
};
