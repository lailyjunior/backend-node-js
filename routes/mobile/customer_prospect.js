//Auth : Lely
var customer_prospect = require('./../../controllers/mobile/customer_prospect');

module.exports = {
    configure: function(app) {
        app.route('/api_religion').get(customer_prospect.selectReligion);
        app.route('/api_poscode').get(customer_prospect.selectPosCode);
        app.route('/api_jenis_customer').get(customer_prospect.selectJenisCustomer);
        app.route('/api_type_customer').get(customer_prospect.selectTypeCustomer);
        app.route('/api_branch').get(customer_prospect.selectBranches);
        app.route('/api_occupation').get(customer_prospect.selectOccupation);
        app.route('/api_sumber').get(customer_prospect.selectSumber);
        app.route('/api_pameran').get(customer_prospect.selectPameran);
        app.route('/api_employee').get(customer_prospect.selectEmployee);
        app.route('/api_lov_customer_prospect').get(customer_prospect.selectCustomerProspectLov);
        app.route('/api_customer_prospect').get(customer_prospect.selectCustomerProspect);
        app.route('/api_customer_prospect').post(customer_prospect.insertCustomerProspect);
        app.route('/api_customer_prospect/:id').put(customer_prospect.updateCustomerProspect);
        app.route('/api_customer_prospect_detail').get(customer_prospect.selectDetailCP);
        app.route('/api_customer_prospect_notif').get(customer_prospect.notifyActivitySalesman);
        app.route('/api_customer_prospect_type_motor').get(customer_prospect.getLovTypeMotor);
        app.route('/api_customer_prospect_nondms').post(customer_prospect.insertCustomerProspectNonDms);

        //FOR SUPERVISOR
        app.route('/api_customer_prospect_spv').post(customer_prospect.insertCustomerProspectSpv);
        app.route('/api_customer_prospect_nondms_spv').post(customer_prospect.insertCustomerProspectNonDmsSpv);
    }
};