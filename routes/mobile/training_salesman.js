//Auth : Lely
var training = require('./../../controllers/mobile/training_salesman');

module.exports = {
    configure: function(app) {
        app.route('/api_list_booking_training').get(training.selectTrainingSalesman);
        app.route('/api_list_booking_training_detail').get(training.detailTrainingSalesman);
    }
};