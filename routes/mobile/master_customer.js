//Auth : Anggi

var master_customer = require('./../../controllers/mobile/master_customer');

module.exports = {
    configure: function(app) {
        app.route('/api_master_customer').get(master_customer.getCustomer);
        app.route('/api_master_customer_detail').get(master_customer.getCustomerDetail);
        // app.route('/api_master_customer').post(master_customer.addCustomer);
        app.route('/api_master_generate_customer').post(master_customer.generateCustomer);
        // app.route('/api_master_customer/:id').put(master_customer.updateCustomer);


        app.route('/api_master_customer_test_add').post(master_customer.addCustomerTest);
        app.route('/api_master_customer_test_edit/:id').put(master_customer.updateCustomerTest);


        app.route('/api_master_customerProspect').put(master_customer.edit);

        // TODMS
        app.route('/api_master_generate_customer_to_DMS').post(master_customer.generateCustomerToDMS);
        app.route('/api_master_customer_edit_to_DMS/:id').put(master_customer.updateCustomerToDMS);
        app.route('/api_master_customer_add_to_DMS').post(master_customer.addCustomerToDMS);

        //validasi ktp
        app.route('/generateCustomerToLogs').post(master_customer.generateCustomerToLogs);
        app.route('/updateCustomerOld').put(master_customer.updateCustomerOld);
        app.route('/updateCustomerOldAdd/:id').put(master_customer.updateCustomerOldAdd);

        //validasi ktp DMS
        app.route('/updateCustomerOldDMS').put(master_customer.updateCustomerOldDMS);
        app.route('/updateCustomerOldAddDMS/:id').put(master_customer.updateCustomerOldAddDMS);
        app.route('/api_master_generate_customer_dms').post(master_customer.generateCustomerDMS);
        app.route('/api_master_customer_test_add_dms').post(master_customer.addCustomerTestDMS);
        app.route('/api_master_customer_test_edit_dms/:id').put(master_customer.updateCustomerTestDMS);
    }
};