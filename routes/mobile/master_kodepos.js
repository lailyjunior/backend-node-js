//Auth : Ratri

var master_kodepos = require('./../../controllers/mobile/master_kodepos');

module.exports = {
    configure: function(app) {
        app.route('/api_master_kodepos').get(master_kodepos.get);

    }
};