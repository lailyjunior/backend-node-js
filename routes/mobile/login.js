//Auth : Ratri

var login = require('./../../controllers/mobile/login');

module.exports = {
    configure: function(app) {
        app.route('/api_login').post(login.login);
        // app.route('/api_login').put(login.logout);
    }
};