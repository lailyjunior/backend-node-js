//Auth : Lely
var master_training = require('./../../controllers/mobile/master_training');

module.exports = {
    configure: function(app) {
        app.route('/api_master_training').get(master_training.selectMasterTraining);
        app.route('/api_master_training').post(master_training.insertMasterTraining);
        app.route('/api_master_training/:id').put(master_training.updateMasterTraining);
        app.route('/api_master_training/:id').delete(master_training.deleteMasterTraining);
    }
};