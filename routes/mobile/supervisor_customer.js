//Auth : Anggi L
var supervisor = require('./../../controllers/mobile/supervisor_customer');

module.exports = {
    configure: function(app) {
        app.route('/api_master_all_supervisor_cust').get(supervisor.getAllSupervisorCustomer);
        app.route('/api_master_all_supervisor_cust_non_dms').get(supervisor.getAllSupervisorCustomerNonDMS);
        app.route('/api_master_supervisor_cust_bysls').get(supervisor.getSupervisorCustomerBySalesman);
    }
};