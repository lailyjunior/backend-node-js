//Auth : Lely
var generate = require('./../../controllers/mobile/generate_spk');

module.exports = {
    configure: function(app) {
        app.route('/api_cara_bayar').get(generate.selectCaraBayar);
        app.route('/api_dp_po').get(generate.selectDpPo);
        app.route('/api_leasing').get(generate.selectLeasing);
        app.route('/api_detail_motor_all').get(generate.selectDetailMotorAll);
        app.route('/api_detail_motor_by_type').get(generate.selectDetailMotorByType);
        app.route('/api_no_spk').get(generate.selectNoSpkBySalesmanID);
        app.route('/api_jumlah_tenor').get(generate.selectJumlahTenor);
        app.route('/api_denda_wilayah').get(generate.selectDendaWilayah);
        app.route('/api_create_spk').post(generate.insertGenerateSpk); // NON DMS
        app.route('/api_create_spk_dms').post(generate.insertGenerateSpkDms); // DMS
        app.route('/api_customers').get(generate.selectCustomers);
        app.route('/api_customer_detail_spk').get(generate.selectCustomerDetails);
        app.route('/api_spk_event').get(generate.getDataEvent);
        app.route('/api_index_spk').get(generate.selectIndexSpk);
        app.route('/api_index_spk_detail').get(generate.selectIndexSpkDetail);
        app.route('/api_index_spk_detail_motor').get(generate.selectIndexSpkDetailMotor);
        app.route('/api_salesman_by_supervisor').get(generate.selectSalesmanBySupervisor);
        app.route('/api_index_spk_all').get(generate.selectIndexSpkAll);
        app.route('/api_index_spk_all_nondms').get(generate.selectIndexSpkAllNonDms);
    }
};