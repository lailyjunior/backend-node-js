//Auth : Lely
var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
global.md5 = require('md5');
var acl = require('express-acl');

global.dateFormat = require('dateformat');

//routes app web backend
var master_cicilan_kredit = require('./routes/web/master_cicilan_kredit');
var master_training = require('./routes/web/master_training');
var master_target = require('./routes/web/master_target');
var master_target_admin = require('./routes/web/master_target_admin');
var index = require('./routes/web/index');
var transaksi_new_event = require('./routes/web/transaksi_new_event');
var transaksi_new_event_non = require('./routes/web/transaksi_new_event_non');
var upload_galery = require('./routes/web/upload_galery');
var master_salesmen = require('./routes/web/master_salesmen');
var master_user = require('./routes/web/master_user');
var master_user_non = require('./routes/web/master_user_non');
var master_customer_prospect = require('./routes/web/master_customer_prospect');
var training_schedule = require('./routes/web/training_schedule');
var approve = require('./routes/web/approve_training');
var master_quiz = require('./routes/web/master_quiz');

//routes app mobile
var api_master_about_us = require('./routes/mobile/master_about_us');
var api_master_event = require('./routes/mobile/master_event');
var api_master_training = require('./routes/mobile/master_training');
var api_login = require('./routes/mobile/login');
var api_customer_prospect = require('./routes/mobile/customer_prospect');
var api_generate_spk = require('./routes/mobile/generate_spk');
var report = require('./routes/mobile/reporting');
var api_target = require('./routes/mobile/target_dashboard');
var api_list_training = require('./routes/mobile/list_training');
var api_list_booking_training = require('./routes/mobile/training_salesman');
var api_view_training_home = require('./routes/mobile/view_training_home');
var api_dashboard_target_prospect = require('./routes/mobile/dashboard_sales');


// ANGGI LISDAYANTI
var api_master_customer = require('./routes/mobile/master_customer');
var api_master_branch = require('./routes/mobile/master_branch');
var api_master_jenis_customer = require('./routes/mobile/master_jenis_customer');
var api_master_kodepos = require('./routes/mobile/master_kodepos');
var api_master_occupation = require('./routes/mobile/master_occupation');
var api_master_religion = require('./routes/mobile/master_religion');
var api_master_sumber = require('./routes/mobile/master_sumber');
var api_master_type_motor = require('./routes/mobile/master_type_motor');
var api_master_transaksi_new_event = require('./routes/mobile/transaksi_new_event');
var api_supervisor_customer = require('./routes/mobile/supervisor_customer');
var api_master_user = require('./routes/mobile/master_user');

//RATRI
var api_switch_salesman = require('./routes/mobile/switch_salesman');
var api_catalog = require('./routes/mobile/catalog');

//NINA
var api_master_activity = require('./routes/mobile/master_activity');

var connection = require('./config/db');
var session = require('express-session');
var SQLiteStore = require('connect-sqlite3')(session);

global.store = new SQLiteStore;

global.uuidv1 = require('uuid/v1');
global.md5 = require('md5');

var app = express();
app.use(session({
    store: store,
    secret: 'lelycantik',
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 6 * 60 * 60 * 1000 }
}));

global.isAuthenticate = function(req, res, next) {

    if ((req.session && req.session.user) || req.path == "/login") {
        return next();
    }
    res.redirect("/login");
}
global.appRoot = __dirname;

var multer = require('multer');
var path = require('path');

var storage = multer.diskStorage({

    destination: function(req, file, callback) {
        callback(null, './uploads')
    },
    filename: function(req, file, callback) {
        var id_photo = uuidv1();
        callback(null, id_photo + "-" + file.originalname);
    }
})

var xlsFilterUrl = [
    '/MasterCustomerProspect/AddSubmit',
]

var multer = multer({
    storage: storage,
    fileFilter: function(req, file, callback) { //file filter
        var xlsFilterUrlIndex = xlsFilterUrl.indexOf(req.originalUrl);
        if (xlsFilterUrlIndex >= 0) {
            if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
                return callback(null, false);;
            }
        }
        callback(null, true);
    }
})
var upload = multer.any();

app.use(upload);

app.engine('ejs', require('ejs-locals'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

var serveIndex = require('serve-index');
app.use(express.static(__dirname + '/views'));
app.use("/files", serveIndex(__dirname + "/uploads"));
app.use("/files", express.static(__dirname + "/uploads"));

connection.init();

//configure mobile app
api_master_about_us.configure(app);
api_master_event.configure(app);
api_master_training.configure(app);
api_login.configure(app);
api_customer_prospect.configure(app);
api_generate_spk.configure(app);
report.configure(app);
api_target.configure(app);

api_master_customer.configure(app);
api_master_branch.configure(app);
api_master_jenis_customer.configure(app);
api_master_kodepos.configure(app);
api_master_occupation.configure(app);
api_master_religion.configure(app);
api_master_sumber.configure(app);
api_master_type_motor.configure(app);
api_master_transaksi_new_event.configure(app);
api_supervisor_customer.configure(app);
api_master_user.configure(app);
api_list_training.configure(app);
api_list_booking_training.configure(app);
api_view_training_home.configure(app);
api_dashboard_target_prospect.configure(app);

//RATRI
api_switch_salesman.configure(app);
api_catalog.configure(app);

api_master_activity.configure(app);

//autentikasi session & acl
app.use(isAuthenticate);

let configObject = {
    filename: 'nacl.json',
    searchPath: 'session.user.title'
};

let responseObject = {
    status: 'Access Denied',
    message: 'You are not authorized to access this resource'
};
acl.config(configObject, responseObject);
app.use(acl.authorize.unless({
    path: ['/login', '/views', '/logout', '/error404']
}));

//data user 
app.use(function(req, res, next) {
    res.locals.user = req.session.user;
    next();
});

// link root 
app.use(function(req, res, next) {
    res.locals.link = req.protocol + "://" + req.get('host');
    next();
});

//configure web backend
master_cicilan_kredit.configure(app);
master_training.configure(app);
index.configure(app);
transaksi_new_event.configure(app);
transaksi_new_event_non.configure(app);
upload_galery.configure(app);
master_salesmen.configure(app);
master_user.configure(app);
master_user_non.configure(app);
master_customer_prospect.configure(app);
master_target.configure(app);
master_target_admin.configure(app);
training_schedule.configure(app);
approve.configure(app);
master_quiz.configure(app);

module.exports = app;